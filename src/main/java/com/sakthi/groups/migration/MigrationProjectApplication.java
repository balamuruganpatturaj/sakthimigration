package com.sakthi.groups.migration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MigrationProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(MigrationProjectApplication.class, args);
	}

}
