package com.sakthi.groups.migration.util;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import org.springframework.stereotype.Component;

@Component
public class CommonFunctionImpl implements CommonFunction {

	@Override
	public Date getDateFromSlot(Integer checkFromDays) {
		LocalDate currentDate = LocalDate.now();
		LocalDate startingDate = currentDate.plusDays(-Long.valueOf(checkFromDays));
		Date purchaseSlot = Date.from(startingDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
		return purchaseSlot;
	}
}
