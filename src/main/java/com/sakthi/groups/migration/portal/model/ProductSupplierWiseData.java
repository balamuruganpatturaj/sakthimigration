package com.sakthi.groups.migration.portal.model;

import java.util.Date;

public class ProductSupplierWiseData {

	public ProductSupplierWiseData(Integer itemCode, String itemName, Double purchaseCount, Double salesCount,
			Date lastPurchaseDate, Date lastSaleDate) {
		super();
		this.itemCode = itemCode;
		this.itemName = itemName;
		this.purchaseCount = purchaseCount;
		this.salesCount = salesCount;
		this.lastPurchaseDate = lastPurchaseDate;
		this.lastSaleDate = lastSaleDate;
	}

	public ProductSupplierWiseData() {
	}

	private Integer itemCode;
	private String itemName;
	private Double purchaseCount;
	private Double salesCount;
	private Date lastPurchaseDate;
	private Date lastSaleDate;

	public Integer getItemCode() {
		return itemCode;
	}

	public void setItemCode(Integer itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public Double getPurchaseCount() {
		return purchaseCount;
	}

	public void setPurchaseCount(Double purchaseCount) {
		this.purchaseCount = purchaseCount;
	}

	public Double getSalesCount() {
		return salesCount;
	}

	public void setSalesCount(Double salesCount) {
		this.salesCount = salesCount;
	}

	public Date getLastPurchaseDate() {
		return lastPurchaseDate;
	}

	public void setLastPurchaseDate(Date lastPurchaseDate) {
		this.lastPurchaseDate = lastPurchaseDate;
	}

	public Date getLastSaleDate() {
		return lastSaleDate;
	}

	public void setLastSaleDate(Date lastSaleDate) {
		this.lastSaleDate = lastSaleDate;
	}

}
