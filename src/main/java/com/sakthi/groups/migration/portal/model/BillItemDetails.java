package com.sakthi.groups.migration.portal.model;

public class BillItemDetails {

	private Integer billNo;
	private Integer billSlNo;
	private String itemName;
	private String itemNativeName;
	private Integer itemUnit;
	private Double itemQuantity;
	private Double itemRate;
	private Double itemTaxAmt;
	private Double itemTotalAmt;
	private Double itemPurchaseRate;
	private Double itemTaxPercent;
	private Double totalTaxAmt;
	private Double itemMaxRate;
	private Integer itemPriceLevel;
	private Integer quantityPriceLvl;
	private Double quantityPriceLvlSelling;
	private String itemEanCode;
	private Double purchasePriceWithoutTax;
	private Double itemProfit;
	private Double netAmt;
	private Double sGstPercent;
	private Double sGstAmt;
	private Double cGstPercent;
	private Double cGstAmt;
	private Double iGstPercent;
	private Double iGstAmt;
	private String itemGstCode;
	private Double savedAmount;

	public Integer getBillNo() {
		return billNo;
	}

	public void setBillNo(Integer billNo) {
		this.billNo = billNo;
	}

	public Integer getBillSlNo() {
		return billSlNo;
	}

	public void setBillSlNo(Integer billSlNo) {
		this.billSlNo = billSlNo;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemNativeName() {
		return itemNativeName;
	}

	public void setItemNativeName(String itemNativeName) {
		this.itemNativeName = itemNativeName;
	}

	public Integer getItemUnit() {
		return itemUnit;
	}

	public void setItemUnit(Integer itemUnit) {
		this.itemUnit = itemUnit;
	}

	public Double getItemQuantity() {
		return itemQuantity;
	}

	public void setItemQuantity(Double itemQuantity) {
		this.itemQuantity = itemQuantity;
	}

	public Double getItemRate() {
		return itemRate;
	}

	public void setItemRate(Double itemRate) {
		this.itemRate = itemRate;
	}

	public Double getItemTaxAmt() {
		return itemTaxAmt;
	}

	public void setItemTaxAmt(Double itemTaxAmt) {
		this.itemTaxAmt = itemTaxAmt;
	}

	public Double getItemTotalAmt() {
		return itemTotalAmt;
	}

	public void setItemTotalAmt(Double itemTotalAmt) {
		this.itemTotalAmt = itemTotalAmt;
	}

	public Double getItemPurchaseRate() {
		return itemPurchaseRate;
	}

	public void setItemPurchaseRate(Double itemPurchaseRate) {
		this.itemPurchaseRate = itemPurchaseRate;
	}

	public Double getItemTaxPercent() {
		return itemTaxPercent;
	}

	public void setItemTaxPercent(Double itemTaxPercent) {
		this.itemTaxPercent = itemTaxPercent;
	}

	public Double getTotalTaxAmt() {
		return totalTaxAmt;
	}

	public void setTotalTaxAmt(Double totalTaxAmt) {
		this.totalTaxAmt = totalTaxAmt;
	}

	public Double getItemMaxRate() {
		return itemMaxRate;
	}

	public void setItemMaxRate(Double itemMaxRate) {
		this.itemMaxRate = itemMaxRate;
	}

	public Integer getItemPriceLevel() {
		return itemPriceLevel;
	}

	public void setItemPriceLevel(Integer itemPriceLevel) {
		this.itemPriceLevel = itemPriceLevel;
	}

	public Integer getQuantityPriceLvl() {
		return quantityPriceLvl;
	}

	public void setQuantityPriceLvl(Integer quantityPriceLvl) {
		this.quantityPriceLvl = quantityPriceLvl;
	}

	public Double getQuantityPriceLvlSelling() {
		return quantityPriceLvlSelling;
	}

	public void setQuantityPriceLvlSelling(Double quantityPriceLvlSelling) {
		this.quantityPriceLvlSelling = quantityPriceLvlSelling;
	}

	public String getItemEanCode() {
		return itemEanCode;
	}

	public void setItemEanCode(String itemEanCode) {
		this.itemEanCode = itemEanCode;
	}

	public Double getPurchasePriceWithoutTax() {
		return purchasePriceWithoutTax;
	}

	public void setPurchasePriceWithoutTax(Double purchasePriceWithoutTax) {
		this.purchasePriceWithoutTax = purchasePriceWithoutTax;
	}

	public Double getItemProfit() {
		return itemProfit;
	}

	public void setItemProfit(Double itemProfit) {
		this.itemProfit = itemProfit;
	}

	public Double getNetAmt() {
		return netAmt;
	}

	public void setNetAmt(Double netAmt) {
		this.netAmt = netAmt;
	}

	public Double getsGstPercent() {
		return sGstPercent;
	}

	public void setsGstPercent(Double sGstPercent) {
		this.sGstPercent = sGstPercent;
	}

	public Double getsGstAmt() {
		return sGstAmt;
	}

	public void setsGstAmt(Double sGstAmt) {
		this.sGstAmt = sGstAmt;
	}

	public Double getcGstPercent() {
		return cGstPercent;
	}

	public void setcGstPercent(Double cGstPercent) {
		this.cGstPercent = cGstPercent;
	}

	public Double getcGstAmt() {
		return cGstAmt;
	}

	public void setcGstAmt(Double cGstAmt) {
		this.cGstAmt = cGstAmt;
	}

	public Double getiGstPercent() {
		return iGstPercent;
	}

	public void setiGstPercent(Double iGstPercent) {
		this.iGstPercent = iGstPercent;
	}

	public Double getiGstAmt() {
		return iGstAmt;
	}

	public void setiGstAmt(Double iGstAmt) {
		this.iGstAmt = iGstAmt;
	}

	public String getItemGstCode() {
		return itemGstCode;
	}

	public void setItemGstCode(String itemGstCode) {
		this.itemGstCode = itemGstCode;
	}

	public Double getSavedAmount() {
		return savedAmount;
	}

	public void setSavedAmount(Double savedAmount) {
		this.savedAmount = savedAmount;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((itemName == null) ? 0 : itemName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BillItemDetails other = (BillItemDetails) obj;
		if (itemName == null) {
			if (other.itemName != null)
				return false;
		} else if (!itemName.equals(other.itemName))
			return false;
		return true;
	}

}
