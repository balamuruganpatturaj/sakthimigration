package com.sakthi.groups.migration.portal.model;

import java.math.BigDecimal;

public class EancodeInputModel {

	private String eanCode;
	private BigDecimal maxRetailPrice;

	public EancodeInputModel() {

	}

	public EancodeInputModel(String eanCode, BigDecimal maxRetailPrice) {
		super();
		this.eanCode = eanCode;
		this.maxRetailPrice = maxRetailPrice;
	}

	public String getEanCode() {
		return eanCode;
	}

	public void setEanCode(String eanCode) {
		this.eanCode = eanCode;
	}

	public BigDecimal getMaxRetailPrice() {
		return maxRetailPrice;
	}

	public void setMaxRetailPrice(BigDecimal maxRetailPrice) {
		this.maxRetailPrice = maxRetailPrice;
	}

}
