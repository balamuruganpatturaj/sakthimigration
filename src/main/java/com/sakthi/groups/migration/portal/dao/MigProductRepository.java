package com.sakthi.groups.migration.portal.dao;

import java.util.List;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.sakthi.groups.migration.portal.model.ProductMaster;

public interface MigProductRepository extends MongoRepository<ProductMaster, ObjectId> {

	ProductMaster findByProductCode(Integer productCode);

	List<ProductMaster> findByProductCodeIn(Set<Integer> itemCodeSet);

}
