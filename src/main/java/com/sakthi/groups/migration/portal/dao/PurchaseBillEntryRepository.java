package com.sakthi.groups.migration.portal.dao;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.sakthi.groups.migration.portal.model.PurchaseBillEntry;

public interface PurchaseBillEntryRepository extends MongoRepository<PurchaseBillEntry, ObjectId> {

	public PurchaseBillEntry findByMrcNo(Integer mrcNo);
	
	List<PurchaseBillEntry> findByDistributorCodeOrderByInvoiceDateDesc(Integer distributorCode);
}
