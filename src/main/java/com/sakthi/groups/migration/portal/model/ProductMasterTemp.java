package com.sakthi.groups.migration.portal.model;

import java.util.List;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "product_master_tmp")
public class ProductMasterTemp {

	@Id
	private ObjectId id;
	@Indexed(unique = true)
	private Integer productCode;
	@Indexed(unique = true)
	private String productAlias;
	@Indexed(unique = true)
	private String productName;
	private Set<String> productEANCode;
	private boolean decimalAllowed;
	private String productCategory1;
	private String productCategory2;
	private String productCompany;
	private String section;
	private String productRelation;
	private String productRelatedWith;
	private Double relationConversion;
	private boolean productStatus;
	private boolean negStockAllowed;
	private boolean rateEditAllowed;
	private String gstTaxSlab;
	private String hsnCode;
	private String productShortName;
	private boolean looseSaleAllowed;
	private String companyCode;
	private String supplierName;
	private boolean loyaltyAllowed;
	private String localLanguageName;
	private String baminiFontName;
	private List<ProductPrice> productPrice;

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public Integer getProductCode() {
		return productCode;
	}

	public void setProductCode(Integer productCode) {
		this.productCode = productCode;
	}

	public String getProductAlias() {
		return productAlias;
	}

	public void setProductAlias(String productAlias) {
		this.productAlias = productAlias;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Set<String> getProductEANCode() {
		return productEANCode;
	}

	public void setProductEANCode(Set<String> productEANCode) {
		this.productEANCode = productEANCode;
	}

	public boolean isDecimalAllowed() {
		return decimalAllowed;
	}

	public void setDecimalAllowed(boolean decimalAllowed) {
		this.decimalAllowed = decimalAllowed;
	}

	public String getProductCategory1() {
		return productCategory1;
	}

	public void setProductCategory1(String productCategory1) {
		this.productCategory1 = productCategory1;
	}

	public String getProductCategory2() {
		return productCategory2;
	}

	public void setProductCategory2(String productCategory2) {
		this.productCategory2 = productCategory2;
	}

	public String getProductCompany() {
		return productCompany;
	}

	public void setProductCompany(String productCompany) {
		this.productCompany = productCompany;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getProductRelation() {
		return productRelation;
	}

	public void setProductRelation(String productRelation) {
		this.productRelation = productRelation;
	}

	public String getProductRelatedWith() {
		return productRelatedWith;
	}

	public void setProductRelatedWith(String productRelatedWith) {
		this.productRelatedWith = productRelatedWith;
	}

	public Double getRelationConversion() {
		return relationConversion;
	}

	public void setRelationConversion(Double relationConversion) {
		this.relationConversion = relationConversion;
	}

	public boolean isProductStatus() {
		return productStatus;
	}

	public void setProductStatus(boolean productStatus) {
		this.productStatus = productStatus;
	}

	public boolean isNegStockAllowed() {
		return negStockAllowed;
	}

	public void setNegStockAllowed(boolean negStockAllowed) {
		this.negStockAllowed = negStockAllowed;
	}

	public boolean isRateEditAllowed() {
		return rateEditAllowed;
	}

	public void setRateEditAllowed(boolean rateEditAllowed) {
		this.rateEditAllowed = rateEditAllowed;
	}

	public String getGstTaxSlab() {
		return gstTaxSlab;
	}

	public void setGstTaxSlab(String gstTaxSlab) {
		this.gstTaxSlab = gstTaxSlab;
	}

	public String getHsnCode() {
		return hsnCode;
	}

	public void setHsnCode(String hsnCode) {
		this.hsnCode = hsnCode;
	}

	public String getProductShortName() {
		return productShortName;
	}

	public void setProductShortName(String productShortName) {
		this.productShortName = productShortName;
	}

	public boolean isLooseSaleAllowed() {
		return looseSaleAllowed;
	}

	public void setLooseSaleAllowed(boolean looseSaleAllowed) {
		this.looseSaleAllowed = looseSaleAllowed;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public boolean isLoyaltyAllowed() {
		return loyaltyAllowed;
	}

	public void setLoyaltyAllowed(boolean loyaltyAllowed) {
		this.loyaltyAllowed = loyaltyAllowed;
	}

	public String getLocalLanguageName() {
		return localLanguageName;
	}

	public void setLocalLanguageName(String localLanguageName) {
		this.localLanguageName = localLanguageName;
	}

	public String getBaminiFontName() {
		return baminiFontName;
	}

	public void setBaminiFontName(String baminiFontName) {
		this.baminiFontName = baminiFontName;
	}

	public List<ProductPrice> getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(List<ProductPrice> productPrice) {
		this.productPrice = productPrice;
	}

}
