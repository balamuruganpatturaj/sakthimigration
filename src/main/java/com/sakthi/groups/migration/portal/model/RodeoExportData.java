package com.sakthi.groups.migration.portal.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RodeoExportData {

	public RodeoExportData(Integer productId, Integer skuId, String productName, String barCode, Integer itemCode,
			Integer gstPercent, Double maxRetailPrice, Double purchasePrice, Double sellingPrice, Double offerPrice,
			Date offerStartDate, Date offerEndDate, boolean productStatus) {
		super();
		this.productId = productId;
		this.skuId = skuId;
		this.productName = productName;
		this.barCode = barCode;
		this.itemCode = itemCode;
		this.gstPercent = gstPercent;
		this.maxRetailPrice = maxRetailPrice;
		this.purchasePrice = purchasePrice;
		this.sellingPrice = sellingPrice;
		this.offerPrice = offerPrice;
		this.offerStartDate = offerStartDate;
		this.offerEndDate = offerEndDate;
		this.productStatus = productStatus;
	}

	public RodeoExportData() {
		super();
	}

	@JsonProperty(value = "Product Id")
	private Integer productId;
	@JsonProperty(value = "SKU Id")
	private Integer skuId;
	@JsonProperty(value = "Product Name")
	private String productName;
	@JsonProperty(value = "BAR CODE")
	private String barCode;
	@JsonProperty(value = "ITEM CODE")
	private Integer itemCode;
	@JsonProperty(value = "GST")
	private Integer gstPercent;
	@JsonProperty(value = "MRP")
	private Double maxRetailPrice;
	@JsonProperty(value = "Purchase Price")
	private Double purchasePrice;
	@JsonProperty(value = "Selling Price")
	private Double sellingPrice;
	@JsonProperty(value = "Offer Price")
	private Double offerPrice;
	@JsonProperty(value = "Offer Start Date")
	private Date offerStartDate;
	@JsonProperty(value = "Offer End Date")
	private Date offerEndDate;
	@JsonProperty(value = "Active")
	private boolean productStatus;

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getSkuId() {
		return skuId;
	}

	public void setSkuId(Integer skuId) {
		this.skuId = skuId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getBarCode() {
		return barCode;
	}

	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}

	public Integer getItemCode() {
		return itemCode;
	}

	public void setItemCode(Integer itemCode) {
		this.itemCode = itemCode;
	}

	public Integer getGstPercent() {
		return gstPercent;
	}

	public void setGstPercent(Integer gstPercent) {
		this.gstPercent = gstPercent;
	}

	public Double getMaxRetailPrice() {
		return maxRetailPrice;
	}

	public void setMaxRetailPrice(Double maxRetailPrice) {
		this.maxRetailPrice = maxRetailPrice;
	}

	public Double getPurchasePrice() {
		return purchasePrice;
	}

	public void setPurchasePrice(Double purchasePrice) {
		this.purchasePrice = purchasePrice;
	}

	public Double getSellingPrice() {
		return sellingPrice;
	}

	public void setSellingPrice(Double sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

	public Double getOfferPrice() {
		return offerPrice;
	}

	public void setOfferPrice(Double offerPrice) {
		this.offerPrice = offerPrice;
	}

	public Date getOfferStartDate() {
		return offerStartDate;
	}

	public void setOfferStartDate(Date offerStartDate) {
		this.offerStartDate = offerStartDate;
	}

	public Date getOfferEndDate() {
		return offerEndDate;
	}

	public void setOfferEndDate(Date offerEndDate) {
		this.offerEndDate = offerEndDate;
	}

	public boolean isProductStatus() {
		return productStatus;
	}

	public void setProductStatus(boolean productStatus) {
		this.productStatus = productStatus;
	}

}
