package com.sakthi.groups.migration.portal.model;

import java.sql.Timestamp;

public class BillInfo {

	private Integer billNo;
	private Timestamp billDate;
	private Integer customerCode;
	private String customerName;
	private String customerAddr;
	private Double discountAmt;
	private Double billAmt;
	private String billType;
	private Double tenderAmt;
	private Double totalItems;
	private Double profit;
	private Integer invoiceNo;
	private String logInUser;
	private String customerArea;
	private Double vatAmt;
	private Double cashAmt;
	private Double couponAmt;
	private Double cardAmt;
	private Double walletAmt;
	private String counterName;
	private boolean isDeliveryBill;
	private Integer priceLevelCode;
	private String customerStreet;
	private String customerPlace;
	private String customerCity;
	private String phoneNo1;
	private String phoneNo2;
	private String phoneNo3;
	private Double sGstAmt;
	private Double cGstAmt;
	private Double iGstAmt;
	private Double loyaltyPoints;
	private Double redeemedPoints;
	private String gstNo;
	private Integer splitBillNo;
	private Integer invType;
	private Timestamp modificationTime;

	public BillInfo(Integer billNo, Timestamp billDate, Integer customerCode, String customerName, String customerAddr,
			Double discountAmt, Double billAmt, String billType, Double tenderAmt, Double totalItems, Double profit,
			Integer invoiceNo, String logInUser, String customerArea, Double vatAmt, Double cashAmt, Double couponAmt,
			Double cardAmt, Double walletAmt, String counterName, boolean isDeliveryBill, Integer priceLevelCode, String customerStreet,
			String customerPlace, String customerCity, String phoneNo1, String phoneNo2, String phoneNo3,
			Double sGstAmt, Double cGstAmt, Double iGstAmt, Double loyaltyPoints, Double redeemedPoints, String gstNo,
			Integer splitBillNo, Integer invType, Timestamp modificationTime) {
		super();
		this.billNo = billNo;
		this.billDate = billDate;
		this.customerCode = customerCode;
		this.customerName = customerName;
		this.customerAddr = customerAddr;
		this.discountAmt = discountAmt;
		this.billAmt = billAmt;
		this.billType = billType;
		this.tenderAmt = tenderAmt;
		this.totalItems = totalItems;
		this.profit = profit;
		this.invoiceNo = invoiceNo;
		this.logInUser = logInUser;
		this.customerArea = customerArea;
		this.vatAmt = vatAmt;
		this.cashAmt = cashAmt;
		this.couponAmt = couponAmt;
		this.cardAmt = cardAmt;
		this.walletAmt = walletAmt;
		this.counterName = counterName;
		this.isDeliveryBill = isDeliveryBill;
		this.priceLevelCode = priceLevelCode;
		this.customerStreet = customerStreet;
		this.customerPlace = customerPlace;
		this.customerCity = customerCity;
		this.phoneNo1 = phoneNo1;
		this.phoneNo2 = phoneNo2;
		this.phoneNo3 = phoneNo3;
		this.sGstAmt = sGstAmt;
		this.cGstAmt = cGstAmt;
		this.iGstAmt = iGstAmt;
		this.loyaltyPoints = loyaltyPoints;
		this.redeemedPoints = redeemedPoints;
		this.gstNo = gstNo;
		this.splitBillNo = splitBillNo;
		this.invType = invType;
		this.modificationTime = modificationTime;
	}

	public BillInfo() {

	}

	public Integer getBillNo() {
		return billNo;
	}

	public void setBillNo(Integer billNo) {
		this.billNo = billNo;
	}

	public Timestamp getBillDate() {
		return billDate;
	}

	public void setBillDate(Timestamp billDate) {
		this.billDate = billDate;
	}

	public Integer getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(Integer customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerAddr() {
		return customerAddr;
	}

	public void setCustomerAddr(String customerAddr) {
		this.customerAddr = customerAddr;
	}

	public Double getDiscountAmt() {
		return discountAmt;
	}

	public void setDiscountAmt(Double discountAmt) {
		this.discountAmt = discountAmt;
	}

	public Double getBillAmt() {
		return billAmt;
	}

	public void setBillAmt(Double billAmt) {
		this.billAmt = billAmt;
	}

	public String getBillType() {
		return billType;
	}

	public void setBillType(String billType) {
		this.billType = billType;
	}

	public Double getTenderAmt() {
		return tenderAmt;
	}

	public void setTenderAmt(Double tenderAmt) {
		this.tenderAmt = tenderAmt;
	}

	public Double getTotalItems() {
		return totalItems;
	}

	public void setTotalItems(Double totalItems) {
		this.totalItems = totalItems;
	}

	public Double getProfit() {
		return profit;
	}

	public void setProfit(Double profit) {
		this.profit = profit;
	}

	public Integer getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(Integer invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public String getLogInUser() {
		return logInUser;
	}

	public void setLogInUser(String logInUser) {
		this.logInUser = logInUser;
	}

	public String getCustomerArea() {
		return customerArea;
	}

	public void setCustomerArea(String customerArea) {
		this.customerArea = customerArea;
	}

	public Double getVatAmt() {
		return vatAmt;
	}

	public void setVatAmt(Double vatAmt) {
		this.vatAmt = vatAmt;
	}

	public Double getCashAmt() {
		return cashAmt;
	}

	public void setCashAmt(Double cashAmt) {
		this.cashAmt = cashAmt;
	}

	public Double getCouponAmt() {
		return couponAmt;
	}

	public void setCouponAmt(Double couponAmt) {
		this.couponAmt = couponAmt;
	}

	public Double getCardAmt() {
		return cardAmt;
	}

	public void setCardAmt(Double cardAmt) {
		this.cardAmt = cardAmt;
	}
	
	public Double getWalletAmt() {
		return walletAmt;
	}

	public void setWalletAmt(Double walletAmt) {
		this.walletAmt = walletAmt;
	}

	public String getCounterName() {
		return counterName;
	}

	public void setCounterName(String counterName) {
		this.counterName = counterName;
	}

	public boolean isDeliveryBill() {
		return isDeliveryBill;
	}

	public void setDeliveryBill(boolean isDeliveryBill) {
		this.isDeliveryBill = isDeliveryBill;
	}

	public Integer getPriceLevelCode() {
		return priceLevelCode;
	}

	public void setPriceLevelCode(Integer priceLevelCode) {
		this.priceLevelCode = priceLevelCode;
	}

	public String getCustomerStreet() {
		return customerStreet;
	}

	public void setCustomerStreet(String customerStreet) {
		this.customerStreet = customerStreet;
	}

	public String getCustomerPlace() {
		return customerPlace;
	}

	public void setCustomerPlace(String customerPlace) {
		this.customerPlace = customerPlace;
	}

	public String getCustomerCity() {
		return customerCity;
	}

	public void setCustomerCity(String customerCity) {
		this.customerCity = customerCity;
	}

	public String getPhoneNo1() {
		return phoneNo1;
	}

	public void setPhoneNo1(String phoneNo1) {
		this.phoneNo1 = phoneNo1;
	}

	public String getPhoneNo2() {
		return phoneNo2;
	}

	public void setPhoneNo2(String phoneNo2) {
		this.phoneNo2 = phoneNo2;
	}

	public String getPhoneNo3() {
		return phoneNo3;
	}

	public void setPhoneNo3(String phoneNo3) {
		this.phoneNo3 = phoneNo3;
	}

	public Double getsGstAmt() {
		return sGstAmt;
	}

	public void setsGstAmt(Double sGstAmt) {
		this.sGstAmt = sGstAmt;
	}

	public Double getcGstAmt() {
		return cGstAmt;
	}

	public void setcGstAmt(Double cGstAmt) {
		this.cGstAmt = cGstAmt;
	}

	public Double getiGstAmt() {
		return iGstAmt;
	}

	public void setiGstAmt(Double iGstAmt) {
		this.iGstAmt = iGstAmt;
	}

	public Double getLoyaltyPoints() {
		return loyaltyPoints;
	}

	public void setLoyaltyPoints(Double loyaltyPoints) {
		this.loyaltyPoints = loyaltyPoints;
	}

	public Double getRedeemedPoints() {
		return redeemedPoints;
	}

	public void setRedeemedPoints(Double redeemedPoints) {
		this.redeemedPoints = redeemedPoints;
	}

	public String getGstNo() {
		return gstNo;
	}

	public void setGstNo(String gstNo) {
		this.gstNo = gstNo;
	}

	public Integer getSplitBillNo() {
		return splitBillNo;
	}

	public void setSplitBillNo(Integer splitBillNo) {
		this.splitBillNo = splitBillNo;
	}

	public Integer getInvType() {
		return invType;
	}

	public void setInvType(Integer invType) {
		this.invType = invType;
	}

	public Timestamp getModificationTime() {
		return modificationTime;
	}

	public void setModificationTime(Timestamp modificationTime) {
		this.modificationTime = modificationTime;
	}
}
