package com.sakthi.groups.migration.portal.model;

import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "purchase_bill_items")
public class PurchaseBillItemDetails {

	public PurchaseBillItemDetails(Integer mrcNo, Integer mrcSerialNo, Integer distributorCode, String invoiceNumber,
			Date invoiceDate, Integer itemCode, Double itemQuantity, String freeItemTag, Double itemFreeQuantity,
			Double itemBasePrice, Double purchaseTaxPercent, Double purchaseTaxAmount, Double itemNetPrice,
			Double itemTotalAmount, Double maxRetailPrice, Double saleTaxPercent, Double saleTaxAmount,
			Double sellingPrice, String batchNumber, Date batchDate, Date manufactureDate, Date expiryDate,
			String mrcPrefix, Double offerPerQuantity, Double cashDiscountAmt, Integer companyId, Integer divisionId,
			Integer locationId, Double frieghtCharge, Double mmdMaxRate, Double tcsPercent, Double tcsTaxAmount,
			Double marginPercent, Double profitPercent, Double previousMarginPercent, Double previousProfitPercent,
			Double previousPurchasePrice, String itemEanCode, Double stateGstPercent, Double stateGstAmount,
			Double centralGstPercent, Double centralGstAmount, Double iGstPercent, Double iGstAmount,
			Double cessGstPercent, Double cessGstAmount, Integer gstCode) {
		super();
		this.mrcNo = mrcNo;
		this.mrcSerialNo = mrcSerialNo;
		this.distributorCode = distributorCode;
		this.invoiceNumber = invoiceNumber;
		this.invoiceDate = invoiceDate;
		this.itemCode = itemCode;
		this.itemQuantity = itemQuantity;
		this.freeItemTag = freeItemTag;
		this.itemFreeQuantity = itemFreeQuantity;
		this.itemBasePrice = itemBasePrice;
		this.purchaseTaxPercent = purchaseTaxPercent;
		this.purchaseTaxAmount = purchaseTaxAmount;
		this.itemNetPrice = itemNetPrice;
		this.itemTotalAmount = itemTotalAmount;
		this.maxRetailPrice = maxRetailPrice;
		this.saleTaxPercent = saleTaxPercent;
		this.saleTaxAmount = saleTaxAmount;
		this.sellingPrice = sellingPrice;
		this.batchNumber = batchNumber;
		this.batchDate = batchDate;
		this.manufactureDate = manufactureDate;
		this.expiryDate = expiryDate;
		this.mrcPrefix = mrcPrefix;
		this.offerPerQuantity = offerPerQuantity;
		this.cashDiscountAmt = cashDiscountAmt;
		this.companyId = companyId;
		this.divisionId = divisionId;
		this.locationId = locationId;
		this.frieghtCharge = frieghtCharge;
		this.mmdMaxRate = mmdMaxRate;
		this.tcsPercent = tcsPercent;
		this.tcsTaxAmount = tcsTaxAmount;
		this.marginPercent = marginPercent;
		this.profitPercent = profitPercent;
		this.previousMarginPercent = previousMarginPercent;
		this.previousProfitPercent = previousProfitPercent;
		this.previousPurchasePrice = previousPurchasePrice;
		this.itemEanCode = itemEanCode;
		this.stateGstPercent = stateGstPercent;
		this.stateGstAmount = stateGstAmount;
		this.centralGstPercent = centralGstPercent;
		this.centralGstAmount = centralGstAmount;
		this.iGstPercent = iGstPercent;
		this.iGstAmount = iGstAmount;
		this.cessGstPercent = cessGstPercent;
		this.cessGstAmount = cessGstAmount;
		this.gstCode = gstCode;
	}

	public PurchaseBillItemDetails() {
		super();
	}

	@Id
	private ObjectId id;
	private Integer mrcNo;
	private Integer mrcSerialNo;
	private Integer distributorCode;
	private String distributorName;
	private String invoiceNumber;
	private Date invoiceDate;
	private Integer itemCode;
	private String productName;
	private Double itemQuantity;
	private String freeItemTag;
	private Double itemFreeQuantity;
	private Double itemBasePrice;
	private Double purchaseTaxPercent;
	private Double purchaseTaxAmount;
	private Double itemNetPrice;
	private Double itemTotalAmount;
	private Double maxRetailPrice;
	private Double saleTaxPercent;
	private Double saleTaxAmount;
	private Double sellingPrice;
	private String batchNumber;
	private Date batchDate;
	private Date manufactureDate;
	private Date expiryDate;
	private String mrcPrefix;
	private Double offerPerQuantity;
	private Double cashDiscountAmt;
	private Integer companyId;
	private Integer divisionId;
	private Integer locationId;
	private Double frieghtCharge;
	private Double mmdMaxRate;
	private Double tcsPercent;
	private Double tcsTaxAmount;
	private Double marginPercent;
	private Double profitPercent;
	private Double previousMarginPercent;
	private Double previousProfitPercent;
	private Double previousPurchasePrice;
	private String itemEanCode;
	private Double stateGstPercent;
	private Double stateGstAmount;
	private Double centralGstPercent;
	private Double centralGstAmount;
	private Double iGstPercent;
	private Double iGstAmount;
	private Double cessGstPercent;
	private Double cessGstAmount;
	private Integer gstCode;

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public Integer getMrcNo() {
		return mrcNo;
	}

	public void setMrcNo(Integer mrcNo) {
		this.mrcNo = mrcNo;
	}

	public Integer getMrcSerialNo() {
		return mrcSerialNo;
	}

	public void setMrcSerialNo(Integer mrcSerialNo) {
		this.mrcSerialNo = mrcSerialNo;
	}

	public Integer getDistributorCode() {
		return distributorCode;
	}

	public void setDistributorCode(Integer distributorCode) {
		this.distributorCode = distributorCode;
	}

	public String getDistributorName() {
		return distributorName;
	}

	public void setDistributorName(String distributorName) {
		this.distributorName = distributorName;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public Integer getItemCode() {
		return itemCode;
	}

	public void setItemCode(Integer itemCode) {
		this.itemCode = itemCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Double getItemQuantity() {
		return itemQuantity;
	}

	public void setItemQuantity(Double itemQuantity) {
		this.itemQuantity = itemQuantity;
	}

	public String getFreeItemTag() {
		return freeItemTag;
	}

	public void setFreeItemTag(String freeItemTag) {
		this.freeItemTag = freeItemTag;
	}

	public Double getItemFreeQuantity() {
		return itemFreeQuantity;
	}

	public void setItemFreeQuantity(Double itemFreeQuantity) {
		this.itemFreeQuantity = itemFreeQuantity;
	}

	public Double getItemBasePrice() {
		return itemBasePrice;
	}

	public void setItemBasePrice(Double itemBasePrice) {
		this.itemBasePrice = itemBasePrice;
	}

	public Double getPurchaseTaxPercent() {
		return purchaseTaxPercent;
	}

	public void setPurchaseTaxPercent(Double purchaseTaxPercent) {
		this.purchaseTaxPercent = purchaseTaxPercent;
	}

	public Double getPurchaseTaxAmount() {
		return purchaseTaxAmount;
	}

	public void setPurchaseTaxAmount(Double purchaseTaxAmount) {
		this.purchaseTaxAmount = purchaseTaxAmount;
	}

	public Double getItemNetPrice() {
		return itemNetPrice;
	}

	public void setItemNetPrice(Double itemNetPrice) {
		this.itemNetPrice = itemNetPrice;
	}

	public Double getItemTotalAmount() {
		return itemTotalAmount;
	}

	public void setItemTotalAmount(Double itemTotalAmount) {
		this.itemTotalAmount = itemTotalAmount;
	}

	public Double getMaxRetailPrice() {
		return maxRetailPrice;
	}

	public void setMaxRetailPrice(Double maxRetailPrice) {
		this.maxRetailPrice = maxRetailPrice;
	}

	public Double getSaleTaxPercent() {
		return saleTaxPercent;
	}

	public void setSaleTaxPercent(Double saleTaxPercent) {
		this.saleTaxPercent = saleTaxPercent;
	}

	public Double getSaleTaxAmount() {
		return saleTaxAmount;
	}

	public void setSaleTaxAmount(Double saleTaxAmount) {
		this.saleTaxAmount = saleTaxAmount;
	}

	public Double getSellingPrice() {
		return sellingPrice;
	}

	public void setSellingPrice(Double sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

	public String getBatchNumber() {
		return batchNumber;
	}

	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}

	public Date getBatchDate() {
		return batchDate;
	}

	public void setBatchDate(Date batchDate) {
		this.batchDate = batchDate;
	}

	public Date getManufactureDate() {
		return manufactureDate;
	}

	public void setManufactureDate(Date manufactureDate) {
		this.manufactureDate = manufactureDate;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getMrcPrefix() {
		return mrcPrefix;
	}

	public void setMrcPrefix(String mrcPrefix) {
		this.mrcPrefix = mrcPrefix;
	}

	public Double getOfferPerQuantity() {
		return offerPerQuantity;
	}

	public void setOfferPerQuantity(Double offerPerQuantity) {
		this.offerPerQuantity = offerPerQuantity;
	}

	public Double getCashDiscountAmt() {
		return cashDiscountAmt;
	}

	public void setCashDiscountAmt(Double cashDiscountAmt) {
		this.cashDiscountAmt = cashDiscountAmt;
	}

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public Integer getDivisionId() {
		return divisionId;
	}

	public void setDivisionId(Integer divisionId) {
		this.divisionId = divisionId;
	}

	public Integer getLocationId() {
		return locationId;
	}

	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}

	public Double getFrieghtCharge() {
		return frieghtCharge;
	}

	public void setFrieghtCharge(Double frieghtCharge) {
		this.frieghtCharge = frieghtCharge;
	}

	public Double getMmdMaxRate() {
		return mmdMaxRate;
	}

	public void setMmdMaxRate(Double mmdMaxRate) {
		this.mmdMaxRate = mmdMaxRate;
	}

	public Double getTcsPercent() {
		return tcsPercent;
	}

	public void setTcsPercent(Double tcsPercent) {
		this.tcsPercent = tcsPercent;
	}

	public Double getTcsTaxAmount() {
		return tcsTaxAmount;
	}

	public void setTcsTaxAmount(Double tcsTaxAmount) {
		this.tcsTaxAmount = tcsTaxAmount;
	}

	public Double getMarginPercent() {
		return marginPercent;
	}

	public void setMarginPercent(Double marginPercent) {
		this.marginPercent = marginPercent;
	}

	public Double getProfitPercent() {
		return profitPercent;
	}

	public void setProfitPercent(Double profitPercent) {
		this.profitPercent = profitPercent;
	}

	public Double getPreviousMarginPercent() {
		return previousMarginPercent;
	}

	public void setPreviousMarginPercent(Double previousMarginPercent) {
		this.previousMarginPercent = previousMarginPercent;
	}

	public Double getPreviousProfitPercent() {
		return previousProfitPercent;
	}

	public void setPreviousProfitPercent(Double previousProfitPercent) {
		this.previousProfitPercent = previousProfitPercent;
	}

	public Double getPreviousPurchasePrice() {
		return previousPurchasePrice;
	}

	public void setPreviousPurchasePrice(Double previousPurchasePrice) {
		this.previousPurchasePrice = previousPurchasePrice;
	}

	public String getItemEanCode() {
		return itemEanCode;
	}

	public void setItemEanCode(String itemEanCode) {
		this.itemEanCode = itemEanCode;
	}

	public Double getStateGstPercent() {
		return stateGstPercent;
	}

	public void setStateGstPercent(Double stateGstPercent) {
		this.stateGstPercent = stateGstPercent;
	}

	public Double getStateGstAmount() {
		return stateGstAmount;
	}

	public void setStateGstAmount(Double stateGstAmount) {
		this.stateGstAmount = stateGstAmount;
	}

	public Double getCentralGstPercent() {
		return centralGstPercent;
	}

	public void setCentralGstPercent(Double centralGstPercent) {
		this.centralGstPercent = centralGstPercent;
	}

	public Double getCentralGstAmount() {
		return centralGstAmount;
	}

	public void setCentralGstAmount(Double centralGstAmount) {
		this.centralGstAmount = centralGstAmount;
	}

	public Double getiGstPercent() {
		return iGstPercent;
	}

	public void setiGstPercent(Double iGstPercent) {
		this.iGstPercent = iGstPercent;
	}

	public Double getiGstAmount() {
		return iGstAmount;
	}

	public void setiGstAmount(Double iGstAmount) {
		this.iGstAmount = iGstAmount;
	}

	public Double getCessGstPercent() {
		return cessGstPercent;
	}

	public void setCessGstPercent(Double cessGstPercent) {
		this.cessGstPercent = cessGstPercent;
	}

	public Double getCessGstAmount() {
		return cessGstAmount;
	}

	public void setCessGstAmount(Double cessGstAmount) {
		this.cessGstAmount = cessGstAmount;
	}

	public Integer getGstCode() {
		return gstCode;
	}

	public void setGstCode(Integer gstCode) {
		this.gstCode = gstCode;
	}
}
