package com.sakthi.groups.migration.portal.model;

public class TopGrossTrendData {

	public TopGrossTrendData() {
		super();
	}

	public TopGrossTrendData(String itemName, Integer itemCode, Double grossSale) {
		super();
		this.itemName = itemName;
		this.itemCode = itemCode;
		this.grossSale = grossSale;
	}

	private String itemName;
	private Integer itemCode;
	private Double grossSale;

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public Integer getItemCode() {
		return itemCode;
	}

	public void setItemCode(Integer itemCode) {
		this.itemCode = itemCode;
	}

	public Double getGrossSale() {
		return grossSale;
	}

	public void setGrossSale(Double grossSale) {
		this.grossSale = grossSale;
	}

}
