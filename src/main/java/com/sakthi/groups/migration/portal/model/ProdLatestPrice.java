package com.sakthi.groups.migration.portal.model;

public class ProdLatestPrice {

	private Double sellingPrice;
	private Double maxRetailPrice;
	private Double purchasePrice;

	public Double getSellingPrice() {
		return sellingPrice;
	}

	public void setSellingPrice(Double sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

	public Double getMaxRetailPrice() {
		return maxRetailPrice;
	}

	public void setMaxRetailPrice(Double maxRetailPrice) {
		this.maxRetailPrice = maxRetailPrice;
	}

	public Double getPurchasePrice() {
		return purchasePrice;
	}

	public void setPurchasePrice(Double purchasePrice) {
		this.purchasePrice = purchasePrice;
	}
}
