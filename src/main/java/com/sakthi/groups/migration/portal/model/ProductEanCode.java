package com.sakthi.groups.migration.portal.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "product_ean_code")
public class ProductEanCode {

	@Id
	private ObjectId id;
	private Integer productCode;
	@Indexed(unique = true)
	private String productEANCode;

	public ProductEanCode(String productEANCode, Integer productCode) {
		super();
		this.productCode = productCode;
		this.productEANCode = productEANCode;
	}

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public Integer getProductCode() {
		return productCode;
	}

	public void setProductCode(Integer productCode) {
		this.productCode = productCode;
	}

	public String getProductEANCode() {
		return productEANCode;
	}

	public void setProductEANCode(String productEANCode) {
		this.productEANCode = productEANCode;
	}

}
