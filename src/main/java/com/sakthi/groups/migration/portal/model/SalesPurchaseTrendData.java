package com.sakthi.groups.migration.portal.model;

import java.util.Date;

public class SalesPurchaseTrendData {

	public SalesPurchaseTrendData(String itemName, Integer itemCode, Date entryDate, Double count) {
		super();
		this.itemName = itemName;
		this.itemCode = itemCode;
		this.entryDate = entryDate;
		this.count = count;
	}

	public SalesPurchaseTrendData() {
		super();
	}

	private String itemName;
	private Integer itemCode;
	private Date entryDate;
	private Double count;

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public Integer getItemCode() {
		return itemCode;
	}

	public void setItemCode(Integer itemCode) {
		this.itemCode = itemCode;
	}

	public Date getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(Date entryDate) {
		this.entryDate = entryDate;
	}

	public Double getCount() {
		return count;
	}

	public void setCount(Double count) {
		this.count = count;
	}

}
