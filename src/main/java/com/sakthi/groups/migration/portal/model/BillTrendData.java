package com.sakthi.groups.migration.portal.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BillTrendData {

	public BillTrendData() {
		super();
	}

	public BillTrendData(Double salesAmount, Double profitAmount, Double cashAmount, Double cardAmount,
			Double upiAmount, Double dueAmount, Double creditAmount, Double loyaltyAmount) {
		super();
		this.salesAmount = salesAmount;
		this.profitAmount = profitAmount;
		this.cashAmount = cashAmount;
		this.cardAmount = cardAmount;
		this.upiAmount = upiAmount;
		this.dueAmount = dueAmount;
		this.creditAmount = creditAmount;
		this.loyaltyAmount = loyaltyAmount;
	}

	@JsonProperty("Sales")
	private Double salesAmount;
	@JsonProperty("Profit")
	private Double profitAmount;
	@JsonProperty("Cash")
	private Double cashAmount;
	@JsonProperty("Card")
	private Double cardAmount;
	@JsonProperty("UPI")
	private Double upiAmount;
	@JsonProperty("Due")
	private Double dueAmount;
	@JsonProperty("Credit")
	private Double creditAmount;
	@JsonProperty("Loyalty")
	private Double loyaltyAmount;

	public Double getSalesAmount() {
		return salesAmount;
	}

	public void setSalesAmount(Double salesAmount) {
		this.salesAmount = salesAmount;
	}

	public Double getProfitAmount() {
		return profitAmount;
	}

	public void setProfitAmount(Double profitAmount) {
		this.profitAmount = profitAmount;
	}

	public Double getCashAmount() {
		return cashAmount;
	}

	public void setCashAmount(Double cashAmount) {
		this.cashAmount = cashAmount;
	}

	public Double getCardAmount() {
		return cardAmount;
	}

	public void setCardAmount(Double cardAmount) {
		this.cardAmount = cardAmount;
	}

	public Double getUpiAmount() {
		return upiAmount;
	}

	public void setUpiAmount(Double upiAmount) {
		this.upiAmount = upiAmount;
	}

	public Double getDueAmount() {
		return dueAmount;
	}

	public void setDueAmount(Double dueAmount) {
		this.dueAmount = dueAmount;
	}

	public Double getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(Double creditAmount) {
		this.creditAmount = creditAmount;
	}

	public Double getLoyaltyAmount() {
		return loyaltyAmount;
	}

	public void setLoyaltyAmount(Double loyaltyAmount) {
		this.loyaltyAmount = loyaltyAmount;
	}

}
