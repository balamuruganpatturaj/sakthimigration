package com.sakthi.groups.migration.portal.model;

import java.util.Date;
import java.util.UUID;

import org.bson.types.Binary;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "delivery_order_detail")
public class DeliveryOrderDetail {

	public DeliveryOrderDetail() {
		super();
	}

	public DeliveryOrderDetail(Integer customerCode, String customerName, String customerAddress,
			String phoneNumber, Date orderPlacedDate, String billType, Integer billNo, Integer invoiceNo, Double billAmount,
			Double payment, String paymentMode, Double balance, String deliveryStatus, String deliveryNotes,
			String deliveryPerson, UUID batchNumber, String billFileName, Binary billFile, boolean isOrderNotified,
			boolean isBillNotified, boolean isPaymentNotified, Date billModifiedDate) {
		super();
		this.customerCode = customerCode;
		this.customerName = customerName;
		this.customerAddress = customerAddress;
		this.phoneNumber = phoneNumber;
		this.orderPlacedDate = orderPlacedDate;
		this.billType = billType;
		this.billNo = billNo;
		this.invoiceNo = invoiceNo;
		this.billAmount = billAmount;
		this.payment = payment;
		this.paymentMode = paymentMode;
		this.balance = balance;
		this.deliveryStatus = deliveryStatus;
		this.deliveryNotes = deliveryNotes;
		this.deliveryPerson = deliveryPerson;
		this.batchNumber = batchNumber;
		this.billFileName = billFileName;
		this.billFile = billFile;
		this.isOrderNotified = isOrderNotified;
		this.isBillNotified = isBillNotified;
		this.isPaymentNotified = isPaymentNotified;
		this.billModifiedDate = billModifiedDate;
	}

	@Id
	private ObjectId id;
	private Integer customerCode;
	private String customerName;
	private String customerAddress;
	private String phoneNumber;
	private Date orderPlacedDate;
	private String billType;
	private Integer billNo;
	private Integer invoiceNo;
	private Double billAmount;
	private Double payment;
	private String paymentMode;
	private Double balance;
	private String deliveryStatus;
	private String deliveryNotes;
	private String deliveryPerson;
	private UUID batchNumber;
	private String billFileName;
	private Binary billFile;
	private boolean isOrderNotified;
	private boolean isBillNotified;
	private boolean isPaymentNotified;
	private Date billModifiedDate;

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public Integer getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(Integer customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Date getOrderPlacedDate() {
		return orderPlacedDate;
	}

	public void setOrderPlacedDate(Date orderPlacedDate) {
		this.orderPlacedDate = orderPlacedDate;
	}

	public String getBillType() {
		return billType;
	}

	public void setBillType(String billType) {
		this.billType = billType;
	}

	public Integer getBillNo() {
		return billNo;
	}

	public void setBillNo(Integer billNo) {
		this.billNo = billNo;
	}

	public Integer getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(Integer invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public Double getBillAmount() {
		return billAmount;
	}

	public void setBillAmount(Double billAmount) {
		this.billAmount = billAmount;
	}

	public Double getPayment() {
		return payment;
	}

	public void setPayment(Double payment) {
		this.payment = payment;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public Double getBalance() {
		return balance;
	}

	public void setBalance(Double balance) {
		this.balance = balance;
	}

	public String getDeliveryStatus() {
		return deliveryStatus;
	}

	public void setDeliveryStatus(String deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}

	public String getDeliveryNotes() {
		return deliveryNotes;
	}

	public void setDeliveryNotes(String deliveryNotes) {
		this.deliveryNotes = deliveryNotes;
	}

	public String getDeliveryPerson() {
		return deliveryPerson;
	}

	public void setDeliveryPerson(String deliveryPerson) {
		this.deliveryPerson = deliveryPerson;
	}

	public UUID getBatchNumber() {
		return batchNumber;
	}

	public void setBatchNumber(UUID batchNumber) {
		this.batchNumber = batchNumber;
	}

	public String getBillFileName() {
		return billFileName;
	}

	public void setBillFileName(String billFileName) {
		this.billFileName = billFileName;
	}

	public Binary getBillFile() {
		return billFile;
	}

	public void setBillFile(Binary billFile) {
		this.billFile = billFile;
	}

	public boolean isOrderNotified() {
		return isOrderNotified;
	}

	public void setOrderNotified(boolean isOrderNotified) {
		this.isOrderNotified = isOrderNotified;
	}

	public boolean isBillNotified() {
		return isBillNotified;
	}

	public void setBillNotified(boolean isBillNotified) {
		this.isBillNotified = isBillNotified;
	}

	public boolean isPaymentNotified() {
		return isPaymentNotified;
	}

	public void setPaymentNotified(boolean isPaymentNotified) {
		this.isPaymentNotified = isPaymentNotified;
	}

	public Date getBillModifiedDate() {
		return billModifiedDate;
	}

	public void setBillModifiedDate(Date billModifiedDate) {
		this.billModifiedDate = billModifiedDate;
	}

}
