package com.sakthi.groups.migration.portal.dao;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.sakthi.groups.migration.portal.model.ProductEanCode;

public interface ProductEanCodeRepository extends MongoRepository<ProductEanCode, ObjectId> {

	ProductEanCode findByProductEANCode(String productEANCode);
}
