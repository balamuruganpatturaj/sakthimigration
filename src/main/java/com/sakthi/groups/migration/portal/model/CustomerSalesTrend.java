package com.sakthi.groups.migration.portal.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CustomerSalesTrend {
	
	public CustomerSalesTrend() {
		super();
	}

	public CustomerSalesTrend(String billRange, Integer billCount, Double salesAmount, Double profit) {
		super();
		this.billRange = billRange;
		this.billCount = billCount;
		this.salesAmount = salesAmount;
		this.profit = profit;
	}

	@JsonProperty("Period")
	private String billRange;
	
	@JsonProperty("Bill Count")
	private Integer billCount;
	
	@JsonProperty("Sales")
	private Double salesAmount;
	
	@JsonProperty("Profit")
	private Double profit;

	public String getBillRange() {
		return billRange;
	}

	public void setBillRange(String billRange) {
		this.billRange = billRange;
	}

	public Integer getBillCount() {
		return billCount;
	}

	public void setBillCount(Integer billCount) {
		this.billCount = billCount;
	}

	public Double getSalesAmount() {
		return salesAmount;
	}

	public void setSalesAmount(Double salesAmount) {
		this.salesAmount = salesAmount;
	}

	public Double getProfit() {
		return profit;
	}

	public void setProfit(Double profit) {
		this.profit = profit;
	}

}
