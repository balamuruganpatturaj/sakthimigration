package com.sakthi.groups.migration.portal.dao;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.sakthi.groups.migration.portal.model.PurchaseBillItemDetails;

public interface PurchaseBillItemRepository extends MongoRepository<PurchaseBillItemDetails, ObjectId> {

	List<PurchaseBillItemDetails> findByMrcNo(Integer mrcNo);

	PurchaseBillItemDetails findByMrcNoAndMrcSerialNo(Integer mrcNo, Integer mrcSerialNo);

	List<PurchaseBillItemDetails> findByItemCodeOrderByMrcNoDesc(Integer itemCode);

}
