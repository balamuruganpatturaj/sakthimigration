package com.sakthi.groups.migration.portal.dao;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.sakthi.groups.migration.portal.model.SupplierMaster;

public interface MigSupplierRepository extends MongoRepository<SupplierMaster, ObjectId> {

	SupplierMaster findBySupplierCode(Integer supplierCode);

}
