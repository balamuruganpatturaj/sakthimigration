package com.sakthi.groups.migration.portal.dao;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.sakthi.groups.migration.portal.model.DeliveryOrderDetail;


public interface DeliveryDetailRepository extends MongoRepository<DeliveryOrderDetail, ObjectId> {

	DeliveryOrderDetail findByPhoneNumberAndOrderPlacedDate(String phoneNumber, Date orderDateFomatted);
	
	DeliveryOrderDetail findByPhoneNumberAndBillNo(String phoneNumber, Integer billNo);

	List<DeliveryOrderDetail> findByPhoneNumber(String phoneNumber);
	
	List<DeliveryOrderDetail> findByBatchNumber(UUID batchNumber);
	
	List<DeliveryOrderDetail> findByDeliveryStatusOrderByOrderPlacedDateDesc(String deliveryStatus);
	
	List<DeliveryOrderDetail> findByDeliveryStatusIn(List<String> deliveryStatus);
	
	List<DeliveryOrderDetail> findByDeliveryStatusNotIn(List<String> deliveryStatus);
	
	DeliveryOrderDetail findByBillNo(Integer billNo);

}
