package com.sakthi.groups.migration.portal.model;

public class UnicodeModel {

	public UnicodeModel(String localLanguageName, String baminiFontName) {
		super();
		this.localLanguageName = localLanguageName;
		this.baminiFontName = baminiFontName;
	}
	
	public UnicodeModel() {
		
	}

	private String localLanguageName;
	private String baminiFontName;

	public String getLocalLanguageName() {
		return localLanguageName;
	}

	public void setLocalLanguageName(String localLanguageName) {
		this.localLanguageName = localLanguageName;
	}

	public String getBaminiFontName() {
		return baminiFontName;
	}

	public void setBaminiFontName(String baminiFontName) {
		this.baminiFontName = baminiFontName;
	}

}
