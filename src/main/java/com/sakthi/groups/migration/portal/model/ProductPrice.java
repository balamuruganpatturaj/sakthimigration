package com.sakthi.groups.migration.portal.model;

import java.util.List;

public class ProductPrice {

	private Double sellingPrice;
	private Double maxRetailPrice;
	private Double purchasePrice;
	private Double availableStock;
	private List<ProductPriceLevel> priceLevel;

	public Double getSellingPrice() {
		return sellingPrice;
	}

	public void setSellingPrice(Double sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

	public Double getMaxRetailPrice() {
		return maxRetailPrice;
	}

	public void setMaxRetailPrice(Double maxRetailPrice) {
		this.maxRetailPrice = maxRetailPrice;
	}

	public Double getPurchasePrice() {
		return purchasePrice;
	}

	public void setPurchasePrice(Double purchasePrice) {
		this.purchasePrice = purchasePrice;
	}

	public List<ProductPriceLevel> getPriceLevel() {
		return priceLevel;
	}

	public void setPriceLevel(List<ProductPriceLevel> priceLevel) {
		this.priceLevel = priceLevel;
	}

	public Double getAvailableStock() {
		return availableStock;
	}

	public void setAvailableStock(Double availableStock) {
		this.availableStock = availableStock;
	}

}
