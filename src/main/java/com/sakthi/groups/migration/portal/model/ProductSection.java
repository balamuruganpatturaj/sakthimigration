package com.sakthi.groups.migration.portal.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;

public class ProductSection {

	@Id
	private ObjectId id;
	private ObjectId productObjId;
	@Indexed(unique = true)
	private Integer productCode;
	@Indexed(unique = true)
	private String productAlias;
	private String section;
	private String subSection;

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public ObjectId getProductObjId() {
		return productObjId;
	}

	public void setProductObjId(ObjectId productObjId) {
		this.productObjId = productObjId;
	}

	public Integer getProductCode() {
		return productCode;
	}

	public void setProductCode(Integer productCode) {
		this.productCode = productCode;
	}

	public String getProductAlias() {
		return productAlias;
	}

	public void setProductAlias(String productAlias) {
		this.productAlias = productAlias;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getSubSection() {
		return subSection;
	}

	public void setSubSection(String subSection) {
		this.subSection = subSection;
	}

}
