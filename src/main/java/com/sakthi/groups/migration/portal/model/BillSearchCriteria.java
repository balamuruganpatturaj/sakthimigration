package com.sakthi.groups.migration.portal.model;

public class BillSearchCriteria {

	private String billStartDate;
	private String billEndDate;
	private Double billStartAmt;
	private Double billEndAmt;
	private Integer billNumber;
	private String counterName;
	private String customerName;
	private boolean pBillSearch;
	private boolean quotationBill;
	private boolean deliveryBill;
	private Integer billFilterMinCount;
	private Integer billFilterMaxCount;
	private Double billFilterMinAmt;
	private Double billFilterMaxAmt;
	private Double billFilterProfitMinAmt;
	private Double billFilterProfitMaxAmt;
	private boolean loyaltyAllowed;
	private Integer itemCode;
	private Integer supplierCode;

	public String getBillStartDate() {
		return billStartDate;
	}

	public void setBillStartDate(String billStartDate) {
		this.billStartDate = billStartDate;
	}

	public String getBillEndDate() {
		return billEndDate;
	}

	public void setBillEndDate(String billEndDate) {
		this.billEndDate = billEndDate;
	}

	public Double getBillStartAmt() {
		return billStartAmt;
	}

	public void setBillStartAmt(Double billStartAmt) {
		this.billStartAmt = billStartAmt;
	}

	public Double getBillEndAmt() {
		return billEndAmt;
	}

	public void setBillEndAmt(Double billEndAmt) {
		this.billEndAmt = billEndAmt;
	}

	public Integer getBillNumber() {
		return billNumber;
	}

	public void setBillNumber(Integer billNumber) {
		this.billNumber = billNumber;
	}

	public String getCounterName() {
		return counterName;
	}

	public void setCounterName(String counterName) {
		this.counterName = counterName;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public boolean isPBillSearch() {
		return pBillSearch;
	}

	public void setPBillSearch(boolean pBillSearch) {
		this.pBillSearch = pBillSearch;
	}

	public boolean isQuotationBill() {
		return quotationBill;
	}

	public void setQuotationBill(boolean quotationBill) {
		this.quotationBill = quotationBill;
	}

	public boolean isDeliveryBill() {
		return deliveryBill;
	}

	public void setDeliveryBill(boolean deliveryBill) {
		this.deliveryBill = deliveryBill;
	}

	public Integer getBillFilterMinCount() {
		return billFilterMinCount;
	}

	public void setBillFilterMinCount(Integer billFilterMinCount) {
		this.billFilterMinCount = billFilterMinCount;
	}

	public Integer getBillFilterMaxCount() {
		return billFilterMaxCount;
	}

	public void setBillFilterMaxCount(Integer billFilterMaxCount) {
		this.billFilterMaxCount = billFilterMaxCount;
	}

	public Double getBillFilterMinAmt() {
		return billFilterMinAmt;
	}

	public void setBillFilterMinAmt(Double billFilterMinAmt) {
		this.billFilterMinAmt = billFilterMinAmt;
	}

	public Double getBillFilterMaxAmt() {
		return billFilterMaxAmt;
	}

	public void setBillFilterMaxAmt(Double billFilterMaxAmt) {
		this.billFilterMaxAmt = billFilterMaxAmt;
	}

	public Double getBillFilterProfitMinAmt() {
		return billFilterProfitMinAmt;
	}

	public void setBillFilterProfitMinAmt(Double billFilterProfitMinAmt) {
		this.billFilterProfitMinAmt = billFilterProfitMinAmt;
	}

	public Double getBillFilterProfitMaxAmt() {
		return billFilterProfitMaxAmt;
	}

	public void setBillFilterProfitMaxAmt(Double billFilterProfitMaxAmt) {
		this.billFilterProfitMaxAmt = billFilterProfitMaxAmt;
	}

	public boolean isLoyaltyAllowed() {
		return loyaltyAllowed;
	}

	public void setLoyaltyAllowed(boolean loyaltyAllowed) {
		this.loyaltyAllowed = loyaltyAllowed;
	}

	public Integer getItemCode() {
		return itemCode;
	}

	public void setItemCode(Integer itemCode) {
		this.itemCode = itemCode;
	}

	public Integer getSupplierCode() {
		return supplierCode;
	}

	public void setSupplierCode(Integer supplierCode) {
		this.supplierCode = supplierCode;
	}

}
