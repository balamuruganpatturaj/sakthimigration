package com.sakthi.groups.migration.portal.model;

import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "purchase_bill_entry")
public class PurchaseBillEntry {

	public PurchaseBillEntry() {
		super();
	}

	public PurchaseBillEntry(String mrcPrefix, Integer mrcNo, Date purchaseEnterDate, Integer distributorCode,
			Double totalItemQuantity, Double billAmount, Double paidAmount, String paidFlag, Date paymentDueDate,
			Double lastPaidDueAmount, String invoiceNo, Date invoiceDate, Double discountAmount, Double creditAmount,
			Double debitAmount, Double invoiceAmount, String invoicePaymentType, Double invoiceDiscountPercent,
			Integer companyId, Integer divisionId, Integer locationId, Double frieghtAmount, Integer purchaseType,
			Double taxAmount, Double manualDiscountAmt, Integer netRateAfterDisc, Double packingChargeAmt,
			Integer inventoryType, Integer goFrugalRefNo, Double tcsPercent, Double tcsAmount, Integer purchaseAckNo,
			Double stateGstAmount, Double centralGstAmount, Double iGstAmount, Double cessGstAmount,
			Integer taxCalculationType, Date mrcEntryDate) {
		super();
		this.mrcPrefix = mrcPrefix;
		this.mrcNo = mrcNo;
		this.purchaseEnterDate = purchaseEnterDate;
		this.distributorCode = distributorCode;
		this.totalItemQuantity = totalItemQuantity;
		this.billAmount = billAmount;
		this.paidAmount = paidAmount;
		this.paidFlag = paidFlag;
		this.paymentDueDate = paymentDueDate;
		this.lastPaidDueAmount = lastPaidDueAmount;
		this.invoiceNo = invoiceNo;
		this.invoiceDate = invoiceDate;
		this.discountAmount = discountAmount;
		this.creditAmount = creditAmount;
		this.debitAmount = debitAmount;
		this.invoiceAmount = invoiceAmount;
		this.invoicePaymentType = invoicePaymentType;
		this.invoiceDiscountPercent = invoiceDiscountPercent;
		this.companyId = companyId;
		this.divisionId = divisionId;
		this.locationId = locationId;
		this.frieghtAmount = frieghtAmount;
		this.purchaseType = purchaseType;
		this.taxAmount = taxAmount;
		this.manualDiscountAmt = manualDiscountAmt;
		this.netRateAfterDisc = netRateAfterDisc;
		this.packingChargeAmt = packingChargeAmt;
		this.inventoryType = inventoryType;
		this.goFrugalRefNo = goFrugalRefNo;
		this.tcsPercent = tcsPercent;
		this.tcsAmount = tcsAmount;
		this.purchaseAckNo = purchaseAckNo;
		this.stateGstAmount = stateGstAmount;
		this.centralGstAmount = centralGstAmount;
		this.iGstAmount = iGstAmount;
		this.cessGstAmount = cessGstAmount;
		this.taxCalculationType = taxCalculationType;
		this.mrcEntryDate = mrcEntryDate;
	}

	@Id
	private ObjectId id;
	private String mrcPrefix;
	private Integer mrcNo;
	private Date purchaseEnterDate;
	private Integer distributorCode;
	private String distributorName;
	private Double totalItemQuantity;
	private Double billAmount;
	private Double paidAmount;
	private String paidFlag;
	private Date paymentDueDate;
	private Double lastPaidDueAmount;
	private String invoiceNo;
	private Date invoiceDate;
	private Double discountAmount;
	private Double creditAmount;
	private Double debitAmount;
	private Double invoiceAmount;
	private String invoicePaymentType;
	private Double invoiceDiscountPercent;
	private Integer companyId;
	private Integer divisionId;
	private Integer locationId;
	private Double frieghtAmount;
	private Integer purchaseType;
	private Double taxAmount;
	private Double manualDiscountAmt;
	private Integer netRateAfterDisc;
	private Double packingChargeAmt;
	private Integer inventoryType;
	private Integer goFrugalRefNo;
	private Double tcsPercent;
	private Double tcsAmount;
	private Integer purchaseAckNo;
	private Double stateGstAmount;
	private Double centralGstAmount;
	private Double iGstAmount;
	private Double cessGstAmount;
	private Integer taxCalculationType;
	private Date mrcEntryDate;

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public String getMrcPrefix() {
		return mrcPrefix;
	}

	public void setMrcPrefix(String mrcPrefix) {
		this.mrcPrefix = mrcPrefix;
	}

	public Integer getMrcNo() {
		return mrcNo;
	}

	public void setMrcNo(Integer mrcNo) {
		this.mrcNo = mrcNo;
	}

	public Date getPurchaseEnterDate() {
		return purchaseEnterDate;
	}

	public void setPurchaseEnterDate(Date purchaseEnterDate) {
		this.purchaseEnterDate = purchaseEnterDate;
	}

	public Integer getDistributorCode() {
		return distributorCode;
	}

	public void setDistributorCode(Integer distributorCode) {
		this.distributorCode = distributorCode;
	}

	public String getDistributorName() {
		return distributorName;
	}

	public void setDistributorName(String distributorName) {
		this.distributorName = distributorName;
	}

	public Double getTotalItemQuantity() {
		return totalItemQuantity;
	}

	public void setTotalItemQuantity(Double totalItemQuantity) {
		this.totalItemQuantity = totalItemQuantity;
	}

	public Double getBillAmount() {
		return billAmount;
	}

	public void setBillAmount(Double billAmount) {
		this.billAmount = billAmount;
	}

	public Double getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(Double paidAmount) {
		this.paidAmount = paidAmount;
	}

	public String getPaidFlag() {
		return paidFlag;
	}

	public void setPaidFlag(String paidFlag) {
		this.paidFlag = paidFlag;
	}

	public Date getPaymentDueDate() {
		return paymentDueDate;
	}

	public void setPaymentDueDate(Date paymentDueDate) {
		this.paymentDueDate = paymentDueDate;
	}

	public Double getLastPaidDueAmount() {
		return lastPaidDueAmount;
	}

	public void setLastPaidDueAmount(Double lastPaidDueAmount) {
		this.lastPaidDueAmount = lastPaidDueAmount;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public Double getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(Double discountAmount) {
		this.discountAmount = discountAmount;
	}

	public Double getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(Double creditAmount) {
		this.creditAmount = creditAmount;
	}

	public Double getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(Double debitAmount) {
		this.debitAmount = debitAmount;
	}

	public Double getInvoiceAmount() {
		return invoiceAmount;
	}

	public void setInvoiceAmount(Double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}

	public String getInvoicePaymentType() {
		return invoicePaymentType;
	}

	public void setInvoicePaymentType(String invoicePaymentType) {
		this.invoicePaymentType = invoicePaymentType;
	}

	public Double getInvoiceDiscountPercent() {
		return invoiceDiscountPercent;
	}

	public void setInvoiceDiscountPercent(Double invoiceDiscountPercent) {
		this.invoiceDiscountPercent = invoiceDiscountPercent;
	}

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public Integer getDivisionId() {
		return divisionId;
	}

	public void setDivisionId(Integer divisionId) {
		this.divisionId = divisionId;
	}

	public Integer getLocationId() {
		return locationId;
	}

	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}

	public Double getFrieghtAmount() {
		return frieghtAmount;
	}

	public void setFrieghtAmount(Double frieghtAmount) {
		this.frieghtAmount = frieghtAmount;
	}

	public Integer getPurchaseType() {
		return purchaseType;
	}

	public void setPurchaseType(Integer purchaseType) {
		this.purchaseType = purchaseType;
	}

	public Double getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public Double getManualDiscountAmt() {
		return manualDiscountAmt;
	}

	public void setManualDiscountAmt(Double manualDiscountAmt) {
		this.manualDiscountAmt = manualDiscountAmt;
	}

	public Integer getNetRateAfterDisc() {
		return netRateAfterDisc;
	}

	public void setNetRateAfterDisc(Integer netRateAfterDisc) {
		this.netRateAfterDisc = netRateAfterDisc;
	}

	public Double getPackingChargeAmt() {
		return packingChargeAmt;
	}

	public void setPackingChargeAmt(Double packingChargeAmt) {
		this.packingChargeAmt = packingChargeAmt;
	}

	public Integer getInventoryType() {
		return inventoryType;
	}

	public void setInventoryType(Integer inventoryType) {
		this.inventoryType = inventoryType;
	}

	public Integer getGoFrugalRefNo() {
		return goFrugalRefNo;
	}

	public void setGoFrugalRefNo(Integer goFrugalRefNo) {
		this.goFrugalRefNo = goFrugalRefNo;
	}

	public Double getTcsPercent() {
		return tcsPercent;
	}

	public void setTcsPercent(Double tcsPercent) {
		this.tcsPercent = tcsPercent;
	}

	public Double getTcsAmount() {
		return tcsAmount;
	}

	public void setTcsAmount(Double tcsAmount) {
		this.tcsAmount = tcsAmount;
	}

	public Integer getPurchaseAckNo() {
		return purchaseAckNo;
	}

	public void setPurchaseAckNo(Integer purchaseAckNo) {
		this.purchaseAckNo = purchaseAckNo;
	}

	public Double getStateGstAmount() {
		return stateGstAmount;
	}

	public void setStateGstAmount(Double stateGstAmount) {
		this.stateGstAmount = stateGstAmount;
	}

	public Double getCentralGstAmount() {
		return centralGstAmount;
	}

	public void setCentralGstAmount(Double centralGstAmount) {
		this.centralGstAmount = centralGstAmount;
	}

	public Double getiGstAmount() {
		return iGstAmount;
	}

	public void setiGstAmount(Double iGstAmount) {
		this.iGstAmount = iGstAmount;
	}

	public Double getCessGstAmount() {
		return cessGstAmount;
	}

	public void setCessGstAmount(Double cessGstAmount) {
		this.cessGstAmount = cessGstAmount;
	}

	public Integer getTaxCalculationType() {
		return taxCalculationType;
	}

	public void setTaxCalculationType(Integer taxCalculationType) {
		this.taxCalculationType = taxCalculationType;
	}

	public Date getMrcEntryDate() {
		return mrcEntryDate;
	}

	public void setMrcEntryDate(Date mrcEntryDate) {
		this.mrcEntryDate = mrcEntryDate;
	}
}
