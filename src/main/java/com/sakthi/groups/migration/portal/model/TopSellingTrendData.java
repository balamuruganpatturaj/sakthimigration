package com.sakthi.groups.migration.portal.model;

public class TopSellingTrendData {

	public TopSellingTrendData() {
		super();
	}

	public TopSellingTrendData(String itemName, Integer itemCode, Integer saleCount) {
		super();
		this.itemName = itemName;
		this.itemCode = itemCode;
		this.saleCount = saleCount;
	}

	private String itemName;
	private Integer itemCode;
	private Integer saleCount;

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public Integer getItemCode() {
		return itemCode;
	}

	public void setItemCode(Integer itemCode) {
		this.itemCode = itemCode;
	}

	public Integer getSaleCount() {
		return saleCount;
	}

	public void setSaleCount(Integer saleCount) {
		this.saleCount = saleCount;
	}

}
