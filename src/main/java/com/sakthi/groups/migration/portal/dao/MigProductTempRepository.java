package com.sakthi.groups.migration.portal.dao;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.sakthi.groups.migration.portal.model.ProductMasterTemp;

public interface MigProductTempRepository extends MongoRepository<ProductMasterTemp, ObjectId> {
	
	ProductMasterTemp findByProductCode(Integer productCode);

}
