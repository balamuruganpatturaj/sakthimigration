package com.sakthi.groups.migration.portal.model;

import java.util.List;
import java.util.Set;

public class BillAnalyticsData {

	public BillAnalyticsData(Integer customerCode, String customerName, String customerAddr, Set<String> phoneNumber,
			Integer billCount, Double totalBillAmount, Double totalProfit, boolean customerNotified,
			boolean giftIssued) {
		super();
		this.customerCode = customerCode;
		this.customerName = customerName;
		this.customerAddr = customerAddr;
		this.phoneNumber = phoneNumber;
		this.billCount = billCount;
		this.totalBillAmount = totalBillAmount;
		this.totalProfit = totalProfit;
		this.customerNotified = customerNotified;
		this.giftIssued = giftIssued;
	}

	public BillAnalyticsData() {
		super();
	}

	private Integer customerCode;
	private String customerName;
	private String customerAddr;
	private Set<String> phoneNumber;
	private Integer billCount;
	private Double totalBillAmount;
	private Double totalProfit;
	private boolean customerNotified;
	private boolean giftIssued;
	private Integer productCount;
	private List<BillInfo> billInfoList;

	public Integer getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(Integer customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerAddr() {
		return customerAddr;
	}

	public void setCustomerAddr(String customerAddr) {
		this.customerAddr = customerAddr;
	}

	public Set<String> getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(Set<String> phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Integer getBillCount() {
		return billCount;
	}

	public void setBillCount(Integer billCount) {
		this.billCount = billCount;
	}

	public Double getTotalBillAmount() {
		return totalBillAmount;
	}

	public void setTotalBillAmount(Double totalBillAmount) {
		this.totalBillAmount = totalBillAmount;
	}

	public Double getTotalProfit() {
		return totalProfit;
	}

	public void setTotalProfit(Double totalProfit) {
		this.totalProfit = totalProfit;
	}

	public boolean isCustomerNotified() {
		return customerNotified;
	}

	public void setCustomerNotified(boolean customerNotified) {
		this.customerNotified = customerNotified;
	}

	public boolean isGiftIssued() {
		return giftIssued;
	}

	public void setGiftIssued(boolean giftIssued) {
		this.giftIssued = giftIssued;
	}

	public Integer getProductCount() {
		return productCount;
	}

	public void setProductCount(Integer productCount) {
		this.productCount = productCount;
	}

	public List<BillInfo> getBillInfoList() {
		return billInfoList;
	}

	public void setBillInfoList(List<BillInfo> billInfoList) {
		this.billInfoList = billInfoList;
	}

}
