package com.sakthi.groups.migration.gofrugal.dao;

import org.springframework.data.repository.CrudRepository;

import com.sakthi.groups.migration.gofrugal.model.MedItemEanCode;

public interface MedItemEanCodeRepository extends CrudRepository<MedItemEanCode, Integer> {

}
