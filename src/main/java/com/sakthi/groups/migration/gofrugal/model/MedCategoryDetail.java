package com.sakthi.groups.migration.gofrugal.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "med_category_dtl")
public class MedCategoryDetail {

	@Id
	@Column(name = "MCD_CAT_CODE")
	private Integer MCD_CAT_CODE;

	@Column(name = "MCD_CAT_ANAME")
	private String MCD_CAT_ANAME;

	@Column(name = "MCD_TYPE_CODE")
	private Integer MCD_TYPE_CODE;

	@Column(name = "MCD_CAT_NAME")
	private String MCD_CAT_NAME;

	@Column(name = "MCD_PARENT_CATCODE")
	private Integer MCD_PARENT_CATCODE;

	@Column(name = "MCD_DEF_TAX_PERC")
	private Double MCD_DEF_TAX_PERC;

	@Column(name = "MCD_DEF_INC_TAX")
	private String MCD_DEF_INC_TAX;

	@Column(name = "mcd_cat_grpcode")
	private String mcd_cat_grpcode;

	@Column(name = "TS")
	private String TS;

	@Column(name = "TSID")
	private Integer TSID;

	@Column(name = "MCD_Active")
	private String MCD_Active;

	@Column(name = "MCD_CAT_SHORT_NAME")
	private Integer MCD_CAT_SHORT_NAME;

	@Column(name = "RETAIL_OUTLET_ID")
	private Integer RETAIL_OUTLET_ID;

	@Column(name = "MCD_APPLIES_ONLINE")
	private Integer MCD_APPLIES_ONLINE;

	@Column(name = "MCD_CATEGORY_ORDER")
	private Integer MCD_CATEGORY_ORDER;

	@Column(name = "mcd_text_align")
	private String mcd_text_align;

	@Column(name = "mcd_image_align")
	private String mcd_image_align;

	@Column(name = "mcd_show_caption")
	private String mcd_show_caption;

	@Column(name = "mcd_coupon_allowed")
	private Integer mcd_coupon_allowed;

	@Column(name = "mcd_emp_id")
	private String mcd_emp_id;

	@Column(name = "mcd_desc")
	private String mcd_desc;

	public Integer getMCD_CAT_CODE() {
		return MCD_CAT_CODE;
	}

	public void setMCD_CAT_CODE(Integer mCD_CAT_CODE) {
		MCD_CAT_CODE = mCD_CAT_CODE;
	}

	public String getMCD_CAT_ANAME() {
		return MCD_CAT_ANAME;
	}

	public void setMCD_CAT_ANAME(String mCD_CAT_ANAME) {
		MCD_CAT_ANAME = mCD_CAT_ANAME;
	}

	public Integer getMCD_TYPE_CODE() {
		return MCD_TYPE_CODE;
	}

	public void setMCD_TYPE_CODE(Integer mCD_TYPE_CODE) {
		MCD_TYPE_CODE = mCD_TYPE_CODE;
	}

	public String getMCD_CAT_NAME() {
		return MCD_CAT_NAME;
	}

	public void setMCD_CAT_NAME(String mCD_CAT_NAME) {
		MCD_CAT_NAME = mCD_CAT_NAME;
	}

	public Integer getMCD_PARENT_CATCODE() {
		return MCD_PARENT_CATCODE;
	}

	public void setMCD_PARENT_CATCODE(Integer mCD_PARENT_CATCODE) {
		MCD_PARENT_CATCODE = mCD_PARENT_CATCODE;
	}

	public Double getMCD_DEF_TAX_PERC() {
		return MCD_DEF_TAX_PERC;
	}

	public void setMCD_DEF_TAX_PERC(Double mCD_DEF_TAX_PERC) {
		MCD_DEF_TAX_PERC = mCD_DEF_TAX_PERC;
	}

	public String getMCD_DEF_INC_TAX() {
		return MCD_DEF_INC_TAX;
	}

	public void setMCD_DEF_INC_TAX(String mCD_DEF_INC_TAX) {
		MCD_DEF_INC_TAX = mCD_DEF_INC_TAX;
	}

	public String getMcd_cat_grpcode() {
		return mcd_cat_grpcode;
	}

	public void setMcd_cat_grpcode(String mcd_cat_grpcode) {
		this.mcd_cat_grpcode = mcd_cat_grpcode;
	}

	public String getTS() {
		return TS;
	}

	public void setTS(String tS) {
		TS = tS;
	}

	public Integer getTSID() {
		return TSID;
	}

	public void setTSID(Integer tSID) {
		TSID = tSID;
	}

	public String getMCD_Active() {
		return MCD_Active;
	}

	public void setMCD_Active(String mCD_Active) {
		MCD_Active = mCD_Active;
	}

	public Integer getMCD_CAT_SHORT_NAME() {
		return MCD_CAT_SHORT_NAME;
	}

	public void setMCD_CAT_SHORT_NAME(Integer mCD_CAT_SHORT_NAME) {
		MCD_CAT_SHORT_NAME = mCD_CAT_SHORT_NAME;
	}

	public Integer getRETAIL_OUTLET_ID() {
		return RETAIL_OUTLET_ID;
	}

	public void setRETAIL_OUTLET_ID(Integer rETAIL_OUTLET_ID) {
		RETAIL_OUTLET_ID = rETAIL_OUTLET_ID;
	}

	public Integer getMCD_APPLIES_ONLINE() {
		return MCD_APPLIES_ONLINE;
	}

	public void setMCD_APPLIES_ONLINE(Integer mCD_APPLIES_ONLINE) {
		MCD_APPLIES_ONLINE = mCD_APPLIES_ONLINE;
	}

	public Integer getMCD_CATEGORY_ORDER() {
		return MCD_CATEGORY_ORDER;
	}

	public void setMCD_CATEGORY_ORDER(Integer mCD_CATEGORY_ORDER) {
		MCD_CATEGORY_ORDER = mCD_CATEGORY_ORDER;
	}

	public String getMcd_text_align() {
		return mcd_text_align;
	}

	public void setMcd_text_align(String mcd_text_align) {
		this.mcd_text_align = mcd_text_align;
	}

	public String getMcd_image_align() {
		return mcd_image_align;
	}

	public void setMcd_image_align(String mcd_image_align) {
		this.mcd_image_align = mcd_image_align;
	}

	public String getMcd_show_caption() {
		return mcd_show_caption;
	}

	public void setMcd_show_caption(String mcd_show_caption) {
		this.mcd_show_caption = mcd_show_caption;
	}

	public Integer getMcd_coupon_allowed() {
		return mcd_coupon_allowed;
	}

	public void setMcd_coupon_allowed(Integer mcd_coupon_allowed) {
		this.mcd_coupon_allowed = mcd_coupon_allowed;
	}

	public String getMcd_emp_id() {
		return mcd_emp_id;
	}

	public void setMcd_emp_id(String mcd_emp_id) {
		this.mcd_emp_id = mcd_emp_id;
	}

	public String getMcd_desc() {
		return mcd_desc;
	}

	public void setMcd_desc(String mcd_desc) {
		this.mcd_desc = mcd_desc;
	}

}
