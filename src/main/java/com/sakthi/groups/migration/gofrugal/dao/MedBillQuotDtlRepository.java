package com.sakthi.groups.migration.gofrugal.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.sakthi.groups.migration.gofrugal.model.MedBillQuotDetail;

public interface MedBillQuotDtlRepository extends CrudRepository<MedBillQuotDetail, Integer> {

	List<MedBillQuotDetail> findByMBDBillNo(Integer billNo);

}
