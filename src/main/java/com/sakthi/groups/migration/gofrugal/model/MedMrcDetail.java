package com.sakthi.groups.migration.gofrugal.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@IdClass(MedMrcDtlId.class)
@Table(name = "MED_MRC_DTL")
public class MedMrcDetail {

	@Id
	@Column(name = "MMD_MRC_NO")
	private Integer mmdMrcNo;

	@Id
	@Column(name = "MMD_MRC_SL_NO")
	private Integer MMD_MRC_SL_NO;

	@Column(name = "MMD_PO_SL_NO")
	private Integer MMD_PO_SL_NO;

	@Column(name = "MMD_ITEM_CODE")
	private Integer MMD_ITEM_CODE;

	@Column(name = "MMD_ORD_UNIT")
	private Integer MMD_ORD_UNIT;

	@Column(name = "MMD_ORD_QTY")
	private Integer MMD_ORD_QTY;

	@Column(name = "MMD_RECD_UNIT")
	private Integer MMD_RECD_UNIT;

	@Column(name = "MMD_RECD_QTY")
	private Double MMD_RECD_QTY;

	@Column(name = "MMD_FREE_QTY_TAG")
	private String MMD_FREE_QTY_TAG;

	@Column(name = "MMD_FREE_QTY")
	private Double MMD_FREE_QTY;

	@Column(name = "MMD_PUR_RATE")
	private Double MMD_PUR_RATE;

	@Column(name = "MMD_PUR_TAX_PER")
	private Double MMD_PUR_TAX_PER;

	@Column(name = "MMD_PUR_TAX_AMT")
	private Double MMD_PUR_TAX_AMT;

	@Column(name = "MMD_PUR_SC_AMT")
	private Double MMD_PUR_SC_AMT;

	@Column(name = "MMD_PUR_PRICE")
	private Double MMD_PUR_PRICE;

	@Column(name = "MMD_PUR_AMOUNT")
	private Double MMD_PUR_AMOUNT;

	@Column(name = "MMD_MRP")
	private Double MMD_MRP;

	@Column(name = "MMD_SALE_TAX_PER")
	private Double MMD_SALE_TAX_PER;

	@Column(name = "MMD_SALE_TAX_AMT")
	private Double MMD_SALE_TAX_AMT;

	@Column(name = "MMD_SALE_SC_AMT")
	private Double MMD_SALE_SC_AMT;

	@Column(name = "MMD_SALE_RATE")
	private Double MMD_SALE_RATE;

	@Column(name = "MMD_DISC_PER")
	private Double MMD_DISC_PER;

	@Column(name = "MMD_DISC_AMT")
	private Double MMD_DISC_AMT;

	@Column(name = "MMD_BATCH_NO")
	private String MMD_BATCH_NO;

	@Column(name = "MMD_BATCH_DT")
	private Timestamp MMD_BATCH_DT;

	@Column(name = "MMD_MFG_DT")
	private Timestamp MMD_MFG_DT;

	@Column(name = "MMD_EXPIRY_DT")
	private Timestamp MMD_EXPIRY_DT;

	@Column(name = "MMD_MRC_PREFIX")
	private String MMD_MRC_PREFIX;

	@Column(name = "MMD_TAG")
	private String MMD_TAG;

	@Column(name = "MMD_PPUR_RATE")
	private Double MMD_PPUR_RATE;

	@Column(name = "MMD_PMRP")
	private Double MMD_PMRP;

	@Column(name = "MMD_OFFER_PER_QTY")
	private Double MMD_OFFER_PER_QTY;

	@Column(name = "MMD_CASH_DISC_AMT")
	private Double MMD_CASH_DISC_AMT;

	@Column(name = "MMD_FQTY_TAX_AMT")
	private Double MMD_FQTY_TAX_AMT;

	@Column(name = "MMD_CONV_TYPE")
	private String MMD_CONV_TYPE;

	@Column(name = "MMD_CONV_FACTOR")
	private Double MMD_CONV_FACTOR;

	@Column(name = "mmd_sitem_code")
	private Double mmd_sitem_code;

	@Column(name = "mmd_sitem_qty")
	private Double mmd_sitem_qty;

	@Column(name = "mmd_compid")
	private Integer mmd_compid;

	@Column(name = "mmd_diviid")
	private Integer mmd_diviid;

	@Column(name = "mmd_locaid")
	private Integer mmd_locaid;

	@Column(name = "MMD_FREIGHT")
	private Double MMD_FREIGHT;

	@Column(name = "mmd_max_rate")
	private Double mmd_max_rate;

	@Column(name = "TS")
	private Timestamp TS;

	@Column(name = "TSID")
	private Integer TSID;

	@Column(name = "mmd_Rn_no")
	private Integer mmd_Rn_no;

	@Column(name = "mmd_Rn_Qty")
	private Double mmd_Rn_Qty;

	@Column(name = "mmd_row_id")
	private Integer mmd_row_id;

	@Column(name = "mmd_cess")
	private Double mmd_cess;

	@Column(name = "mmd_obser")
	private Double mmd_obser;

	@Column(name = "mmd_pkd_dt")
	private Timestamp mmd_pkd_dt;

	@Column(name = "mmd_tax_SC_PUR")
	private Double mmd_tax_SC_PUR;

	@Column(name = "mmd_bags")
	private Double mmd_bags;

	@Column(name = "mmd_po_no")
	private Integer mmd_po_no;

	@Column(name = "mmd_formula_id")
	private Integer mmd_formula_id;

	@Column(name = "RETAIL_OUTLET_ID")
	private Integer RETAIL_OUTLET_ID;

	@Column(name = "mmd_mat_unique_ref")
	private Integer mmd_mat_unique_ref;

	@Column(name = "mmd_packChrg_amt")
	private Double mmd_packChrg_amt;

	@Column(name = "mmd_cess_per")
	private Double mmd_cess_per;

	@Column(name = "mmd_obser_per")
	private Double mmd_obser_per;

	@Column(name = "mmd_ed_cess_per")
	private Double mmd_ed_cess_per;

	@Column(name = "mmd_ed_cess_amt")
	private Double mmd_ed_cess_amt;

	@Column(name = "mmd_adnl_ed_cess_per")
	private Double mmd_adnl_ed_cess_per;

	@Column(name = "mmd_adnl_ed_cess_amt")
	private Double mmd_adnl_ed_cess_amt;

	@Column(name = "mmd_ex_duty_perc")
	private Double mmd_ex_duty_perc;

	@Column(name = "mmd_ex_duty_amt")
	private Double mmd_ex_duty_amt;

	@Column(name = "mmd_shapecharge_code")
	private Integer mmd_shapecharge_code;

	@Column(name = "mmd_shapecharge_perc")
	private Double mmd_shapecharge_perc;

	@Column(name = "mmd_shapecharge_amt")
	private Double mmd_shapecharge_amt;

	@Column(name = "mmd_tcs_perc")
	private Double mmd_tcs_perc;

	@Column(name = "mmd_tcs_amt")
	private Double mmd_tcs_amt;

	@Column(name = "mmd_billed_qty")
	private Double mmd_billed_qty;

	@Column(name = "mmd_notrecd_qty")
	private Double mmd_notrecd_qty;

	@Column(name = "mmd_margin_perc")
	private Double mmd_margin_perc;

	@Column(name = "mmd_profit_perc")
	private Double mmd_profit_perc;

	@Column(name = "mmd_prev_margin_perc")
	private Double mmd_prev_margin_perc;

	@Column(name = "mmd_prev_profit_perc")
	private Double mmd_prev_profit_perc;

	@Column(name = "mmd_prev_pur_price")
	private Double mmd_prev_pur_price;

	@Column(name = "mmd_ADDLN_DISC1_AMT")
	private Double mmd_ADDLN_DISC1_AMT;

	@Column(name = "mmd_ADDLN_DISC2_AMT")
	private Double mmd_ADDLN_DISC2_AMT;

	@Column(name = "mmd_ADDLN_DISC3_AMT")
	private Double mmd_ADDLN_DISC3_AMT;

	@Column(name = "mmd_OtherCharge")
	private Double mmd_OtherCharge;

	@Column(name = "mmd_eancode")
	private String mmd_eancode;

	@Column(name = "mmd_addln_disc1_perc")
	private Double mmd_addln_disc1_perc;

	@Column(name = "mmd_addln_disc2_perc")
	private Double mmd_addln_disc2_perc;

	@Column(name = "mmd_addln_disc3_perc")
	private Double mmd_addln_disc3_perc;

	@Column(name = "mmd_item_remarks")
	private String mmd_item_remarks;

	@Column(name = "mmd_sgst_perc")
	private Double mmd_sgst_perc;

	@Column(name = "mmd_sgst_amt")
	private Double mmd_sgst_amt;

	@Column(name = "mmd_cgst_perc")
	private Double mmd_cgst_perc;

	@Column(name = "mmd_cgst_amt")
	private Double mmd_cgst_amt;

	@Column(name = "mmd_igst_perc")
	private Double mmd_igst_perc;

	@Column(name = "mmd_igst_amt")
	private Double mmd_igst_amt;

	@Column(name = "mmd_gst_cess_perc")
	private Double mmd_gst_cess_perc;

	@Column(name = "mmd_gst_cess_amt")
	private Double mmd_gst_cess_amt;

	@Column(name = "mmd_gst_basedon")
	private Integer mmd_gst_basedon;

	@Column(name = "mmd_abatement_perc")
	private Integer mmd_abatement_perc;

	@Column(name = "mmd_gst_code")
	private Integer mmd_gst_code;

	@Column(name = "mmd_extra_cess_amt")
	private Double mmd_extra_cess_amt;

	@Column(name = "mmd_Extra_Charge_TaxAmt")
	private Double mmd_Extra_Charge_TaxAmt;

	@Column(name = "mmd_weight")
	private Double mmd_weight;

	@Column(name = "mmd_weight_Rateperkg")
	private Double mmd_weight_Rateperkg;

	@Column(name = "mmd_avg_pieceweight")
	private Double mmd_avg_pieceweight;

	@Column(name = "MMD_PUR_PRICE_ON_MARGIN")
	private Double MMD_PUR_PRICE_ON_MARGIN;

	@Column(name = "MMD_TAXAMT_ON_MARGIN")
	private Double MMD_TAXAMT_ON_MARGIN;

	@Column(name = "mmd_neg_stock_adjust")
	private Integer mmd_neg_stock_adjust;

	@Column(name = "mmd_travel_exp_amt")
	private Double mmd_travel_exp_amt;

	@Column(name = "mmd_load_unload_charge")
	private Double mmd_load_unload_charge;

	@Column(name = "mmd_cust_duty_amt")
	private Double mmd_cust_duty_amt;

	@Column(name = "mmd_Tot_sgst_amt")
	private Double mmd_Tot_sgst_amt;

	@Column(name = "mmd_Tot_cgst_amt")
	private Double mmd_Tot_cgst_amt;

	@Column(name = "mmd_Tot_igst_amt")
	private Double mmd_Tot_igst_amt;

	@Column(name = "mmd_Tot_gst_cess_amt")
	private Double mmd_Tot_gst_cess_amt;

	@Column(name = "mmd_Tot_pur_tax_amt")
	private Double mmd_Tot_pur_tax_amt;

	@Column(name = "mmd_tot_pur_sc_amt")
	private Double mmd_tot_pur_sc_amt;

	@Column(name = "mmd_goods_tcs_amt")
	private Double mmd_goods_tcs_amt;

	@Column(name = "mmd_conv_cost")
	private Double mmd_conv_cost;

	public Integer getMmdMrcNo() {
		return mmdMrcNo;
	}

	public void setMmdMrcNo(Integer mmdMrcNo) {
		this.mmdMrcNo = mmdMrcNo;
	}

	public Integer getMMD_MRC_SL_NO() {
		return MMD_MRC_SL_NO;
	}

	public void setMMD_MRC_SL_NO(Integer mMD_MRC_SL_NO) {
		MMD_MRC_SL_NO = mMD_MRC_SL_NO;
	}

	public Integer getMMD_PO_SL_NO() {
		return MMD_PO_SL_NO;
	}

	public void setMMD_PO_SL_NO(Integer mMD_PO_SL_NO) {
		MMD_PO_SL_NO = mMD_PO_SL_NO;
	}

	public Integer getMMD_ITEM_CODE() {
		return MMD_ITEM_CODE;
	}

	public void setMMD_ITEM_CODE(Integer mMD_ITEM_CODE) {
		MMD_ITEM_CODE = mMD_ITEM_CODE;
	}

	public Integer getMMD_ORD_UNIT() {
		return MMD_ORD_UNIT;
	}

	public void setMMD_ORD_UNIT(Integer mMD_ORD_UNIT) {
		MMD_ORD_UNIT = mMD_ORD_UNIT;
	}

	public Integer getMMD_ORD_QTY() {
		return MMD_ORD_QTY;
	}

	public void setMMD_ORD_QTY(Integer mMD_ORD_QTY) {
		MMD_ORD_QTY = mMD_ORD_QTY;
	}

	public Integer getMMD_RECD_UNIT() {
		return MMD_RECD_UNIT;
	}

	public void setMMD_RECD_UNIT(Integer mMD_RECD_UNIT) {
		MMD_RECD_UNIT = mMD_RECD_UNIT;
	}

	public Double getMMD_RECD_QTY() {
		return MMD_RECD_QTY;
	}

	public void setMMD_RECD_QTY(Double mMD_RECD_QTY) {
		MMD_RECD_QTY = mMD_RECD_QTY;
	}

	public String getMMD_FREE_QTY_TAG() {
		return MMD_FREE_QTY_TAG;
	}

	public void setMMD_FREE_QTY_TAG(String mMD_FREE_QTY_TAG) {
		MMD_FREE_QTY_TAG = mMD_FREE_QTY_TAG;
	}

	public Double getMMD_FREE_QTY() {
		return MMD_FREE_QTY;
	}

	public void setMMD_FREE_QTY(Double mMD_FREE_QTY) {
		MMD_FREE_QTY = mMD_FREE_QTY;
	}

	public Double getMMD_PUR_RATE() {
		return MMD_PUR_RATE;
	}

	public void setMMD_PUR_RATE(Double mMD_PUR_RATE) {
		MMD_PUR_RATE = mMD_PUR_RATE;
	}

	public Double getMMD_PUR_TAX_PER() {
		return MMD_PUR_TAX_PER;
	}

	public void setMMD_PUR_TAX_PER(Double mMD_PUR_TAX_PER) {
		MMD_PUR_TAX_PER = mMD_PUR_TAX_PER;
	}

	public Double getMMD_PUR_TAX_AMT() {
		return MMD_PUR_TAX_AMT;
	}

	public void setMMD_PUR_TAX_AMT(Double mMD_PUR_TAX_AMT) {
		MMD_PUR_TAX_AMT = mMD_PUR_TAX_AMT;
	}

	public Double getMMD_PUR_SC_AMT() {
		return MMD_PUR_SC_AMT;
	}

	public void setMMD_PUR_SC_AMT(Double mMD_PUR_SC_AMT) {
		MMD_PUR_SC_AMT = mMD_PUR_SC_AMT;
	}

	public Double getMMD_PUR_PRICE() {
		return MMD_PUR_PRICE;
	}

	public void setMMD_PUR_PRICE(Double mMD_PUR_PRICE) {
		MMD_PUR_PRICE = mMD_PUR_PRICE;
	}

	public Double getMMD_PUR_AMOUNT() {
		return MMD_PUR_AMOUNT;
	}

	public void setMMD_PUR_AMOUNT(Double mMD_PUR_AMOUNT) {
		MMD_PUR_AMOUNT = mMD_PUR_AMOUNT;
	}

	public Double getMMD_MRP() {
		return MMD_MRP;
	}

	public void setMMD_MRP(Double mMD_MRP) {
		MMD_MRP = mMD_MRP;
	}

	public Double getMMD_SALE_TAX_PER() {
		return MMD_SALE_TAX_PER;
	}

	public void setMMD_SALE_TAX_PER(Double mMD_SALE_TAX_PER) {
		MMD_SALE_TAX_PER = mMD_SALE_TAX_PER;
	}

	public Double getMMD_SALE_TAX_AMT() {
		return MMD_SALE_TAX_AMT;
	}

	public void setMMD_SALE_TAX_AMT(Double mMD_SALE_TAX_AMT) {
		MMD_SALE_TAX_AMT = mMD_SALE_TAX_AMT;
	}

	public Double getMMD_SALE_SC_AMT() {
		return MMD_SALE_SC_AMT;
	}

	public void setMMD_SALE_SC_AMT(Double mMD_SALE_SC_AMT) {
		MMD_SALE_SC_AMT = mMD_SALE_SC_AMT;
	}

	public Double getMMD_SALE_RATE() {
		return MMD_SALE_RATE;
	}

	public void setMMD_SALE_RATE(Double mMD_SALE_RATE) {
		MMD_SALE_RATE = mMD_SALE_RATE;
	}

	public Double getMMD_DISC_PER() {
		return MMD_DISC_PER;
	}

	public void setMMD_DISC_PER(Double mMD_DISC_PER) {
		MMD_DISC_PER = mMD_DISC_PER;
	}

	public Double getMMD_DISC_AMT() {
		return MMD_DISC_AMT;
	}

	public void setMMD_DISC_AMT(Double mMD_DISC_AMT) {
		MMD_DISC_AMT = mMD_DISC_AMT;
	}

	public String getMMD_BATCH_NO() {
		return MMD_BATCH_NO;
	}

	public void setMMD_BATCH_NO(String mMD_BATCH_NO) {
		MMD_BATCH_NO = mMD_BATCH_NO;
	}

	public Timestamp getMMD_BATCH_DT() {
		return MMD_BATCH_DT;
	}

	public void setMMD_BATCH_DT(Timestamp mMD_BATCH_DT) {
		MMD_BATCH_DT = mMD_BATCH_DT;
	}

	public Timestamp getMMD_MFG_DT() {
		return MMD_MFG_DT;
	}

	public void setMMD_MFG_DT(Timestamp mMD_MFG_DT) {
		MMD_MFG_DT = mMD_MFG_DT;
	}

	public Timestamp getMMD_EXPIRY_DT() {
		return MMD_EXPIRY_DT;
	}

	public void setMMD_EXPIRY_DT(Timestamp mMD_EXPIRY_DT) {
		MMD_EXPIRY_DT = mMD_EXPIRY_DT;
	}

	public String getMMD_MRC_PREFIX() {
		return MMD_MRC_PREFIX;
	}

	public void setMMD_MRC_PREFIX(String mMD_MRC_PREFIX) {
		MMD_MRC_PREFIX = mMD_MRC_PREFIX;
	}

	public String getMMD_TAG() {
		return MMD_TAG;
	}

	public void setMMD_TAG(String mMD_TAG) {
		MMD_TAG = mMD_TAG;
	}

	public Double getMMD_PPUR_RATE() {
		return MMD_PPUR_RATE;
	}

	public void setMMD_PPUR_RATE(Double mMD_PPUR_RATE) {
		MMD_PPUR_RATE = mMD_PPUR_RATE;
	}

	public Double getMMD_PMRP() {
		return MMD_PMRP;
	}

	public void setMMD_PMRP(Double mMD_PMRP) {
		MMD_PMRP = mMD_PMRP;
	}

	public Double getMMD_OFFER_PER_QTY() {
		return MMD_OFFER_PER_QTY;
	}

	public void setMMD_OFFER_PER_QTY(Double mMD_OFFER_PER_QTY) {
		MMD_OFFER_PER_QTY = mMD_OFFER_PER_QTY;
	}

	public Double getMMD_CASH_DISC_AMT() {
		return MMD_CASH_DISC_AMT;
	}

	public void setMMD_CASH_DISC_AMT(Double mMD_CASH_DISC_AMT) {
		MMD_CASH_DISC_AMT = mMD_CASH_DISC_AMT;
	}

	public Double getMMD_FQTY_TAX_AMT() {
		return MMD_FQTY_TAX_AMT;
	}

	public void setMMD_FQTY_TAX_AMT(Double mMD_FQTY_TAX_AMT) {
		MMD_FQTY_TAX_AMT = mMD_FQTY_TAX_AMT;
	}

	public String getMMD_CONV_TYPE() {
		return MMD_CONV_TYPE;
	}

	public void setMMD_CONV_TYPE(String mMD_CONV_TYPE) {
		MMD_CONV_TYPE = mMD_CONV_TYPE;
	}

	public Double getMMD_CONV_FACTOR() {
		return MMD_CONV_FACTOR;
	}

	public void setMMD_CONV_FACTOR(Double mMD_CONV_FACTOR) {
		MMD_CONV_FACTOR = mMD_CONV_FACTOR;
	}

	public Double getMmd_sitem_code() {
		return mmd_sitem_code;
	}

	public void setMmd_sitem_code(Double mmd_sitem_code) {
		this.mmd_sitem_code = mmd_sitem_code;
	}

	public Double getMmd_sitem_qty() {
		return mmd_sitem_qty;
	}

	public void setMmd_sitem_qty(Double mmd_sitem_qty) {
		this.mmd_sitem_qty = mmd_sitem_qty;
	}

	public Integer getMmd_compid() {
		return mmd_compid;
	}

	public void setMmd_compid(Integer mmd_compid) {
		this.mmd_compid = mmd_compid;
	}

	public Integer getMmd_diviid() {
		return mmd_diviid;
	}

	public void setMmd_diviid(Integer mmd_diviid) {
		this.mmd_diviid = mmd_diviid;
	}

	public Integer getMmd_locaid() {
		return mmd_locaid;
	}

	public void setMmd_locaid(Integer mmd_locaid) {
		this.mmd_locaid = mmd_locaid;
	}

	public Double getMMD_FREIGHT() {
		return MMD_FREIGHT;
	}

	public void setMMD_FREIGHT(Double mMD_FREIGHT) {
		MMD_FREIGHT = mMD_FREIGHT;
	}

	public Double getMmd_max_rate() {
		return mmd_max_rate;
	}

	public void setMmd_max_rate(Double mmd_max_rate) {
		this.mmd_max_rate = mmd_max_rate;
	}

	public Timestamp getTS() {
		return TS;
	}

	public void setTS(Timestamp tS) {
		TS = tS;
	}

	public Integer getTSID() {
		return TSID;
	}

	public void setTSID(Integer tSID) {
		TSID = tSID;
	}

	public Integer getMmd_Rn_no() {
		return mmd_Rn_no;
	}

	public void setMmd_Rn_no(Integer mmd_Rn_no) {
		this.mmd_Rn_no = mmd_Rn_no;
	}

	public Double getMmd_Rn_Qty() {
		return mmd_Rn_Qty;
	}

	public void setMmd_Rn_Qty(Double mmd_Rn_Qty) {
		this.mmd_Rn_Qty = mmd_Rn_Qty;
	}

	public Integer getMmd_row_id() {
		return mmd_row_id;
	}

	public void setMmd_row_id(Integer mmd_row_id) {
		this.mmd_row_id = mmd_row_id;
	}

	public Double getMmd_cess() {
		return mmd_cess;
	}

	public void setMmd_cess(Double mmd_cess) {
		this.mmd_cess = mmd_cess;
	}

	public Double getMmd_obser() {
		return mmd_obser;
	}

	public void setMmd_obser(Double mmd_obser) {
		this.mmd_obser = mmd_obser;
	}

	public Timestamp getMmd_pkd_dt() {
		return mmd_pkd_dt;
	}

	public void setMmd_pkd_dt(Timestamp mmd_pkd_dt) {
		this.mmd_pkd_dt = mmd_pkd_dt;
	}

	public Double getMmd_tax_SC_PUR() {
		return mmd_tax_SC_PUR;
	}

	public void setMmd_tax_SC_PUR(Double mmd_tax_SC_PUR) {
		this.mmd_tax_SC_PUR = mmd_tax_SC_PUR;
	}

	public Double getMmd_bags() {
		return mmd_bags;
	}

	public void setMmd_bags(Double mmd_bags) {
		this.mmd_bags = mmd_bags;
	}

	public Integer getMmd_po_no() {
		return mmd_po_no;
	}

	public void setMmd_po_no(Integer mmd_po_no) {
		this.mmd_po_no = mmd_po_no;
	}

	public Integer getMmd_formula_id() {
		return mmd_formula_id;
	}

	public void setMmd_formula_id(Integer mmd_formula_id) {
		this.mmd_formula_id = mmd_formula_id;
	}

	public Integer getRETAIL_OUTLET_ID() {
		return RETAIL_OUTLET_ID;
	}

	public void setRETAIL_OUTLET_ID(Integer rETAIL_OUTLET_ID) {
		RETAIL_OUTLET_ID = rETAIL_OUTLET_ID;
	}

	public Integer getMmd_mat_unique_ref() {
		return mmd_mat_unique_ref;
	}

	public void setMmd_mat_unique_ref(Integer mmd_mat_unique_ref) {
		this.mmd_mat_unique_ref = mmd_mat_unique_ref;
	}

	public Double getMmd_packChrg_amt() {
		return mmd_packChrg_amt;
	}

	public void setMmd_packChrg_amt(Double mmd_packChrg_amt) {
		this.mmd_packChrg_amt = mmd_packChrg_amt;
	}

	public Double getMmd_cess_per() {
		return mmd_cess_per;
	}

	public void setMmd_cess_per(Double mmd_cess_per) {
		this.mmd_cess_per = mmd_cess_per;
	}

	public Double getMmd_obser_per() {
		return mmd_obser_per;
	}

	public void setMmd_obser_per(Double mmd_obser_per) {
		this.mmd_obser_per = mmd_obser_per;
	}

	public Double getMmd_ed_cess_per() {
		return mmd_ed_cess_per;
	}

	public void setMmd_ed_cess_per(Double mmd_ed_cess_per) {
		this.mmd_ed_cess_per = mmd_ed_cess_per;
	}

	public Double getMmd_ed_cess_amt() {
		return mmd_ed_cess_amt;
	}

	public void setMmd_ed_cess_amt(Double mmd_ed_cess_amt) {
		this.mmd_ed_cess_amt = mmd_ed_cess_amt;
	}

	public Double getMmd_adnl_ed_cess_per() {
		return mmd_adnl_ed_cess_per;
	}

	public void setMmd_adnl_ed_cess_per(Double mmd_adnl_ed_cess_per) {
		this.mmd_adnl_ed_cess_per = mmd_adnl_ed_cess_per;
	}

	public Double getMmd_adnl_ed_cess_amt() {
		return mmd_adnl_ed_cess_amt;
	}

	public void setMmd_adnl_ed_cess_amt(Double mmd_adnl_ed_cess_amt) {
		this.mmd_adnl_ed_cess_amt = mmd_adnl_ed_cess_amt;
	}

	public Double getMmd_ex_duty_perc() {
		return mmd_ex_duty_perc;
	}

	public void setMmd_ex_duty_perc(Double mmd_ex_duty_perc) {
		this.mmd_ex_duty_perc = mmd_ex_duty_perc;
	}

	public Double getMmd_ex_duty_amt() {
		return mmd_ex_duty_amt;
	}

	public void setMmd_ex_duty_amt(Double mmd_ex_duty_amt) {
		this.mmd_ex_duty_amt = mmd_ex_duty_amt;
	}

	public Integer getMmd_shapecharge_code() {
		return mmd_shapecharge_code;
	}

	public void setMmd_shapecharge_code(Integer mmd_shapecharge_code) {
		this.mmd_shapecharge_code = mmd_shapecharge_code;
	}

	public Double getMmd_shapecharge_perc() {
		return mmd_shapecharge_perc;
	}

	public void setMmd_shapecharge_perc(Double mmd_shapecharge_perc) {
		this.mmd_shapecharge_perc = mmd_shapecharge_perc;
	}

	public Double getMmd_shapecharge_amt() {
		return mmd_shapecharge_amt;
	}

	public void setMmd_shapecharge_amt(Double mmd_shapecharge_amt) {
		this.mmd_shapecharge_amt = mmd_shapecharge_amt;
	}

	public Double getMmd_tcs_perc() {
		return mmd_tcs_perc;
	}

	public void setMmd_tcs_perc(Double mmd_tcs_perc) {
		this.mmd_tcs_perc = mmd_tcs_perc;
	}

	public Double getMmd_tcs_amt() {
		return mmd_tcs_amt;
	}

	public void setMmd_tcs_amt(Double mmd_tcs_amt) {
		this.mmd_tcs_amt = mmd_tcs_amt;
	}

	public Double getMmd_billed_qty() {
		return mmd_billed_qty;
	}

	public void setMmd_billed_qty(Double mmd_billed_qty) {
		this.mmd_billed_qty = mmd_billed_qty;
	}

	public Double getMmd_notrecd_qty() {
		return mmd_notrecd_qty;
	}

	public void setMmd_notrecd_qty(Double mmd_notrecd_qty) {
		this.mmd_notrecd_qty = mmd_notrecd_qty;
	}

	public Double getMmd_margin_perc() {
		return mmd_margin_perc;
	}

	public void setMmd_margin_perc(Double mmd_margin_perc) {
		this.mmd_margin_perc = mmd_margin_perc;
	}

	public Double getMmd_profit_perc() {
		return mmd_profit_perc;
	}

	public void setMmd_profit_perc(Double mmd_profit_perc) {
		this.mmd_profit_perc = mmd_profit_perc;
	}

	public Double getMmd_prev_margin_perc() {
		return mmd_prev_margin_perc;
	}

	public void setMmd_prev_margin_perc(Double mmd_prev_margin_perc) {
		this.mmd_prev_margin_perc = mmd_prev_margin_perc;
	}

	public Double getMmd_prev_profit_perc() {
		return mmd_prev_profit_perc;
	}

	public void setMmd_prev_profit_perc(Double mmd_prev_profit_perc) {
		this.mmd_prev_profit_perc = mmd_prev_profit_perc;
	}

	public Double getMmd_prev_pur_price() {
		return mmd_prev_pur_price;
	}

	public void setMmd_prev_pur_price(Double mmd_prev_pur_price) {
		this.mmd_prev_pur_price = mmd_prev_pur_price;
	}

	public Double getMmd_ADDLN_DISC1_AMT() {
		return mmd_ADDLN_DISC1_AMT;
	}

	public void setMmd_ADDLN_DISC1_AMT(Double mmd_ADDLN_DISC1_AMT) {
		this.mmd_ADDLN_DISC1_AMT = mmd_ADDLN_DISC1_AMT;
	}

	public Double getMmd_ADDLN_DISC2_AMT() {
		return mmd_ADDLN_DISC2_AMT;
	}

	public void setMmd_ADDLN_DISC2_AMT(Double mmd_ADDLN_DISC2_AMT) {
		this.mmd_ADDLN_DISC2_AMT = mmd_ADDLN_DISC2_AMT;
	}

	public Double getMmd_ADDLN_DISC3_AMT() {
		return mmd_ADDLN_DISC3_AMT;
	}

	public void setMmd_ADDLN_DISC3_AMT(Double mmd_ADDLN_DISC3_AMT) {
		this.mmd_ADDLN_DISC3_AMT = mmd_ADDLN_DISC3_AMT;
	}

	public Double getMmd_OtherCharge() {
		return mmd_OtherCharge;
	}

	public void setMmd_OtherCharge(Double mmd_OtherCharge) {
		this.mmd_OtherCharge = mmd_OtherCharge;
	}

	public String getMmd_eancode() {
		return mmd_eancode;
	}

	public void setMmd_eancode(String mmd_eancode) {
		this.mmd_eancode = mmd_eancode;
	}

	public Double getMmd_addln_disc1_perc() {
		return mmd_addln_disc1_perc;
	}

	public void setMmd_addln_disc1_perc(Double mmd_addln_disc1_perc) {
		this.mmd_addln_disc1_perc = mmd_addln_disc1_perc;
	}

	public Double getMmd_addln_disc2_perc() {
		return mmd_addln_disc2_perc;
	}

	public void setMmd_addln_disc2_perc(Double mmd_addln_disc2_perc) {
		this.mmd_addln_disc2_perc = mmd_addln_disc2_perc;
	}

	public Double getMmd_addln_disc3_perc() {
		return mmd_addln_disc3_perc;
	}

	public void setMmd_addln_disc3_perc(Double mmd_addln_disc3_perc) {
		this.mmd_addln_disc3_perc = mmd_addln_disc3_perc;
	}

	public String getMmd_item_remarks() {
		return mmd_item_remarks;
	}

	public void setMmd_item_remarks(String mmd_item_remarks) {
		this.mmd_item_remarks = mmd_item_remarks;
	}

	public Double getMmd_sgst_perc() {
		return mmd_sgst_perc;
	}

	public void setMmd_sgst_perc(Double mmd_sgst_perc) {
		this.mmd_sgst_perc = mmd_sgst_perc;
	}

	public Double getMmd_sgst_amt() {
		return mmd_sgst_amt;
	}

	public void setMmd_sgst_amt(Double mmd_sgst_amt) {
		this.mmd_sgst_amt = mmd_sgst_amt;
	}

	public Double getMmd_cgst_perc() {
		return mmd_cgst_perc;
	}

	public void setMmd_cgst_perc(Double mmd_cgst_perc) {
		this.mmd_cgst_perc = mmd_cgst_perc;
	}

	public Double getMmd_cgst_amt() {
		return mmd_cgst_amt;
	}

	public void setMmd_cgst_amt(Double mmd_cgst_amt) {
		this.mmd_cgst_amt = mmd_cgst_amt;
	}

	public Double getMmd_igst_perc() {
		return mmd_igst_perc;
	}

	public void setMmd_igst_perc(Double mmd_igst_perc) {
		this.mmd_igst_perc = mmd_igst_perc;
	}

	public Double getMmd_igst_amt() {
		return mmd_igst_amt;
	}

	public void setMmd_igst_amt(Double mmd_igst_amt) {
		this.mmd_igst_amt = mmd_igst_amt;
	}

	public Double getMmd_gst_cess_perc() {
		return mmd_gst_cess_perc;
	}

	public void setMmd_gst_cess_perc(Double mmd_gst_cess_perc) {
		this.mmd_gst_cess_perc = mmd_gst_cess_perc;
	}

	public Double getMmd_gst_cess_amt() {
		return mmd_gst_cess_amt;
	}

	public void setMmd_gst_cess_amt(Double mmd_gst_cess_amt) {
		this.mmd_gst_cess_amt = mmd_gst_cess_amt;
	}

	public Integer getMmd_gst_basedon() {
		return mmd_gst_basedon;
	}

	public void setMmd_gst_basedon(Integer mmd_gst_basedon) {
		this.mmd_gst_basedon = mmd_gst_basedon;
	}

	public Integer getMmd_abatement_perc() {
		return mmd_abatement_perc;
	}

	public void setMmd_abatement_perc(Integer mmd_abatement_perc) {
		this.mmd_abatement_perc = mmd_abatement_perc;
	}

	public Integer getMmd_gst_code() {
		return mmd_gst_code;
	}

	public void setMmd_gst_code(Integer mmd_gst_code) {
		this.mmd_gst_code = mmd_gst_code;
	}

	public Double getMmd_extra_cess_amt() {
		return mmd_extra_cess_amt;
	}

	public void setMmd_extra_cess_amt(Double mmd_extra_cess_amt) {
		this.mmd_extra_cess_amt = mmd_extra_cess_amt;
	}

	public Double getMmd_Extra_Charge_TaxAmt() {
		return mmd_Extra_Charge_TaxAmt;
	}

	public void setMmd_Extra_Charge_TaxAmt(Double mmd_Extra_Charge_TaxAmt) {
		this.mmd_Extra_Charge_TaxAmt = mmd_Extra_Charge_TaxAmt;
	}

	public Double getMmd_weight() {
		return mmd_weight;
	}

	public void setMmd_weight(Double mmd_weight) {
		this.mmd_weight = mmd_weight;
	}

	public Double getMmd_weight_Rateperkg() {
		return mmd_weight_Rateperkg;
	}

	public void setMmd_weight_Rateperkg(Double mmd_weight_Rateperkg) {
		this.mmd_weight_Rateperkg = mmd_weight_Rateperkg;
	}

	public Double getMmd_avg_pieceweight() {
		return mmd_avg_pieceweight;
	}

	public void setMmd_avg_pieceweight(Double mmd_avg_pieceweight) {
		this.mmd_avg_pieceweight = mmd_avg_pieceweight;
	}

	public Double getMMD_PUR_PRICE_ON_MARGIN() {
		return MMD_PUR_PRICE_ON_MARGIN;
	}

	public void setMMD_PUR_PRICE_ON_MARGIN(Double mMD_PUR_PRICE_ON_MARGIN) {
		MMD_PUR_PRICE_ON_MARGIN = mMD_PUR_PRICE_ON_MARGIN;
	}

	public Double getMMD_TAXAMT_ON_MARGIN() {
		return MMD_TAXAMT_ON_MARGIN;
	}

	public void setMMD_TAXAMT_ON_MARGIN(Double mMD_TAXAMT_ON_MARGIN) {
		MMD_TAXAMT_ON_MARGIN = mMD_TAXAMT_ON_MARGIN;
	}

	public Integer getMmd_neg_stock_adjust() {
		return mmd_neg_stock_adjust;
	}

	public void setMmd_neg_stock_adjust(Integer mmd_neg_stock_adjust) {
		this.mmd_neg_stock_adjust = mmd_neg_stock_adjust;
	}

	public Double getMmd_travel_exp_amt() {
		return mmd_travel_exp_amt;
	}

	public void setMmd_travel_exp_amt(Double mmd_travel_exp_amt) {
		this.mmd_travel_exp_amt = mmd_travel_exp_amt;
	}

	public Double getMmd_load_unload_charge() {
		return mmd_load_unload_charge;
	}

	public void setMmd_load_unload_charge(Double mmd_load_unload_charge) {
		this.mmd_load_unload_charge = mmd_load_unload_charge;
	}

	public Double getMmd_cust_duty_amt() {
		return mmd_cust_duty_amt;
	}

	public void setMmd_cust_duty_amt(Double mmd_cust_duty_amt) {
		this.mmd_cust_duty_amt = mmd_cust_duty_amt;
	}

	public Double getMmd_Tot_sgst_amt() {
		return mmd_Tot_sgst_amt;
	}

	public void setMmd_Tot_sgst_amt(Double mmd_Tot_sgst_amt) {
		this.mmd_Tot_sgst_amt = mmd_Tot_sgst_amt;
	}

	public Double getMmd_Tot_cgst_amt() {
		return mmd_Tot_cgst_amt;
	}

	public void setMmd_Tot_cgst_amt(Double mmd_Tot_cgst_amt) {
		this.mmd_Tot_cgst_amt = mmd_Tot_cgst_amt;
	}

	public Double getMmd_Tot_igst_amt() {
		return mmd_Tot_igst_amt;
	}

	public void setMmd_Tot_igst_amt(Double mmd_Tot_igst_amt) {
		this.mmd_Tot_igst_amt = mmd_Tot_igst_amt;
	}

	public Double getMmd_Tot_gst_cess_amt() {
		return mmd_Tot_gst_cess_amt;
	}

	public void setMmd_Tot_gst_cess_amt(Double mmd_Tot_gst_cess_amt) {
		this.mmd_Tot_gst_cess_amt = mmd_Tot_gst_cess_amt;
	}

	public Double getMmd_Tot_pur_tax_amt() {
		return mmd_Tot_pur_tax_amt;
	}

	public void setMmd_Tot_pur_tax_amt(Double mmd_Tot_pur_tax_amt) {
		this.mmd_Tot_pur_tax_amt = mmd_Tot_pur_tax_amt;
	}

	public Double getMmd_tot_pur_sc_amt() {
		return mmd_tot_pur_sc_amt;
	}

	public void setMmd_tot_pur_sc_amt(Double mmd_tot_pur_sc_amt) {
		this.mmd_tot_pur_sc_amt = mmd_tot_pur_sc_amt;
	}

	public Double getMmd_goods_tcs_amt() {
		return mmd_goods_tcs_amt;
	}

	public void setMmd_goods_tcs_amt(Double mmd_goods_tcs_amt) {
		this.mmd_goods_tcs_amt = mmd_goods_tcs_amt;
	}

	public Double getMmd_conv_cost() {
		return mmd_conv_cost;
	}

	public void setMmd_conv_cost(Double mmd_conv_cost) {
		this.mmd_conv_cost = mmd_conv_cost;
	}

}
