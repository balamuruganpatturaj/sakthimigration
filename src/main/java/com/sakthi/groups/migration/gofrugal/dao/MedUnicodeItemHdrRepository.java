package com.sakthi.groups.migration.gofrugal.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.sakthi.groups.migration.gofrugal.model.MedUnicodeItemHdr;

public interface MedUnicodeItemHdrRepository extends CrudRepository<MedUnicodeItemHdr, Integer> {

	@Query("SELECT u.MUIH_UNICODE_ITEM_NAME FROM MedUnicodeItemHdr u WHERE u.MUIH_ITEM_CODE = ?1")
	public String findByMUIH_ITEM_CODE(Integer MUIH_ITEM_CODE);
}
