package com.sakthi.groups.migration.gofrugal.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "VU_LOYALTY_POINTS")
public class MedLoyaltyPointsVW {

	@Id
	@Column(name = "customerCode")
	private Integer customerCode;

	@Column(name = "loyaltyCode")
	private Integer loyaltyCode;

	@Column(name = "loyaltyPoints")
	private Double loyaltyPoints;

	@Column(name = "loyaltyAmount")
	private Double loyaltyAmount;

	@Column(name = "redeemableFrom")
	private Timestamp redeemableFrom;

	public Integer getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(Integer customerCode) {
		this.customerCode = customerCode;
	}

	public Integer getLoyaltyCode() {
		return loyaltyCode;
	}

	public void setLoyaltyCode(Integer loyaltyCode) {
		this.loyaltyCode = loyaltyCode;
	}

	public Double getLoyaltyPoints() {
		return loyaltyPoints;
	}

	public void setLoyaltyPoints(Double loyaltyPoints) {
		this.loyaltyPoints = loyaltyPoints;
	}

	public Double getLoyaltyAmount() {
		return loyaltyAmount;
	}

	public void setLoyaltyAmount(Double loyaltyAmount) {
		this.loyaltyAmount = loyaltyAmount;
	}

	public Timestamp getRedeemableFrom() {
		return redeemableFrom;
	}

	public void setRedeemableFrom(Timestamp redeemableFrom) {
		this.redeemableFrom = redeemableFrom;
	}

}
