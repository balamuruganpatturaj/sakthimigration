package com.sakthi.groups.migration.gofrugal.dao;

import org.springframework.data.repository.CrudRepository;

import com.sakthi.groups.migration.gofrugal.model.MedPriceLevel;

public interface MedPriceLevelRepository extends CrudRepository<MedPriceLevel, Integer> {

}
