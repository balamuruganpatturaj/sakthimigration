package com.sakthi.groups.migration.gofrugal.dao;

import org.springframework.data.repository.CrudRepository;

import com.sakthi.groups.migration.gofrugal.model.MedLoyaltyPointsVW;

public interface MedLoyaltyPointsVWRepository extends CrudRepository<MedLoyaltyPointsVW, Integer> {

}
