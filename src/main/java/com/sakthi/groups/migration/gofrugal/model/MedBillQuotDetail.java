package com.sakthi.groups.migration.gofrugal.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MED_BILLQUOT_DTL")
public class MedBillQuotDetail {

	
	@Column(name = "MBD_BILL_NO")
	private Integer mBDBillNo;

	@Id
	@Column(name = "MBD_BILL_SL_NO")
	private Integer MBD_BILL_SL_NO;

	@Column(name = "MBD_ITEM_CODE")
	private Integer MBD_ITEM_CODE;

	@Column(name = "MBD_ITEM_UNIT")
	private Integer MBD_ITEM_UNIT;

	@Column(name = "MBD_BATCH_NO")
	private String MBD_BATCH_NO;

	@Column(name = "MBD_EXP_DT")
	private Timestamp MBD_EXP_DT;

	@Column(name = "MBD_ITEM_QTY")
	private Double MBD_ITEM_QTY;

	@Column(name = "MBD_ITEM_RATE")
	private Double MBD_ITEM_RATE;

	@Column(name = "MBD_ITEM_TAX_AMT")
	private Double MBD_ITEM_TAX_AMT;

	@Column(name = "MBD_ITEM_AMOUNT")
	private Double MBD_ITEM_AMOUNT;

	@Column(name = "MBD_ITEM_TYPE")
	private Integer MBD_ITEM_TYPE;

	@Column(name = "MBD_PUR_RATE")
	private Double MBD_PUR_RATE;

	@Column(name = "MBD_DIST_CODE")
	private Integer MBD_DIST_CODE;

	@Column(name = "MBD_ITEM_TAX_PERC")
	private Double MBD_ITEM_TAX_PERC;

	@Column(name = "MBD_PURCHASE_RATE")
	private Double MBD_PURCHASE_RATE;

	@Column(name = "MBD_TOT_TAX_AMT")
	private Double MBD_TOT_TAX_AMT;

	@Column(name = "MBD_DISC_AMT")
	private Double MBD_DISC_AMT;

	@Column(name = "MBD_INCLUSIVEFLAG")
	private String MBD_INCLUSIVEFLAG;

	@Column(name = "MBD_TAX_CONFIG")
	private Integer MBD_TAX_CONFIG;

	@Column(name = "mbd_item_rowid")
	private Integer mbd_item_rowid;

	@Column(name = "mbd_item_deliver")
	private String mbd_item_deliver;

	@Column(name = "mbd_item_schm_code")
	private Double mbd_item_schm_code;

	@Column(name = "mbd_item_schm_slno")
	private Double mbd_item_schm_slno;

	@Column(name = "mbd_item_schm_discperc")
	private Double mbd_item_schm_discperc;

	@Column(name = "mbd_item_schm_discamt")
	private String mbd_item_schm_discamt;

	@Column(name = "mbd_grind_amt")
	private Double mbd_grind_amt;

	@Column(name = "mbd_compid")
	private Double mbd_compid;

	@Column(name = "mbd_diviid")
	private Double mbd_diviid;

	@Column(name = "mbd_locaid")
	private Double mbd_locaid;

	@Column(name = "mbd_max_rate")
	private Double mbd_max_rate;

	@Column(name = "mbd_unit")
	private Double mbd_unit;

	@Column(name = "mbd_price_level")
	private Integer mbd_price_level;

	@Column(name = "mbd_Free_item")
	private Integer mbd_Free_item;

	@Column(name = "mbd_rst_per")
	private Double mbd_rst_per;

	@Column(name = "mbd_rst_amt")
	private Double mbd_rst_amt;

	@Column(name = "mbd_rstsc_per")
	private Double mbd_rstsc_per;

	@Column(name = "mbd_rstsc_amt")
	private Double mbd_rstsc_amt;

	@Column(name = "mbd_REP_CODE")
	private Integer mbd_REP_CODE;

	@Column(name = "mbd_REP_COMM")
	private Double mbd_REP_COMM;

	@Column(name = "MBD_BILL_TOTDISC")
	private Double MBD_BILL_TOTDISC;

	@Column(name = "TS")
	private Timestamp TS;

	@Column(name = "TSID")
	private Integer TSID;

	@Column(name = "MBD_DISC_PERC")
	private Double MBD_DISC_PERC;

	@Column(name = "mbd_rep_amt")
	private Double mbd_rep_amt;

	@Column(name = "mbd_selling")
	private Double mbd_selling;

	@Column(name = "mbd_sur_charge_perc")
	private Double mbd_sur_charge_perc;

	@Column(name = "mbd_sur_charge_amount")
	private Double mbd_sur_charge_amount;

	@Column(name = "mbd_id_withouttax")
	private Integer mbd_id_withouttax;

	@Column(name = "mbd_cd_withtax")
	private Integer mbd_cd_withtax;

	@Column(name = "mbd_conv_type")
	private String mbd_conv_type;

	@Column(name = "mbd_conv_factor")
	private Double mbd_conv_factor;

	@Column(name = "mbd_modifier")
	private Integer mbd_modifier;

	@Column(name = "mbd_modifierless")
	private Integer mbd_modifierless;

	@Column(name = "mbd_qty_pricelvl")
	private Integer mbd_qty_pricelvl;

	@Column(name = "mbd_mat_unique_ref")
	private Integer mbd_mat_unique_ref;

	@Column(name = "RETAIL_OUTLET_ID")
	private Integer RETAIL_OUTLET_ID;

	@Column(name = "mbd_min_selling")
	private Double mbd_min_selling;

	@Column(name = "mbd_vat_on_srvtax")
	private Integer mbd_vat_on_srvtax;

	@Column(name = "mbd_sgst_perc")
	private Double mbd_sgst_perc;

	@Column(name = "mbd_sgst_amt")
	private Double mbd_sgst_amt;

	@Column(name = "mbd_cgst_perc")
	private Double mbd_cgst_perc;

	@Column(name = "mbd_cgst_amt")
	private Double mbd_cgst_amt;

	@Column(name = "mbd_igst_perc")
	private Double mbd_igst_perc;

	@Column(name = "mbd_igst_amt")
	private Double mbd_igst_amt;

	@Column(name = "mbd_cess_perc")
	private Double mbd_cess_perc;

	@Column(name = "mbd_cess_amt")
	private Double mbd_cess_amt;

	@Column(name = "mbd_gst_code")
	private Integer mbd_gst_code;

	@Column(name = "mbd_abatement_perc")
	private Double mbd_abatement_perc;

	@Column(name = "mbd_gst_basedon")
	private Integer mbd_gst_basedon;

	@Column(name = "mbd_tax_calc_on")
	private Integer mbd_tax_calc_on;

	@Column(name = "mbd_ExtraCess_Per_Qty")
	private Double mbd_ExtraCess_Per_Qty;

	@Column(name = "mbd_extra_cess_amt")
	private Double mbd_extra_cess_amt;

	@Column(name = "mbd_pur_price_wot")
	private Double mbd_pur_price_wot;

	@Column(name = "MBD_CALAMITYCESS_PERC")
	private Double MBD_CALAMITYCESS_PERC;

	public Integer getmBDBillNo() {
		return mBDBillNo;
	}

	public void setmBDBillNo(Integer mBDBillNo) {
		this.mBDBillNo = mBDBillNo;
	}

	public Integer getMBD_BILL_SL_NO() {
		return MBD_BILL_SL_NO;
	}

	public void setMBD_BILL_SL_NO(Integer mBD_BILL_SL_NO) {
		MBD_BILL_SL_NO = mBD_BILL_SL_NO;
	}

	public Integer getMBD_ITEM_CODE() {
		return MBD_ITEM_CODE;
	}

	public void setMBD_ITEM_CODE(Integer mBD_ITEM_CODE) {
		MBD_ITEM_CODE = mBD_ITEM_CODE;
	}

	public Integer getMBD_ITEM_UNIT() {
		return MBD_ITEM_UNIT;
	}

	public void setMBD_ITEM_UNIT(Integer mBD_ITEM_UNIT) {
		MBD_ITEM_UNIT = mBD_ITEM_UNIT;
	}

	public String getMBD_BATCH_NO() {
		return MBD_BATCH_NO;
	}

	public void setMBD_BATCH_NO(String mBD_BATCH_NO) {
		MBD_BATCH_NO = mBD_BATCH_NO;
	}

	public Timestamp getMBD_EXP_DT() {
		return MBD_EXP_DT;
	}

	public void setMBD_EXP_DT(Timestamp mBD_EXP_DT) {
		MBD_EXP_DT = mBD_EXP_DT;
	}

	public Double getMBD_ITEM_QTY() {
		return MBD_ITEM_QTY;
	}

	public void setMBD_ITEM_QTY(Double mBD_ITEM_QTY) {
		MBD_ITEM_QTY = mBD_ITEM_QTY;
	}

	public Double getMBD_ITEM_RATE() {
		return MBD_ITEM_RATE;
	}

	public void setMBD_ITEM_RATE(Double mBD_ITEM_RATE) {
		MBD_ITEM_RATE = mBD_ITEM_RATE;
	}

	public Double getMBD_ITEM_TAX_AMT() {
		return MBD_ITEM_TAX_AMT;
	}

	public void setMBD_ITEM_TAX_AMT(Double mBD_ITEM_TAX_AMT) {
		MBD_ITEM_TAX_AMT = mBD_ITEM_TAX_AMT;
	}

	public Double getMBD_ITEM_AMOUNT() {
		return MBD_ITEM_AMOUNT;
	}

	public void setMBD_ITEM_AMOUNT(Double mBD_ITEM_AMOUNT) {
		MBD_ITEM_AMOUNT = mBD_ITEM_AMOUNT;
	}

	public Integer getMBD_ITEM_TYPE() {
		return MBD_ITEM_TYPE;
	}

	public void setMBD_ITEM_TYPE(Integer mBD_ITEM_TYPE) {
		MBD_ITEM_TYPE = mBD_ITEM_TYPE;
	}

	public Double getMBD_PUR_RATE() {
		return MBD_PUR_RATE;
	}

	public void setMBD_PUR_RATE(Double mBD_PUR_RATE) {
		MBD_PUR_RATE = mBD_PUR_RATE;
	}

	public Integer getMBD_DIST_CODE() {
		return MBD_DIST_CODE;
	}

	public void setMBD_DIST_CODE(Integer mBD_DIST_CODE) {
		MBD_DIST_CODE = mBD_DIST_CODE;
	}

	public Double getMBD_ITEM_TAX_PERC() {
		return MBD_ITEM_TAX_PERC;
	}

	public void setMBD_ITEM_TAX_PERC(Double mBD_ITEM_TAX_PERC) {
		MBD_ITEM_TAX_PERC = mBD_ITEM_TAX_PERC;
	}

	public Double getMBD_PURCHASE_RATE() {
		return MBD_PURCHASE_RATE;
	}

	public void setMBD_PURCHASE_RATE(Double mBD_PURCHASE_RATE) {
		MBD_PURCHASE_RATE = mBD_PURCHASE_RATE;
	}

	public Double getMBD_TOT_TAX_AMT() {
		return MBD_TOT_TAX_AMT;
	}

	public void setMBD_TOT_TAX_AMT(Double mBD_TOT_TAX_AMT) {
		MBD_TOT_TAX_AMT = mBD_TOT_TAX_AMT;
	}

	public Double getMBD_DISC_AMT() {
		return MBD_DISC_AMT;
	}

	public void setMBD_DISC_AMT(Double mBD_DISC_AMT) {
		MBD_DISC_AMT = mBD_DISC_AMT;
	}

	public String getMBD_INCLUSIVEFLAG() {
		return MBD_INCLUSIVEFLAG;
	}

	public void setMBD_INCLUSIVEFLAG(String mBD_INCLUSIVEFLAG) {
		MBD_INCLUSIVEFLAG = mBD_INCLUSIVEFLAG;
	}

	public Integer getMBD_TAX_CONFIG() {
		return MBD_TAX_CONFIG;
	}

	public void setMBD_TAX_CONFIG(Integer mBD_TAX_CONFIG) {
		MBD_TAX_CONFIG = mBD_TAX_CONFIG;
	}

	public Integer getMbd_item_rowid() {
		return mbd_item_rowid;
	}

	public void setMbd_item_rowid(Integer mbd_item_rowid) {
		this.mbd_item_rowid = mbd_item_rowid;
	}

	public String getMbd_item_deliver() {
		return mbd_item_deliver;
	}

	public void setMbd_item_deliver(String mbd_item_deliver) {
		this.mbd_item_deliver = mbd_item_deliver;
	}

	public Double getMbd_item_schm_code() {
		return mbd_item_schm_code;
	}

	public void setMbd_item_schm_code(Double mbd_item_schm_code) {
		this.mbd_item_schm_code = mbd_item_schm_code;
	}

	public Double getMbd_item_schm_slno() {
		return mbd_item_schm_slno;
	}

	public void setMbd_item_schm_slno(Double mbd_item_schm_slno) {
		this.mbd_item_schm_slno = mbd_item_schm_slno;
	}

	public Double getMbd_item_schm_discperc() {
		return mbd_item_schm_discperc;
	}

	public void setMbd_item_schm_discperc(Double mbd_item_schm_discperc) {
		this.mbd_item_schm_discperc = mbd_item_schm_discperc;
	}

	public String getMbd_item_schm_discamt() {
		return mbd_item_schm_discamt;
	}

	public void setMbd_item_schm_discamt(String mbd_item_schm_discamt) {
		this.mbd_item_schm_discamt = mbd_item_schm_discamt;
	}

	public Double getMbd_grind_amt() {
		return mbd_grind_amt;
	}

	public void setMbd_grind_amt(Double mbd_grind_amt) {
		this.mbd_grind_amt = mbd_grind_amt;
	}

	public Double getMbd_compid() {
		return mbd_compid;
	}

	public void setMbd_compid(Double mbd_compid) {
		this.mbd_compid = mbd_compid;
	}

	public Double getMbd_diviid() {
		return mbd_diviid;
	}

	public void setMbd_diviid(Double mbd_diviid) {
		this.mbd_diviid = mbd_diviid;
	}

	public Double getMbd_locaid() {
		return mbd_locaid;
	}

	public void setMbd_locaid(Double mbd_locaid) {
		this.mbd_locaid = mbd_locaid;
	}

	public Double getMbd_max_rate() {
		return mbd_max_rate;
	}

	public void setMbd_max_rate(Double mbd_max_rate) {
		this.mbd_max_rate = mbd_max_rate;
	}

	public Double getMbd_unit() {
		return mbd_unit;
	}

	public void setMbd_unit(Double mbd_unit) {
		this.mbd_unit = mbd_unit;
	}

	public Integer getMbd_price_level() {
		return mbd_price_level;
	}

	public void setMbd_price_level(Integer mbd_price_level) {
		this.mbd_price_level = mbd_price_level;
	}

	public Integer getMbd_Free_item() {
		return mbd_Free_item;
	}

	public void setMbd_Free_item(Integer mbd_Free_item) {
		this.mbd_Free_item = mbd_Free_item;
	}

	public Double getMbd_rst_per() {
		return mbd_rst_per;
	}

	public void setMbd_rst_per(Double mbd_rst_per) {
		this.mbd_rst_per = mbd_rst_per;
	}

	public Double getMbd_rst_amt() {
		return mbd_rst_amt;
	}

	public void setMbd_rst_amt(Double mbd_rst_amt) {
		this.mbd_rst_amt = mbd_rst_amt;
	}

	public Double getMbd_rstsc_per() {
		return mbd_rstsc_per;
	}

	public void setMbd_rstsc_per(Double mbd_rstsc_per) {
		this.mbd_rstsc_per = mbd_rstsc_per;
	}

	public Double getMbd_rstsc_amt() {
		return mbd_rstsc_amt;
	}

	public void setMbd_rstsc_amt(Double mbd_rstsc_amt) {
		this.mbd_rstsc_amt = mbd_rstsc_amt;
	}

	public Integer getMbd_REP_CODE() {
		return mbd_REP_CODE;
	}

	public void setMbd_REP_CODE(Integer mbd_REP_CODE) {
		this.mbd_REP_CODE = mbd_REP_CODE;
	}

	public Double getMbd_REP_COMM() {
		return mbd_REP_COMM;
	}

	public void setMbd_REP_COMM(Double mbd_REP_COMM) {
		this.mbd_REP_COMM = mbd_REP_COMM;
	}

	public Double getMBD_BILL_TOTDISC() {
		return MBD_BILL_TOTDISC;
	}

	public void setMBD_BILL_TOTDISC(Double mBD_BILL_TOTDISC) {
		MBD_BILL_TOTDISC = mBD_BILL_TOTDISC;
	}

	public Timestamp getTS() {
		return TS;
	}

	public void setTS(Timestamp tS) {
		TS = tS;
	}

	public Integer getTSID() {
		return TSID;
	}

	public void setTSID(Integer tSID) {
		TSID = tSID;
	}

	public Double getMBD_DISC_PERC() {
		return MBD_DISC_PERC;
	}

	public void setMBD_DISC_PERC(Double mBD_DISC_PERC) {
		MBD_DISC_PERC = mBD_DISC_PERC;
	}

	public Double getMbd_rep_amt() {
		return mbd_rep_amt;
	}

	public void setMbd_rep_amt(Double mbd_rep_amt) {
		this.mbd_rep_amt = mbd_rep_amt;
	}

	public Double getMbd_selling() {
		return mbd_selling;
	}

	public void setMbd_selling(Double mbd_selling) {
		this.mbd_selling = mbd_selling;
	}

	public Double getMbd_sur_charge_perc() {
		return mbd_sur_charge_perc;
	}

	public void setMbd_sur_charge_perc(Double mbd_sur_charge_perc) {
		this.mbd_sur_charge_perc = mbd_sur_charge_perc;
	}

	public Double getMbd_sur_charge_amount() {
		return mbd_sur_charge_amount;
	}

	public void setMbd_sur_charge_amount(Double mbd_sur_charge_amount) {
		this.mbd_sur_charge_amount = mbd_sur_charge_amount;
	}

	public Integer getMbd_id_withouttax() {
		return mbd_id_withouttax;
	}

	public void setMbd_id_withouttax(Integer mbd_id_withouttax) {
		this.mbd_id_withouttax = mbd_id_withouttax;
	}

	public Integer getMbd_cd_withtax() {
		return mbd_cd_withtax;
	}

	public void setMbd_cd_withtax(Integer mbd_cd_withtax) {
		this.mbd_cd_withtax = mbd_cd_withtax;
	}

	public String getMbd_conv_type() {
		return mbd_conv_type;
	}

	public void setMbd_conv_type(String mbd_conv_type) {
		this.mbd_conv_type = mbd_conv_type;
	}

	public Double getMbd_conv_factor() {
		return mbd_conv_factor;
	}

	public void setMbd_conv_factor(Double mbd_conv_factor) {
		this.mbd_conv_factor = mbd_conv_factor;
	}

	public Integer getMbd_modifier() {
		return mbd_modifier;
	}

	public void setMbd_modifier(Integer mbd_modifier) {
		this.mbd_modifier = mbd_modifier;
	}

	public Integer getMbd_modifierless() {
		return mbd_modifierless;
	}

	public void setMbd_modifierless(Integer mbd_modifierless) {
		this.mbd_modifierless = mbd_modifierless;
	}

	public Integer getMbd_qty_pricelvl() {
		return mbd_qty_pricelvl;
	}

	public void setMbd_qty_pricelvl(Integer mbd_qty_pricelvl) {
		this.mbd_qty_pricelvl = mbd_qty_pricelvl;
	}

	public Integer getMbd_mat_unique_ref() {
		return mbd_mat_unique_ref;
	}

	public void setMbd_mat_unique_ref(Integer mbd_mat_unique_ref) {
		this.mbd_mat_unique_ref = mbd_mat_unique_ref;
	}

	public Integer getRETAIL_OUTLET_ID() {
		return RETAIL_OUTLET_ID;
	}

	public void setRETAIL_OUTLET_ID(Integer rETAIL_OUTLET_ID) {
		RETAIL_OUTLET_ID = rETAIL_OUTLET_ID;
	}

	public Double getMbd_min_selling() {
		return mbd_min_selling;
	}

	public void setMbd_min_selling(Double mbd_min_selling) {
		this.mbd_min_selling = mbd_min_selling;
	}

	public Integer getMbd_vat_on_srvtax() {
		return mbd_vat_on_srvtax;
	}

	public void setMbd_vat_on_srvtax(Integer mbd_vat_on_srvtax) {
		this.mbd_vat_on_srvtax = mbd_vat_on_srvtax;
	}

	public Double getMbd_sgst_perc() {
		return mbd_sgst_perc;
	}

	public void setMbd_sgst_perc(Double mbd_sgst_perc) {
		this.mbd_sgst_perc = mbd_sgst_perc;
	}

	public Double getMbd_sgst_amt() {
		return mbd_sgst_amt;
	}

	public void setMbd_sgst_amt(Double mbd_sgst_amt) {
		this.mbd_sgst_amt = mbd_sgst_amt;
	}

	public Double getMbd_cgst_perc() {
		return mbd_cgst_perc;
	}

	public void setMbd_cgst_perc(Double mbd_cgst_perc) {
		this.mbd_cgst_perc = mbd_cgst_perc;
	}

	public Double getMbd_cgst_amt() {
		return mbd_cgst_amt;
	}

	public void setMbd_cgst_amt(Double mbd_cgst_amt) {
		this.mbd_cgst_amt = mbd_cgst_amt;
	}

	public Double getMbd_igst_perc() {
		return mbd_igst_perc;
	}

	public void setMbd_igst_perc(Double mbd_igst_perc) {
		this.mbd_igst_perc = mbd_igst_perc;
	}

	public Double getMbd_igst_amt() {
		return mbd_igst_amt;
	}

	public void setMbd_igst_amt(Double mbd_igst_amt) {
		this.mbd_igst_amt = mbd_igst_amt;
	}

	public Double getMbd_cess_perc() {
		return mbd_cess_perc;
	}

	public void setMbd_cess_perc(Double mbd_cess_perc) {
		this.mbd_cess_perc = mbd_cess_perc;
	}

	public Double getMbd_cess_amt() {
		return mbd_cess_amt;
	}

	public void setMbd_cess_amt(Double mbd_cess_amt) {
		this.mbd_cess_amt = mbd_cess_amt;
	}

	public Integer getMbd_gst_code() {
		return mbd_gst_code;
	}

	public void setMbd_gst_code(Integer mbd_gst_code) {
		this.mbd_gst_code = mbd_gst_code;
	}

	public Double getMbd_abatement_perc() {
		return mbd_abatement_perc;
	}

	public void setMbd_abatement_perc(Double mbd_abatement_perc) {
		this.mbd_abatement_perc = mbd_abatement_perc;
	}

	public Integer getMbd_gst_basedon() {
		return mbd_gst_basedon;
	}

	public void setMbd_gst_basedon(Integer mbd_gst_basedon) {
		this.mbd_gst_basedon = mbd_gst_basedon;
	}

	public Integer getMbd_tax_calc_on() {
		return mbd_tax_calc_on;
	}

	public void setMbd_tax_calc_on(Integer mbd_tax_calc_on) {
		this.mbd_tax_calc_on = mbd_tax_calc_on;
	}

	public Double getMbd_ExtraCess_Per_Qty() {
		return mbd_ExtraCess_Per_Qty;
	}

	public void setMbd_ExtraCess_Per_Qty(Double mbd_ExtraCess_Per_Qty) {
		this.mbd_ExtraCess_Per_Qty = mbd_ExtraCess_Per_Qty;
	}

	public Double getMbd_extra_cess_amt() {
		return mbd_extra_cess_amt;
	}

	public void setMbd_extra_cess_amt(Double mbd_extra_cess_amt) {
		this.mbd_extra_cess_amt = mbd_extra_cess_amt;
	}

	public Double getMbd_pur_price_wot() {
		return mbd_pur_price_wot;
	}

	public void setMbd_pur_price_wot(Double mbd_pur_price_wot) {
		this.mbd_pur_price_wot = mbd_pur_price_wot;
	}

	public Double getMBD_CALAMITYCESS_PERC() {
		return MBD_CALAMITYCESS_PERC;
	}

	public void setMBD_CALAMITYCESS_PERC(Double mBD_CALAMITYCESS_PERC) {
		MBD_CALAMITYCESS_PERC = mBD_CALAMITYCESS_PERC;
	}
}
