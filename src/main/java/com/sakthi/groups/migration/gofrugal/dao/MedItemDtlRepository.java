package com.sakthi.groups.migration.gofrugal.dao;

import org.springframework.data.repository.CrudRepository;

import com.sakthi.groups.migration.gofrugal.model.MedItemDtl;

public interface MedItemDtlRepository extends CrudRepository<MedItemDtl, Integer> {

}
