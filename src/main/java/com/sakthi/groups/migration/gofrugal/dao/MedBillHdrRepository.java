package com.sakthi.groups.migration.gofrugal.dao;

import org.springframework.data.repository.CrudRepository;

import com.sakthi.groups.migration.gofrugal.model.MedBillHdr;

public interface MedBillHdrRepository extends CrudRepository<MedBillHdr, Integer> {

	public MedBillHdr findByMbhBillNo(Integer mBH_BILL_NO);

}
