package com.sakthi.groups.migration.gofrugal.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "med_price_level_item")
public class MedPriceLevelItem {

	@Id
	@Column(name = "mpli_row_id")
	private Integer mpli_row_id;

	@Column(name = "mpli_price_level_code")
	private Integer mpli_price_level_code;

	@Column(name = "mpli_rate")
	private Double mpli_rate;

	@Column(name = "mpli_item_code")
	private Integer mpli_item_code;

	@Column(name = "mpli_selling")
	private Double mpli_selling;

	@Column(name = "TS")
	private Timestamp TS;

	@Column(name = "TSID")
	private Integer TSID;

	@Column(name = "RETAIL_OUTLET_ID")
	private Integer RETAIL_OUTLET_ID;

	public Integer getMpli_row_id() {
		return mpli_row_id;
	}

	public void setMpli_row_id(Integer mpli_row_id) {
		this.mpli_row_id = mpli_row_id;
	}

	public Integer getMpli_price_level_code() {
		return mpli_price_level_code;
	}

	public void setMpli_price_level_code(Integer mpli_price_level_code) {
		this.mpli_price_level_code = mpli_price_level_code;
	}

	public Double getMpli_rate() {
		return mpli_rate;
	}

	public void setMpli_rate(Double mpli_rate) {
		this.mpli_rate = mpli_rate;
	}

	public Integer getMpli_item_code() {
		return mpli_item_code;
	}

	public void setMpli_item_code(Integer mpli_item_code) {
		this.mpli_item_code = mpli_item_code;
	}

	public Double getMpli_selling() {
		return mpli_selling;
	}

	public void setMpli_selling(Double mpli_selling) {
		this.mpli_selling = mpli_selling;
	}

	public Timestamp getTS() {
		return TS;
	}

	public void setTS(Timestamp tS) {
		TS = tS;
	}

	public Integer getTSID() {
		return TSID;
	}

	public void setTSID(Integer tSID) {
		TSID = tSID;
	}

	public Integer getRETAIL_OUTLET_ID() {
		return RETAIL_OUTLET_ID;
	}

	public void setRETAIL_OUTLET_ID(Integer rETAIL_OUTLET_ID) {
		RETAIL_OUTLET_ID = rETAIL_OUTLET_ID;
	}

}
