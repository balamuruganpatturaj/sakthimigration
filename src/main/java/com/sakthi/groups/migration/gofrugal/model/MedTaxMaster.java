package com.sakthi.groups.migration.gofrugal.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "med_tax_mast")
public class MedTaxMaster {

	@Id
	@Column(name = "mtm_tax_code")
	private Integer mtm_tax_code;

	@Column(name = "mtm_cst_rate")
	private Integer mtm_cst_rate;

	@Column(name = "mtm_tngst_rate")
	private Integer mtm_tngst_rate;

	@Column(name = "mtm_sconst_rate")
	private Integer mtm_sconst_rate;

	@Column(name = "msm_tax_print")
	private String msm_tax_print;

	@Column(name = "MTM_BILL_TYPE")
	private String MTM_BILL_TYPE;

	@Column(name = "MTM_EXCISE_TAX")
	private Integer MTM_EXCISE_TAX;

	@Column(name = "TS")
	private Timestamp TS;

	@Column(name = "TSID")
	private Integer TSID;

	@Column(name = "mtm_tax_desc")
	private String mtm_tax_desc;

	@Column(name = "RETAIL_OUTLET_ID")
	private Integer RETAIL_OUTLET_ID;

	@Column(name = "MTM_igst")
	private Double MTM_igst;

	@Column(name = "MTM_cgst")
	private Double MTM_cgst;

	@Column(name = "MTM_sgst")
	private Double MTM_sgst;

	@Column(name = "MTM_isgst")
	private Integer MTM_isgst;

	@Column(name = "MTM_gst_cess")
	private Double MTM_gst_cess;

	@Column(name = "MTM_is_gst_exempted")
	private Integer MTM_is_gst_exempted;

	@Column(name = "mtm_filing_taxcode")
	private String mtm_filing_taxcode;

	@Column(name = "mtm_free_Tax")
	private String mtm_free_Tax;

	public Integer getMtm_tax_code() {
		return mtm_tax_code;
	}

	public void setMtm_tax_code(Integer mtm_tax_code) {
		this.mtm_tax_code = mtm_tax_code;
	}

	public Integer getMtm_cst_rate() {
		return mtm_cst_rate;
	}

	public void setMtm_cst_rate(Integer mtm_cst_rate) {
		this.mtm_cst_rate = mtm_cst_rate;
	}

	public Integer getMtm_tngst_rate() {
		return mtm_tngst_rate;
	}

	public void setMtm_tngst_rate(Integer mtm_tngst_rate) {
		this.mtm_tngst_rate = mtm_tngst_rate;
	}

	public Integer getMtm_sconst_rate() {
		return mtm_sconst_rate;
	}

	public void setMtm_sconst_rate(Integer mtm_sconst_rate) {
		this.mtm_sconst_rate = mtm_sconst_rate;
	}

	public String getMsm_tax_print() {
		return msm_tax_print;
	}

	public void setMsm_tax_print(String msm_tax_print) {
		this.msm_tax_print = msm_tax_print;
	}

	public String getMTM_BILL_TYPE() {
		return MTM_BILL_TYPE;
	}

	public void setMTM_BILL_TYPE(String mTM_BILL_TYPE) {
		MTM_BILL_TYPE = mTM_BILL_TYPE;
	}

	public Integer getMTM_EXCISE_TAX() {
		return MTM_EXCISE_TAX;
	}

	public void setMTM_EXCISE_TAX(Integer mTM_EXCISE_TAX) {
		MTM_EXCISE_TAX = mTM_EXCISE_TAX;
	}

	public Timestamp getTS() {
		return TS;
	}

	public void setTS(Timestamp tS) {
		TS = tS;
	}

	public Integer getTSID() {
		return TSID;
	}

	public void setTSID(Integer tSID) {
		TSID = tSID;
	}

	public String getMtm_tax_desc() {
		return mtm_tax_desc;
	}

	public void setMtm_tax_desc(String mtm_tax_desc) {
		this.mtm_tax_desc = mtm_tax_desc;
	}

	public Integer getRETAIL_OUTLET_ID() {
		return RETAIL_OUTLET_ID;
	}

	public void setRETAIL_OUTLET_ID(Integer rETAIL_OUTLET_ID) {
		RETAIL_OUTLET_ID = rETAIL_OUTLET_ID;
	}

	public Double getMTM_igst() {
		return MTM_igst;
	}

	public void setMTM_igst(Double mTM_igst) {
		MTM_igst = mTM_igst;
	}

	public Double getMTM_cgst() {
		return MTM_cgst;
	}

	public void setMTM_cgst(Double mTM_cgst) {
		MTM_cgst = mTM_cgst;
	}

	public Double getMTM_sgst() {
		return MTM_sgst;
	}

	public void setMTM_sgst(Double mTM_sgst) {
		MTM_sgst = mTM_sgst;
	}

	public Integer getMTM_isgst() {
		return MTM_isgst;
	}

	public void setMTM_isgst(Integer mTM_isgst) {
		MTM_isgst = mTM_isgst;
	}

	public Double getMTM_gst_cess() {
		return MTM_gst_cess;
	}

	public void setMTM_gst_cess(Double mTM_gst_cess) {
		MTM_gst_cess = mTM_gst_cess;
	}

	public Integer getMTM_is_gst_exempted() {
		return MTM_is_gst_exempted;
	}

	public void setMTM_is_gst_exempted(Integer mTM_is_gst_exempted) {
		MTM_is_gst_exempted = mTM_is_gst_exempted;
	}

	public String getMtm_filing_taxcode() {
		return mtm_filing_taxcode;
	}

	public void setMtm_filing_taxcode(String mtm_filing_taxcode) {
		this.mtm_filing_taxcode = mtm_filing_taxcode;
	}

	public String getMtm_free_Tax() {
		return mtm_free_Tax;
	}

	public void setMtm_free_Tax(String mtm_free_Tax) {
		this.mtm_free_Tax = mtm_free_Tax;
	}

}
