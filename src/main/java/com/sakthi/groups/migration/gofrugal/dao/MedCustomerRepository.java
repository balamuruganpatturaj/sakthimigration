package com.sakthi.groups.migration.gofrugal.dao;

import org.springframework.data.repository.CrudRepository;

import com.sakthi.groups.migration.gofrugal.model.MedCustomerMaster;

public interface MedCustomerRepository extends CrudRepository<MedCustomerMaster, Long> {

}
