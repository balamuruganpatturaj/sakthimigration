package com.sakthi.groups.migration.gofrugal.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "med_item_hdr")
public class MedItemHdr {

	@Id
	@Column(name = "MIH_ITEM_CODE")
	private Integer MIH_ITEM_CODE;

	@Column(name = "MIH_ITEM_NAME")
	private String MIH_ITEM_NAME;

	@Column(name = "MIH_ITEM_MFR_CODE")
	private Integer MIH_ITEM_MFR_CODE;

	@Column(name = "MIH_MITEM_TYPE")
	private Double MIH_MITEM_TYPE;

	@Column(name = "MIH_ITEM_TOT_STOCK")
	private Double MIH_ITEM_TOT_STOCK;

	@Column(name = "MIH_ITEM_TEMP_STOCK")
	private Double MIH_ITEM_TEMP_STOCK;

	@Column(name = "MIH_ITEM_MIN_QTY")
	private Double MIH_ITEM_MIN_QTY;

	@Column(name = "MIH_ITEM_MAX_QTY")
	private Double MIH_ITEM_MAX_QTY;

	@Column(name = "MIH_ITEM_RACK")
	private String MIH_ITEM_RACK;

	@Column(name = "MIH_ITEM_SHELF")
	private String MIH_ITEM_SHELF;

	@Column(name = "MIH_ITEM_BOX")
	private Integer MIH_ITEM_BOX;

	@Column(name = "MIH_ITEM_CRTDT")
	private Timestamp MIH_ITEM_CRTDT;

	@Column(name = "MIH_OP_STOCK")
	private Integer MIH_OP_STOCK;

	@Column(name = "MIH_OP_AMT")
	private Double MIH_OP_AMT;

	@Column(name = "MIH_INCLUSIVETAX")
	private String MIH_INCLUSIVETAX;

	@Column(name = "MIH_AUD_STATUS")
	private String MIH_AUD_STATUS;

	@Column(name = "MIH_AUD_DATE")
	private Timestamp MIH_AUD_DATE;

	@Column(name = "MIH_TYPE_UPDATED")
	private String MIH_TYPE_UPDATED;

	@Column(name = "MIH_LOOSE_SALE_ALLOWED")
	private String MIH_LOOSE_SALE_ALLOWED;

	@Column(name = "MIH_MIN_BILL_QTY")
	private Integer MIH_MIN_BILL_QTY;

	@Column(name = "MIH_BE_CONF")
	private String MIH_BE_CONF;

	@Column(name = "MIH_PREPARED")
	private String MIH_PREPARED;

	@Column(name = "MIH_TRADE_CONF")
	private String MIH_TRADE_CONF;

	@Column(name = "MIH_ISSCHEME")
	private String MIH_ISSCHEME;

	@Column(name = "MIH_CATEGORY_1")
	private Integer MIH_CATEGORY_1;

	@Column(name = "MIH_CATEGORY_2")
	private Integer MIH_CATEGORY_2;

	@Column(name = "MIH_CATEGORY_3")
	private Integer MIH_CATEGORY_3;

	@Column(name = "MIH_CATEGORY_4")
	private Integer MIH_CATEGORY_4;

	@Column(name = "MIH_CATEGORY_5")
	private Integer MIH_CATEGORY_5;

	@Column(name = "MIH_CATEGORY_6")
	private Integer MIH_CATEGORY_6;

	@Column(name = "MIH_CATEGORY_7")
	private Integer MIH_CATEGORY_7;

	@Column(name = "MIH_CATEGORY_8")
	private Integer MIH_CATEGORY_8;

	@Column(name = "MIH_CATEGORY_9")
	private Integer MIH_CATEGORY_9;

	@Column(name = "MIH_CATEGORY_10")
	private Integer MIH_CATEGORY_10;

	@Column(name = "MIH_ITEM_RECEIPT_UNIT")
	private Integer MIH_ITEM_RECEIPT_UNIT;

	@Column(name = "MIH_RST_TAX")
	private Double MIH_RST_TAX;

	@Column(name = "MIH_RST_SURRST_TAX")
	private Double MIH_RST_SURRST_TAX;

	@Column(name = "MIH_ITEM_ALIAS")
	private String MIH_ITEM_ALIAS;

	@Column(name = "mih_item_bulk_code")
	private Double mih_item_bulk_code;

	@Column(name = "mih_item_repack_convertion")
	private Double MIH_ITEM_REPACK_CONVERTION;

	@Column(name = "mih_decimal_point")
	private Double mih_decimal_point;

	@Column(name = "mih_grind_per")
	private Double mih_grind_per;

	@Column(name = "mih_cust_off")
	private String mih_cust_off;

	@Column(name = "mih_remain_pass")
	private String mih_remain_pass;

	@Column(name = "mih_remain_qty")
	private Double mih_remain_qty;

	@Column(name = "mih_compid")
	private Integer mih_compid;

	@Column(name = "mih_diviid")
	private Integer mih_diviid;

	@Column(name = "MIH_EANCODE")
	private String MIH_EANCODE;

	@Column(name = "MIH_CREATED_DATE")
	private Timestamp MIH_CREATED_DATE;

	@Column(name = "mih_unit")
	private String mih_unit;

	@Column(name = "mih_item_name_short")
	private String mih_item_name_short;

	@Column(name = "MIH_SALETAX_CODE")
	private Integer MIH_SALETAX_CODE;

	@Column(name = "MIH_PURTAX_CODE")
	private Integer MIH_PURTAX_CODE;

	@Column(name = "MIH_REP_CODE")
	private Integer MIH_REP_CODE;

	@Column(name = "MIH_REP_COMM")
	private Double MIH_REP_COMM;

	@Column(name = "TS")
	private Timestamp TS;

	@Column(name = "TSID")
	private Integer TSID;

	@Column(name = "mih_rep_comm_amt")
	private Double mih_rep_comm_amt;

	@Column(name = "mih_ValidMrp")
	private Integer mih_ValidMrp;

	@Column(name = "MIH_CATEGORY_11")
	private Integer MIH_CATEGORY_11;

	@Column(name = "mih_phy_vfn_date")
	private Timestamp mih_phy_vfn_date;

	@Column(name = "mih_weight")
	private String mih_weight;

	@Column(name = "MIH_LOYALTY_ALLOWED")
	private Integer MIH_LOYALTY_ALLOWED;

	@Column(name = "mih_pkd_conf")
	private String mih_pkd_conf;

	@Column(name = "mih_shelf_life")
	private Integer mih_shelf_life;

	@Column(name = "mih_min_shelf_life")
	private Integer mih_min_shelf_life;

	@Column(name = "mih_min_shelf_life_perc")
	private Integer mih_min_shelf_life_perc;

	@Column(name = "MIH_PUF")
	private Integer MIH_PUF;

	@Column(name = "MIH_SUF")
	private Integer MIH_SUF;

	@Column(name = "MIH_SIZE")
	private Integer MIH_SIZE;

	@Column(name = "MIH_LEAD_TIME")
	private Integer MIH_LEAD_TIME;

	@Column(name = "MIH_TOLE_LMT")
	private Integer MIH_TOLE_LMT;

	@Column(name = "MIH_MARGIN_BASED")
	private String MIH_MARGIN_BASED;

	@Column(name = "MIH_SKU_TYPE")
	private String MIH_SKU_TYPE;

	@Column(name = "MIH_SKU_ORD_GRP")
	private String MIH_SKU_ORD_GRP;

	@Column(name = "MIH_REPL_TIME")
	private Integer MIH_REPL_TIME;

	@Column(name = "MIH_BUFFER_DAYS")
	private Integer MIH_BUFFER_DAYS;

	@Column(name = "MIH_GATE_KEEP_MARGIN")
	private Double MIH_GATE_KEEP_MARGIN;

	@Column(name = "MIH_MSF")
	private Integer MIH_MSF;

	@Column(name = "MIH_INDENT_EDIT")
	private String MIH_INDENT_EDIT;

	@Column(name = "MIH_AVAILABILITY")
	private String MIH_AVAILABILITY;

	@Column(name = "mih_send_weight")
	private Integer mih_send_weight;

	@Column(name = "mih_weight_sale_type")
	private Integer mih_weight_sale_type;

	@Column(name = "mih_reorder_qty")
	private Double mih_reorder_qty;

	@Column(name = "mih_allow_discount")
	private String mih_allow_discount;

	@Column(name = "mih_allow_addcharge")
	private String mih_allow_addcharge;

	@Column(name = "mih_reorder_min_days")
	private Integer mih_reorder_min_days;

	@Column(name = "mih_reorder_max_days")
	private Integer mih_reorder_max_days;

	@Column(name = "MIH_ALLOW_SALE_RETURN")
	private String MIH_ALLOW_SALE_RETURN;

	@Column(name = "MIH_ALLOW_RATE_EDIT")
	private String MIH_ALLOW_RATE_EDIT;

	@Column(name = "MIH_ALLOW_NEGATIVE_STOCK")
	private String MIH_ALLOW_NEGATIVE_STOCK;

	@Column(name = "MIH_ProdType")
	private Integer MIH_ProdType;

	@Column(name = "Mih_warranty_month")
	private Integer Mih_warranty_month;

	@Column(name = "Mih_Serviceable")
	private Integer Mih_Serviceable;

	@Column(name = "mih_ex_duty_code")
	private Integer mih_ex_duty_code;

	@Column(name = "mih_item_weight")
	private Double mih_item_weight;

	@Column(name = "mih_allow_sales")
	private String mih_allow_sales;

	@Column(name = "mih_batch_conf")
	private String mih_batch_conf;

	@Column(name = "MIH_ALLOW_PURCHASE")
	private String MIH_ALLOW_PURCHASE;

	@Column(name = "RETAIL_OUTLET_ID")
	private Integer RETAIL_OUTLET_ID;

	@Column(name = "mih_Commodity_code")
	private Integer mih_Commodity_code;

	@Column(name = "MIH_SERVICE_TAX")
	private String MIH_SERVICE_TAX;

	@Column(name = "mih_mat_tableid")
	private Integer mih_mat_tableid;

	@Column(name = "MIH_ITEM_DISC_PERC")
	private Double MIH_ITEM_DISC_PERC;

	@Column(name = "MIH_ITEM_DISC_AMT")
	private Double MIH_ITEM_DISC_AMT;

	@Column(name = "mih_item_group")
	private Integer mih_item_group;

	@Column(name = "MIH_APPLIES_ONLINE")
	private Integer MIH_APPLIES_ONLINE;

	@Column(name = "mih_Container_Allowed")
	private Integer mih_Container_Allowed;

	@Column(name = "mih_price_conf_id")
	private Integer mih_price_conf_id;

	@Column(name = "mih_barcode_print")
	private Integer mih_barcode_print;

	@Column(name = "Mih_Allow_Half_Pizza")
	private Integer Mih_Allow_Half_Pizza;

	@Column(name = "mih_min_selling")
	private Double mih_min_selling;

	@Column(name = "MIH_ALLOW_WASTAGE")
	private String MIH_ALLOW_WASTAGE;

	@Column(name = "mih_kindofbook")
	private String mih_kindofbook;

	@Column(name = "mih_title")
	private String mih_title;

	@Column(name = "mih_YOP")
	private String mih_YOP;

	@Column(name = "mih_show_caption")
	private Integer mih_show_caption;

	@Column(name = "mih_text_align")
	private Integer mih_text_align;

	@Column(name = "mih_image_align")
	private Integer mih_image_align;

	@Column(name = "mih_select_rate_conf")
	private Integer mih_select_rate_conf;

	@Column(name = "mih_variant_conf")
	private String mih_variant_conf;

	@Column(name = "mih_item_uom")
	private String mih_item_uom;

	@Column(name = "mih_item_subUom")
	private String mih_item_subUom;

	@Column(name = "mih_uom_convertion")
	private Double mih_uom_convertion;

	@Column(name = "mih_veg")
	private String mih_veg;

	@Column(name = "Mih_ShowOptional_Status")
	private Integer Mih_ShowOptional_Status;

	@Column(name = "Mih_extra_desc")
	private String Mih_extra_desc;

	@Column(name = "mih_dish_prep")
	private String mih_dish_prep;

	@Column(name = "mih_srvtax_conf")
	private String mih_srvtax_conf;

	@Column(name = "mih_srvchrg_conf")
	private String mih_srvchrg_conf;

	@Column(name = "mih_Eancode_As_Barcode")
	private String mih_Eancode_As_Barcode;

	@Column(name = "mih_Free_Tax")
	private Integer mih_Free_Tax;

	@Column(name = "Mih_Favourite")
	private Integer Mih_Favourite;

	@Column(name = "Mih_spiciness_level")
	private Integer Mih_spiciness_level;

	@Column(name = "mih_Excise_duty_applicable")
	private String mih_Excise_duty_applicable;

	@Column(name = "MIH_SCM_ITEM_CODE")
	private String MIH_SCM_ITEM_CODE;

	@Column(name = "mih_Allow_Change_Selling")
	private String mih_Allow_Change_Selling;

	@Column(name = "mih_Noof_sticker")
	private String mih_Noof_sticker;

	@Column(name = "mih_reorder_type")
	private String mih_reorder_type;

	@Column(name = "mih_reorder_formula")
	private String mih_reorder_formula;

	@Column(name = "mih_avg_NDays_sales")
	private String mih_avg_NDays_sales;

	@Column(name = "mih_locaid")
	private String mih_locaid;

	@Column(name = "mih_MinStkLevel_Perc1")
	private Double mih_MinStkLevel_Perc1;

	@Column(name = "mih_MinStkLevel_Perc2")
	private Double mih_MinStkLevel_Perc2;

	@Column(name = "mih_eancode_required")
	private Integer mih_eancode_required;

	@Column(name = "mih_barcode_profile")
	private String mih_barcode_profile;

	@Column(name = "Mih_AllowLiquorTax")
	private String Mih_AllowLiquorTax;

	@Column(name = "mih_blockcombo_duplicate")
	private String mih_blockcombo_duplicate;

	@Column(name = "mih_image_path")
	private String mih_image_path;

	@Column(name = "mih_poexcess_perc")
	private Double mih_poexcess_perc;

	@Column(name = "MIH_GST_CODE")
	private Integer MIH_GST_CODE;

	@Column(name = "MIH_HSN_CODE")
	private String MIH_HSN_CODE;

	@Column(name = "MIH_PUR_CALC_GST_ON")
	private String MIH_PUR_CALC_GST_ON;

	@Column(name = "MIH_PUR_ABATEMENT")
	private String MIH_PUR_ABATEMENT;

	@Column(name = "MIH_SAL_CALC_GST_ON")
	private String MIH_SAL_CALC_GST_ON;

	@Column(name = "MIH_SAL_ABATEMENT")
	private String MIH_SAL_ABATEMENT;

	@Column(name = "MIH_Tax_CALC_ON")
	private String MIH_Tax_CALC_ON;

	@Column(name = "MIH_Tax_type")
	private String MIH_Tax_type;

	@Column(name = "mih_semifinished_kit")
	private String mih_semifinished_kit;

	@Column(name = "mih_ExtraCess_Per_Qty")
	private String mih_ExtraCess_Per_Qty;

	@Column(name = "mih_ItemSelect_Mode")
	private String mih_ItemSelect_Mode;

	@Column(name = "mih_Daily_Phy_Audit")
	private String mih_Daily_Phy_Audit;

	@Column(name = "mih_stk_Available_Wh")
	private String mih_stk_Available_Wh;

	@Column(name = "mih_Ndays_NweekSales")
	private String mih_Ndays_NweekSales;

	@Column(name = "mih_Perc_Nweek_sales")
	private String mih_Perc_Nweek_sales;

	@Column(name = "mih_gst_uom")
	private String mih_gst_uom;

	@Column(name = "mih_block_outlet_purchase")
	private String mih_block_outlet_purchase;

	@Column(name = "mih_soexcess_perc")
	private String mih_soexcess_perc;

	@Column(name = "mih_max_uniquebarcode")
	private String mih_max_uniquebarcode;

	@Column(name = "MIH_PUR_PRICE")
	private Double MIH_PUR_PRICE;

	@Column(name = "mih_indent_notallow")
	private String mih_indent_notallow;

	@Column(name = "mih_reorder_hold_days")
	private String mih_reorder_hold_days;

	@Column(name = "mih_erp_ref_code")
	private String mih_erp_ref_code;

	@Column(name = "mih_ret_days")
	private String mih_ret_days;

	public Integer getMIH_ITEM_CODE() {
		return MIH_ITEM_CODE;
	}

	public void setMIH_ITEM_CODE(Integer mIH_ITEM_CODE) {
		MIH_ITEM_CODE = mIH_ITEM_CODE;
	}

	public String getMIH_ITEM_NAME() {
		return MIH_ITEM_NAME;
	}

	public void setMIH_ITEM_NAME(String mIH_ITEM_NAME) {
		MIH_ITEM_NAME = mIH_ITEM_NAME;
	}

	public Integer getMIH_ITEM_MFR_CODE() {
		return MIH_ITEM_MFR_CODE;
	}

	public void setMIH_ITEM_MFR_CODE(Integer mIH_ITEM_MFR_CODE) {
		MIH_ITEM_MFR_CODE = mIH_ITEM_MFR_CODE;
	}

	public Double getMIH_MITEM_TYPE() {
		return MIH_MITEM_TYPE;
	}

	public void setMIH_MITEM_TYPE(Double mIH_MITEM_TYPE) {
		MIH_MITEM_TYPE = mIH_MITEM_TYPE;
	}

	public Double getMIH_ITEM_TOT_STOCK() {
		return MIH_ITEM_TOT_STOCK;
	}

	public void setMIH_ITEM_TOT_STOCK(Double mIH_ITEM_TOT_STOCK) {
		MIH_ITEM_TOT_STOCK = mIH_ITEM_TOT_STOCK;
	}

	public Double getMIH_ITEM_TEMP_STOCK() {
		return MIH_ITEM_TEMP_STOCK;
	}

	public void setMIH_ITEM_TEMP_STOCK(Double mIH_ITEM_TEMP_STOCK) {
		MIH_ITEM_TEMP_STOCK = mIH_ITEM_TEMP_STOCK;
	}

	public Double getMIH_ITEM_MIN_QTY() {
		return MIH_ITEM_MIN_QTY;
	}

	public void setMIH_ITEM_MIN_QTY(Double mIH_ITEM_MIN_QTY) {
		MIH_ITEM_MIN_QTY = mIH_ITEM_MIN_QTY;
	}

	public Double getMIH_ITEM_MAX_QTY() {
		return MIH_ITEM_MAX_QTY;
	}

	public void setMIH_ITEM_MAX_QTY(Double mIH_ITEM_MAX_QTY) {
		MIH_ITEM_MAX_QTY = mIH_ITEM_MAX_QTY;
	}

	public String getMIH_ITEM_RACK() {
		return MIH_ITEM_RACK;
	}

	public void setMIH_ITEM_RACK(String mIH_ITEM_RACK) {
		MIH_ITEM_RACK = mIH_ITEM_RACK;
	}

	public String getMIH_ITEM_SHELF() {
		return MIH_ITEM_SHELF;
	}

	public void setMIH_ITEM_SHELF(String mIH_ITEM_SHELF) {
		MIH_ITEM_SHELF = mIH_ITEM_SHELF;
	}

	public Integer getMIH_ITEM_BOX() {
		return MIH_ITEM_BOX;
	}

	public void setMIH_ITEM_BOX(Integer mIH_ITEM_BOX) {
		MIH_ITEM_BOX = mIH_ITEM_BOX;
	}

	public Timestamp getMIH_ITEM_CRTDT() {
		return MIH_ITEM_CRTDT;
	}

	public void setMIH_ITEM_CRTDT(Timestamp mIH_ITEM_CRTDT) {
		MIH_ITEM_CRTDT = mIH_ITEM_CRTDT;
	}

	public Integer getMIH_OP_STOCK() {
		return MIH_OP_STOCK;
	}

	public void setMIH_OP_STOCK(Integer mIH_OP_STOCK) {
		MIH_OP_STOCK = mIH_OP_STOCK;
	}

	public Double getMIH_OP_AMT() {
		return MIH_OP_AMT;
	}

	public void setMIH_OP_AMT(Double mIH_OP_AMT) {
		MIH_OP_AMT = mIH_OP_AMT;
	}

	public String getMIH_INCLUSIVETAX() {
		return MIH_INCLUSIVETAX;
	}

	public void setMIH_INCLUSIVETAX(String mIH_INCLUSIVETAX) {
		MIH_INCLUSIVETAX = mIH_INCLUSIVETAX;
	}

	public String getMIH_AUD_STATUS() {
		return MIH_AUD_STATUS;
	}

	public void setMIH_AUD_STATUS(String mIH_AUD_STATUS) {
		MIH_AUD_STATUS = mIH_AUD_STATUS;
	}

	public Timestamp getMIH_AUD_DATE() {
		return MIH_AUD_DATE;
	}

	public void setMIH_AUD_DATE(Timestamp mIH_AUD_DATE) {
		MIH_AUD_DATE = mIH_AUD_DATE;
	}

	public String getMIH_TYPE_UPDATED() {
		return MIH_TYPE_UPDATED;
	}

	public void setMIH_TYPE_UPDATED(String mIH_TYPE_UPDATED) {
		MIH_TYPE_UPDATED = mIH_TYPE_UPDATED;
	}

	public String getMIH_LOOSE_SALE_ALLOWED() {
		return MIH_LOOSE_SALE_ALLOWED;
	}

	public void setMIH_LOOSE_SALE_ALLOWED(String mIH_LOOSE_SALE_ALLOWED) {
		MIH_LOOSE_SALE_ALLOWED = mIH_LOOSE_SALE_ALLOWED;
	}

	public Integer getMIH_MIN_BILL_QTY() {
		return MIH_MIN_BILL_QTY;
	}

	public void setMIH_MIN_BILL_QTY(Integer mIH_MIN_BILL_QTY) {
		MIH_MIN_BILL_QTY = mIH_MIN_BILL_QTY;
	}

	public String getMIH_BE_CONF() {
		return MIH_BE_CONF;
	}

	public void setMIH_BE_CONF(String mIH_BE_CONF) {
		MIH_BE_CONF = mIH_BE_CONF;
	}

	public String getMIH_PREPARED() {
		return MIH_PREPARED;
	}

	public void setMIH_PREPARED(String mIH_PREPARED) {
		MIH_PREPARED = mIH_PREPARED;
	}

	public String getMIH_TRADE_CONF() {
		return MIH_TRADE_CONF;
	}

	public void setMIH_TRADE_CONF(String mIH_TRADE_CONF) {
		MIH_TRADE_CONF = mIH_TRADE_CONF;
	}

	public String getMIH_ISSCHEME() {
		return MIH_ISSCHEME;
	}

	public void setMIH_ISSCHEME(String mIH_ISSCHEME) {
		MIH_ISSCHEME = mIH_ISSCHEME;
	}

	public Integer getMIH_CATEGORY_1() {
		return MIH_CATEGORY_1;
	}

	public void setMIH_CATEGORY_1(Integer mIH_CATEGORY_1) {
		MIH_CATEGORY_1 = mIH_CATEGORY_1;
	}

	public Integer getMIH_CATEGORY_2() {
		return MIH_CATEGORY_2;
	}

	public void setMIH_CATEGORY_2(Integer mIH_CATEGORY_2) {
		MIH_CATEGORY_2 = mIH_CATEGORY_2;
	}

	public Integer getMIH_CATEGORY_3() {
		return MIH_CATEGORY_3;
	}

	public void setMIH_CATEGORY_3(Integer mIH_CATEGORY_3) {
		MIH_CATEGORY_3 = mIH_CATEGORY_3;
	}

	public Integer getMIH_CATEGORY_4() {
		return MIH_CATEGORY_4;
	}

	public void setMIH_CATEGORY_4(Integer mIH_CATEGORY_4) {
		MIH_CATEGORY_4 = mIH_CATEGORY_4;
	}

	public Integer getMIH_CATEGORY_5() {
		return MIH_CATEGORY_5;
	}

	public void setMIH_CATEGORY_5(Integer mIH_CATEGORY_5) {
		MIH_CATEGORY_5 = mIH_CATEGORY_5;
	}

	public Integer getMIH_CATEGORY_6() {
		return MIH_CATEGORY_6;
	}

	public void setMIH_CATEGORY_6(Integer mIH_CATEGORY_6) {
		MIH_CATEGORY_6 = mIH_CATEGORY_6;
	}

	public Integer getMIH_CATEGORY_7() {
		return MIH_CATEGORY_7;
	}

	public void setMIH_CATEGORY_7(Integer mIH_CATEGORY_7) {
		MIH_CATEGORY_7 = mIH_CATEGORY_7;
	}

	public Integer getMIH_CATEGORY_8() {
		return MIH_CATEGORY_8;
	}

	public void setMIH_CATEGORY_8(Integer mIH_CATEGORY_8) {
		MIH_CATEGORY_8 = mIH_CATEGORY_8;
	}

	public Integer getMIH_CATEGORY_9() {
		return MIH_CATEGORY_9;
	}

	public void setMIH_CATEGORY_9(Integer mIH_CATEGORY_9) {
		MIH_CATEGORY_9 = mIH_CATEGORY_9;
	}

	public Integer getMIH_CATEGORY_10() {
		return MIH_CATEGORY_10;
	}

	public void setMIH_CATEGORY_10(Integer mIH_CATEGORY_10) {
		MIH_CATEGORY_10 = mIH_CATEGORY_10;
	}

	public Integer getMIH_ITEM_RECEIPT_UNIT() {
		return MIH_ITEM_RECEIPT_UNIT;
	}

	public void setMIH_ITEM_RECEIPT_UNIT(Integer mIH_ITEM_RECEIPT_UNIT) {
		MIH_ITEM_RECEIPT_UNIT = mIH_ITEM_RECEIPT_UNIT;
	}

	public Double getMIH_RST_TAX() {
		return MIH_RST_TAX;
	}

	public void setMIH_RST_TAX(Double mIH_RST_TAX) {
		MIH_RST_TAX = mIH_RST_TAX;
	}

	public Double getMIH_RST_SURRST_TAX() {
		return MIH_RST_SURRST_TAX;
	}

	public void setMIH_RST_SURRST_TAX(Double mIH_RST_SURRST_TAX) {
		MIH_RST_SURRST_TAX = mIH_RST_SURRST_TAX;
	}

	public String getMIH_ITEM_ALIAS() {
		return MIH_ITEM_ALIAS;
	}

	public void setMIH_ITEM_ALIAS(String mIH_ITEM_ALIAS) {
		MIH_ITEM_ALIAS = mIH_ITEM_ALIAS;
	}

	public Double getMih_item_bulk_code() {
		return mih_item_bulk_code;
	}

	public void setMih_item_bulk_code(Double mih_item_bulk_code) {
		this.mih_item_bulk_code = mih_item_bulk_code;
	}

	public Double getMIH_ITEM_REPACK_CONVERTION() {
		return MIH_ITEM_REPACK_CONVERTION;
	}

	public void setMIH_ITEM_REPACK_CONVERTION(Double mIH_ITEM_REPACK_CONVERTION) {
		MIH_ITEM_REPACK_CONVERTION = mIH_ITEM_REPACK_CONVERTION;
	}

	public Double getMih_decimal_point() {
		return mih_decimal_point;
	}

	public void setMih_decimal_point(Double mih_decimal_point) {
		this.mih_decimal_point = mih_decimal_point;
	}

	public Double getMih_grind_per() {
		return mih_grind_per;
	}

	public void setMih_grind_per(Double mih_grind_per) {
		this.mih_grind_per = mih_grind_per;
	}

	public String getMih_cust_off() {
		return mih_cust_off;
	}

	public void setMih_cust_off(String mih_cust_off) {
		this.mih_cust_off = mih_cust_off;
	}

	public String getMih_remain_pass() {
		return mih_remain_pass;
	}

	public void setMih_remain_pass(String mih_remain_pass) {
		this.mih_remain_pass = mih_remain_pass;
	}

	public Double getMih_remain_qty() {
		return mih_remain_qty;
	}

	public void setMih_remain_qty(Double mih_remain_qty) {
		this.mih_remain_qty = mih_remain_qty;
	}

	public Integer getMih_compid() {
		return mih_compid;
	}

	public void setMih_compid(Integer mih_compid) {
		this.mih_compid = mih_compid;
	}

	public Integer getMih_diviid() {
		return mih_diviid;
	}

	public void setMih_diviid(Integer mih_diviid) {
		this.mih_diviid = mih_diviid;
	}

	public String getMIH_EANCODE() {
		return MIH_EANCODE;
	}

	public void setMIH_EANCODE(String mIH_EANCODE) {
		MIH_EANCODE = mIH_EANCODE;
	}

	public Timestamp getMIH_CREATED_DATE() {
		return MIH_CREATED_DATE;
	}

	public void setMIH_CREATED_DATE(Timestamp mIH_CREATED_DATE) {
		MIH_CREATED_DATE = mIH_CREATED_DATE;
	}

	public String getMih_unit() {
		return mih_unit;
	}

	public void setMih_unit(String mih_unit) {
		this.mih_unit = mih_unit;
	}

	public String getMih_item_name_short() {
		return mih_item_name_short;
	}

	public void setMih_item_name_short(String mih_item_name_short) {
		this.mih_item_name_short = mih_item_name_short;
	}

	public Integer getMIH_SALETAX_CODE() {
		return MIH_SALETAX_CODE;
	}

	public void setMIH_SALETAX_CODE(Integer mIH_SALETAX_CODE) {
		MIH_SALETAX_CODE = mIH_SALETAX_CODE;
	}

	public Integer getMIH_PURTAX_CODE() {
		return MIH_PURTAX_CODE;
	}

	public void setMIH_PURTAX_CODE(Integer mIH_PURTAX_CODE) {
		MIH_PURTAX_CODE = mIH_PURTAX_CODE;
	}

	public Integer getMIH_REP_CODE() {
		return MIH_REP_CODE;
	}

	public void setMIH_REP_CODE(Integer mIH_REP_CODE) {
		MIH_REP_CODE = mIH_REP_CODE;
	}

	public Double getMIH_REP_COMM() {
		return MIH_REP_COMM;
	}

	public void setMIH_REP_COMM(Double mIH_REP_COMM) {
		MIH_REP_COMM = mIH_REP_COMM;
	}

	public Timestamp getTS() {
		return TS;
	}

	public void setTS(Timestamp tS) {
		TS = tS;
	}

	public Integer getTSID() {
		return TSID;
	}

	public void setTSID(Integer tSID) {
		TSID = tSID;
	}

	public Double getMih_rep_comm_amt() {
		return mih_rep_comm_amt;
	}

	public void setMih_rep_comm_amt(Double mih_rep_comm_amt) {
		this.mih_rep_comm_amt = mih_rep_comm_amt;
	}

	public Integer getMih_ValidMrp() {
		return mih_ValidMrp;
	}

	public void setMih_ValidMrp(Integer mih_ValidMrp) {
		this.mih_ValidMrp = mih_ValidMrp;
	}

	public Integer getMIH_CATEGORY_11() {
		return MIH_CATEGORY_11;
	}

	public void setMIH_CATEGORY_11(Integer mIH_CATEGORY_11) {
		MIH_CATEGORY_11 = mIH_CATEGORY_11;
	}

	public Timestamp getMih_phy_vfn_date() {
		return mih_phy_vfn_date;
	}

	public void setMih_phy_vfn_date(Timestamp mih_phy_vfn_date) {
		this.mih_phy_vfn_date = mih_phy_vfn_date;
	}

	public String getMih_weight() {
		return mih_weight;
	}

	public void setMih_weight(String mih_weight) {
		this.mih_weight = mih_weight;
	}

	public Integer getMIH_LOYALTY_ALLOWED() {
		return MIH_LOYALTY_ALLOWED;
	}

	public void setMIH_LOYALTY_ALLOWED(Integer mIH_LOYALTY_ALLOWED) {
		MIH_LOYALTY_ALLOWED = mIH_LOYALTY_ALLOWED;
	}

	public String getMih_pkd_conf() {
		return mih_pkd_conf;
	}

	public void setMih_pkd_conf(String mih_pkd_conf) {
		this.mih_pkd_conf = mih_pkd_conf;
	}

	public Integer getMih_shelf_life() {
		return mih_shelf_life;
	}

	public void setMih_shelf_life(Integer mih_shelf_life) {
		this.mih_shelf_life = mih_shelf_life;
	}

	public Integer getMih_min_shelf_life() {
		return mih_min_shelf_life;
	}

	public void setMih_min_shelf_life(Integer mih_min_shelf_life) {
		this.mih_min_shelf_life = mih_min_shelf_life;
	}

	public Integer getMih_min_shelf_life_perc() {
		return mih_min_shelf_life_perc;
	}

	public void setMih_min_shelf_life_perc(Integer mih_min_shelf_life_perc) {
		this.mih_min_shelf_life_perc = mih_min_shelf_life_perc;
	}

	public Integer getMIH_PUF() {
		return MIH_PUF;
	}

	public void setMIH_PUF(Integer mIH_PUF) {
		MIH_PUF = mIH_PUF;
	}

	public Integer getMIH_SUF() {
		return MIH_SUF;
	}

	public void setMIH_SUF(Integer mIH_SUF) {
		MIH_SUF = mIH_SUF;
	}

	public Integer getMIH_SIZE() {
		return MIH_SIZE;
	}

	public void setMIH_SIZE(Integer mIH_SIZE) {
		MIH_SIZE = mIH_SIZE;
	}

	public Integer getMIH_LEAD_TIME() {
		return MIH_LEAD_TIME;
	}

	public void setMIH_LEAD_TIME(Integer mIH_LEAD_TIME) {
		MIH_LEAD_TIME = mIH_LEAD_TIME;
	}

	public Integer getMIH_TOLE_LMT() {
		return MIH_TOLE_LMT;
	}

	public void setMIH_TOLE_LMT(Integer mIH_TOLE_LMT) {
		MIH_TOLE_LMT = mIH_TOLE_LMT;
	}

	public String getMIH_MARGIN_BASED() {
		return MIH_MARGIN_BASED;
	}

	public void setMIH_MARGIN_BASED(String mIH_MARGIN_BASED) {
		MIH_MARGIN_BASED = mIH_MARGIN_BASED;
	}

	public String getMIH_SKU_TYPE() {
		return MIH_SKU_TYPE;
	}

	public void setMIH_SKU_TYPE(String mIH_SKU_TYPE) {
		MIH_SKU_TYPE = mIH_SKU_TYPE;
	}

	public String getMIH_SKU_ORD_GRP() {
		return MIH_SKU_ORD_GRP;
	}

	public void setMIH_SKU_ORD_GRP(String mIH_SKU_ORD_GRP) {
		MIH_SKU_ORD_GRP = mIH_SKU_ORD_GRP;
	}

	public Integer getMIH_REPL_TIME() {
		return MIH_REPL_TIME;
	}

	public void setMIH_REPL_TIME(Integer mIH_REPL_TIME) {
		MIH_REPL_TIME = mIH_REPL_TIME;
	}

	public Integer getMIH_BUFFER_DAYS() {
		return MIH_BUFFER_DAYS;
	}

	public void setMIH_BUFFER_DAYS(Integer mIH_BUFFER_DAYS) {
		MIH_BUFFER_DAYS = mIH_BUFFER_DAYS;
	}

	public Double getMIH_GATE_KEEP_MARGIN() {
		return MIH_GATE_KEEP_MARGIN;
	}

	public void setMIH_GATE_KEEP_MARGIN(Double mIH_GATE_KEEP_MARGIN) {
		MIH_GATE_KEEP_MARGIN = mIH_GATE_KEEP_MARGIN;
	}

	public Integer getMIH_MSF() {
		return MIH_MSF;
	}

	public void setMIH_MSF(Integer mIH_MSF) {
		MIH_MSF = mIH_MSF;
	}

	public String getMIH_INDENT_EDIT() {
		return MIH_INDENT_EDIT;
	}

	public void setMIH_INDENT_EDIT(String mIH_INDENT_EDIT) {
		MIH_INDENT_EDIT = mIH_INDENT_EDIT;
	}

	public String getMIH_AVAILABILITY() {
		return MIH_AVAILABILITY;
	}

	public void setMIH_AVAILABILITY(String mIH_AVAILABILITY) {
		MIH_AVAILABILITY = mIH_AVAILABILITY;
	}

	public Integer getMih_send_weight() {
		return mih_send_weight;
	}

	public void setMih_send_weight(Integer mih_send_weight) {
		this.mih_send_weight = mih_send_weight;
	}

	public Integer getMih_weight_sale_type() {
		return mih_weight_sale_type;
	}

	public void setMih_weight_sale_type(Integer mih_weight_sale_type) {
		this.mih_weight_sale_type = mih_weight_sale_type;
	}

	public Double getMih_reorder_qty() {
		return mih_reorder_qty;
	}

	public void setMih_reorder_qty(Double mih_reorder_qty) {
		this.mih_reorder_qty = mih_reorder_qty;
	}

	public String getMih_allow_discount() {
		return mih_allow_discount;
	}

	public void setMih_allow_discount(String mih_allow_discount) {
		this.mih_allow_discount = mih_allow_discount;
	}

	public String getMih_allow_addcharge() {
		return mih_allow_addcharge;
	}

	public void setMih_allow_addcharge(String mih_allow_addcharge) {
		this.mih_allow_addcharge = mih_allow_addcharge;
	}

	public Integer getMih_reorder_min_days() {
		return mih_reorder_min_days;
	}

	public void setMih_reorder_min_days(Integer mih_reorder_min_days) {
		this.mih_reorder_min_days = mih_reorder_min_days;
	}

	public Integer getMih_reorder_max_days() {
		return mih_reorder_max_days;
	}

	public void setMih_reorder_max_days(Integer mih_reorder_max_days) {
		this.mih_reorder_max_days = mih_reorder_max_days;
	}

	public String getMIH_ALLOW_SALE_RETURN() {
		return MIH_ALLOW_SALE_RETURN;
	}

	public void setMIH_ALLOW_SALE_RETURN(String mIH_ALLOW_SALE_RETURN) {
		MIH_ALLOW_SALE_RETURN = mIH_ALLOW_SALE_RETURN;
	}

	public String getMIH_ALLOW_RATE_EDIT() {
		return MIH_ALLOW_RATE_EDIT;
	}

	public void setMIH_ALLOW_RATE_EDIT(String mIH_ALLOW_RATE_EDIT) {
		MIH_ALLOW_RATE_EDIT = mIH_ALLOW_RATE_EDIT;
	}

	public String getMIH_ALLOW_NEGATIVE_STOCK() {
		return MIH_ALLOW_NEGATIVE_STOCK;
	}

	public void setMIH_ALLOW_NEGATIVE_STOCK(String mIH_ALLOW_NEGATIVE_STOCK) {
		MIH_ALLOW_NEGATIVE_STOCK = mIH_ALLOW_NEGATIVE_STOCK;
	}

	public Integer getMIH_ProdType() {
		return MIH_ProdType;
	}

	public void setMIH_ProdType(Integer mIH_ProdType) {
		MIH_ProdType = mIH_ProdType;
	}

	public Integer getMih_warranty_month() {
		return Mih_warranty_month;
	}

	public void setMih_warranty_month(Integer mih_warranty_month) {
		Mih_warranty_month = mih_warranty_month;
	}

	public Integer getMih_Serviceable() {
		return Mih_Serviceable;
	}

	public void setMih_Serviceable(Integer mih_Serviceable) {
		Mih_Serviceable = mih_Serviceable;
	}

	public Integer getMih_ex_duty_code() {
		return mih_ex_duty_code;
	}

	public void setMih_ex_duty_code(Integer mih_ex_duty_code) {
		this.mih_ex_duty_code = mih_ex_duty_code;
	}

	public Double getMih_item_weight() {
		return mih_item_weight;
	}

	public void setMih_item_weight(Double mih_item_weight) {
		this.mih_item_weight = mih_item_weight;
	}

	public String getMih_allow_sales() {
		return mih_allow_sales;
	}

	public void setMih_allow_sales(String mih_allow_sales) {
		this.mih_allow_sales = mih_allow_sales;
	}

	public String getMih_batch_conf() {
		return mih_batch_conf;
	}

	public void setMih_batch_conf(String mih_batch_conf) {
		this.mih_batch_conf = mih_batch_conf;
	}

	public String getMIH_ALLOW_PURCHASE() {
		return MIH_ALLOW_PURCHASE;
	}

	public void setMIH_ALLOW_PURCHASE(String mIH_ALLOW_PURCHASE) {
		MIH_ALLOW_PURCHASE = mIH_ALLOW_PURCHASE;
	}

	public Integer getRETAIL_OUTLET_ID() {
		return RETAIL_OUTLET_ID;
	}

	public void setRETAIL_OUTLET_ID(Integer rETAIL_OUTLET_ID) {
		RETAIL_OUTLET_ID = rETAIL_OUTLET_ID;
	}

	public Integer getMih_Commodity_code() {
		return mih_Commodity_code;
	}

	public void setMih_Commodity_code(Integer mih_Commodity_code) {
		this.mih_Commodity_code = mih_Commodity_code;
	}

	public String getMIH_SERVICE_TAX() {
		return MIH_SERVICE_TAX;
	}

	public void setMIH_SERVICE_TAX(String mIH_SERVICE_TAX) {
		MIH_SERVICE_TAX = mIH_SERVICE_TAX;
	}

	public Integer getMih_mat_tableid() {
		return mih_mat_tableid;
	}

	public void setMih_mat_tableid(Integer mih_mat_tableid) {
		this.mih_mat_tableid = mih_mat_tableid;
	}

	public Double getMIH_ITEM_DISC_PERC() {
		return MIH_ITEM_DISC_PERC;
	}

	public void setMIH_ITEM_DISC_PERC(Double mIH_ITEM_DISC_PERC) {
		MIH_ITEM_DISC_PERC = mIH_ITEM_DISC_PERC;
	}

	public Double getMIH_ITEM_DISC_AMT() {
		return MIH_ITEM_DISC_AMT;
	}

	public void setMIH_ITEM_DISC_AMT(Double mIH_ITEM_DISC_AMT) {
		MIH_ITEM_DISC_AMT = mIH_ITEM_DISC_AMT;
	}

	public Integer getMih_item_group() {
		return mih_item_group;
	}

	public void setMih_item_group(Integer mih_item_group) {
		this.mih_item_group = mih_item_group;
	}

	public Integer getMIH_APPLIES_ONLINE() {
		return MIH_APPLIES_ONLINE;
	}

	public void setMIH_APPLIES_ONLINE(Integer mIH_APPLIES_ONLINE) {
		MIH_APPLIES_ONLINE = mIH_APPLIES_ONLINE;
	}

	public Integer getMih_Container_Allowed() {
		return mih_Container_Allowed;
	}

	public void setMih_Container_Allowed(Integer mih_Container_Allowed) {
		this.mih_Container_Allowed = mih_Container_Allowed;
	}

	public Integer getMih_price_conf_id() {
		return mih_price_conf_id;
	}

	public void setMih_price_conf_id(Integer mih_price_conf_id) {
		this.mih_price_conf_id = mih_price_conf_id;
	}

	public Integer getMih_barcode_print() {
		return mih_barcode_print;
	}

	public void setMih_barcode_print(Integer mih_barcode_print) {
		this.mih_barcode_print = mih_barcode_print;
	}

	public Integer getMih_Allow_Half_Pizza() {
		return Mih_Allow_Half_Pizza;
	}

	public void setMih_Allow_Half_Pizza(Integer mih_Allow_Half_Pizza) {
		Mih_Allow_Half_Pizza = mih_Allow_Half_Pizza;
	}

	public Double getMih_min_selling() {
		return mih_min_selling;
	}

	public void setMih_min_selling(Double mih_min_selling) {
		this.mih_min_selling = mih_min_selling;
	}

	public String getMIH_ALLOW_WASTAGE() {
		return MIH_ALLOW_WASTAGE;
	}

	public void setMIH_ALLOW_WASTAGE(String mIH_ALLOW_WASTAGE) {
		MIH_ALLOW_WASTAGE = mIH_ALLOW_WASTAGE;
	}

	public String getMih_kindofbook() {
		return mih_kindofbook;
	}

	public void setMih_kindofbook(String mih_kindofbook) {
		this.mih_kindofbook = mih_kindofbook;
	}

	public String getMih_title() {
		return mih_title;
	}

	public void setMih_title(String mih_title) {
		this.mih_title = mih_title;
	}

	public String getMih_YOP() {
		return mih_YOP;
	}

	public void setMih_YOP(String mih_YOP) {
		this.mih_YOP = mih_YOP;
	}

	public Integer getMih_show_caption() {
		return mih_show_caption;
	}

	public void setMih_show_caption(Integer mih_show_caption) {
		this.mih_show_caption = mih_show_caption;
	}

	public Integer getMih_text_align() {
		return mih_text_align;
	}

	public void setMih_text_align(Integer mih_text_align) {
		this.mih_text_align = mih_text_align;
	}

	public Integer getMih_image_align() {
		return mih_image_align;
	}

	public void setMih_image_align(Integer mih_image_align) {
		this.mih_image_align = mih_image_align;
	}

	public Integer getMih_select_rate_conf() {
		return mih_select_rate_conf;
	}

	public void setMih_select_rate_conf(Integer mih_select_rate_conf) {
		this.mih_select_rate_conf = mih_select_rate_conf;
	}

	public String getMih_variant_conf() {
		return mih_variant_conf;
	}

	public void setMih_variant_conf(String mih_variant_conf) {
		this.mih_variant_conf = mih_variant_conf;
	}

	public String getMih_item_uom() {
		return mih_item_uom;
	}

	public void setMih_item_uom(String mih_item_uom) {
		this.mih_item_uom = mih_item_uom;
	}

	public String getMih_item_subUom() {
		return mih_item_subUom;
	}

	public void setMih_item_subUom(String mih_item_subUom) {
		this.mih_item_subUom = mih_item_subUom;
	}

	public Double getMih_uom_convertion() {
		return mih_uom_convertion;
	}

	public void setMih_uom_convertion(Double mih_uom_convertion) {
		this.mih_uom_convertion = mih_uom_convertion;
	}

	public String getMih_veg() {
		return mih_veg;
	}

	public void setMih_veg(String mih_veg) {
		this.mih_veg = mih_veg;
	}

	public Integer getMih_ShowOptional_Status() {
		return Mih_ShowOptional_Status;
	}

	public void setMih_ShowOptional_Status(Integer mih_ShowOptional_Status) {
		Mih_ShowOptional_Status = mih_ShowOptional_Status;
	}

	public String getMih_extra_desc() {
		return Mih_extra_desc;
	}

	public void setMih_extra_desc(String mih_extra_desc) {
		Mih_extra_desc = mih_extra_desc;
	}

	public String getMih_dish_prep() {
		return mih_dish_prep;
	}

	public void setMih_dish_prep(String mih_dish_prep) {
		this.mih_dish_prep = mih_dish_prep;
	}

	public String getMih_srvtax_conf() {
		return mih_srvtax_conf;
	}

	public void setMih_srvtax_conf(String mih_srvtax_conf) {
		this.mih_srvtax_conf = mih_srvtax_conf;
	}

	public String getMih_srvchrg_conf() {
		return mih_srvchrg_conf;
	}

	public void setMih_srvchrg_conf(String mih_srvchrg_conf) {
		this.mih_srvchrg_conf = mih_srvchrg_conf;
	}

	public String getMih_Eancode_As_Barcode() {
		return mih_Eancode_As_Barcode;
	}

	public void setMih_Eancode_As_Barcode(String mih_Eancode_As_Barcode) {
		this.mih_Eancode_As_Barcode = mih_Eancode_As_Barcode;
	}

	public Integer getMih_Free_Tax() {
		return mih_Free_Tax;
	}

	public void setMih_Free_Tax(Integer mih_Free_Tax) {
		this.mih_Free_Tax = mih_Free_Tax;
	}

	public Integer getMih_Favourite() {
		return Mih_Favourite;
	}

	public void setMih_Favourite(Integer mih_Favourite) {
		Mih_Favourite = mih_Favourite;
	}

	public Integer getMih_spiciness_level() {
		return Mih_spiciness_level;
	}

	public void setMih_spiciness_level(Integer mih_spiciness_level) {
		Mih_spiciness_level = mih_spiciness_level;
	}

	public String getMih_Excise_duty_applicable() {
		return mih_Excise_duty_applicable;
	}

	public void setMih_Excise_duty_applicable(String mih_Excise_duty_applicable) {
		this.mih_Excise_duty_applicable = mih_Excise_duty_applicable;
	}

	public String getMIH_SCM_ITEM_CODE() {
		return MIH_SCM_ITEM_CODE;
	}

	public void setMIH_SCM_ITEM_CODE(String mIH_SCM_ITEM_CODE) {
		MIH_SCM_ITEM_CODE = mIH_SCM_ITEM_CODE;
	}

	public String getMih_Allow_Change_Selling() {
		return mih_Allow_Change_Selling;
	}

	public void setMih_Allow_Change_Selling(String mih_Allow_Change_Selling) {
		this.mih_Allow_Change_Selling = mih_Allow_Change_Selling;
	}

	public String getMih_Noof_sticker() {
		return mih_Noof_sticker;
	}

	public void setMih_Noof_sticker(String mih_Noof_sticker) {
		this.mih_Noof_sticker = mih_Noof_sticker;
	}

	public String getMih_reorder_type() {
		return mih_reorder_type;
	}

	public void setMih_reorder_type(String mih_reorder_type) {
		this.mih_reorder_type = mih_reorder_type;
	}

	public String getMih_reorder_formula() {
		return mih_reorder_formula;
	}

	public void setMih_reorder_formula(String mih_reorder_formula) {
		this.mih_reorder_formula = mih_reorder_formula;
	}

	public String getMih_avg_NDays_sales() {
		return mih_avg_NDays_sales;
	}

	public void setMih_avg_NDays_sales(String mih_avg_NDays_sales) {
		this.mih_avg_NDays_sales = mih_avg_NDays_sales;
	}

	public String getMih_locaid() {
		return mih_locaid;
	}

	public void setMih_locaid(String mih_locaid) {
		this.mih_locaid = mih_locaid;
	}

	public Double getMih_MinStkLevel_Perc1() {
		return mih_MinStkLevel_Perc1;
	}

	public void setMih_MinStkLevel_Perc1(Double mih_MinStkLevel_Perc1) {
		this.mih_MinStkLevel_Perc1 = mih_MinStkLevel_Perc1;
	}

	public Double getMih_MinStkLevel_Perc2() {
		return mih_MinStkLevel_Perc2;
	}

	public void setMih_MinStkLevel_Perc2(Double mih_MinStkLevel_Perc2) {
		this.mih_MinStkLevel_Perc2 = mih_MinStkLevel_Perc2;
	}

	public Integer getMih_eancode_required() {
		return mih_eancode_required;
	}

	public void setMih_eancode_required(Integer mih_eancode_required) {
		this.mih_eancode_required = mih_eancode_required;
	}

	public String getMih_barcode_profile() {
		return mih_barcode_profile;
	}

	public void setMih_barcode_profile(String mih_barcode_profile) {
		this.mih_barcode_profile = mih_barcode_profile;
	}

	public String getMih_AllowLiquorTax() {
		return Mih_AllowLiquorTax;
	}

	public void setMih_AllowLiquorTax(String mih_AllowLiquorTax) {
		Mih_AllowLiquorTax = mih_AllowLiquorTax;
	}

	public String getMih_blockcombo_duplicate() {
		return mih_blockcombo_duplicate;
	}

	public void setMih_blockcombo_duplicate(String mih_blockcombo_duplicate) {
		this.mih_blockcombo_duplicate = mih_blockcombo_duplicate;
	}

	public String getMih_image_path() {
		return mih_image_path;
	}

	public void setMih_image_path(String mih_image_path) {
		this.mih_image_path = mih_image_path;
	}

	public Double getMih_poexcess_perc() {
		return mih_poexcess_perc;
	}

	public void setMih_poexcess_perc(Double mih_poexcess_perc) {
		this.mih_poexcess_perc = mih_poexcess_perc;
	}

	public Integer getMIH_GST_CODE() {
		return MIH_GST_CODE;
	}

	public void setMIH_GST_CODE(Integer mIH_GST_CODE) {
		MIH_GST_CODE = mIH_GST_CODE;
	}

	public String getMIH_HSN_CODE() {
		return MIH_HSN_CODE;
	}

	public void setMIH_HSN_CODE(String mIH_HSN_CODE) {
		MIH_HSN_CODE = mIH_HSN_CODE;
	}

	public String getMIH_PUR_CALC_GST_ON() {
		return MIH_PUR_CALC_GST_ON;
	}

	public void setMIH_PUR_CALC_GST_ON(String mIH_PUR_CALC_GST_ON) {
		MIH_PUR_CALC_GST_ON = mIH_PUR_CALC_GST_ON;
	}

	public String getMIH_PUR_ABATEMENT() {
		return MIH_PUR_ABATEMENT;
	}

	public void setMIH_PUR_ABATEMENT(String mIH_PUR_ABATEMENT) {
		MIH_PUR_ABATEMENT = mIH_PUR_ABATEMENT;
	}

	public String getMIH_SAL_CALC_GST_ON() {
		return MIH_SAL_CALC_GST_ON;
	}

	public void setMIH_SAL_CALC_GST_ON(String mIH_SAL_CALC_GST_ON) {
		MIH_SAL_CALC_GST_ON = mIH_SAL_CALC_GST_ON;
	}

	public String getMIH_SAL_ABATEMENT() {
		return MIH_SAL_ABATEMENT;
	}

	public void setMIH_SAL_ABATEMENT(String mIH_SAL_ABATEMENT) {
		MIH_SAL_ABATEMENT = mIH_SAL_ABATEMENT;
	}

	public String getMIH_Tax_CALC_ON() {
		return MIH_Tax_CALC_ON;
	}

	public void setMIH_Tax_CALC_ON(String mIH_Tax_CALC_ON) {
		MIH_Tax_CALC_ON = mIH_Tax_CALC_ON;
	}

	public String getMIH_Tax_type() {
		return MIH_Tax_type;
	}

	public void setMIH_Tax_type(String mIH_Tax_type) {
		MIH_Tax_type = mIH_Tax_type;
	}

	public String getMih_semifinished_kit() {
		return mih_semifinished_kit;
	}

	public void setMih_semifinished_kit(String mih_semifinished_kit) {
		this.mih_semifinished_kit = mih_semifinished_kit;
	}

	public String getMih_ExtraCess_Per_Qty() {
		return mih_ExtraCess_Per_Qty;
	}

	public void setMih_ExtraCess_Per_Qty(String mih_ExtraCess_Per_Qty) {
		this.mih_ExtraCess_Per_Qty = mih_ExtraCess_Per_Qty;
	}

	public String getMih_ItemSelect_Mode() {
		return mih_ItemSelect_Mode;
	}

	public void setMih_ItemSelect_Mode(String mih_ItemSelect_Mode) {
		this.mih_ItemSelect_Mode = mih_ItemSelect_Mode;
	}

	public String getMih_Daily_Phy_Audit() {
		return mih_Daily_Phy_Audit;
	}

	public void setMih_Daily_Phy_Audit(String mih_Daily_Phy_Audit) {
		this.mih_Daily_Phy_Audit = mih_Daily_Phy_Audit;
	}

	public String getMih_stk_Available_Wh() {
		return mih_stk_Available_Wh;
	}

	public void setMih_stk_Available_Wh(String mih_stk_Available_Wh) {
		this.mih_stk_Available_Wh = mih_stk_Available_Wh;
	}

	public String getMih_Ndays_NweekSales() {
		return mih_Ndays_NweekSales;
	}

	public void setMih_Ndays_NweekSales(String mih_Ndays_NweekSales) {
		this.mih_Ndays_NweekSales = mih_Ndays_NweekSales;
	}

	public String getMih_Perc_Nweek_sales() {
		return mih_Perc_Nweek_sales;
	}

	public void setMih_Perc_Nweek_sales(String mih_Perc_Nweek_sales) {
		this.mih_Perc_Nweek_sales = mih_Perc_Nweek_sales;
	}

	public String getMih_gst_uom() {
		return mih_gst_uom;
	}

	public void setMih_gst_uom(String mih_gst_uom) {
		this.mih_gst_uom = mih_gst_uom;
	}

	public String getMih_block_outlet_purchase() {
		return mih_block_outlet_purchase;
	}

	public void setMih_block_outlet_purchase(String mih_block_outlet_purchase) {
		this.mih_block_outlet_purchase = mih_block_outlet_purchase;
	}

	public String getMih_soexcess_perc() {
		return mih_soexcess_perc;
	}

	public void setMih_soexcess_perc(String mih_soexcess_perc) {
		this.mih_soexcess_perc = mih_soexcess_perc;
	}

	public String getMih_max_uniquebarcode() {
		return mih_max_uniquebarcode;
	}

	public void setMih_max_uniquebarcode(String mih_max_uniquebarcode) {
		this.mih_max_uniquebarcode = mih_max_uniquebarcode;
	}

	public Double getMIH_PUR_PRICE() {
		return MIH_PUR_PRICE;
	}

	public void setMIH_PUR_PRICE(Double mIH_PUR_PRICE) {
		MIH_PUR_PRICE = mIH_PUR_PRICE;
	}

	public String getMih_indent_notallow() {
		return mih_indent_notallow;
	}

	public void setMih_indent_notallow(String mih_indent_notallow) {
		this.mih_indent_notallow = mih_indent_notallow;
	}

	public String getMih_reorder_hold_days() {
		return mih_reorder_hold_days;
	}

	public void setMih_reorder_hold_days(String mih_reorder_hold_days) {
		this.mih_reorder_hold_days = mih_reorder_hold_days;
	}

	public String getMih_erp_ref_code() {
		return mih_erp_ref_code;
	}

	public void setMih_erp_ref_code(String mih_erp_ref_code) {
		this.mih_erp_ref_code = mih_erp_ref_code;
	}

	public String getMih_ret_days() {
		return mih_ret_days;
	}

	public void setMih_ret_days(String mih_ret_days) {
		this.mih_ret_days = mih_ret_days;
	}

}
