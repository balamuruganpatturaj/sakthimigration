package com.sakthi.groups.migration.gofrugal.model;

import java.io.Serializable;

public class MedMrcDtlId implements Serializable {

	public MedMrcDtlId() {
		super();
	}

	public MedMrcDtlId(Integer mmdMrcNo, Integer mMD_MRC_SL_NO) {
		super();
		this.mmdMrcNo = mmdMrcNo;
		MMD_MRC_SL_NO = mMD_MRC_SL_NO;
	}

	private static final long serialVersionUID = 1L;

	private Integer mmdMrcNo;
	private Integer MMD_MRC_SL_NO;

	public Integer getMmdMrcNo() {
		return mmdMrcNo;
	}

	public void setMmdMrcNo(Integer mmdMrcNo) {
		this.mmdMrcNo = mmdMrcNo;
	}

	public Integer getMMD_MRC_SL_NO() {
		return MMD_MRC_SL_NO;
	}

	public void setMMD_MRC_SL_NO(Integer mMD_MRC_SL_NO) {
		MMD_MRC_SL_NO = mMD_MRC_SL_NO;
	}

}
