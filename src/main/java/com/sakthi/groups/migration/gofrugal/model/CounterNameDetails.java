package com.sakthi.groups.migration.gofrugal.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "COUNTER_NAME")
public class CounterNameDetails {

	@Id
	@Column(name = "cn_ctr_code")
	private String cn_ctr_code;

	@Column(name = "cn_ctr_desc")
	private String cn_ctr_desc;

	@Column(name = "cn_compid")
	private Double cn_compid;

	@Column(name = "cn_diviid")
	private Double cn_diviid;

	@Column(name = "TS")
	private Timestamp TS;

	@Column(name = "TSID")
	private Integer TSID;

	@Column(name = "cn_ctr_tag")
	private String cn_ctr_tag;

	@Column(name = "cn_IpAddress")
	private String cn_IpAddress;

	@Column(name = "RETAIL_OUTLET_ID")
	private Integer RETAIL_OUTLET_ID;

	@Column(name = "Cn_LastBackup_Time")
	private Timestamp Cn_LastBackup_Time;

	@Column(name = "Cn_FullBackUp_Completed")
	private String Cn_FullBackUp_Completed;

	@Column(name = "Cn_Allow_OnlineSync")
	private String Cn_Allow_OnlineSync;

	@Column(name = "Cn_Client_Name")
	private String Cn_Client_Name;

	@Column(name = "cn_tenant_code")
	private Integer cn_tenant_code;

	@Column(name = "cn_biometric_enable")
	private Integer cn_biometric_enable;

	@Column(name = "CN_REGISTER_KEY")
	private String CN_REGISTER_KEY;

	@Column(name = "CN_ADDON_TYPE")
	private Integer CN_ADDON_TYPE;

	@Column(name = "CN_START_SEQ")
	private Integer CN_START_SEQ;

	@Column(name = "CN_PROFILE_CODE")
	private Integer CN_PROFILE_CODE;

	@Column(name = "CN_LOCATION_CODE")
	private Integer CN_LOCATION_CODE;

	@Column(name = "CN_CATEGORY_MAPPED")
	private Integer CN_CATEGORY_MAPPED;

	@Column(name = "cn_addon_payment_group")
	private Integer cn_addon_payment_group;

	@Column(name = "cn_pdc_Status")
	private Integer cn_pdc_Status;

	@Column(name = "cn_dbrestore_status")
	private Integer cn_dbrestore_status;

	@Column(name = "Cn_Bill_location_code")
	private Integer Cn_Bill_location_code;

	public String getCn_ctr_code() {
		return cn_ctr_code;
	}

	public void setCn_ctr_code(String cn_ctr_code) {
		this.cn_ctr_code = cn_ctr_code;
	}

	public String getCn_ctr_desc() {
		return cn_ctr_desc;
	}

	public void setCn_ctr_desc(String cn_ctr_desc) {
		this.cn_ctr_desc = cn_ctr_desc;
	}

	public Double getCn_compid() {
		return cn_compid;
	}

	public void setCn_compid(Double cn_compid) {
		this.cn_compid = cn_compid;
	}

	public Double getCn_diviid() {
		return cn_diviid;
	}

	public void setCn_diviid(Double cn_diviid) {
		this.cn_diviid = cn_diviid;
	}

	public Timestamp getTS() {
		return TS;
	}

	public void setTS(Timestamp tS) {
		TS = tS;
	}

	public Integer getTSID() {
		return TSID;
	}

	public void setTSID(Integer tSID) {
		TSID = tSID;
	}

	public String getCn_ctr_tag() {
		return cn_ctr_tag;
	}

	public void setCn_ctr_tag(String cn_ctr_tag) {
		this.cn_ctr_tag = cn_ctr_tag;
	}

	public String getCn_IpAddress() {
		return cn_IpAddress;
	}

	public void setCn_IpAddress(String cn_IpAddress) {
		this.cn_IpAddress = cn_IpAddress;
	}

	public Integer getRETAIL_OUTLET_ID() {
		return RETAIL_OUTLET_ID;
	}

	public void setRETAIL_OUTLET_ID(Integer rETAIL_OUTLET_ID) {
		RETAIL_OUTLET_ID = rETAIL_OUTLET_ID;
	}

	public Timestamp getCn_LastBackup_Time() {
		return Cn_LastBackup_Time;
	}

	public void setCn_LastBackup_Time(Timestamp cn_LastBackup_Time) {
		Cn_LastBackup_Time = cn_LastBackup_Time;
	}

	public String getCn_FullBackUp_Completed() {
		return Cn_FullBackUp_Completed;
	}

	public void setCn_FullBackUp_Completed(String cn_FullBackUp_Completed) {
		Cn_FullBackUp_Completed = cn_FullBackUp_Completed;
	}

	public String getCn_Allow_OnlineSync() {
		return Cn_Allow_OnlineSync;
	}

	public void setCn_Allow_OnlineSync(String cn_Allow_OnlineSync) {
		Cn_Allow_OnlineSync = cn_Allow_OnlineSync;
	}

	public String getCn_Client_Name() {
		return Cn_Client_Name;
	}

	public void setCn_Client_Name(String cn_Client_Name) {
		Cn_Client_Name = cn_Client_Name;
	}

	public Integer getCn_tenant_code() {
		return cn_tenant_code;
	}

	public void setCn_tenant_code(Integer cn_tenant_code) {
		this.cn_tenant_code = cn_tenant_code;
	}

	public Integer getCn_biometric_enable() {
		return cn_biometric_enable;
	}

	public void setCn_biometric_enable(Integer cn_biometric_enable) {
		this.cn_biometric_enable = cn_biometric_enable;
	}

	public String getCN_REGISTER_KEY() {
		return CN_REGISTER_KEY;
	}

	public void setCN_REGISTER_KEY(String cN_REGISTER_KEY) {
		CN_REGISTER_KEY = cN_REGISTER_KEY;
	}

	public Integer getCN_ADDON_TYPE() {
		return CN_ADDON_TYPE;
	}

	public void setCN_ADDON_TYPE(Integer cN_ADDON_TYPE) {
		CN_ADDON_TYPE = cN_ADDON_TYPE;
	}

	public Integer getCN_START_SEQ() {
		return CN_START_SEQ;
	}

	public void setCN_START_SEQ(Integer cN_START_SEQ) {
		CN_START_SEQ = cN_START_SEQ;
	}

	public Integer getCN_PROFILE_CODE() {
		return CN_PROFILE_CODE;
	}

	public void setCN_PROFILE_CODE(Integer cN_PROFILE_CODE) {
		CN_PROFILE_CODE = cN_PROFILE_CODE;
	}

	public Integer getCN_LOCATION_CODE() {
		return CN_LOCATION_CODE;
	}

	public void setCN_LOCATION_CODE(Integer cN_LOCATION_CODE) {
		CN_LOCATION_CODE = cN_LOCATION_CODE;
	}

	public Integer getCN_CATEGORY_MAPPED() {
		return CN_CATEGORY_MAPPED;
	}

	public void setCN_CATEGORY_MAPPED(Integer cN_CATEGORY_MAPPED) {
		CN_CATEGORY_MAPPED = cN_CATEGORY_MAPPED;
	}

	public Integer getCn_addon_payment_group() {
		return cn_addon_payment_group;
	}

	public void setCn_addon_payment_group(Integer cn_addon_payment_group) {
		this.cn_addon_payment_group = cn_addon_payment_group;
	}

	public Integer getCn_pdc_Status() {
		return cn_pdc_Status;
	}

	public void setCn_pdc_Status(Integer cn_pdc_Status) {
		this.cn_pdc_Status = cn_pdc_Status;
	}

	public Integer getCn_dbrestore_status() {
		return cn_dbrestore_status;
	}

	public void setCn_dbrestore_status(Integer cn_dbrestore_status) {
		this.cn_dbrestore_status = cn_dbrestore_status;
	}

	public Integer getCn_Bill_location_code() {
		return Cn_Bill_location_code;
	}

	public void setCn_Bill_location_code(Integer cn_Bill_location_code) {
		Cn_Bill_location_code = cn_Bill_location_code;
	}

}
