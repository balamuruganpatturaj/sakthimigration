package com.sakthi.groups.migration.gofrugal.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "med_price_level")
public class MedPriceLevel {

	@Id
	@Column(name = "mpl_code")
	private Integer mpl_code;

	@Column(name = "mpl_name")
	private String mpl_name;

	@Column(name = "TS")
	private Timestamp TS;

	@Column(name = "TSID")
	private Integer TSID;

	@Column(name = "mpl_short_name")
	private String mpl_short_name;

	@Column(name = "RETAIL_OUTLET_ID")
	private Integer RETAIL_OUTLET_ID;

	@Column(name = "mpl_type")
	private String mpl_type;

	@Column(name = "mpl_status")
	private String mpl_status;

	@Column(name = "mpl_parent_category")
	private String mpl_parent_category;

	@Column(name = "mpl_Hq_ref_id")
	private String mpl_Hq_ref_id;

	public Integer getMpl_code() {
		return mpl_code;
	}

	public void setMpl_code(Integer mpl_code) {
		this.mpl_code = mpl_code;
	}

	public String getMpl_name() {
		return mpl_name;
	}

	public void setMpl_name(String mpl_name) {
		this.mpl_name = mpl_name;
	}

	public Timestamp getTS() {
		return TS;
	}

	public void setTS(Timestamp tS) {
		TS = tS;
	}

	public Integer getTSID() {
		return TSID;
	}

	public void setTSID(Integer tSID) {
		TSID = tSID;
	}

	public String getMpl_short_name() {
		return mpl_short_name;
	}

	public void setMpl_short_name(String mpl_short_name) {
		this.mpl_short_name = mpl_short_name;
	}

	public Integer getRETAIL_OUTLET_ID() {
		return RETAIL_OUTLET_ID;
	}

	public void setRETAIL_OUTLET_ID(Integer rETAIL_OUTLET_ID) {
		RETAIL_OUTLET_ID = rETAIL_OUTLET_ID;
	}

	public String getMpl_type() {
		return mpl_type;
	}

	public void setMpl_type(String mpl_type) {
		this.mpl_type = mpl_type;
	}

	public String getMpl_status() {
		return mpl_status;
	}

	public void setMpl_status(String mpl_status) {
		this.mpl_status = mpl_status;
	}

	public String getMpl_parent_category() {
		return mpl_parent_category;
	}

	public void setMpl_parent_category(String mpl_parent_category) {
		this.mpl_parent_category = mpl_parent_category;
	}

	public String getMpl_Hq_ref_id() {
		return mpl_Hq_ref_id;
	}

	public void setMpl_Hq_ref_id(String mpl_Hq_ref_id) {
		this.mpl_Hq_ref_id = mpl_Hq_ref_id;
	}
}
