package com.sakthi.groups.migration.gofrugal.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.sakthi.groups.migration.gofrugal.model.MedMrcHdr;

public interface MedMrcHdrRepository extends CrudRepository<MedMrcHdr, Integer> {

	public List<MedMrcHdr> findByMmhMrcDateGreaterThan(Date findDate);

	public List<MedMrcHdr> findByMmhDistCodeOrderByMmhDistBillDtDesc(Integer mmhDistCode);
	
	@Query("SELECT u FROM MedMrcHdr u WHERE u.MMH_MRC_NO = ?1")
	public MedMrcHdr findByMmhMrcNo(Integer mmhMrcNo);
}
