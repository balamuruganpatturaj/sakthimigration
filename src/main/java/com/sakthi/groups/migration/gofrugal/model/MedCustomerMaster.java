package com.sakthi.groups.migration.gofrugal.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MED_CUSTOMER_MAST")
public class MedCustomerMaster {

	@Id
	@Column(name = "MCM_CUST_CODE")
	private Integer MCM_CUST_CODE;
	@Column(name = "MCM_CUST_NAME")
	private String MCM_CUST_NAME;
	@Column(name = "MCM_CUST_ADDR1")
	private String MCM_CUST_ADDR1;
	@Column(name = "MCM_CUST_ADDR2")
	private String MCM_CUST_ADDR2;
	@Column(name = "MCM_CUST_PIN")
	private String MCM_CUST_PIN;
	@Column(name = "MCM_CUST_TEL")
	private String MCM_CUST_TEL;
	@Column(name = "MCM_CUST_CREDIT_BAL")
	private Float MCM_CUST_CREDIT_BAL;
	@Column(name = "MCM_CUST_CREDIT_LIMIT")
	private Float MCM_CUST_CREDIT_LIMIT;
	@Column(name = "MCM_CUST_STATUS")
	private String MCM_CUST_STATUS;
	@Column(name = "MCM_CUST_TAG")
	private String MCM_CUST_TAG;
	@Column(name = "MCM_OLD_CUST_CODE")
	private Integer MCM_OLD_CUST_CODE;
	@Column(name = "MCM_CUST_TITLE")
	private String MCM_CUST_TITLE;
	@Column(name = "MCM_CREDIT_DAYS")
	private String MCM_CREDIT_DAYS;
	@Column(name = "mcm_area_code")
	private Integer mcm_area_code;
	@Column(name = "mcm_compid")
	private Integer mcm_compid;
	@Column(name = "mcm_diviid")
	private Integer mcm_diviid;
	@Column(name = "mcm_cust_id")
	private String mcm_cust_id;
	@Column(name = "mcm_type_code")
	private Integer mcm_type_code;
	@Column(name = "mcm_rep_code")
	private Integer mcm_rep_code;
	@Column(name = "mcm_gst")
	private String mcm_gst;
	@Column(name = "mcm_vattin")
	private String mcm_vattin;
	@Column(name = "mcm_bank_name")
	private String mcm_bank_name;
	@Column(name = "mcm_bank_place")
	private String mcm_bank_place;
	@Column(name = "mcm_transport_name")
	private String mcm_transport_name;
	@Column(name = "mcm_price_level")
	private Integer mcm_price_level;
	@Column(name = "mcm_place")
	private String mcm_place;
	@Column(name = "mcm_cst")
	private String mcm_cst;
	@Column(name = "mcm_valid")
	private Integer mcm_valid;
	@Column(name = "mcm_valid_from")
	private Date mcm_valid_from;
	@Column(name = "mcm_valid_to")
	private Date mcm_valid_to;
	@Column(name = "mcm_exd")
	private String mcm_exd;
	@Column(name = "mcm_phone1")
	private String mcm_phone1;
	@Column(name = "mcm_phone2")
	private String mcm_phone2;
	@Column(name = "mcm_License")
	private String mcm_License;
	@Column(name = "mcm_mar_date")
	private Date mcm_mar_date;
	@Column(name = "mcm_bdate")
	private Date mcm_bdate;
	@Column(name = "mcm_Contact_name")
	private String mcm_Contact_name;
	@Column(name = "TS")
	private Date TS;
	@Column(name = "TSID")
	private Integer TSID;
	@Column(name = "mcm_disc_perc")
	private Float mcm_disc_perc;
	@Column(name = "cid")
	private String cid;
	@Column(name = "IS_GLOBAL")
	private Integer IS_GLOBAL;
	@Column(name = "mcm_sale_type")
	private Integer mcm_sale_type;
	@Column(name = "mcm_currency")
	private String mcm_currency;
	@Column(name = "mcm_addr3")
	private String mcm_addr3;
	@Column(name = "mcm_city")
	private String mcm_city;
	@Column(name = "mcm_landmark")
	private String mcm_landmark;
	@Column(name = "mcm_credit_allowed")
	private Integer mcm_credit_allowed;
	@Column(name = "mcm_loyalty_allowed")
	private Integer mcm_loyalty_allowed;
	@Column(name = "mcm_password")
	private String mcm_password;
	@Column(name = "mcm_email")
	private String mcm_email;
	@Column(name = "hq_homeoutlet_id")
	private Integer hq_homeoutlet_id;
	@Column(name = "hq_retail_street_id")
	private Integer hq_retail_street_id;
	@Column(name = "mcm_hq_id")
	private Integer mcm_hq_id;
	@Column(name = "mcm_promotion_allowed")
	private Integer mcm_promotion_allowed;
	@Column(name = "RETAIL_OUTLET_ID")
	private Integer RETAIL_OUTLET_ID;
	@Column(name = "MCM_CREATED_BY")
	private String MCM_CREATED_BY;
	@Column(name = "MCM_CREATED_DT_TIME")
	private Date MCM_CREATED_DT_TIME;
	@Column(name = "MCM_UPDATED_BY")
	private String MCM_UPDATED_BY;
	@Column(name = "MCM_UPDATED_DT_TIMe")
	private Date MCM_UPDATED_DT_TIMe;
	@Column(name = "MCM_Loyalty_FromDate")
	private Date MCM_Loyalty_FromDate;
	@Column(name = "MCM_Loyalty_ToDate")
	private Date MCM_Loyalty_ToDate;
	@Column(name = "MCM_NOA")
	private Integer MCM_NOA;
	@Column(name = "MCM_NOC")
	private Integer MCM_NOC;
	@Column(name = "mcm_with_cform")
	private Integer mcm_with_cform;
	@Column(name = "mcm_cst_perc")
	private Float mcm_cst_perc;
	@Column(name = "mcm_state_loc_id")
	private Integer mcm_state_loc_id;
	@Column(name = "MCM_Container_Allowed")
	private Integer MCM_Container_Allowed;
	@Column(name = "mcm_first_name")
	private String mcm_first_name;
	@Column(name = "mcm_cust_company_name")
	private String mcm_cust_company_name;
	@Column(name = "mcm_mobile")
	private String mcm_mobile;
	@Column(name = "mcm_nick_name")
	private String mcm_nick_name;
	@Column(name = "mcm_allergic_to")
	private String mcm_allergic_to;
	@Column(name = "mcm_transport_code")
	private Integer mcm_transport_code;
	@Column(name = "mcm_nt_sec_code")
	private Integer mcm_nt_sec_code;
	@Column(name = "mcm_subscription_type")
	private Integer mcm_subscription_type;
	@Column(name = "hq_delivery_outlet")
	private Integer hq_delivery_outlet;
	@Column(name = "mcm_cust_remarks")
	private String mcm_cust_remarks;
	@Column(name = "mcm_group_code")
	private String mcm_group_code;
	@Column(name = "MCM_CUST_outbal")
	private Float MCM_CUST_outbal;
	@Column(name = "voucher_lock")
	private String voucher_lock;
	@Column(name = "MCM_MOB_ORDRMKS")
	private String MCM_MOB_ORDRMKS;
	@Column(name = "mcm_weborder_access")
	private String mcm_weborder_access;
	@Column(name = "mcm_mob_password")
	private String mcm_mob_password;
	@Column(name = "mcm_emp_id")
	private String mcm_emp_id;
	@Column(name = "MCM_gstno")
	private String MCM_gstno;
	@Column(name = "MCM_pan_no")
	private String MCM_pan_no;
	@Column(name = "MCM_aadhar_no")
	private String MCM_aadhar_no;
	@Column(name = "MCM_state_code")
	private Integer MCM_state_code;
	@Column(name = "MCM_is_exempted")
	private Integer MCM_is_exempted;
	@Column(name = "MCM_exempted_type")
	private String MCM_exempted_type;
	@Column(name = "mcm_location_type")
	private String mcm_location_type;
	@Column(name = "MCM_GST_REG_TYPE")
	private String MCM_GST_REG_TYPE;
	@Column(name = "MCM_COUNTRY_CODE")
	private String MCM_COUNTRY_CODE;
	@Column(name = "mcm_loyalty_pin")
	private String mcm_loyalty_pin;
	@Column(name = "mcm_salbeat_code")
	private String mcm_salbeat_code;
	@Column(name = "mcm_Marketbeat_code")
	private String mcm_Marketbeat_code;
	@Column(name = "mcm_latitude")
	private String mcm_latitude;
	@Column(name = "mcm_longitude")
	private String mcm_longitude;
	@Column(name = "MCM_EWAY_DIST")
	private Float MCM_EWAY_DIST;

	public Integer getMCM_CUST_CODE() {
		return MCM_CUST_CODE;
	}

	public void setMCM_CUST_CODE(Integer mCM_CUST_CODE) {
		MCM_CUST_CODE = mCM_CUST_CODE;
	}

	public String getMCM_CUST_NAME() {
		return MCM_CUST_NAME;
	}

	public void setMCM_CUST_NAME(String mCM_CUST_NAME) {
		MCM_CUST_NAME = mCM_CUST_NAME;
	}

	public String getMCM_CUST_ADDR1() {
		return MCM_CUST_ADDR1;
	}

	public void setMCM_CUST_ADDR1(String mCM_CUST_ADDR1) {
		MCM_CUST_ADDR1 = mCM_CUST_ADDR1;
	}

	public String getMCM_CUST_ADDR2() {
		return MCM_CUST_ADDR2;
	}

	public void setMCM_CUST_ADDR2(String mCM_CUST_ADDR2) {
		MCM_CUST_ADDR2 = mCM_CUST_ADDR2;
	}

	public String getMCM_CUST_PIN() {
		return MCM_CUST_PIN;
	}

	public void setMCM_CUST_PIN(String mCM_CUST_PIN) {
		MCM_CUST_PIN = mCM_CUST_PIN;
	}

	public String getMCM_CUST_TEL() {
		return MCM_CUST_TEL;
	}

	public void setMCM_CUST_TEL(String mCM_CUST_TEL) {
		MCM_CUST_TEL = mCM_CUST_TEL;
	}

	public Float getMCM_CUST_CREDIT_BAL() {
		return MCM_CUST_CREDIT_BAL;
	}

	public void setMCM_CUST_CREDIT_BAL(Float mCM_CUST_CREDIT_BAL) {
		MCM_CUST_CREDIT_BAL = mCM_CUST_CREDIT_BAL;
	}

	public Float getMCM_CUST_CREDIT_LIMIT() {
		return MCM_CUST_CREDIT_LIMIT;
	}

	public void setMCM_CUST_CREDIT_LIMIT(Float mCM_CUST_CREDIT_LIMIT) {
		MCM_CUST_CREDIT_LIMIT = mCM_CUST_CREDIT_LIMIT;
	}

	public String getMCM_CUST_STATUS() {
		return MCM_CUST_STATUS;
	}

	public void setMCM_CUST_STATUS(String mCM_CUST_STATUS) {
		MCM_CUST_STATUS = mCM_CUST_STATUS;
	}

	public String getMCM_CUST_TAG() {
		return MCM_CUST_TAG;
	}

	public void setMCM_CUST_TAG(String mCM_CUST_TAG) {
		MCM_CUST_TAG = mCM_CUST_TAG;
	}

	public Integer getMCM_OLD_CUST_CODE() {
		return MCM_OLD_CUST_CODE;
	}

	public void setMCM_OLD_CUST_CODE(Integer mCM_OLD_CUST_CODE) {
		MCM_OLD_CUST_CODE = mCM_OLD_CUST_CODE;
	}

	public String getMCM_CUST_TITLE() {
		return MCM_CUST_TITLE;
	}

	public void setMCM_CUST_TITLE(String mCM_CUST_TITLE) {
		MCM_CUST_TITLE = mCM_CUST_TITLE;
	}

	public String getMCM_CREDIT_DAYS() {
		return MCM_CREDIT_DAYS;
	}

	public void setMCM_CREDIT_DAYS(String mCM_CREDIT_DAYS) {
		MCM_CREDIT_DAYS = mCM_CREDIT_DAYS;
	}

	public Integer getMcm_area_code() {
		return mcm_area_code;
	}

	public void setMcm_area_code(Integer mcm_area_code) {
		this.mcm_area_code = mcm_area_code;
	}

	public Integer getMcm_compid() {
		return mcm_compid;
	}

	public void setMcm_compid(Integer mcm_compid) {
		this.mcm_compid = mcm_compid;
	}

	public Integer getMcm_diviid() {
		return mcm_diviid;
	}

	public void setMcm_diviid(Integer mcm_diviid) {
		this.mcm_diviid = mcm_diviid;
	}

	public String getMcm_cust_id() {
		return mcm_cust_id;
	}

	public void setMcm_cust_id(String mcm_cust_id) {
		this.mcm_cust_id = mcm_cust_id;
	}

	public Integer getMcm_type_code() {
		return mcm_type_code;
	}

	public void setMcm_type_code(Integer mcm_type_code) {
		this.mcm_type_code = mcm_type_code;
	}

	public Integer getMcm_rep_code() {
		return mcm_rep_code;
	}

	public void setMcm_rep_code(Integer mcm_rep_code) {
		this.mcm_rep_code = mcm_rep_code;
	}

	public String getMcm_gst() {
		return mcm_gst;
	}

	public void setMcm_gst(String mcm_gst) {
		this.mcm_gst = mcm_gst;
	}

	public String getMcm_vattin() {
		return mcm_vattin;
	}

	public void setMcm_vattin(String mcm_vattin) {
		this.mcm_vattin = mcm_vattin;
	}

	public String getMcm_bank_name() {
		return mcm_bank_name;
	}

	public void setMcm_bank_name(String mcm_bank_name) {
		this.mcm_bank_name = mcm_bank_name;
	}

	public String getMcm_bank_place() {
		return mcm_bank_place;
	}

	public void setMcm_bank_place(String mcm_bank_place) {
		this.mcm_bank_place = mcm_bank_place;
	}

	public String getMcm_transport_name() {
		return mcm_transport_name;
	}

	public void setMcm_transport_name(String mcm_transport_name) {
		this.mcm_transport_name = mcm_transport_name;
	}

	public Integer getMcm_price_level() {
		return mcm_price_level;
	}

	public void setMcm_price_level(Integer mcm_price_level) {
		this.mcm_price_level = mcm_price_level;
	}

	public String getMcm_place() {
		return mcm_place;
	}

	public void setMcm_place(String mcm_place) {
		this.mcm_place = mcm_place;
	}

	public String getMcm_cst() {
		return mcm_cst;
	}

	public void setMcm_cst(String mcm_cst) {
		this.mcm_cst = mcm_cst;
	}

	public Integer getMcm_valid() {
		return mcm_valid;
	}

	public void setMcm_valid(Integer mcm_valid) {
		this.mcm_valid = mcm_valid;
	}

	public Date getMcm_valid_from() {
		return mcm_valid_from;
	}

	public void setMcm_valid_from(Date mcm_valid_from) {
		this.mcm_valid_from = mcm_valid_from;
	}

	public Date getMcm_valid_to() {
		return mcm_valid_to;
	}

	public void setMcm_valid_to(Date mcm_valid_to) {
		this.mcm_valid_to = mcm_valid_to;
	}

	public String getMcm_exd() {
		return mcm_exd;
	}

	public void setMcm_exd(String mcm_exd) {
		this.mcm_exd = mcm_exd;
	}

	public String getMcm_phone1() {
		return mcm_phone1;
	}

	public void setMcm_phone1(String mcm_phone1) {
		this.mcm_phone1 = mcm_phone1;
	}

	public String getMcm_phone2() {
		return mcm_phone2;
	}

	public void setMcm_phone2(String mcm_phone2) {
		this.mcm_phone2 = mcm_phone2;
	}

	public String getMcm_License() {
		return mcm_License;
	}

	public void setMcm_License(String mcm_License) {
		this.mcm_License = mcm_License;
	}

	public Date getMcm_mar_date() {
		return mcm_mar_date;
	}

	public void setMcm_mar_date(Date mcm_mar_date) {
		this.mcm_mar_date = mcm_mar_date;
	}

	public Date getMcm_bdate() {
		return mcm_bdate;
	}

	public void setMcm_bdate(Date mcm_bdate) {
		this.mcm_bdate = mcm_bdate;
	}

	public String getMcm_Contact_name() {
		return mcm_Contact_name;
	}

	public void setMcm_Contact_name(String mcm_Contact_name) {
		this.mcm_Contact_name = mcm_Contact_name;
	}

	public Date getTS() {
		return TS;
	}

	public void setTS(Date tS) {
		TS = tS;
	}

	public Integer getTSID() {
		return TSID;
	}

	public void setTSID(Integer tSID) {
		TSID = tSID;
	}

	public Float getMcm_disc_perc() {
		return mcm_disc_perc;
	}

	public void setMcm_disc_perc(Float mcm_disc_perc) {
		this.mcm_disc_perc = mcm_disc_perc;
	}

	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	public Integer getIS_GLOBAL() {
		return IS_GLOBAL;
	}

	public void setIS_GLOBAL(Integer iS_GLOBAL) {
		IS_GLOBAL = iS_GLOBAL;
	}

	public Integer getMcm_sale_type() {
		return mcm_sale_type;
	}

	public void setMcm_sale_type(Integer mcm_sale_type) {
		this.mcm_sale_type = mcm_sale_type;
	}

	public String getMcm_currency() {
		return mcm_currency;
	}

	public void setMcm_currency(String mcm_currency) {
		this.mcm_currency = mcm_currency;
	}

	public String getMcm_addr3() {
		return mcm_addr3;
	}

	public void setMcm_addr3(String mcm_addr3) {
		this.mcm_addr3 = mcm_addr3;
	}

	public String getMcm_city() {
		return mcm_city;
	}

	public void setMcm_city(String mcm_city) {
		this.mcm_city = mcm_city;
	}

	public String getMcm_landmark() {
		return mcm_landmark;
	}

	public void setMcm_landmark(String mcm_landmark) {
		this.mcm_landmark = mcm_landmark;
	}

	public Integer getMcm_credit_allowed() {
		return mcm_credit_allowed;
	}

	public void setMcm_credit_allowed(Integer mcm_credit_allowed) {
		this.mcm_credit_allowed = mcm_credit_allowed;
	}

	public Integer getMcm_loyalty_allowed() {
		return mcm_loyalty_allowed;
	}

	public void setMcm_loyalty_allowed(Integer mcm_loyalty_allowed) {
		this.mcm_loyalty_allowed = mcm_loyalty_allowed;
	}

	public String getMcm_password() {
		return mcm_password;
	}

	public void setMcm_password(String mcm_password) {
		this.mcm_password = mcm_password;
	}

	public String getMcm_email() {
		return mcm_email;
	}

	public void setMcm_email(String mcm_email) {
		this.mcm_email = mcm_email;
	}

	public Integer getHq_homeoutlet_id() {
		return hq_homeoutlet_id;
	}

	public void setHq_homeoutlet_id(Integer hq_homeoutlet_id) {
		this.hq_homeoutlet_id = hq_homeoutlet_id;
	}

	public Integer getHq_retail_street_id() {
		return hq_retail_street_id;
	}

	public void setHq_retail_street_id(Integer hq_retail_street_id) {
		this.hq_retail_street_id = hq_retail_street_id;
	}

	public Integer getMcm_hq_id() {
		return mcm_hq_id;
	}

	public void setMcm_hq_id(Integer mcm_hq_id) {
		this.mcm_hq_id = mcm_hq_id;
	}

	public Integer getMcm_promotion_allowed() {
		return mcm_promotion_allowed;
	}

	public void setMcm_promotion_allowed(Integer mcm_promotion_allowed) {
		this.mcm_promotion_allowed = mcm_promotion_allowed;
	}

	public Integer getRETAIL_OUTLET_ID() {
		return RETAIL_OUTLET_ID;
	}

	public void setRETAIL_OUTLET_ID(Integer rETAIL_OUTLET_ID) {
		RETAIL_OUTLET_ID = rETAIL_OUTLET_ID;
	}

	public String getMCM_CREATED_BY() {
		return MCM_CREATED_BY;
	}

	public void setMCM_CREATED_BY(String mCM_CREATED_BY) {
		MCM_CREATED_BY = mCM_CREATED_BY;
	}

	public Date getMCM_CREATED_DT_TIME() {
		return MCM_CREATED_DT_TIME;
	}

	public void setMCM_CREATED_DT_TIME(Date mCM_CREATED_DT_TIME) {
		MCM_CREATED_DT_TIME = mCM_CREATED_DT_TIME;
	}

	public String getMCM_UPDATED_BY() {
		return MCM_UPDATED_BY;
	}

	public void setMCM_UPDATED_BY(String mCM_UPDATED_BY) {
		MCM_UPDATED_BY = mCM_UPDATED_BY;
	}

	public Date getMCM_UPDATED_DT_TIMe() {
		return MCM_UPDATED_DT_TIMe;
	}

	public void setMCM_UPDATED_DT_TIMe(Date mCM_UPDATED_DT_TIMe) {
		MCM_UPDATED_DT_TIMe = mCM_UPDATED_DT_TIMe;
	}

	public Date getMCM_Loyalty_FromDate() {
		return MCM_Loyalty_FromDate;
	}

	public void setMCM_Loyalty_FromDate(Date mCM_Loyalty_FromDate) {
		MCM_Loyalty_FromDate = mCM_Loyalty_FromDate;
	}

	public Date getMCM_Loyalty_ToDate() {
		return MCM_Loyalty_ToDate;
	}

	public void setMCM_Loyalty_ToDate(Date mCM_Loyalty_ToDate) {
		MCM_Loyalty_ToDate = mCM_Loyalty_ToDate;
	}

	public Integer getMCM_NOA() {
		return MCM_NOA;
	}

	public void setMCM_NOA(Integer mCM_NOA) {
		MCM_NOA = mCM_NOA;
	}

	public Integer getMCM_NOC() {
		return MCM_NOC;
	}

	public void setMCM_NOC(Integer mCM_NOC) {
		MCM_NOC = mCM_NOC;
	}

	public Integer getMcm_with_cform() {
		return mcm_with_cform;
	}

	public void setMcm_with_cform(Integer mcm_with_cform) {
		this.mcm_with_cform = mcm_with_cform;
	}

	public Float getMcm_cst_perc() {
		return mcm_cst_perc;
	}

	public void setMcm_cst_perc(Float mcm_cst_perc) {
		this.mcm_cst_perc = mcm_cst_perc;
	}

	public Integer getMcm_state_loc_id() {
		return mcm_state_loc_id;
	}

	public void setMcm_state_loc_id(Integer mcm_state_loc_id) {
		this.mcm_state_loc_id = mcm_state_loc_id;
	}

	public Integer getMCM_Container_Allowed() {
		return MCM_Container_Allowed;
	}

	public void setMCM_Container_Allowed(Integer mCM_Container_Allowed) {
		MCM_Container_Allowed = mCM_Container_Allowed;
	}

	public String getMcm_first_name() {
		return mcm_first_name;
	}

	public void setMcm_first_name(String mcm_first_name) {
		this.mcm_first_name = mcm_first_name;
	}

	public String getMcm_cust_company_name() {
		return mcm_cust_company_name;
	}

	public void setMcm_cust_company_name(String mcm_cust_company_name) {
		this.mcm_cust_company_name = mcm_cust_company_name;
	}

	public String getMcm_mobile() {
		return mcm_mobile;
	}

	public void setMcm_mobile(String mcm_mobile) {
		this.mcm_mobile = mcm_mobile;
	}

	public String getMcm_nick_name() {
		return mcm_nick_name;
	}

	public void setMcm_nick_name(String mcm_nick_name) {
		this.mcm_nick_name = mcm_nick_name;
	}

	public String getMcm_allergic_to() {
		return mcm_allergic_to;
	}

	public void setMcm_allergic_to(String mcm_allergic_to) {
		this.mcm_allergic_to = mcm_allergic_to;
	}

	public Integer getMcm_transport_code() {
		return mcm_transport_code;
	}

	public void setMcm_transport_code(Integer mcm_transport_code) {
		this.mcm_transport_code = mcm_transport_code;
	}

	public Integer getMcm_nt_sec_code() {
		return mcm_nt_sec_code;
	}

	public void setMcm_nt_sec_code(Integer mcm_nt_sec_code) {
		this.mcm_nt_sec_code = mcm_nt_sec_code;
	}

	public Integer getMcm_subscription_type() {
		return mcm_subscription_type;
	}

	public void setMcm_subscription_type(Integer mcm_subscription_type) {
		this.mcm_subscription_type = mcm_subscription_type;
	}

	public Integer getHq_delivery_outlet() {
		return hq_delivery_outlet;
	}

	public void setHq_delivery_outlet(Integer hq_delivery_outlet) {
		this.hq_delivery_outlet = hq_delivery_outlet;
	}

	public String getMcm_cust_remarks() {
		return mcm_cust_remarks;
	}

	public void setMcm_cust_remarks(String mcm_cust_remarks) {
		this.mcm_cust_remarks = mcm_cust_remarks;
	}

	public String getMcm_group_code() {
		return mcm_group_code;
	}

	public void setMcm_group_code(String mcm_group_code) {
		this.mcm_group_code = mcm_group_code;
	}

	public Float getMCM_CUST_outbal() {
		return MCM_CUST_outbal;
	}

	public void setMCM_CUST_outbal(Float mCM_CUST_outbal) {
		MCM_CUST_outbal = mCM_CUST_outbal;
	}

	public String getVoucher_lock() {
		return voucher_lock;
	}

	public void setVoucher_lock(String voucher_lock) {
		this.voucher_lock = voucher_lock;
	}

	public String getMCM_MOB_ORDRMKS() {
		return MCM_MOB_ORDRMKS;
	}

	public void setMCM_MOB_ORDRMKS(String mCM_MOB_ORDRMKS) {
		MCM_MOB_ORDRMKS = mCM_MOB_ORDRMKS;
	}

	public String getMcm_weborder_access() {
		return mcm_weborder_access;
	}

	public void setMcm_weborder_access(String mcm_weborder_access) {
		this.mcm_weborder_access = mcm_weborder_access;
	}

	public String getMcm_mob_password() {
		return mcm_mob_password;
	}

	public void setMcm_mob_password(String mcm_mob_password) {
		this.mcm_mob_password = mcm_mob_password;
	}

	public String getMcm_emp_id() {
		return mcm_emp_id;
	}

	public void setMcm_emp_id(String mcm_emp_id) {
		this.mcm_emp_id = mcm_emp_id;
	}

	public String getMCM_gstno() {
		return MCM_gstno;
	}

	public void setMCM_gstno(String mCM_gstno) {
		MCM_gstno = mCM_gstno;
	}

	public String getMCM_pan_no() {
		return MCM_pan_no;
	}

	public void setMCM_pan_no(String mCM_pan_no) {
		MCM_pan_no = mCM_pan_no;
	}

	public String getMCM_aadhar_no() {
		return MCM_aadhar_no;
	}

	public void setMCM_aadhar_no(String mCM_aadhar_no) {
		MCM_aadhar_no = mCM_aadhar_no;
	}

	public Integer getMCM_state_code() {
		return MCM_state_code;
	}

	public void setMCM_state_code(Integer mCM_state_code) {
		MCM_state_code = mCM_state_code;
	}

	public Integer getMCM_is_exempted() {
		return MCM_is_exempted;
	}

	public void setMCM_is_exempted(Integer mCM_is_exempted) {
		MCM_is_exempted = mCM_is_exempted;
	}

	public String getMCM_exempted_type() {
		return MCM_exempted_type;
	}

	public void setMCM_exempted_type(String mCM_exempted_type) {
		MCM_exempted_type = mCM_exempted_type;
	}

	public String getMcm_location_type() {
		return mcm_location_type;
	}

	public void setMcm_location_type(String mcm_location_type) {
		this.mcm_location_type = mcm_location_type;
	}

	public String getMCM_GST_REG_TYPE() {
		return MCM_GST_REG_TYPE;
	}

	public void setMCM_GST_REG_TYPE(String mCM_GST_REG_TYPE) {
		MCM_GST_REG_TYPE = mCM_GST_REG_TYPE;
	}

	public String getMCM_COUNTRY_CODE() {
		return MCM_COUNTRY_CODE;
	}

	public void setMCM_COUNTRY_CODE(String mCM_COUNTRY_CODE) {
		MCM_COUNTRY_CODE = mCM_COUNTRY_CODE;
	}

	public String getMcm_loyalty_pin() {
		return mcm_loyalty_pin;
	}

	public void setMcm_loyalty_pin(String mcm_loyalty_pin) {
		this.mcm_loyalty_pin = mcm_loyalty_pin;
	}

	public String getMcm_salbeat_code() {
		return mcm_salbeat_code;
	}

	public void setMcm_salbeat_code(String mcm_salbeat_code) {
		this.mcm_salbeat_code = mcm_salbeat_code;
	}

	public String getMcm_Marketbeat_code() {
		return mcm_Marketbeat_code;
	}

	public void setMcm_Marketbeat_code(String mcm_Marketbeat_code) {
		this.mcm_Marketbeat_code = mcm_Marketbeat_code;
	}

	public String getMcm_latitude() {
		return mcm_latitude;
	}

	public void setMcm_latitude(String mcm_latitude) {
		this.mcm_latitude = mcm_latitude;
	}

	public String getMcm_longitude() {
		return mcm_longitude;
	}

	public void setMcm_longitude(String mcm_longitude) {
		this.mcm_longitude = mcm_longitude;
	}

	public Float getMCM_EWAY_DIST() {
		return MCM_EWAY_DIST;
	}

	public void setMCM_EWAY_DIST(Float mCM_EWAY_DIST) {
		MCM_EWAY_DIST = mCM_EWAY_DIST;
	}
}
