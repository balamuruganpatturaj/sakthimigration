package com.sakthi.groups.migration.gofrugal.dao;

import org.springframework.data.repository.CrudRepository;

import com.sakthi.groups.migration.gofrugal.model.CounterNameDetails;

public interface CounterNameDtlRepository extends CrudRepository<CounterNameDetails, Integer> {

}
