package com.sakthi.groups.migration.gofrugal.dao;

import org.springframework.data.repository.CrudRepository;

import com.sakthi.groups.migration.gofrugal.model.MedDistrubutorMaster;

public interface MedDistrubutorRepository extends CrudRepository<MedDistrubutorMaster, Long>  {
	
	
}
