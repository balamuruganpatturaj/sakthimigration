package com.sakthi.groups.migration.gofrugal.dao;

import org.springframework.data.repository.CrudRepository;

import com.sakthi.groups.migration.gofrugal.model.MedPriceLevelItem;

public interface MedPriceLevelItemRepository extends CrudRepository<MedPriceLevelItem, Integer> {

}
