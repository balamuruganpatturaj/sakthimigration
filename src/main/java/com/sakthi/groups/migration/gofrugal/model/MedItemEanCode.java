package com.sakthi.groups.migration.gofrugal.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "med_item_eancode")
public class MedItemEanCode {

	@Column(name = "mie_Item_code")
	private Integer mie_Item_code;

	@Column(name = "Mie_Eancode")
	private String Mie_Eancode;

	@Column(name = "TS")
	private Timestamp TS;

	@Id
	@Column(name = "TSID")
	private Integer TSID;

	@Column(name = "mie_conv_code")
	private Integer mie_conv_code;

	@Column(name = "mie_conv_selling")
	private Double mie_conv_selling;

	@Column(name = "Hq_ref_id")
	private Integer Hq_ref_id;

	@Column(name = "mie_duplicate_from")
	private String mie_duplicate_from;

	@Column(name = "mie_duplicate_ref_id")
	private String mie_duplicate_ref_id;

	@Column(name = "mie_hq_print_Refno")
	private String mie_hq_print_Refno;

	@Column(name = "mie_hq_print_Status")
	private String mie_hq_print_Status;

	@Column(name = "mie_old_conv_selling")
	private String mie_old_conv_selling;

	public Integer getMie_Item_code() {
		return mie_Item_code;
	}

	public void setMie_Item_code(Integer mie_Item_code) {
		this.mie_Item_code = mie_Item_code;
	}

	public String getMie_Eancode() {
		return Mie_Eancode;
	}

	public void setMie_Eancode(String mie_Eancode) {
		Mie_Eancode = mie_Eancode;
	}

	public Timestamp getTS() {
		return TS;
	}

	public void setTS(Timestamp tS) {
		TS = tS;
	}

	public Integer getTSID() {
		return TSID;
	}

	public void setTSID(Integer tSID) {
		TSID = tSID;
	}

	public Integer getMie_conv_code() {
		return mie_conv_code;
	}

	public void setMie_conv_code(Integer mie_conv_code) {
		this.mie_conv_code = mie_conv_code;
	}

	public Double getMie_conv_selling() {
		return mie_conv_selling;
	}

	public void setMie_conv_selling(Double mie_conv_selling) {
		this.mie_conv_selling = mie_conv_selling;
	}

	public Integer getHq_ref_id() {
		return Hq_ref_id;
	}

	public void setHq_ref_id(Integer hq_ref_id) {
		Hq_ref_id = hq_ref_id;
	}

	public String getMie_duplicate_from() {
		return mie_duplicate_from;
	}

	public void setMie_duplicate_from(String mie_duplicate_from) {
		this.mie_duplicate_from = mie_duplicate_from;
	}

	public String getMie_duplicate_ref_id() {
		return mie_duplicate_ref_id;
	}

	public void setMie_duplicate_ref_id(String mie_duplicate_ref_id) {
		this.mie_duplicate_ref_id = mie_duplicate_ref_id;
	}

	public String getMie_hq_print_Refno() {
		return mie_hq_print_Refno;
	}

	public void setMie_hq_print_Refno(String mie_hq_print_Refno) {
		this.mie_hq_print_Refno = mie_hq_print_Refno;
	}

	public String getMie_hq_print_Status() {
		return mie_hq_print_Status;
	}

	public void setMie_hq_print_Status(String mie_hq_print_Status) {
		this.mie_hq_print_Status = mie_hq_print_Status;
	}

	public String getMie_old_conv_selling() {
		return mie_old_conv_selling;
	}

	public void setMie_old_conv_selling(String mie_old_conv_selling) {
		this.mie_old_conv_selling = mie_old_conv_selling;
	}

}
