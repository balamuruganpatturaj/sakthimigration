package com.sakthi.groups.migration.gofrugal.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "med_loyalty_dtl")
public class MedLoyaltyDetail {

	@Column(name = "mld_offer_code")
	private Integer mldOfferCode;

	@Column(name = "mld_code")
	private Integer mldCode;

	@Column(name = "mld_cust_code")
	private Integer mldCustCode;

	@Column(name = "mld_amount")
	private Double mldAmount;

	@Column(name = "mld_points")
	private Double mldPoints;

	@Column(name = "mld_amt_balance")
	private Double mldAmtBalance;

	@Column(name = "mld_bill_date")
	private Timestamp mldBillDate;

	@Column(name = "mld_bill_no")
	private Integer mldBillNo;

	@Column(name = "mld_pbill_no")
	private Integer mldPbillNo;

	@Column(name = "mld_counter_code")
	private String mldCounterCode;

	@Column(name = "mld_bill_amount")
	private Double mldBillAmount;

	@Column(name = "TS")
	private Timestamp TS;

	@Id
	@Column(name = "TSID")
	private Integer TSID;

	@Column(name = "mld_tran_type")
	private String mldTranType;

	@Column(name = "RETAIL_OUTLET_ID")
	private Integer retailUutletid;

	@Column(name = "timestamp")
	private String timestamp;

	@Column(name = "mld_bonus_points")
	private Double mldBonusPoints;

	@Column(name = "MLD_POINT_STATUS")
	private String mldPointStatus;

	@Column(name = "mld_Card_number")
	private String mldCardNumber;

	@Column(name = "MLD_REDEM_OUTLET_ID")
	private Integer mldRedemOutletId;

	@Column(name = "mld_hq_loyalty")
	private Double mldHqLoyalty;

	@Column(name = "mld_gftLoyalty_unsendData")
	private String mldGftLoyaltyUnsendData;

	@Column(name = "mld_dontCalc_Loyalty_amt")
	private Double mldDontCalcLoyaltyAmt;

	@Column(name = "mld_offer_points")
	private Double mldOfferPoints;

	public Integer getMldOfferCode() {
		return mldOfferCode;
	}

	public void setMldOfferCode(Integer mldOfferCode) {
		this.mldOfferCode = mldOfferCode;
	}

	public Integer getMldCode() {
		return mldCode;
	}

	public void setMldCode(Integer mldCode) {
		this.mldCode = mldCode;
	}

	public Integer getMldCustCode() {
		return mldCustCode;
	}

	public void setMldCustCode(Integer mldCustCode) {
		this.mldCustCode = mldCustCode;
	}

	public Double getMldAmount() {
		return mldAmount;
	}

	public void setMldAmount(Double mldAmount) {
		this.mldAmount = mldAmount;
	}

	public Double getMldPoints() {
		return mldPoints;
	}

	public void setMldPoints(Double mldPoints) {
		this.mldPoints = mldPoints;
	}

	public Double getMldAmtBalance() {
		return mldAmtBalance;
	}

	public void setMldAmtBalance(Double mldAmtBalance) {
		this.mldAmtBalance = mldAmtBalance;
	}

	public Timestamp getMldBillDate() {
		return mldBillDate;
	}

	public void setMldBillDate(Timestamp mldBillDate) {
		this.mldBillDate = mldBillDate;
	}

	public Integer getMldBillNo() {
		return mldBillNo;
	}

	public void setMldBillNo(Integer mldBillNo) {
		this.mldBillNo = mldBillNo;
	}

	public Integer getMldPbillNo() {
		return mldPbillNo;
	}

	public void setMldPbillNo(Integer mldPbillNo) {
		this.mldPbillNo = mldPbillNo;
	}

	public String getMldCounterCode() {
		return mldCounterCode;
	}

	public void setMldCounterCode(String mldCounterCode) {
		this.mldCounterCode = mldCounterCode;
	}

	public Double getMldBillAmount() {
		return mldBillAmount;
	}

	public void setMldBillAmount(Double mldBillAmount) {
		this.mldBillAmount = mldBillAmount;
	}

	public Timestamp getTS() {
		return TS;
	}

	public void setTS(Timestamp tS) {
		TS = tS;
	}

	public Integer getTSID() {
		return TSID;
	}

	public void setTSID(Integer tSID) {
		TSID = tSID;
	}

	public String getMldTranType() {
		return mldTranType;
	}

	public void setMldTranType(String mldTranType) {
		this.mldTranType = mldTranType;
	}

	public Integer getRetailUutletid() {
		return retailUutletid;
	}

	public void setRetailUutletid(Integer retailUutletid) {
		this.retailUutletid = retailUutletid;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public Double getMldBonusPoints() {
		return mldBonusPoints;
	}

	public void setMldBonusPoints(Double mldBonusPoints) {
		this.mldBonusPoints = mldBonusPoints;
	}

	public String getMldPointStatus() {
		return mldPointStatus;
	}

	public void setMldPointStatus(String mldPointStatus) {
		this.mldPointStatus = mldPointStatus;
	}

	public String getMldCardNumber() {
		return mldCardNumber;
	}

	public void setMldCardNumber(String mldCardNumber) {
		this.mldCardNumber = mldCardNumber;
	}

	public Integer getMldRedemOutletId() {
		return mldRedemOutletId;
	}

	public void setMldRedemOutletId(Integer mldRedemOutletId) {
		this.mldRedemOutletId = mldRedemOutletId;
	}

	public Double getMldHqLoyalty() {
		return mldHqLoyalty;
	}

	public void setMldHqLoyalty(Double mldHqLoyalty) {
		this.mldHqLoyalty = mldHqLoyalty;
	}

	public String getMldGftLoyaltyUnsendData() {
		return mldGftLoyaltyUnsendData;
	}

	public void setMldGftLoyaltyUnsendData(String mldGftLoyaltyUnsendData) {
		this.mldGftLoyaltyUnsendData = mldGftLoyaltyUnsendData;
	}

	public Double getMldDontCalcLoyaltyAmt() {
		return mldDontCalcLoyaltyAmt;
	}

	public void setMldDontCalcLoyaltyAmt(Double mldDontCalcLoyaltyAmt) {
		this.mldDontCalcLoyaltyAmt = mldDontCalcLoyaltyAmt;
	}

	public Double getMldOfferPoints() {
		return mldOfferPoints;
	}

	public void setMldOfferPoints(Double mldOfferPoints) {
		this.mldOfferPoints = mldOfferPoints;
	}

}
