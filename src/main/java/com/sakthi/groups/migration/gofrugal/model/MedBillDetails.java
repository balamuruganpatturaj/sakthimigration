package com.sakthi.groups.migration.gofrugal.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@IdClass(MedBillDetailsId.class)
@Table(name = "MED_BILL_DTL")
public class MedBillDetails {

	@Id
	@Column(name = "MBD_BILL_NO")
	private Integer mBDBillNo;

	@Id
	@Column(name = "MBD_BILL_SL_NO")
	private Integer MBD_BILL_SL_NO;

	@Column(name = "MBD_ITEM_CODE")
	private Integer MBD_ITEM_CODE;

	@Column(name = "MBD_ITEM_UNIT")
	private Integer MBD_ITEM_UNIT;

	@Column(name = "MBD_BATCH_NO")
	private String MBD_BATCH_NO;

	@Column(name = "MBD_EXP_DT")
	private Timestamp MBD_EXP_DT;

	@Column(name = "MBD_ITEM_QTY")
	private Double MBD_ITEM_QTY;

	@Column(name = "MBD_ITEM_RATE")
	private Double MBD_ITEM_RATE;

	@Column(name = "MBD_ITEM_TAX_AMT")
	private Double MBD_ITEM_TAX_AMT;

	@Column(name = "MBD_ITEM_AMOUNT")
	private Double MBD_ITEM_AMOUNT;

	@Column(name = "MBD_ITEM_TYPE")
	private Integer MBD_ITEM_TYPE;

	@Column(name = "MBD_PUR_RATE")
	private Double MBD_PUR_RATE;

	@Column(name = "MBD_DIST_CODE")
	private Integer MBD_DIST_CODE;

	@Column(name = "MBD_ITEM_TAX_PERC")
	private Double MBD_ITEM_TAX_PERC;

	@Column(name = "MBD_PURCHASE_RATE")
	private Double MBD_PURCHASE_RATE;

	@Column(name = "MBD_TOT_TAX_AMT")
	private Double MBD_TOT_TAX_AMT;

	@Column(name = "MBD_DISC_AMT")
	private Double MBD_DISC_AMT;

	@Column(name = "MBD_INCLUSIVEFLAG")
	private String MBD_INCLUSIVEFLAG;

	@Column(name = "MBD_TAX_CONFIG")
	private Integer MBD_TAX_CONFIG;

	@Column(name = "mbd_item_rowid")
	private Integer mbd_item_rowid;

	@Column(name = "mbd_item_deliver")
	private String mbd_item_deliver;

	@Column(name = "mbd_item_schm_code")
	private Double mbd_item_schm_code;

	@Column(name = "mbd_item_schm_slno")
	private Double mbd_item_schm_slno;

	@Column(name = "mbd_item_schm_discperc")
	private Double mbd_item_schm_discperc;

	@Column(name = "mbd_item_schm_discamt")
	private Double mbd_item_schm_discamt;

	@Column(name = "mbd_grind_amt")
	private Double mbd_grind_amt;

	@Column(name = "mbd_compid")
	private Integer mbd_compid;

	@Column(name = "mbd_diviid")
	private Integer mbd_diviid;

	@Column(name = "mbd_locaid")
	private Integer mbd_locaid;

	@Column(name = "mbd_max_rate")
	private Double mbd_max_rate;

	@Column(name = "mbd_unit")
	private Double mbd_unit;

	@Column(name = "mbd_price_level")
	private Integer mbd_price_level;

	@Column(name = "mbd_Free_item")
	private Integer mbd_Free_item;

	@Column(name = "mbd_rst_per")
	private Double mbd_rst_per;

	@Column(name = "mbd_rst_amt")
	private Double mbd_rst_amt;

	@Column(name = "mbd_rstsc_per")
	private Double mbd_rstsc_per;

	@Column(name = "mbd_rstsc_amt")
	private Double mbd_rstsc_amt;

	@Column(name = "mbd_REP_CODE")
	private Integer mbd_REP_CODE;

	@Column(name = "mbd_REP_COMM")
	private Double mbd_REP_COMM;

	@Column(name = "MBD_BILL_TOTDISC")
	private Double MBD_BILL_TOTDISC;

	@Column(name = "TS")
	private Timestamp TS;

	@Column(name = "TSID")
	private Integer TSID;

	@Column(name = "MBD_DISC_PERC")
	private Double MBD_DISC_PERC;

	@Column(name = "mbd_rep_amt")
	private Double mbd_rep_amt;

	@Column(name = "mbd_orig_rate")
	private Double mbd_orig_rate;

	@Column(name = "mbd_sd_no")
	private Integer mbd_sd_no;

	@Column(name = "mbd_sd_qty")
	private Double mbd_sd_qty;

	@Column(name = "mbd_qty_pricelvl")
	private Integer mbd_qty_pricelvl;

	@Column(name = "mbd_qty_pricelvl_selling")
	private Double mbd_qty_pricelvl_selling;

	@Column(name = "mbd_weight")
	private Double mbd_weight;

	@Column(name = "mbd_modifier")
	private Integer mbd_modifier;

	@Column(name = "mbd_modifierless")
	private Integer mbd_modifierless;

	@Column(name = "mbd_available_weight")
	private Double mbd_available_weight;

	@Column(name = "mbd_tax_sur_amt")
	private Double mbd_tax_sur_amt;

	@Column(name = "mbd_tax_sur_per")
	private Double mbd_tax_sur_per;

	@Column(name = "mbd_tax_sur_ItemAmount")
	private Double mbd_tax_sur_ItemAmount;

	@Column(name = "mbd_id_withouttax")
	private Integer mbd_id_withouttax;

	@Column(name = "mbd_cd_withtax")
	private Integer mbd_cd_withtax;

	@Column(name = "mbd_shapecharge_code")
	private Integer mbd_shapecharge_code;

	@Column(name = "mbd_shapecharge_perc")
	private Double mbd_shapecharge_perc;

	@Column(name = "mbd_shapecharge_amt")
	private Double mbd_shapecharge_amt;

	@Column(name = "mbd_item_remarks")
	private String mbd_item_remarks;

	@Column(name = "mbd_bags")
	private Double mbd_bags;

	@Column(name = "RETAIL_OUTLET_ID")
	private Integer RETAIL_OUTLET_ID;

	@Column(name = "mbd_scan_details")
	private String mbd_scan_details;

	@Column(name = "mbd_sale_ret_slip_no")
	private Integer mbd_sale_ret_slip_no;

	@Column(name = "mbd_sale_ret_slip_sl_no")
	private Integer mbd_sale_ret_slip_sl_no;

	@Column(name = "mbd_home_delivery_qty")
	private Double mbd_home_delivery_qty;

	@Column(name = "timestamp")
	private String timestamp;

	@Column(name = "mbd_offer_code")
	private Integer mbd_offer_code;

	@Column(name = "mbd_bill_schm_dis_amt")
	private Double mbd_bill_schm_dis_amt;

	@Column(name = "mbd_item_schm_oth_amt")
	private Double mbd_item_schm_oth_amt;

	@Column(name = "mbd_service_charge")
	private Double mbd_service_charge;

	@Column(name = "mbd_service_charge_amt")
	private Double mbd_service_charge_amt;

	@Column(name = "mbd_mat_unique_ref")
	private Integer mbd_mat_unique_ref;

	@Column(name = "mbd_org_slno")
	private Integer mbd_org_slno;

	@Column(name = "mbd_conv_type")
	private String mbd_conv_type;

	@Column(name = "mbd_conv_factor")
	private Double mbd_conv_factor;

	@Column(name = "mbd_eancode")
	private String mbd_eancode;

	@Column(name = "mbd_pur_price_wot")
	private Double mbd_pur_price_wot;

	@Column(name = "mbd_profit_amt")
	private Double mbd_profit_amt;

	@Column(name = "mbd_net_amt")
	private Double mbd_net_amt;

	@Column(name = "mbd_half_half")
	private Integer mbd_half_half;

	@Column(name = "mbd_void_qty")
	private Double mbd_void_qty;

	@Column(name = "Mbd_Modifier_Hdr_id")
	private Integer Mbd_Modifier_Hdr_id;

	@Column(name = "Mbd_Modifier_dtl_id")
	private Integer Mbd_Modifier_dtl_id;

	@Column(name = "Mbd_Modifier_Desc")
	private String Mbd_Modifier_Desc;

	@Column(name = "Mbd_Modifier_Parent_Code")
	private Integer Mbd_Modifier_Parent_Code;

	@Column(name = "mbd_coupon_disc_amt")
	private Double mbd_coupon_disc_amt;

	@Column(name = "mbd_min_selling")
	private Double mbd_min_selling;

	@Column(name = "mbd_comboset_id")
	private Integer mbd_comboset_id;

	@Column(name = "mbd_def_incl_tag")
	private String mbd_def_incl_tag;

	@Column(name = "mbd_invtype")
	private Integer mbdInvType;

	@Column(name = "mbd_split_bno_pk")
	private Integer mbdSplitBnoPk;

	@Column(name = "mbd_vat_on_srvtax")
	private Integer mbd_vat_on_srvtax;

	@Column(name = "mbd_discount_code")
	private Integer mbd_discount_code;

	@Column(name = "mbd_service_tax")
	private Double mbd_service_tax;

	@Column(name = "mbd_service_tax_amt")
	private Double mbd_service_tax_amt;

	@Column(name = "mbd_item_group")
	private Integer mbd_item_group;

	@Column(name = "mbd_openitem_remarks")
	private String mbd_openitem_remarks;

	@Column(name = "mbd_sgst_perc")
	private Double mbd_sgst_perc;

	@Column(name = "mbd_sgst_amt")
	private Double mbd_sgst_amt;

	@Column(name = "mbd_cgst_perc")
	private Double mbd_cgst_perc;

	@Column(name = "mbd_cgst_amt")
	private Double mbd_cgst_amt;

	@Column(name = "mbd_igst_perc")
	private Double mbd_igst_perc;

	@Column(name = "mbd_igst_amt")
	private Double mbd_igst_amt;

	@Column(name = "mbd_cess_perc")
	private Double mbd_cess_perc;

	@Column(name = "mbd_cess_amt")
	private Double mbd_cess_amt;

	@Column(name = "mbd_gst_code")
	private String mbd_gst_code;

	@Column(name = "mbd_abatement_perc")
	private Double mbd_abatement_perc;

	@Column(name = "mbd_gst_basedon")
	private Integer mbd_gst_basedon;

	@Column(name = "mbd_tax_calc_on")
	private Integer mbd_tax_calc_on;

	@Column(name = "mbd_free_Tax_calc")
	private Integer mbd_free_Tax_calc;

	@Column(name = "mbd_extra_cess_amt")
	private Double mbd_extra_cess_amt;

	@Column(name = "mbd_ExtraCess_Per_Qty")
	private Double mbd_ExtraCess_Per_Qty;

	@Column(name = "mbd_excise_Perc")
	private Double mbd_excise_Perc;

	@Column(name = "mbd_excise_amt")
	private Double mbd_excise_amt;

	@Column(name = "mbd_conv_qty")
	private String mbd_conv_qty;

	@Column(name = "mbd_conv_rate")
	private String mbd_conv_rate;

	@Column(name = "MBD_CALAMITYCESS_PERC")
	private String MBD_CALAMITYCESS_PERC;

	@Column(name = "MBD_CALAMITYCESS_AMT")
	private String MBD_CALAMITYCESS_AMT;

	@Column(name = "mbd_item_schm_real_discperc")
	private String mbd_item_schm_real_discperc;

	@Column(name = "mbd_offer_refno")
	private String mbd_offer_refno;

	@Column(name = "mbd_albumcharge_code")
	private String mbd_albumcharge_code;

	@Column(name = "mbd_albumcharge_perc")
	private String mbd_albumcharge_perc;

	@Column(name = "mbd_albumcharge_amt")
	private String mbd_albumcharge_amt;

	public Integer getmBDBillNo() {
		return mBDBillNo;
	}

	public void setmBDBillNo(Integer mBDBillNo) {
		this.mBDBillNo = mBDBillNo;
	}

	public Integer getMBD_BILL_SL_NO() {
		return MBD_BILL_SL_NO;
	}

	public void setMBD_BILL_SL_NO(Integer mBD_BILL_SL_NO) {
		MBD_BILL_SL_NO = mBD_BILL_SL_NO;
	}

	public Integer getMBD_ITEM_CODE() {
		return MBD_ITEM_CODE;
	}

	public void setMBD_ITEM_CODE(Integer mBD_ITEM_CODE) {
		MBD_ITEM_CODE = mBD_ITEM_CODE;
	}

	public Integer getMBD_ITEM_UNIT() {
		return MBD_ITEM_UNIT;
	}

	public void setMBD_ITEM_UNIT(Integer mBD_ITEM_UNIT) {
		MBD_ITEM_UNIT = mBD_ITEM_UNIT;
	}

	public String getMBD_BATCH_NO() {
		return MBD_BATCH_NO;
	}

	public void setMBD_BATCH_NO(String mBD_BATCH_NO) {
		MBD_BATCH_NO = mBD_BATCH_NO;
	}

	public Timestamp getMBD_EXP_DT() {
		return MBD_EXP_DT;
	}

	public void setMBD_EXP_DT(Timestamp mBD_EXP_DT) {
		MBD_EXP_DT = mBD_EXP_DT;
	}

	public Double getMBD_ITEM_QTY() {
		return MBD_ITEM_QTY;
	}

	public void setMBD_ITEM_QTY(Double mBD_ITEM_QTY) {
		MBD_ITEM_QTY = mBD_ITEM_QTY;
	}

	public Double getMBD_ITEM_RATE() {
		return MBD_ITEM_RATE;
	}

	public void setMBD_ITEM_RATE(Double mBD_ITEM_RATE) {
		MBD_ITEM_RATE = mBD_ITEM_RATE;
	}

	public Double getMBD_ITEM_TAX_AMT() {
		return MBD_ITEM_TAX_AMT;
	}

	public void setMBD_ITEM_TAX_AMT(Double mBD_ITEM_TAX_AMT) {
		MBD_ITEM_TAX_AMT = mBD_ITEM_TAX_AMT;
	}

	public Double getMBD_ITEM_AMOUNT() {
		return MBD_ITEM_AMOUNT;
	}

	public void setMBD_ITEM_AMOUNT(Double mBD_ITEM_AMOUNT) {
		MBD_ITEM_AMOUNT = mBD_ITEM_AMOUNT;
	}

	public Integer getMBD_ITEM_TYPE() {
		return MBD_ITEM_TYPE;
	}

	public void setMBD_ITEM_TYPE(Integer mBD_ITEM_TYPE) {
		MBD_ITEM_TYPE = mBD_ITEM_TYPE;
	}

	public Double getMBD_PUR_RATE() {
		return MBD_PUR_RATE;
	}

	public void setMBD_PUR_RATE(Double mBD_PUR_RATE) {
		MBD_PUR_RATE = mBD_PUR_RATE;
	}

	public Integer getMBD_DIST_CODE() {
		return MBD_DIST_CODE;
	}

	public void setMBD_DIST_CODE(Integer mBD_DIST_CODE) {
		MBD_DIST_CODE = mBD_DIST_CODE;
	}

	public Double getMBD_ITEM_TAX_PERC() {
		return MBD_ITEM_TAX_PERC;
	}

	public void setMBD_ITEM_TAX_PERC(Double mBD_ITEM_TAX_PERC) {
		MBD_ITEM_TAX_PERC = mBD_ITEM_TAX_PERC;
	}

	public Double getMBD_PURCHASE_RATE() {
		return MBD_PURCHASE_RATE;
	}

	public void setMBD_PURCHASE_RATE(Double mBD_PURCHASE_RATE) {
		MBD_PURCHASE_RATE = mBD_PURCHASE_RATE;
	}

	public Double getMBD_TOT_TAX_AMT() {
		return MBD_TOT_TAX_AMT;
	}

	public void setMBD_TOT_TAX_AMT(Double mBD_TOT_TAX_AMT) {
		MBD_TOT_TAX_AMT = mBD_TOT_TAX_AMT;
	}

	public Double getMBD_DISC_AMT() {
		return MBD_DISC_AMT;
	}

	public void setMBD_DISC_AMT(Double mBD_DISC_AMT) {
		MBD_DISC_AMT = mBD_DISC_AMT;
	}

	public String getMBD_INCLUSIVEFLAG() {
		return MBD_INCLUSIVEFLAG;
	}

	public void setMBD_INCLUSIVEFLAG(String mBD_INCLUSIVEFLAG) {
		MBD_INCLUSIVEFLAG = mBD_INCLUSIVEFLAG;
	}

	public Integer getMBD_TAX_CONFIG() {
		return MBD_TAX_CONFIG;
	}

	public void setMBD_TAX_CONFIG(Integer mBD_TAX_CONFIG) {
		MBD_TAX_CONFIG = mBD_TAX_CONFIG;
	}

	public Integer getMbd_item_rowid() {
		return mbd_item_rowid;
	}

	public void setMbd_item_rowid(Integer mbd_item_rowid) {
		this.mbd_item_rowid = mbd_item_rowid;
	}

	public String getMbd_item_deliver() {
		return mbd_item_deliver;
	}

	public void setMbd_item_deliver(String mbd_item_deliver) {
		this.mbd_item_deliver = mbd_item_deliver;
	}

	public Double getMbd_item_schm_code() {
		return mbd_item_schm_code;
	}

	public void setMbd_item_schm_code(Double mbd_item_schm_code) {
		this.mbd_item_schm_code = mbd_item_schm_code;
	}

	public Double getMbd_item_schm_slno() {
		return mbd_item_schm_slno;
	}

	public void setMbd_item_schm_slno(Double mbd_item_schm_slno) {
		this.mbd_item_schm_slno = mbd_item_schm_slno;
	}

	public Double getMbd_item_schm_discperc() {
		return mbd_item_schm_discperc;
	}

	public void setMbd_item_schm_discperc(Double mbd_item_schm_discperc) {
		this.mbd_item_schm_discperc = mbd_item_schm_discperc;
	}

	public Double getMbd_item_schm_discamt() {
		return mbd_item_schm_discamt;
	}

	public void setMbd_item_schm_discamt(Double mbd_item_schm_discamt) {
		this.mbd_item_schm_discamt = mbd_item_schm_discamt;
	}

	public Double getMbd_grind_amt() {
		return mbd_grind_amt;
	}

	public void setMbd_grind_amt(Double mbd_grind_amt) {
		this.mbd_grind_amt = mbd_grind_amt;
	}

	public Integer getMbd_compid() {
		return mbd_compid;
	}

	public void setMbd_compid(Integer mbd_compid) {
		this.mbd_compid = mbd_compid;
	}

	public Integer getMbd_diviid() {
		return mbd_diviid;
	}

	public void setMbd_diviid(Integer mbd_diviid) {
		this.mbd_diviid = mbd_diviid;
	}

	public Integer getMbd_locaid() {
		return mbd_locaid;
	}

	public void setMbd_locaid(Integer mbd_locaid) {
		this.mbd_locaid = mbd_locaid;
	}

	public Double getMbd_max_rate() {
		return mbd_max_rate;
	}

	public void setMbd_max_rate(Double mbd_max_rate) {
		this.mbd_max_rate = mbd_max_rate;
	}

	public Double getMbd_unit() {
		return mbd_unit;
	}

	public void setMbd_unit(Double mbd_unit) {
		this.mbd_unit = mbd_unit;
	}

	public Integer getMbd_price_level() {
		return mbd_price_level;
	}

	public void setMbd_price_level(Integer mbd_price_level) {
		this.mbd_price_level = mbd_price_level;
	}

	public Integer getMbd_Free_item() {
		return mbd_Free_item;
	}

	public void setMbd_Free_item(Integer mbd_Free_item) {
		this.mbd_Free_item = mbd_Free_item;
	}

	public Double getMbd_rst_per() {
		return mbd_rst_per;
	}

	public void setMbd_rst_per(Double mbd_rst_per) {
		this.mbd_rst_per = mbd_rst_per;
	}

	public Double getMbd_rst_amt() {
		return mbd_rst_amt;
	}

	public void setMbd_rst_amt(Double mbd_rst_amt) {
		this.mbd_rst_amt = mbd_rst_amt;
	}

	public Double getMbd_rstsc_per() {
		return mbd_rstsc_per;
	}

	public void setMbd_rstsc_per(Double mbd_rstsc_per) {
		this.mbd_rstsc_per = mbd_rstsc_per;
	}

	public Double getMbd_rstsc_amt() {
		return mbd_rstsc_amt;
	}

	public void setMbd_rstsc_amt(Double mbd_rstsc_amt) {
		this.mbd_rstsc_amt = mbd_rstsc_amt;
	}

	public Integer getMbd_REP_CODE() {
		return mbd_REP_CODE;
	}

	public void setMbd_REP_CODE(Integer mbd_REP_CODE) {
		this.mbd_REP_CODE = mbd_REP_CODE;
	}

	public Double getMbd_REP_COMM() {
		return mbd_REP_COMM;
	}

	public void setMbd_REP_COMM(Double mbd_REP_COMM) {
		this.mbd_REP_COMM = mbd_REP_COMM;
	}

	public Double getMBD_BILL_TOTDISC() {
		return MBD_BILL_TOTDISC;
	}

	public void setMBD_BILL_TOTDISC(Double mBD_BILL_TOTDISC) {
		MBD_BILL_TOTDISC = mBD_BILL_TOTDISC;
	}

	public Timestamp getTS() {
		return TS;
	}

	public void setTS(Timestamp tS) {
		TS = tS;
	}

	public Integer getTSID() {
		return TSID;
	}

	public void setTSID(Integer tSID) {
		TSID = tSID;
	}

	public Double getMBD_DISC_PERC() {
		return MBD_DISC_PERC;
	}

	public void setMBD_DISC_PERC(Double mBD_DISC_PERC) {
		MBD_DISC_PERC = mBD_DISC_PERC;
	}

	public Double getMbd_rep_amt() {
		return mbd_rep_amt;
	}

	public void setMbd_rep_amt(Double mbd_rep_amt) {
		this.mbd_rep_amt = mbd_rep_amt;
	}

	public Double getMbd_orig_rate() {
		return mbd_orig_rate;
	}

	public void setMbd_orig_rate(Double mbd_orig_rate) {
		this.mbd_orig_rate = mbd_orig_rate;
	}

	public Integer getMbd_sd_no() {
		return mbd_sd_no;
	}

	public void setMbd_sd_no(Integer mbd_sd_no) {
		this.mbd_sd_no = mbd_sd_no;
	}

	public Double getMbd_sd_qty() {
		return mbd_sd_qty;
	}

	public void setMbd_sd_qty(Double mbd_sd_qty) {
		this.mbd_sd_qty = mbd_sd_qty;
	}

	public Integer getMbd_qty_pricelvl() {
		return mbd_qty_pricelvl;
	}

	public void setMbd_qty_pricelvl(Integer mbd_qty_pricelvl) {
		this.mbd_qty_pricelvl = mbd_qty_pricelvl;
	}

	public Double getMbd_qty_pricelvl_selling() {
		return mbd_qty_pricelvl_selling;
	}

	public void setMbd_qty_pricelvl_selling(Double mbd_qty_pricelvl_selling) {
		this.mbd_qty_pricelvl_selling = mbd_qty_pricelvl_selling;
	}

	public Double getMbd_weight() {
		return mbd_weight;
	}

	public void setMbd_weight(Double mbd_weight) {
		this.mbd_weight = mbd_weight;
	}

	public Integer getMbd_modifier() {
		return mbd_modifier;
	}

	public void setMbd_modifier(Integer mbd_modifier) {
		this.mbd_modifier = mbd_modifier;
	}

	public Integer getMbd_modifierless() {
		return mbd_modifierless;
	}

	public void setMbd_modifierless(Integer mbd_modifierless) {
		this.mbd_modifierless = mbd_modifierless;
	}

	public Double getMbd_available_weight() {
		return mbd_available_weight;
	}

	public void setMbd_available_weight(Double mbd_available_weight) {
		this.mbd_available_weight = mbd_available_weight;
	}

	public Double getMbd_tax_sur_amt() {
		return mbd_tax_sur_amt;
	}

	public void setMbd_tax_sur_amt(Double mbd_tax_sur_amt) {
		this.mbd_tax_sur_amt = mbd_tax_sur_amt;
	}

	public Double getMbd_tax_sur_per() {
		return mbd_tax_sur_per;
	}

	public void setMbd_tax_sur_per(Double mbd_tax_sur_per) {
		this.mbd_tax_sur_per = mbd_tax_sur_per;
	}

	public Double getMbd_tax_sur_ItemAmount() {
		return mbd_tax_sur_ItemAmount;
	}

	public void setMbd_tax_sur_ItemAmount(Double mbd_tax_sur_ItemAmount) {
		this.mbd_tax_sur_ItemAmount = mbd_tax_sur_ItemAmount;
	}

	public Integer getMbd_id_withouttax() {
		return mbd_id_withouttax;
	}

	public void setMbd_id_withouttax(Integer mbd_id_withouttax) {
		this.mbd_id_withouttax = mbd_id_withouttax;
	}

	public Integer getMbd_cd_withtax() {
		return mbd_cd_withtax;
	}

	public void setMbd_cd_withtax(Integer mbd_cd_withtax) {
		this.mbd_cd_withtax = mbd_cd_withtax;
	}

	public Integer getMbd_shapecharge_code() {
		return mbd_shapecharge_code;
	}

	public void setMbd_shapecharge_code(Integer mbd_shapecharge_code) {
		this.mbd_shapecharge_code = mbd_shapecharge_code;
	}

	public Double getMbd_shapecharge_perc() {
		return mbd_shapecharge_perc;
	}

	public void setMbd_shapecharge_perc(Double mbd_shapecharge_perc) {
		this.mbd_shapecharge_perc = mbd_shapecharge_perc;
	}

	public Double getMbd_shapecharge_amt() {
		return mbd_shapecharge_amt;
	}

	public void setMbd_shapecharge_amt(Double mbd_shapecharge_amt) {
		this.mbd_shapecharge_amt = mbd_shapecharge_amt;
	}

	public String getMbd_item_remarks() {
		return mbd_item_remarks;
	}

	public void setMbd_item_remarks(String mbd_item_remarks) {
		this.mbd_item_remarks = mbd_item_remarks;
	}

	public Double getMbd_bags() {
		return mbd_bags;
	}

	public void setMbd_bags(Double mbd_bags) {
		this.mbd_bags = mbd_bags;
	}

	public Integer getRETAIL_OUTLET_ID() {
		return RETAIL_OUTLET_ID;
	}

	public void setRETAIL_OUTLET_ID(Integer rETAIL_OUTLET_ID) {
		RETAIL_OUTLET_ID = rETAIL_OUTLET_ID;
	}

	public String getMbd_scan_details() {
		return mbd_scan_details;
	}

	public void setMbd_scan_details(String mbd_scan_details) {
		this.mbd_scan_details = mbd_scan_details;
	}

	public Integer getMbd_sale_ret_slip_no() {
		return mbd_sale_ret_slip_no;
	}

	public void setMbd_sale_ret_slip_no(Integer mbd_sale_ret_slip_no) {
		this.mbd_sale_ret_slip_no = mbd_sale_ret_slip_no;
	}

	public Integer getMbd_sale_ret_slip_sl_no() {
		return mbd_sale_ret_slip_sl_no;
	}

	public void setMbd_sale_ret_slip_sl_no(Integer mbd_sale_ret_slip_sl_no) {
		this.mbd_sale_ret_slip_sl_no = mbd_sale_ret_slip_sl_no;
	}

	public Double getMbd_home_delivery_qty() {
		return mbd_home_delivery_qty;
	}

	public void setMbd_home_delivery_qty(Double mbd_home_delivery_qty) {
		this.mbd_home_delivery_qty = mbd_home_delivery_qty;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public Integer getMbd_offer_code() {
		return mbd_offer_code;
	}

	public void setMbd_offer_code(Integer mbd_offer_code) {
		this.mbd_offer_code = mbd_offer_code;
	}

	public Double getMbd_bill_schm_dis_amt() {
		return mbd_bill_schm_dis_amt;
	}

	public void setMbd_bill_schm_dis_amt(Double mbd_bill_schm_dis_amt) {
		this.mbd_bill_schm_dis_amt = mbd_bill_schm_dis_amt;
	}

	public Double getMbd_item_schm_oth_amt() {
		return mbd_item_schm_oth_amt;
	}

	public void setMbd_item_schm_oth_amt(Double mbd_item_schm_oth_amt) {
		this.mbd_item_schm_oth_amt = mbd_item_schm_oth_amt;
	}

	public Double getMbd_service_charge() {
		return mbd_service_charge;
	}

	public void setMbd_service_charge(Double mbd_service_charge) {
		this.mbd_service_charge = mbd_service_charge;
	}

	public Double getMbd_service_charge_amt() {
		return mbd_service_charge_amt;
	}

	public void setMbd_service_charge_amt(Double mbd_service_charge_amt) {
		this.mbd_service_charge_amt = mbd_service_charge_amt;
	}

	public Integer getMbd_mat_unique_ref() {
		return mbd_mat_unique_ref;
	}

	public void setMbd_mat_unique_ref(Integer mbd_mat_unique_ref) {
		this.mbd_mat_unique_ref = mbd_mat_unique_ref;
	}

	public Integer getMbd_org_slno() {
		return mbd_org_slno;
	}

	public void setMbd_org_slno(Integer mbd_org_slno) {
		this.mbd_org_slno = mbd_org_slno;
	}

	public String getMbd_conv_type() {
		return mbd_conv_type;
	}

	public void setMbd_conv_type(String mbd_conv_type) {
		this.mbd_conv_type = mbd_conv_type;
	}

	public Double getMbd_conv_factor() {
		return mbd_conv_factor;
	}

	public void setMbd_conv_factor(Double mbd_conv_factor) {
		this.mbd_conv_factor = mbd_conv_factor;
	}

	public String getMbd_eancode() {
		return mbd_eancode;
	}

	public void setMbd_eancode(String mbd_eancode) {
		this.mbd_eancode = mbd_eancode;
	}

	public Double getMbd_pur_price_wot() {
		return mbd_pur_price_wot;
	}

	public void setMbd_pur_price_wot(Double mbd_pur_price_wot) {
		this.mbd_pur_price_wot = mbd_pur_price_wot;
	}

	public Double getMbd_profit_amt() {
		return mbd_profit_amt;
	}

	public void setMbd_profit_amt(Double mbd_profit_amt) {
		this.mbd_profit_amt = mbd_profit_amt;
	}

	public Double getMbd_net_amt() {
		return mbd_net_amt;
	}

	public void setMbd_net_amt(Double mbd_net_amt) {
		this.mbd_net_amt = mbd_net_amt;
	}

	public Integer getMbd_half_half() {
		return mbd_half_half;
	}

	public void setMbd_half_half(Integer mbd_half_half) {
		this.mbd_half_half = mbd_half_half;
	}

	public Double getMbd_void_qty() {
		return mbd_void_qty;
	}

	public void setMbd_void_qty(Double mbd_void_qty) {
		this.mbd_void_qty = mbd_void_qty;
	}

	public Integer getMbd_Modifier_Hdr_id() {
		return Mbd_Modifier_Hdr_id;
	}

	public void setMbd_Modifier_Hdr_id(Integer mbd_Modifier_Hdr_id) {
		Mbd_Modifier_Hdr_id = mbd_Modifier_Hdr_id;
	}

	public Integer getMbd_Modifier_dtl_id() {
		return Mbd_Modifier_dtl_id;
	}

	public void setMbd_Modifier_dtl_id(Integer mbd_Modifier_dtl_id) {
		Mbd_Modifier_dtl_id = mbd_Modifier_dtl_id;
	}

	public String getMbd_Modifier_Desc() {
		return Mbd_Modifier_Desc;
	}

	public void setMbd_Modifier_Desc(String mbd_Modifier_Desc) {
		Mbd_Modifier_Desc = mbd_Modifier_Desc;
	}

	public Integer getMbd_Modifier_Parent_Code() {
		return Mbd_Modifier_Parent_Code;
	}

	public void setMbd_Modifier_Parent_Code(Integer mbd_Modifier_Parent_Code) {
		Mbd_Modifier_Parent_Code = mbd_Modifier_Parent_Code;
	}

	public Double getMbd_coupon_disc_amt() {
		return mbd_coupon_disc_amt;
	}

	public void setMbd_coupon_disc_amt(Double mbd_coupon_disc_amt) {
		this.mbd_coupon_disc_amt = mbd_coupon_disc_amt;
	}

	public Double getMbd_min_selling() {
		return mbd_min_selling;
	}

	public void setMbd_min_selling(Double mbd_min_selling) {
		this.mbd_min_selling = mbd_min_selling;
	}

	public Integer getMbd_comboset_id() {
		return mbd_comboset_id;
	}

	public void setMbd_comboset_id(Integer mbd_comboset_id) {
		this.mbd_comboset_id = mbd_comboset_id;
	}

	public String getMbd_def_incl_tag() {
		return mbd_def_incl_tag;
	}

	public void setMbd_def_incl_tag(String mbd_def_incl_tag) {
		this.mbd_def_incl_tag = mbd_def_incl_tag;
	}

	public Integer getMbdInvType() {
		return mbdInvType;
	}

	public void setMbdInvType(Integer mbdInvType) {
		this.mbdInvType = mbdInvType;
	}

	public Integer getMbdSplitBnoPk() {
		return mbdSplitBnoPk;
	}

	public void setMbdSplitBnoPk(Integer mbdSplitBnoPk) {
		this.mbdSplitBnoPk = mbdSplitBnoPk;
	}

	public Integer getMbd_vat_on_srvtax() {
		return mbd_vat_on_srvtax;
	}

	public void setMbd_vat_on_srvtax(Integer mbd_vat_on_srvtax) {
		this.mbd_vat_on_srvtax = mbd_vat_on_srvtax;
	}

	public Integer getMbd_discount_code() {
		return mbd_discount_code;
	}

	public void setMbd_discount_code(Integer mbd_discount_code) {
		this.mbd_discount_code = mbd_discount_code;
	}

	public Double getMbd_service_tax() {
		return mbd_service_tax;
	}

	public void setMbd_service_tax(Double mbd_service_tax) {
		this.mbd_service_tax = mbd_service_tax;
	}

	public Double getMbd_service_tax_amt() {
		return mbd_service_tax_amt;
	}

	public void setMbd_service_tax_amt(Double mbd_service_tax_amt) {
		this.mbd_service_tax_amt = mbd_service_tax_amt;
	}

	public Integer getMbd_item_group() {
		return mbd_item_group;
	}

	public void setMbd_item_group(Integer mbd_item_group) {
		this.mbd_item_group = mbd_item_group;
	}

	public String getMbd_openitem_remarks() {
		return mbd_openitem_remarks;
	}

	public void setMbd_openitem_remarks(String mbd_openitem_remarks) {
		this.mbd_openitem_remarks = mbd_openitem_remarks;
	}

	public Double getMbd_sgst_perc() {
		return mbd_sgst_perc;
	}

	public void setMbd_sgst_perc(Double mbd_sgst_perc) {
		this.mbd_sgst_perc = mbd_sgst_perc;
	}

	public Double getMbd_sgst_amt() {
		return mbd_sgst_amt;
	}

	public void setMbd_sgst_amt(Double mbd_sgst_amt) {
		this.mbd_sgst_amt = mbd_sgst_amt;
	}

	public Double getMbd_cgst_perc() {
		return mbd_cgst_perc;
	}

	public void setMbd_cgst_perc(Double mbd_cgst_perc) {
		this.mbd_cgst_perc = mbd_cgst_perc;
	}

	public Double getMbd_cgst_amt() {
		return mbd_cgst_amt;
	}

	public void setMbd_cgst_amt(Double mbd_cgst_amt) {
		this.mbd_cgst_amt = mbd_cgst_amt;
	}

	public Double getMbd_igst_perc() {
		return mbd_igst_perc;
	}

	public void setMbd_igst_perc(Double mbd_igst_perc) {
		this.mbd_igst_perc = mbd_igst_perc;
	}

	public Double getMbd_igst_amt() {
		return mbd_igst_amt;
	}

	public void setMbd_igst_amt(Double mbd_igst_amt) {
		this.mbd_igst_amt = mbd_igst_amt;
	}

	public Double getMbd_cess_perc() {
		return mbd_cess_perc;
	}

	public void setMbd_cess_perc(Double mbd_cess_perc) {
		this.mbd_cess_perc = mbd_cess_perc;
	}

	public Double getMbd_cess_amt() {
		return mbd_cess_amt;
	}

	public void setMbd_cess_amt(Double mbd_cess_amt) {
		this.mbd_cess_amt = mbd_cess_amt;
	}

	public String getMbd_gst_code() {
		return mbd_gst_code;
	}

	public void setMbd_gst_code(String mbd_gst_code) {
		this.mbd_gst_code = mbd_gst_code;
	}

	public Double getMbd_abatement_perc() {
		return mbd_abatement_perc;
	}

	public void setMbd_abatement_perc(Double mbd_abatement_perc) {
		this.mbd_abatement_perc = mbd_abatement_perc;
	}

	public Integer getMbd_gst_basedon() {
		return mbd_gst_basedon;
	}

	public void setMbd_gst_basedon(Integer mbd_gst_basedon) {
		this.mbd_gst_basedon = mbd_gst_basedon;
	}

	public Integer getMbd_tax_calc_on() {
		return mbd_tax_calc_on;
	}

	public void setMbd_tax_calc_on(Integer mbd_tax_calc_on) {
		this.mbd_tax_calc_on = mbd_tax_calc_on;
	}

	public Integer getMbd_free_Tax_calc() {
		return mbd_free_Tax_calc;
	}

	public void setMbd_free_Tax_calc(Integer mbd_free_Tax_calc) {
		this.mbd_free_Tax_calc = mbd_free_Tax_calc;
	}

	public Double getMbd_extra_cess_amt() {
		return mbd_extra_cess_amt;
	}

	public void setMbd_extra_cess_amt(Double mbd_extra_cess_amt) {
		this.mbd_extra_cess_amt = mbd_extra_cess_amt;
	}

	public Double getMbd_ExtraCess_Per_Qty() {
		return mbd_ExtraCess_Per_Qty;
	}

	public void setMbd_ExtraCess_Per_Qty(Double mbd_ExtraCess_Per_Qty) {
		this.mbd_ExtraCess_Per_Qty = mbd_ExtraCess_Per_Qty;
	}

	public Double getMbd_excise_Perc() {
		return mbd_excise_Perc;
	}

	public void setMbd_excise_Perc(Double mbd_excise_Perc) {
		this.mbd_excise_Perc = mbd_excise_Perc;
	}

	public Double getMbd_excise_amt() {
		return mbd_excise_amt;
	}

	public void setMbd_excise_amt(Double mbd_excise_amt) {
		this.mbd_excise_amt = mbd_excise_amt;
	}

	public String getMbd_conv_qty() {
		return mbd_conv_qty;
	}

	public void setMbd_conv_qty(String mbd_conv_qty) {
		this.mbd_conv_qty = mbd_conv_qty;
	}

	public String getMbd_conv_rate() {
		return mbd_conv_rate;
	}

	public void setMbd_conv_rate(String mbd_conv_rate) {
		this.mbd_conv_rate = mbd_conv_rate;
	}

	public String getMBD_CALAMITYCESS_PERC() {
		return MBD_CALAMITYCESS_PERC;
	}

	public void setMBD_CALAMITYCESS_PERC(String mBD_CALAMITYCESS_PERC) {
		MBD_CALAMITYCESS_PERC = mBD_CALAMITYCESS_PERC;
	}

	public String getMBD_CALAMITYCESS_AMT() {
		return MBD_CALAMITYCESS_AMT;
	}

	public void setMBD_CALAMITYCESS_AMT(String mBD_CALAMITYCESS_AMT) {
		MBD_CALAMITYCESS_AMT = mBD_CALAMITYCESS_AMT;
	}

	public String getMbd_item_schm_real_discperc() {
		return mbd_item_schm_real_discperc;
	}

	public void setMbd_item_schm_real_discperc(String mbd_item_schm_real_discperc) {
		this.mbd_item_schm_real_discperc = mbd_item_schm_real_discperc;
	}

	public String getMbd_offer_refno() {
		return mbd_offer_refno;
	}

	public void setMbd_offer_refno(String mbd_offer_refno) {
		this.mbd_offer_refno = mbd_offer_refno;
	}

	public String getMbd_albumcharge_code() {
		return mbd_albumcharge_code;
	}

	public void setMbd_albumcharge_code(String mbd_albumcharge_code) {
		this.mbd_albumcharge_code = mbd_albumcharge_code;
	}

	public String getMbd_albumcharge_perc() {
		return mbd_albumcharge_perc;
	}

	public void setMbd_albumcharge_perc(String mbd_albumcharge_perc) {
		this.mbd_albumcharge_perc = mbd_albumcharge_perc;
	}

	public String getMbd_albumcharge_amt() {
		return mbd_albumcharge_amt;
	}

	public void setMbd_albumcharge_amt(String mbd_albumcharge_amt) {
		this.mbd_albumcharge_amt = mbd_albumcharge_amt;
	}
}
