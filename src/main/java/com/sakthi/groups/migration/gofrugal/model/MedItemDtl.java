package com.sakthi.groups.migration.gofrugal.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "med_item_dtl")

public class MedItemDtl {

	@Id
	@Column(name = "MID_ITEM_CODE")
	private Integer MID_ITEM_CODE;

	@Column(name = "MID_RECEIPT_QTY")
	private Double MID_RECEIPT_QTY;

	@Column(name = "MID_RECEIPT_UNIT")
	private Integer MID_RECEIPT_UNIT;

	@Column(name = "MID_BAL_STOCK")
	private Double MID_BAL_STOCK;

	@Column(name = "MID_BATCH_DT")
	private Timestamp MID_BATCH_DT;

	@Column(name = "MID_EXPIRY_DT")
	private Timestamp MID_EXPIRY_DT;

	@Column(name = "MID_BATCH_NO")
	private Integer MID_BATCH_NO;

	@Column(name = "MID_MRP")
	private Double MID_MRP;

	@Column(name = "MID_SALE_TAX")
	private Double MID_SALE_TAX;

	@Column(name = "MID_PUR_PRICE")
	private Double MID_PUR_PRICE;

	@Column(name = "MID_PUR_TAX")
	private Double MID_PUR_TAX;

	@Column(name = "MID_MRC_NO")
	private Integer MID_MRC_NO;

	@Column(name = "MID_MRC_DT")
	private Timestamp MID_MRC_DT;

	@Column(name = "MID_DIST_CODE")
	private Integer MID_DIST_CODE;

	@Column(name = "MID_FREE_TAG")
	private String MID_FREE_TAG;

	@Column(name = "MID_TEMP_QTY")
	private Double MID_TEMP_QTY;

	@Column(name = "MID_TEMP_TAG")
	private String MID_TEMP_TAG;

	@Column(name = "MID_SALE_TAX_PERC")
	private Double MID_SALE_TAX_PERC;

	@Column(name = "MID_PUR_TAX_PERC")
	private Double MID_PUR_TAX_PERC;

	@Column(name = "MID_PROFIT_PERC")
	private Double MID_PROFIT_PERC;

	@Column(name = "mid_mrc_prefix")
	private String mid_mrc_prefix;

	@Column(name = "mid_dmrc_no")
	private String mid_dmrc_no;

	@Column(name = "mid_row_id")
	private Integer mid_row_id;

	@Column(name = "mid_compid")
	private Integer mid_compid;

	@Column(name = "mid_diviid")
	private Integer mid_diviid;

	@Column(name = "mid_locaid")
	private Integer mid_locaid;

	@Column(name = "mid_max_rate")
	private Double mid_max_rate;

	@Column(name = "mid_pur_disc_amt")
	private Double mid_pur_disc_amt;

	@Column(name = "mid_pur_disc_perc")
	private Double mid_pur_disc_perc;

	@Column(name = "TS")
	private Timestamp TS;

	@Column(name = "MID_PUR_DT")
	private Timestamp MID_PUR_DT;

	@Column(name = "MID_PUR_REF")
	private Integer MID_PUR_REF;

	@Column(name = "mid_pur_stock")
	private Double mid_pur_stock;

	@Column(name = "mid_cust_offer")
	private String mid_cust_offer;

	@Column(name = "mid_source_qty")
	private Integer mid_source_qty;

	@Column(name = "mid_offer_qty")
	private Integer mid_offer_qty;

	@Column(name = "mid_damage_stock")
	private Double mid_damage_stock;

	@Column(name = "mid_stock_class")
	private Double mid_stock_class;

	@Column(name = "mid_is_sold")
	private Integer mid_is_sold;

	@Column(name = "mid_erm_trans_id")
	private String mid_erm_trans_id;

	@Column(name = "mid_Base_Selling_Conf")
	private Integer mid_Base_Selling_Conf;

	@Column(name = "mid_Base_Selling_id")
	private Integer mid_Base_Selling_id;

	@Column(name = "mid_pkd_dt")
	private Timestamp mid_pkd_dt;

	@Column(name = "mid_OldBal_stock")
	private Double mid_OldBal_stock;

	@Column(name = "mid_eancode")
	private String mid_eancode;

	@Column(name = "mid_old_bags")
	private Double mid_old_bags;

	@Column(name = "mid_bags")
	private Double mid_bags;

	@Column(name = "RETAIL_OUTLET_ID")
	private Integer RETAIL_OUTLET_ID;

	@Column(name = "mid_mat_unique_ref")
	private Integer mid_mat_unique_ref;

	@Column(name = "mid_cid")
	private Integer mid_cid;

	@Column(name = "hq_purref_outletid")
	private Integer hq_purref_outletid;

	@Column(name = "mid_pur_rate")
	private Double mid_pur_rate;

	@Column(name = "mid_invtype")
	private Integer mid_invtype;

	@Column(name = "mid_tax_type")
	private Integer mid_tax_type;

	@Column(name = "mid_hold_qty")
	private String mid_hold_qty;

	@Column(name = "mid_unique_Barcode")
	private String mid_unique_Barcode;

	@Column(name = "mid_org_pur_price")
	private String mid_org_pur_price;

	public Integer getMID_ITEM_CODE() {
		return MID_ITEM_CODE;
	}

	public void setMID_ITEM_CODE(Integer mID_ITEM_CODE) {
		MID_ITEM_CODE = mID_ITEM_CODE;
	}

	public Double getMID_RECEIPT_QTY() {
		return MID_RECEIPT_QTY;
	}

	public void setMID_RECEIPT_QTY(Double mID_RECEIPT_QTY) {
		MID_RECEIPT_QTY = mID_RECEIPT_QTY;
	}

	public Integer getMID_RECEIPT_UNIT() {
		return MID_RECEIPT_UNIT;
	}

	public void setMID_RECEIPT_UNIT(Integer mID_RECEIPT_UNIT) {
		MID_RECEIPT_UNIT = mID_RECEIPT_UNIT;
	}

	public Double getMID_BAL_STOCK() {
		return MID_BAL_STOCK;
	}

	public void setMID_BAL_STOCK(Double mID_BAL_STOCK) {
		MID_BAL_STOCK = mID_BAL_STOCK;
	}

	public Timestamp getMID_BATCH_DT() {
		return MID_BATCH_DT;
	}

	public void setMID_BATCH_DT(Timestamp mID_BATCH_DT) {
		MID_BATCH_DT = mID_BATCH_DT;
	}

	public Timestamp getMID_EXPIRY_DT() {
		return MID_EXPIRY_DT;
	}

	public void setMID_EXPIRY_DT(Timestamp mID_EXPIRY_DT) {
		MID_EXPIRY_DT = mID_EXPIRY_DT;
	}

	public Integer getMID_BATCH_NO() {
		return MID_BATCH_NO;
	}

	public void setMID_BATCH_NO(Integer mID_BATCH_NO) {
		MID_BATCH_NO = mID_BATCH_NO;
	}

	public Double getMID_MRP() {
		return MID_MRP;
	}

	public void setMID_MRP(Double mID_MRP) {
		MID_MRP = mID_MRP;
	}

	public Double getMID_SALE_TAX() {
		return MID_SALE_TAX;
	}

	public void setMID_SALE_TAX(Double mID_SALE_TAX) {
		MID_SALE_TAX = mID_SALE_TAX;
	}

	public Double getMID_PUR_PRICE() {
		return MID_PUR_PRICE;
	}

	public void setMID_PUR_PRICE(Double mID_PUR_PRICE) {
		MID_PUR_PRICE = mID_PUR_PRICE;
	}

	public Double getMID_PUR_TAX() {
		return MID_PUR_TAX;
	}

	public void setMID_PUR_TAX(Double mID_PUR_TAX) {
		MID_PUR_TAX = mID_PUR_TAX;
	}

	public Integer getMID_MRC_NO() {
		return MID_MRC_NO;
	}

	public void setMID_MRC_NO(Integer mID_MRC_NO) {
		MID_MRC_NO = mID_MRC_NO;
	}

	public Timestamp getMID_MRC_DT() {
		return MID_MRC_DT;
	}

	public void setMID_MRC_DT(Timestamp mID_MRC_DT) {
		MID_MRC_DT = mID_MRC_DT;
	}

	public Integer getMID_DIST_CODE() {
		return MID_DIST_CODE;
	}

	public void setMID_DIST_CODE(Integer mID_DIST_CODE) {
		MID_DIST_CODE = mID_DIST_CODE;
	}

	public String getMID_FREE_TAG() {
		return MID_FREE_TAG;
	}

	public void setMID_FREE_TAG(String mID_FREE_TAG) {
		MID_FREE_TAG = mID_FREE_TAG;
	}

	public Double getMID_TEMP_QTY() {
		return MID_TEMP_QTY;
	}

	public void setMID_TEMP_QTY(Double mID_TEMP_QTY) {
		MID_TEMP_QTY = mID_TEMP_QTY;
	}

	public String getMID_TEMP_TAG() {
		return MID_TEMP_TAG;
	}

	public void setMID_TEMP_TAG(String mID_TEMP_TAG) {
		MID_TEMP_TAG = mID_TEMP_TAG;
	}

	public Double getMID_SALE_TAX_PERC() {
		return MID_SALE_TAX_PERC;
	}

	public void setMID_SALE_TAX_PERC(Double mID_SALE_TAX_PERC) {
		MID_SALE_TAX_PERC = mID_SALE_TAX_PERC;
	}

	public Double getMID_PUR_TAX_PERC() {
		return MID_PUR_TAX_PERC;
	}

	public void setMID_PUR_TAX_PERC(Double mID_PUR_TAX_PERC) {
		MID_PUR_TAX_PERC = mID_PUR_TAX_PERC;
	}

	public Double getMID_PROFIT_PERC() {
		return MID_PROFIT_PERC;
	}

	public void setMID_PROFIT_PERC(Double mID_PROFIT_PERC) {
		MID_PROFIT_PERC = mID_PROFIT_PERC;
	}

	public String getMid_mrc_prefix() {
		return mid_mrc_prefix;
	}

	public void setMid_mrc_prefix(String mid_mrc_prefix) {
		this.mid_mrc_prefix = mid_mrc_prefix;
	}

	public String getMid_dmrc_no() {
		return mid_dmrc_no;
	}

	public void setMid_dmrc_no(String mid_dmrc_no) {
		this.mid_dmrc_no = mid_dmrc_no;
	}

	public Integer getMid_row_id() {
		return mid_row_id;
	}

	public void setMid_row_id(Integer mid_row_id) {
		this.mid_row_id = mid_row_id;
	}

	public Integer getMid_compid() {
		return mid_compid;
	}

	public void setMid_compid(Integer mid_compid) {
		this.mid_compid = mid_compid;
	}

	public Integer getMid_diviid() {
		return mid_diviid;
	}

	public void setMid_diviid(Integer mid_diviid) {
		this.mid_diviid = mid_diviid;
	}

	public Integer getMid_locaid() {
		return mid_locaid;
	}

	public void setMid_locaid(Integer mid_locaid) {
		this.mid_locaid = mid_locaid;
	}

	public Double getMid_max_rate() {
		return mid_max_rate;
	}

	public void setMid_max_rate(Double mid_max_rate) {
		this.mid_max_rate = mid_max_rate;
	}

	public Double getMid_pur_disc_amt() {
		return mid_pur_disc_amt;
	}

	public void setMid_pur_disc_amt(Double mid_pur_disc_amt) {
		this.mid_pur_disc_amt = mid_pur_disc_amt;
	}

	public Double getMid_pur_disc_perc() {
		return mid_pur_disc_perc;
	}

	public void setMid_pur_disc_perc(Double mid_pur_disc_perc) {
		this.mid_pur_disc_perc = mid_pur_disc_perc;
	}

	public Timestamp getTS() {
		return TS;
	}

	public void setTS(Timestamp tS) {
		TS = tS;
	}

	public Timestamp getMID_PUR_DT() {
		return MID_PUR_DT;
	}

	public void setMID_PUR_DT(Timestamp mID_PUR_DT) {
		MID_PUR_DT = mID_PUR_DT;
	}

	public Integer getMID_PUR_REF() {
		return MID_PUR_REF;
	}

	public void setMID_PUR_REF(Integer mID_PUR_REF) {
		MID_PUR_REF = mID_PUR_REF;
	}

	public Double getMid_pur_stock() {
		return mid_pur_stock;
	}

	public void setMid_pur_stock(Double mid_pur_stock) {
		this.mid_pur_stock = mid_pur_stock;
	}

	public String getMid_cust_offer() {
		return mid_cust_offer;
	}

	public void setMid_cust_offer(String mid_cust_offer) {
		this.mid_cust_offer = mid_cust_offer;
	}

	public Integer getMid_source_qty() {
		return mid_source_qty;
	}

	public void setMid_source_qty(Integer mid_source_qty) {
		this.mid_source_qty = mid_source_qty;
	}

	public Integer getMid_offer_qty() {
		return mid_offer_qty;
	}

	public void setMid_offer_qty(Integer mid_offer_qty) {
		this.mid_offer_qty = mid_offer_qty;
	}

	public Double getMid_damage_stock() {
		return mid_damage_stock;
	}

	public void setMid_damage_stock(Double mid_damage_stock) {
		this.mid_damage_stock = mid_damage_stock;
	}

	public Double getMid_stock_class() {
		return mid_stock_class;
	}

	public void setMid_stock_class(Double mid_stock_class) {
		this.mid_stock_class = mid_stock_class;
	}

	public Integer getMid_is_sold() {
		return mid_is_sold;
	}

	public void setMid_is_sold(Integer mid_is_sold) {
		this.mid_is_sold = mid_is_sold;
	}

	public String getMid_erm_trans_id() {
		return mid_erm_trans_id;
	}

	public void setMid_erm_trans_id(String mid_erm_trans_id) {
		this.mid_erm_trans_id = mid_erm_trans_id;
	}

	public Integer getMid_Base_Selling_Conf() {
		return mid_Base_Selling_Conf;
	}

	public void setMid_Base_Selling_Conf(Integer mid_Base_Selling_Conf) {
		this.mid_Base_Selling_Conf = mid_Base_Selling_Conf;
	}

	public Integer getMid_Base_Selling_id() {
		return mid_Base_Selling_id;
	}

	public void setMid_Base_Selling_id(Integer mid_Base_Selling_id) {
		this.mid_Base_Selling_id = mid_Base_Selling_id;
	}

	public Timestamp getMid_pkd_dt() {
		return mid_pkd_dt;
	}

	public void setMid_pkd_dt(Timestamp mid_pkd_dt) {
		this.mid_pkd_dt = mid_pkd_dt;
	}

	public Double getMid_OldBal_stock() {
		return mid_OldBal_stock;
	}

	public void setMid_OldBal_stock(Double mid_OldBal_stock) {
		this.mid_OldBal_stock = mid_OldBal_stock;
	}

	public String getMid_eancode() {
		return mid_eancode;
	}

	public void setMid_eancode(String mid_eancode) {
		this.mid_eancode = mid_eancode;
	}

	public Double getMid_old_bags() {
		return mid_old_bags;
	}

	public void setMid_old_bags(Double mid_old_bags) {
		this.mid_old_bags = mid_old_bags;
	}

	public Double getMid_bags() {
		return mid_bags;
	}

	public void setMid_bags(Double mid_bags) {
		this.mid_bags = mid_bags;
	}

	public Integer getRETAIL_OUTLET_ID() {
		return RETAIL_OUTLET_ID;
	}

	public void setRETAIL_OUTLET_ID(Integer rETAIL_OUTLET_ID) {
		RETAIL_OUTLET_ID = rETAIL_OUTLET_ID;
	}

	public Integer getMid_mat_unique_ref() {
		return mid_mat_unique_ref;
	}

	public void setMid_mat_unique_ref(Integer mid_mat_unique_ref) {
		this.mid_mat_unique_ref = mid_mat_unique_ref;
	}

	public Integer getMid_cid() {
		return mid_cid;
	}

	public void setMid_cid(Integer mid_cid) {
		this.mid_cid = mid_cid;
	}

	public Integer getHq_purref_outletid() {
		return hq_purref_outletid;
	}

	public void setHq_purref_outletid(Integer hq_purref_outletid) {
		this.hq_purref_outletid = hq_purref_outletid;
	}

	public Double getMid_pur_rate() {
		return mid_pur_rate;
	}

	public void setMid_pur_rate(Double mid_pur_rate) {
		this.mid_pur_rate = mid_pur_rate;
	}

	public Integer getMid_invtype() {
		return mid_invtype;
	}

	public void setMid_invtype(Integer mid_invtype) {
		this.mid_invtype = mid_invtype;
	}

	public Integer getMid_tax_type() {
		return mid_tax_type;
	}

	public void setMid_tax_type(Integer mid_tax_type) {
		this.mid_tax_type = mid_tax_type;
	}

	public String getMid_hold_qty() {
		return mid_hold_qty;
	}

	public void setMid_hold_qty(String mid_hold_qty) {
		this.mid_hold_qty = mid_hold_qty;
	}

	public String getMid_unique_Barcode() {
		return mid_unique_Barcode;
	}

	public void setMid_unique_Barcode(String mid_unique_Barcode) {
		this.mid_unique_Barcode = mid_unique_Barcode;
	}

	public String getMid_org_pur_price() {
		return mid_org_pur_price;
	}

	public void setMid_org_pur_price(String mid_org_pur_price) {
		this.mid_org_pur_price = mid_org_pur_price;
	}

}
