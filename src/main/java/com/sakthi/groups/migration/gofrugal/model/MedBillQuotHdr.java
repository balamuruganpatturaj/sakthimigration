package com.sakthi.groups.migration.gofrugal.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MED_BILLQUOT_HDR")
public class MedBillQuotHdr {

	@Id
	@Column(name = "MBH_BILL_NO")
	private Integer MBH_BILL_NO;

	@Column(name = "MBH_BILL_DATE")
	private Timestamp MBH_BILL_DATE;

	@Column(name = "MBH_BILL_DOCT_CODE")
	private Integer MBH_BILL_DOCT_CODE;

	@Column(name = "MBH_BILL_DOCT_NAME")
	private String MBH_BILL_DOCT_NAME;

	@Column(name = "MBH_BILL_CUST_CODE")
	private Integer MBH_BILL_CUST_CODE;

	@Column(name = "MBH_BILL_CUST_NAME")
	private String MBH_BILL_CUST_NAME;

	@Column(name = "MBH_BILL_CUST_ADDR")
	private String MBH_BILL_CUST_ADDR;

	@Column(name = "MBH_BILL_DUE_DT")
	private Timestamp MBH_BILL_DUE_DT;

	@Column(name = "MBH_BILL_STATUS")
	private String MBH_BILL_STATUS;

	@Column(name = "MBH_DISC_AMOUNT")
	private Double MBH_DISC_AMOUNT;

	@Column(name = "MBH_BILL_AMOUNT")
	private Double MBH_BILL_AMOUNT;

	@Column(name = "MBH_BILL_TYPE")
	private String MBH_BILL_TYPE;

	@Column(name = "MBH_BILL_AMT_TEND")
	private Double MBH_BILL_AMT_TEND;

	@Column(name = "MBH_BILL_NARRATION")
	private String MBH_BILL_NARRATION;

	@Column(name = "MBH_TOT_ITEMS")
	private Integer MBH_TOT_ITEMS;

	@Column(name = "MBH_SCH_NO")
	private Integer MBH_SCH_NO;

	@Column(name = "MBH_PROFIT")
	private Double MBH_PROFIT;

	@Column(name = "MBH_PBILL_NO")
	private Integer MBH_PBILL_NO;

	@Column(name = "MBH_VNO")
	private Integer MBH_VNO;

	@Column(name = "MBH_RSTTAXAMT")
	private Double MBH_RSTTAXAMT;

	@Column(name = "MBH_RSTSURAMT")
	private Double MBH_RSTSURAMT;

	@Column(name = "MBH_LOG_ID")
	private String MBH_LOG_ID;

	@Column(name = "MBH_SESS_ID")
	private Integer MBH_SESS_ID;

	@Column(name = "MBH_SESS_DT")
	private String MBH_SESS_DT;

	@Column(name = "MBH_BILL_DIFFAMOUNT")
	private Double MBH_BILL_DIFFAMOUNT;

	@Column(name = "MBH_BILL_CUST_ADDR1")
	private String MBH_BILL_CUST_ADDR1;

	@Column(name = "MBH_VAT_AMT")
	private Double MBH_VAT_AMT;

	@Column(name = "MBH_DISC_PERC")
	private Double MBH_DISC_PERC;

	@Column(name = "MBH_CASH_AMT")
	private Double MBH_CASH_AMT;

	@Column(name = "MBH_CHEQUE_AMT")
	private Double MBH_CHEQUE_AMT;

	@Column(name = "MBH_COUPON_AMT")
	private Double MBH_COUPON_AMT;

	@Column(name = "MBH_CARD_AMT")
	private Double MBH_CARD_AMT;

	@Column(name = "MBH_CREDIT_AMT")
	private Double MBH_CREDIT_AMT;

	@Column(name = "MBH_COUPON_SRV")
	private Double MBH_COUPON_SRV;

	@Column(name = "MBH_COUPON_SRV_AMT")
	private Double MBH_COUPON_SRV_AMT;

	@Column(name = "MBH_CARD_SRV")
	private Double MBH_CARD_SRV;

	@Column(name = "MBH_CARD_SRV_AMT")
	private Double MBH_CARD_SRV_AMT;

	@Column(name = "MBH_COUNTER_CODE")
	private String MBH_COUNTER_CODE;

	@Column(name = "MBH_schm_amt")
	private Double MBH_schm_amt;

	@Column(name = "MBH_bill_schm_amt")
	private Double MBH_bill_schm_amt;

	@Column(name = "MBH_BILL_offer_code")
	private Double MBH_BILL_offer_code;

	@Column(name = "MBH_BILL_OFFER_SLNO")
	private Double MBH_BILL_OFFER_SLNO;

	@Column(name = "mbh_grind_amt")
	private Double mbh_grind_amt;

	@Column(name = "mbh_deliv_bill")
	private Integer mbh_deliv_bill;

	@Column(name = "mbh_deliv_date")
	private Timestamp mbh_deliv_date;

	@Column(name = "mbh_deliv_time")
	private String mbh_deliv_time;

	@Column(name = "mbh_deliv_boys")
	private Integer mbh_deliv_boys;

	@Column(name = "mbh_deliv_tag")
	private Integer mbh_deliv_tag;

	@Column(name = "mbh_deliv_print_tag")
	private Integer mbh_deliv_print_tag;

	@Column(name = "mbh_price_level")
	private Integer mbh_price_level;

	@Column(name = "mbh_chq_name")
	private String mbh_chq_name;

	@Column(name = "mbh_chq_no")
	private String mbh_chq_no;

	@Column(name = "mbh_coup_name")
	private String mbh_coup_name;

	@Column(name = "mbh_card_name")
	private String mbh_card_name;

	@Column(name = "mbh_card_hold_name")
	private String mbh_card_hold_name;

	@Column(name = "mbh_card_no")
	private String mbh_card_no;

	@Column(name = "mbh_compid")
	private Double mbh_compid;

	@Column(name = "mbh_diviid")
	private Double mbh_diviid;

	@Column(name = "mbh_locaid")
	private Double mbh_locaid;

	@Column(name = "mbh_bill_type_name")
	private String mbh_bill_type_name;

	@Column(name = "mbh_rep_name")
	private String mbh_rep_name;

	@Column(name = "mbh_ord_dt")
	private Timestamp mbh_ord_dt;

	@Column(name = "mbh_ord_no")
	private String mbh_ord_no;

	@Column(name = "MBH_ADDR1")
	private String MBH_ADDR1;

	@Column(name = "MBH_Manual")
	private String MBH_Manual;

	@Column(name = "mbh_loc_grpcode")
	private String mbh_loc_grpcode;

	@Column(name = "mbh_invoice")
	private String mbh_invoice;

	@Column(name = "MBH_BILL_TOTDISCAmt")
	private Double MBH_BILL_TOTDISCAmt;

	@Column(name = "TS")
	private Timestamp TS;

	@Column(name = "TSID")
	private Integer TSID;

	@Column(name = "Mbh_Sales_Type")
	private Integer Mbh_Sales_Type;

	@Column(name = "mbh_pack_charge")
	private Double mbh_pack_charge;

	@Column(name = "mbh_deliv_charge")
	private Double mbh_deliv_charge;

	@Column(name = "mbh_cess")
	private Double mbh_cess;

	@Column(name = "mbh_obser")
	private Double mbh_obser;

	@Column(name = "mbh_sur_charge_amount")
	private Double mbh_sur_charge_amount;

	@Column(name = "mbh_other_charges")
	private Double mbh_other_charges;

	@Column(name = "mbh_service_tax")
	private Double mbh_service_tax;

	@Column(name = "mbh_service_tax_amt")
	private Double mbh_service_tax_amt;

	@Column(name = "RETAIL_OUTLET_ID")
	private Integer RETAIL_OUTLET_ID;

	@Column(name = "timestamp")
	private Timestamp timestamp;

	@Column(name = "mbh_sgst_amt")
	private Double mbh_sgst_amt;

	@Column(name = "mbh_cgst_amt")
	private Double mbh_cgst_amt;

	@Column(name = "mbh_igst_amt")
	private Double mbh_igst_amt;

	@Column(name = "mbh_cess_amt")
	private Double mbh_cess_amt;

	@Column(name = "mbh_tax_calc_type")
	private Integer mbh_tax_calc_type;

	@Column(name = "mbh_location_type")
	private String mbh_location_type;

	@Column(name = "mbh_is_exempt")
	private Integer mbh_is_exempt;

	@Column(name = "mbh_otherCharge_tax_type")
	private Integer mbh_otherCharge_tax_type;

	@Column(name = "mbh_otherCharge_taxPerc")
	private Double mbh_otherCharge_taxPerc;

	@Column(name = "mbh_otherCharge_taxAmt")
	private Double mbh_otherCharge_taxAmt;

	@Column(name = "mbh_extra_cess_amt")
	private Double mbh_extra_cess_amt;

	@Column(name = "MBH_CALAMITYCESS_AMT")
	private Double MBH_CALAMITYCESS_AMT;

	public Integer getMBH_BILL_NO() {
		return MBH_BILL_NO;
	}

	public void setMBH_BILL_NO(Integer mBH_BILL_NO) {
		MBH_BILL_NO = mBH_BILL_NO;
	}

	public Timestamp getMBH_BILL_DATE() {
		return MBH_BILL_DATE;
	}

	public void setMBH_BILL_DATE(Timestamp mBH_BILL_DATE) {
		MBH_BILL_DATE = mBH_BILL_DATE;
	}

	public Integer getMBH_BILL_DOCT_CODE() {
		return MBH_BILL_DOCT_CODE;
	}

	public void setMBH_BILL_DOCT_CODE(Integer mBH_BILL_DOCT_CODE) {
		MBH_BILL_DOCT_CODE = mBH_BILL_DOCT_CODE;
	}

	public String getMBH_BILL_DOCT_NAME() {
		return MBH_BILL_DOCT_NAME;
	}

	public void setMBH_BILL_DOCT_NAME(String mBH_BILL_DOCT_NAME) {
		MBH_BILL_DOCT_NAME = mBH_BILL_DOCT_NAME;
	}

	public Integer getMBH_BILL_CUST_CODE() {
		return MBH_BILL_CUST_CODE;
	}

	public void setMBH_BILL_CUST_CODE(Integer mBH_BILL_CUST_CODE) {
		MBH_BILL_CUST_CODE = mBH_BILL_CUST_CODE;
	}

	public String getMBH_BILL_CUST_NAME() {
		return MBH_BILL_CUST_NAME;
	}

	public void setMBH_BILL_CUST_NAME(String mBH_BILL_CUST_NAME) {
		MBH_BILL_CUST_NAME = mBH_BILL_CUST_NAME;
	}

	public String getMBH_BILL_CUST_ADDR() {
		return MBH_BILL_CUST_ADDR;
	}

	public void setMBH_BILL_CUST_ADDR(String mBH_BILL_CUST_ADDR) {
		MBH_BILL_CUST_ADDR = mBH_BILL_CUST_ADDR;
	}

	public Timestamp getMBH_BILL_DUE_DT() {
		return MBH_BILL_DUE_DT;
	}

	public void setMBH_BILL_DUE_DT(Timestamp mBH_BILL_DUE_DT) {
		MBH_BILL_DUE_DT = mBH_BILL_DUE_DT;
	}

	public String getMBH_BILL_STATUS() {
		return MBH_BILL_STATUS;
	}

	public void setMBH_BILL_STATUS(String mBH_BILL_STATUS) {
		MBH_BILL_STATUS = mBH_BILL_STATUS;
	}

	public Double getMBH_DISC_AMOUNT() {
		return MBH_DISC_AMOUNT;
	}

	public void setMBH_DISC_AMOUNT(Double mBH_DISC_AMOUNT) {
		MBH_DISC_AMOUNT = mBH_DISC_AMOUNT;
	}

	public Double getMBH_BILL_AMOUNT() {
		return MBH_BILL_AMOUNT;
	}

	public void setMBH_BILL_AMOUNT(Double mBH_BILL_AMOUNT) {
		MBH_BILL_AMOUNT = mBH_BILL_AMOUNT;
	}

	public String getMBH_BILL_TYPE() {
		return MBH_BILL_TYPE;
	}

	public void setMBH_BILL_TYPE(String mBH_BILL_TYPE) {
		MBH_BILL_TYPE = mBH_BILL_TYPE;
	}

	public Double getMBH_BILL_AMT_TEND() {
		return MBH_BILL_AMT_TEND;
	}

	public void setMBH_BILL_AMT_TEND(Double mBH_BILL_AMT_TEND) {
		MBH_BILL_AMT_TEND = mBH_BILL_AMT_TEND;
	}

	public String getMBH_BILL_NARRATION() {
		return MBH_BILL_NARRATION;
	}

	public void setMBH_BILL_NARRATION(String mBH_BILL_NARRATION) {
		MBH_BILL_NARRATION = mBH_BILL_NARRATION;
	}

	public Integer getMBH_TOT_ITEMS() {
		return MBH_TOT_ITEMS;
	}

	public void setMBH_TOT_ITEMS(Integer mBH_TOT_ITEMS) {
		MBH_TOT_ITEMS = mBH_TOT_ITEMS;
	}

	public Integer getMBH_SCH_NO() {
		return MBH_SCH_NO;
	}

	public void setMBH_SCH_NO(Integer mBH_SCH_NO) {
		MBH_SCH_NO = mBH_SCH_NO;
	}

	public Double getMBH_PROFIT() {
		return MBH_PROFIT;
	}

	public void setMBH_PROFIT(Double mBH_PROFIT) {
		MBH_PROFIT = mBH_PROFIT;
	}

	public Integer getMBH_PBILL_NO() {
		return MBH_PBILL_NO;
	}

	public void setMBH_PBILL_NO(Integer mBH_PBILL_NO) {
		MBH_PBILL_NO = mBH_PBILL_NO;
	}

	public Integer getMBH_VNO() {
		return MBH_VNO;
	}

	public void setMBH_VNO(Integer mBH_VNO) {
		MBH_VNO = mBH_VNO;
	}

	public Double getMBH_RSTTAXAMT() {
		return MBH_RSTTAXAMT;
	}

	public void setMBH_RSTTAXAMT(Double mBH_RSTTAXAMT) {
		MBH_RSTTAXAMT = mBH_RSTTAXAMT;
	}

	public Double getMBH_RSTSURAMT() {
		return MBH_RSTSURAMT;
	}

	public void setMBH_RSTSURAMT(Double mBH_RSTSURAMT) {
		MBH_RSTSURAMT = mBH_RSTSURAMT;
	}

	public String getMBH_LOG_ID() {
		return MBH_LOG_ID;
	}

	public void setMBH_LOG_ID(String mBH_LOG_ID) {
		MBH_LOG_ID = mBH_LOG_ID;
	}

	public Integer getMBH_SESS_ID() {
		return MBH_SESS_ID;
	}

	public void setMBH_SESS_ID(Integer mBH_SESS_ID) {
		MBH_SESS_ID = mBH_SESS_ID;
	}

	public String getMBH_SESS_DT() {
		return MBH_SESS_DT;
	}

	public void setMBH_SESS_DT(String mBH_SESS_DT) {
		MBH_SESS_DT = mBH_SESS_DT;
	}

	public Double getMBH_BILL_DIFFAMOUNT() {
		return MBH_BILL_DIFFAMOUNT;
	}

	public void setMBH_BILL_DIFFAMOUNT(Double mBH_BILL_DIFFAMOUNT) {
		MBH_BILL_DIFFAMOUNT = mBH_BILL_DIFFAMOUNT;
	}

	public String getMBH_BILL_CUST_ADDR1() {
		return MBH_BILL_CUST_ADDR1;
	}

	public void setMBH_BILL_CUST_ADDR1(String mBH_BILL_CUST_ADDR1) {
		MBH_BILL_CUST_ADDR1 = mBH_BILL_CUST_ADDR1;
	}

	public Double getMBH_VAT_AMT() {
		return MBH_VAT_AMT;
	}

	public void setMBH_VAT_AMT(Double mBH_VAT_AMT) {
		MBH_VAT_AMT = mBH_VAT_AMT;
	}

	public Double getMBH_DISC_PERC() {
		return MBH_DISC_PERC;
	}

	public void setMBH_DISC_PERC(Double mBH_DISC_PERC) {
		MBH_DISC_PERC = mBH_DISC_PERC;
	}

	public Double getMBH_CASH_AMT() {
		return MBH_CASH_AMT;
	}

	public void setMBH_CASH_AMT(Double mBH_CASH_AMT) {
		MBH_CASH_AMT = mBH_CASH_AMT;
	}

	public Double getMBH_CHEQUE_AMT() {
		return MBH_CHEQUE_AMT;
	}

	public void setMBH_CHEQUE_AMT(Double mBH_CHEQUE_AMT) {
		MBH_CHEQUE_AMT = mBH_CHEQUE_AMT;
	}

	public Double getMBH_COUPON_AMT() {
		return MBH_COUPON_AMT;
	}

	public void setMBH_COUPON_AMT(Double mBH_COUPON_AMT) {
		MBH_COUPON_AMT = mBH_COUPON_AMT;
	}

	public Double getMBH_CARD_AMT() {
		return MBH_CARD_AMT;
	}

	public void setMBH_CARD_AMT(Double mBH_CARD_AMT) {
		MBH_CARD_AMT = mBH_CARD_AMT;
	}

	public Double getMBH_CREDIT_AMT() {
		return MBH_CREDIT_AMT;
	}

	public void setMBH_CREDIT_AMT(Double mBH_CREDIT_AMT) {
		MBH_CREDIT_AMT = mBH_CREDIT_AMT;
	}

	public Double getMBH_COUPON_SRV() {
		return MBH_COUPON_SRV;
	}

	public void setMBH_COUPON_SRV(Double mBH_COUPON_SRV) {
		MBH_COUPON_SRV = mBH_COUPON_SRV;
	}

	public Double getMBH_COUPON_SRV_AMT() {
		return MBH_COUPON_SRV_AMT;
	}

	public void setMBH_COUPON_SRV_AMT(Double mBH_COUPON_SRV_AMT) {
		MBH_COUPON_SRV_AMT = mBH_COUPON_SRV_AMT;
	}

	public Double getMBH_CARD_SRV() {
		return MBH_CARD_SRV;
	}

	public void setMBH_CARD_SRV(Double mBH_CARD_SRV) {
		MBH_CARD_SRV = mBH_CARD_SRV;
	}

	public Double getMBH_CARD_SRV_AMT() {
		return MBH_CARD_SRV_AMT;
	}

	public void setMBH_CARD_SRV_AMT(Double mBH_CARD_SRV_AMT) {
		MBH_CARD_SRV_AMT = mBH_CARD_SRV_AMT;
	}

	public String getMBH_COUNTER_CODE() {
		return MBH_COUNTER_CODE;
	}

	public void setMBH_COUNTER_CODE(String mBH_COUNTER_CODE) {
		MBH_COUNTER_CODE = mBH_COUNTER_CODE;
	}

	public Double getMBH_schm_amt() {
		return MBH_schm_amt;
	}

	public void setMBH_schm_amt(Double mBH_schm_amt) {
		MBH_schm_amt = mBH_schm_amt;
	}

	public Double getMBH_bill_schm_amt() {
		return MBH_bill_schm_amt;
	}

	public void setMBH_bill_schm_amt(Double mBH_bill_schm_amt) {
		MBH_bill_schm_amt = mBH_bill_schm_amt;
	}

	public Double getMBH_BILL_offer_code() {
		return MBH_BILL_offer_code;
	}

	public void setMBH_BILL_offer_code(Double mBH_BILL_offer_code) {
		MBH_BILL_offer_code = mBH_BILL_offer_code;
	}

	public Double getMBH_BILL_OFFER_SLNO() {
		return MBH_BILL_OFFER_SLNO;
	}

	public void setMBH_BILL_OFFER_SLNO(Double mBH_BILL_OFFER_SLNO) {
		MBH_BILL_OFFER_SLNO = mBH_BILL_OFFER_SLNO;
	}

	public Double getMbh_grind_amt() {
		return mbh_grind_amt;
	}

	public void setMbh_grind_amt(Double mbh_grind_amt) {
		this.mbh_grind_amt = mbh_grind_amt;
	}

	public Integer getMbh_deliv_bill() {
		return mbh_deliv_bill;
	}

	public void setMbh_deliv_bill(Integer mbh_deliv_bill) {
		this.mbh_deliv_bill = mbh_deliv_bill;
	}

	public Timestamp getMbh_deliv_date() {
		return mbh_deliv_date;
	}

	public void setMbh_deliv_date(Timestamp mbh_deliv_date) {
		this.mbh_deliv_date = mbh_deliv_date;
	}

	public String getMbh_deliv_time() {
		return mbh_deliv_time;
	}

	public void setMbh_deliv_time(String mbh_deliv_time) {
		this.mbh_deliv_time = mbh_deliv_time;
	}

	public Integer getMbh_deliv_boys() {
		return mbh_deliv_boys;
	}

	public void setMbh_deliv_boys(Integer mbh_deliv_boys) {
		this.mbh_deliv_boys = mbh_deliv_boys;
	}

	public Integer getMbh_deliv_tag() {
		return mbh_deliv_tag;
	}

	public void setMbh_deliv_tag(Integer mbh_deliv_tag) {
		this.mbh_deliv_tag = mbh_deliv_tag;
	}

	public Integer getMbh_deliv_print_tag() {
		return mbh_deliv_print_tag;
	}

	public void setMbh_deliv_print_tag(Integer mbh_deliv_print_tag) {
		this.mbh_deliv_print_tag = mbh_deliv_print_tag;
	}

	public Integer getMbh_price_level() {
		return mbh_price_level;
	}

	public void setMbh_price_level(Integer mbh_price_level) {
		this.mbh_price_level = mbh_price_level;
	}

	public String getMbh_chq_name() {
		return mbh_chq_name;
	}

	public void setMbh_chq_name(String mbh_chq_name) {
		this.mbh_chq_name = mbh_chq_name;
	}

	public String getMbh_chq_no() {
		return mbh_chq_no;
	}

	public void setMbh_chq_no(String mbh_chq_no) {
		this.mbh_chq_no = mbh_chq_no;
	}

	public String getMbh_coup_name() {
		return mbh_coup_name;
	}

	public void setMbh_coup_name(String mbh_coup_name) {
		this.mbh_coup_name = mbh_coup_name;
	}

	public String getMbh_card_name() {
		return mbh_card_name;
	}

	public void setMbh_card_name(String mbh_card_name) {
		this.mbh_card_name = mbh_card_name;
	}

	public String getMbh_card_hold_name() {
		return mbh_card_hold_name;
	}

	public void setMbh_card_hold_name(String mbh_card_hold_name) {
		this.mbh_card_hold_name = mbh_card_hold_name;
	}

	public String getMbh_card_no() {
		return mbh_card_no;
	}

	public void setMbh_card_no(String mbh_card_no) {
		this.mbh_card_no = mbh_card_no;
	}

	public Double getMbh_compid() {
		return mbh_compid;
	}

	public void setMbh_compid(Double mbh_compid) {
		this.mbh_compid = mbh_compid;
	}

	public Double getMbh_diviid() {
		return mbh_diviid;
	}

	public void setMbh_diviid(Double mbh_diviid) {
		this.mbh_diviid = mbh_diviid;
	}

	public Double getMbh_locaid() {
		return mbh_locaid;
	}

	public void setMbh_locaid(Double mbh_locaid) {
		this.mbh_locaid = mbh_locaid;
	}

	public String getMbh_bill_type_name() {
		return mbh_bill_type_name;
	}

	public void setMbh_bill_type_name(String mbh_bill_type_name) {
		this.mbh_bill_type_name = mbh_bill_type_name;
	}

	public String getMbh_rep_name() {
		return mbh_rep_name;
	}

	public void setMbh_rep_name(String mbh_rep_name) {
		this.mbh_rep_name = mbh_rep_name;
	}

	public Timestamp getMbh_ord_dt() {
		return mbh_ord_dt;
	}

	public void setMbh_ord_dt(Timestamp mbh_ord_dt) {
		this.mbh_ord_dt = mbh_ord_dt;
	}

	public String getMbh_ord_no() {
		return mbh_ord_no;
	}

	public void setMbh_ord_no(String mbh_ord_no) {
		this.mbh_ord_no = mbh_ord_no;
	}

	public String getMBH_ADDR1() {
		return MBH_ADDR1;
	}

	public void setMBH_ADDR1(String mBH_ADDR1) {
		MBH_ADDR1 = mBH_ADDR1;
	}

	public String getMBH_Manual() {
		return MBH_Manual;
	}

	public void setMBH_Manual(String mBH_Manual) {
		MBH_Manual = mBH_Manual;
	}

	public String getMbh_loc_grpcode() {
		return mbh_loc_grpcode;
	}

	public void setMbh_loc_grpcode(String mbh_loc_grpcode) {
		this.mbh_loc_grpcode = mbh_loc_grpcode;
	}

	public String getMbh_invoice() {
		return mbh_invoice;
	}

	public void setMbh_invoice(String mbh_invoice) {
		this.mbh_invoice = mbh_invoice;
	}

	public Double getMBH_BILL_TOTDISCAmt() {
		return MBH_BILL_TOTDISCAmt;
	}

	public void setMBH_BILL_TOTDISCAmt(Double mBH_BILL_TOTDISCAmt) {
		MBH_BILL_TOTDISCAmt = mBH_BILL_TOTDISCAmt;
	}

	public Timestamp getTS() {
		return TS;
	}

	public void setTS(Timestamp tS) {
		TS = tS;
	}

	public Integer getTSID() {
		return TSID;
	}

	public void setTSID(Integer tSID) {
		TSID = tSID;
	}

	public Integer getMbh_Sales_Type() {
		return Mbh_Sales_Type;
	}

	public void setMbh_Sales_Type(Integer mbh_Sales_Type) {
		Mbh_Sales_Type = mbh_Sales_Type;
	}

	public Double getMbh_pack_charge() {
		return mbh_pack_charge;
	}

	public void setMbh_pack_charge(Double mbh_pack_charge) {
		this.mbh_pack_charge = mbh_pack_charge;
	}

	public Double getMbh_deliv_charge() {
		return mbh_deliv_charge;
	}

	public void setMbh_deliv_charge(Double mbh_deliv_charge) {
		this.mbh_deliv_charge = mbh_deliv_charge;
	}

	public Double getMbh_cess() {
		return mbh_cess;
	}

	public void setMbh_cess(Double mbh_cess) {
		this.mbh_cess = mbh_cess;
	}

	public Double getMbh_obser() {
		return mbh_obser;
	}

	public void setMbh_obser(Double mbh_obser) {
		this.mbh_obser = mbh_obser;
	}

	public Double getMbh_sur_charge_amount() {
		return mbh_sur_charge_amount;
	}

	public void setMbh_sur_charge_amount(Double mbh_sur_charge_amount) {
		this.mbh_sur_charge_amount = mbh_sur_charge_amount;
	}

	public Double getMbh_other_charges() {
		return mbh_other_charges;
	}

	public void setMbh_other_charges(Double mbh_other_charges) {
		this.mbh_other_charges = mbh_other_charges;
	}

	public Double getMbh_service_tax() {
		return mbh_service_tax;
	}

	public void setMbh_service_tax(Double mbh_service_tax) {
		this.mbh_service_tax = mbh_service_tax;
	}

	public Double getMbh_service_tax_amt() {
		return mbh_service_tax_amt;
	}

	public void setMbh_service_tax_amt(Double mbh_service_tax_amt) {
		this.mbh_service_tax_amt = mbh_service_tax_amt;
	}

	public Integer getRETAIL_OUTLET_ID() {
		return RETAIL_OUTLET_ID;
	}

	public void setRETAIL_OUTLET_ID(Integer rETAIL_OUTLET_ID) {
		RETAIL_OUTLET_ID = rETAIL_OUTLET_ID;
	}

	public Timestamp getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	public Double getMbh_sgst_amt() {
		return mbh_sgst_amt;
	}

	public void setMbh_sgst_amt(Double mbh_sgst_amt) {
		this.mbh_sgst_amt = mbh_sgst_amt;
	}

	public Double getMbh_cgst_amt() {
		return mbh_cgst_amt;
	}

	public void setMbh_cgst_amt(Double mbh_cgst_amt) {
		this.mbh_cgst_amt = mbh_cgst_amt;
	}

	public Double getMbh_igst_amt() {
		return mbh_igst_amt;
	}

	public void setMbh_igst_amt(Double mbh_igst_amt) {
		this.mbh_igst_amt = mbh_igst_amt;
	}

	public Double getMbh_cess_amt() {
		return mbh_cess_amt;
	}

	public void setMbh_cess_amt(Double mbh_cess_amt) {
		this.mbh_cess_amt = mbh_cess_amt;
	}

	public Integer getMbh_tax_calc_type() {
		return mbh_tax_calc_type;
	}

	public void setMbh_tax_calc_type(Integer mbh_tax_calc_type) {
		this.mbh_tax_calc_type = mbh_tax_calc_type;
	}

	public String getMbh_location_type() {
		return mbh_location_type;
	}

	public void setMbh_location_type(String mbh_location_type) {
		this.mbh_location_type = mbh_location_type;
	}

	public Integer getMbh_is_exempt() {
		return mbh_is_exempt;
	}

	public void setMbh_is_exempt(Integer mbh_is_exempt) {
		this.mbh_is_exempt = mbh_is_exempt;
	}

	public Integer getMbh_otherCharge_tax_type() {
		return mbh_otherCharge_tax_type;
	}

	public void setMbh_otherCharge_tax_type(Integer mbh_otherCharge_tax_type) {
		this.mbh_otherCharge_tax_type = mbh_otherCharge_tax_type;
	}

	public Double getMbh_otherCharge_taxPerc() {
		return mbh_otherCharge_taxPerc;
	}

	public void setMbh_otherCharge_taxPerc(Double mbh_otherCharge_taxPerc) {
		this.mbh_otherCharge_taxPerc = mbh_otherCharge_taxPerc;
	}

	public Double getMbh_otherCharge_taxAmt() {
		return mbh_otherCharge_taxAmt;
	}

	public void setMbh_otherCharge_taxAmt(Double mbh_otherCharge_taxAmt) {
		this.mbh_otherCharge_taxAmt = mbh_otherCharge_taxAmt;
	}

	public Double getMbh_extra_cess_amt() {
		return mbh_extra_cess_amt;
	}

	public void setMbh_extra_cess_amt(Double mbh_extra_cess_amt) {
		this.mbh_extra_cess_amt = mbh_extra_cess_amt;
	}

	public Double getMBH_CALAMITYCESS_AMT() {
		return MBH_CALAMITYCESS_AMT;
	}

	public void setMBH_CALAMITYCESS_AMT(Double mBH_CALAMITYCESS_AMT) {
		MBH_CALAMITYCESS_AMT = mBH_CALAMITYCESS_AMT;
	}

}
