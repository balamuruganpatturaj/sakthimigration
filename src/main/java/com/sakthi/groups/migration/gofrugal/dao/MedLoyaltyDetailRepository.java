package com.sakthi.groups.migration.gofrugal.dao;

import org.springframework.data.repository.CrudRepository;

import com.sakthi.groups.migration.gofrugal.model.MedLoyaltyDetail;

public interface MedLoyaltyDetailRepository extends CrudRepository<MedLoyaltyDetail, Integer> {
	
}
