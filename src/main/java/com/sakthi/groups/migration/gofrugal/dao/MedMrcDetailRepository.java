package com.sakthi.groups.migration.gofrugal.dao;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.sakthi.groups.migration.gofrugal.model.MedMrcDetail;

public interface MedMrcDetailRepository extends CrudRepository<MedMrcDetail, Integer> {

	List<MedMrcDetail> findByMmdMrcNoIn(Set<Integer> mrcNoSet);
	
	@Query("SELECT u.MMD_ITEM_CODE FROM MedMrcDetail u WHERE u.mmdMrcNo IN (?1)")
	public Set<Integer> findByMIH_ITEM_CODE(Set<Integer> mmdMrcNo);
	
	@Query("SELECT u FROM MedMrcDetail u WHERE u.mmdMrcNo = ?1")
	List<MedMrcDetail> findByMrcNo(Integer mmdMrcNo);
	
	@Query("SELECT u FROM MedMrcDetail u WHERE u.MMD_ITEM_CODE = ?1 order by u.mmdMrcNo desc")
	List<MedMrcDetail> findByMmdItemCode(Integer MMD_ITEM_CODE);

}
