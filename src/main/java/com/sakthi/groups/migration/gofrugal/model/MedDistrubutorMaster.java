package com.sakthi.groups.migration.gofrugal.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MED_DISTRIBUTOR_MAST")

public class MedDistrubutorMaster {

	@Id
	@Column(name = "MDM_DIST_CODE")
	private Integer mdmDistCode;
	@Column(name = "MDM_DIST_NAME")
	private String mdmDistName;
	@Column(name = "MDM_SHORT_NAME")
	private String mdmShortName;
	@Column(name = "MDM_DIST_ADDR1")
	private String mdmDistAddr1;
	@Column(name = "MDM_DIST_ADDR2")
	private String mdmDistAddr2;
	@Column(name = "MDM_DIST_ADDR3")
	private String mdmDistAddr3;
	@Column(name = "MDM_DIST_PIN")
	private Integer mdmDistPin;
	@Column(name = "MDM_DIST_TEL_RES")
	private String mdmDistTelRes;
	@Column(name = "MDM_DIST_TEL_OFF")
	private String mdmDistTelOff;
	@Column(name = "MDM_DIST_CELLPH")
	private String mdmDistCellPhone;
	@Column(name = "MDM_DIST_EMAIL_ID")
	private String mdmDistEmailId;
	@Column(name = "MDM_DIST_FAX")
	private String mdmDistFax;
	@Column(name = "MDM_CONT_PERSON")
	private String mdmContactPerson;
	@Column(name = "MDM_SALES_MAN1")
	private String mdmSalesMan1;
	@Column(name = "MDM_SALES_MAN2")
	private String mdmSalesMan2;
	@Column(name = "MDM_DIST_CR_DAYS")
	private Integer MDM_DIST_CR_DAYS;
	@Column(name = "MDM_TRADE_DISC_PER")
	private Float MDM_TRADE_DISC_PER;
	@Column(name = "MDM_DIST_TNGST_NO")
	private String MDM_DIST_TNGST_NO;
	@Column(name = "MDM_CREDITS")
	private Float MDM_CREDITS;
	@Column(name = "MDM_DEBITS")
	private Float MDM_DEBITS;
	@Column(name = "MDM_TAG")
	private String MDM_TAG;
	@Column(name = "MDM_OLD_DIST_CODE")
	private Integer MDM_OLD_DIST_CODE;
	@Column(name = "MDM_VAT_TIN")
	private String MDM_VAT_TIN;
	@Column(name = "MDM_LOCATION")
	private String MDM_LOCATION;
	@Column(name = "MDM_BALANCE")
	private Float MDM_BALANCE;
	@Column(name = "MDM_DRUG_LICNO")
	private String MDM_DRUG_LICNO;
	@Column(name = "mdm_compid")
	private Integer mdm_compid;
	@Column(name = "mdm_diviid")
	private Integer mdm_diviid;
	@Column(name = "TS")
	private Timestamp TS;
	@Column(name = "TSID")
	private Integer TSID;
	@Column(name = "mdm_purc_type")
	private Integer mdm_purc_type;
	@Column(name = "cid")
	private String cid;
	@Column(name = "IS_GLOBAL")
	private Integer IS_GLOBAL;
	@Column(name = "MDM_CURRENCY")
	private Integer MDM_CURRENCY;
	@Column(name = "mdm_hq_id")
	private Integer mdm_hq_id;
	@Column(name = "RETAIL_OUTLET_ID")
	private Integer RETAIL_OUTLET_ID;
	@Column(name = "MDM_CREATED_BY")
	private String MDM_CREATED_BY;
	@Column(name = "MDM_CREATED_DT_TIME")
	private Timestamp MDM_CREATED_DT_TIME;
	@Column(name = "MDM_UPDATED_BY")
	private String MDM_UPDATED_BY;
	@Column(name = "MDM_UPDATED_DT_TIMe")
	private Timestamp MDM_UPDATED_DT_TIMe;
	@Column(name = "mdm_formula_id")
	private Integer mdm_formula_id;
	@Column(name = "mdm_with_cform")
	private Integer mdm_with_cform;
	@Column(name = "mdm_cst_perc")
	private Float mdm_cst_perc;
	@Column(name = "mdm_nt_sec_code")
	private Integer mdm_nt_sec_code;
	@Column(name = "mdm_sun")
	private Integer mdm_sun;
	@Column(name = "mdm_Mon")
	private Integer mdm_Mon;
	@Column(name = "mdm_tue")
	private Integer mdm_tue;
	@Column(name = "mdm_Wed")
	private Integer mdm_Wed;
	@Column(name = "mdm_Thu")
	private Integer mdm_Thu;
	@Column(name = "mdm_Fri")
	private Integer mdm_Fri;
	@Column(name = "mdm_Sat")
	private Integer mdm_Sat;
	@Column(name = "MDM_SKIP_REPORT")
	private Integer MDM_SKIP_REPORT;
	@Column(name = "MDM_CHEQUE_PREPARE_NAME")
	private String MDM_CHEQUE_PREPARE_NAME;
	@Column(name = "HQ_HOMEOUTLET_ID")
	private Integer HQ_HOMEOUTLET_ID;
	@Column(name = "Mdm_PoExpDate_NDays")
	private Integer Mdm_PoExpDate_NDays;
	@Column(name = "MDM_SUPPLY_BASEDON")
	private Integer MDM_SUPPLY_BASEDON;
	@Column(name = "MDM_PO_TYPE")
	private Integer MDM_PO_TYPE;
	@Column(name = "MDM_NET_CREDIT_LIMIT")
	private Float MDM_NET_CREDIT_LIMIT;
	@Column(name = "MDM_gstno")
	private String MDM_gstno;
	@Column(name = "MDM_pan_no")
	private String MDM_pan_no;
	@Column(name = "MDM_aadhar_no")
	private String MDM_aadhar_no;
	@Column(name = "MDM_state_code")
	private Integer MDM_state_code;
	@Column(name = "MDM_GST_REG_TYPE")
	private Integer MDM_GST_REG_TYPE;
	@Column(name = "mdm_route_driver_no")
	private String mdm_route_driver_no;
	@Column(name = "MDM_COUNTRY_CODE")
	private Integer MDM_COUNTRY_CODE;
	@Column(name = "mdm_invtype")
	private String mdm_invtype;
	@Column(name = "mdm_route_driver_id")
	private Integer mdm_route_driver_id;
	@Column(name = "mdm_rep_code")
	private Integer mdm_rep_code;

	public Integer getMdmDistCode() {
		return mdmDistCode;
	}

	public void setMdmDistCode(Integer mdmDistCode) {
		this.mdmDistCode = mdmDistCode;
	}

	public String getMdmDistName() {
		return mdmDistName;
	}

	public void setMdmDistName(String mdmDistName) {
		this.mdmDistName = mdmDistName;
	}

	public String getMdmShortName() {
		return mdmShortName;
	}

	public void setMdmShortName(String mdmShortName) {
		this.mdmShortName = mdmShortName;
	}

	public String getMdmDistAddr1() {
		return mdmDistAddr1;
	}

	public void setMdmDistAddr1(String mdmDistAddr1) {
		this.mdmDistAddr1 = mdmDistAddr1;
	}

	public String getMdmDistAddr2() {
		return mdmDistAddr2;
	}

	public void setMdmDistAddr2(String mdmDistAddr2) {
		this.mdmDistAddr2 = mdmDistAddr2;
	}

	public String getMdmDistAddr3() {
		return mdmDistAddr3;
	}

	public void setMdmDistAddr3(String mdmDistAddr3) {
		this.mdmDistAddr3 = mdmDistAddr3;
	}

	public Integer getMdmDistPin() {
		return mdmDistPin;
	}

	public void setMdmDistPin(Integer mdmDistPin) {
		this.mdmDistPin = mdmDistPin;
	}

	public String getMdmDistTelRes() {
		return mdmDistTelRes;
	}

	public void setMdmDistTelRes(String mdmDistTelRes) {
		this.mdmDistTelRes = mdmDistTelRes;
	}

	public String getMdmDistTelOff() {
		return mdmDistTelOff;
	}

	public void setMdmDistTelOff(String mdmDistTelOff) {
		this.mdmDistTelOff = mdmDistTelOff;
	}

	public String getMdmDistCellPhone() {
		return mdmDistCellPhone;
	}

	public void setMdmDistCellPhone(String mdmDistCellPhone) {
		this.mdmDistCellPhone = mdmDistCellPhone;
	}

	public String getMdmDistEmailId() {
		return mdmDistEmailId;
	}

	public void setMdmDistEmailId(String mdmDistEmailId) {
		this.mdmDistEmailId = mdmDistEmailId;
	}

	public String getMdmDistFax() {
		return mdmDistFax;
	}

	public void setMdmDistFax(String mdmDistFax) {
		this.mdmDistFax = mdmDistFax;
	}

	public String getMdmContactPerson() {
		return mdmContactPerson;
	}

	public void setMdmContactPerson(String mdmContactPerson) {
		this.mdmContactPerson = mdmContactPerson;
	}

	public String getMdmSalesMan1() {
		return mdmSalesMan1;
	}

	public void setMdmSalesMan1(String mdmSalesMan1) {
		this.mdmSalesMan1 = mdmSalesMan1;
	}

	public String getMdmSalesMan2() {
		return mdmSalesMan2;
	}

	public void setMdmSalesMan2(String mdmSalesMan2) {
		this.mdmSalesMan2 = mdmSalesMan2;
	}

	public Integer getMDM_DIST_CR_DAYS() {
		return MDM_DIST_CR_DAYS;
	}

	public void setMDM_DIST_CR_DAYS(Integer mDM_DIST_CR_DAYS) {
		MDM_DIST_CR_DAYS = mDM_DIST_CR_DAYS;
	}

	public Float getMDM_TRADE_DISC_PER() {
		return MDM_TRADE_DISC_PER;
	}

	public void setMDM_TRADE_DISC_PER(Float mDM_TRADE_DISC_PER) {
		MDM_TRADE_DISC_PER = mDM_TRADE_DISC_PER;
	}

	public String getMDM_DIST_TNGST_NO() {
		return MDM_DIST_TNGST_NO;
	}

	public void setMDM_DIST_TNGST_NO(String mDM_DIST_TNGST_NO) {
		MDM_DIST_TNGST_NO = mDM_DIST_TNGST_NO;
	}

	public Float getMDM_CREDITS() {
		return MDM_CREDITS;
	}

	public void setMDM_CREDITS(Float mDM_CREDITS) {
		MDM_CREDITS = mDM_CREDITS;
	}

	public Float getMDM_DEBITS() {
		return MDM_DEBITS;
	}

	public void setMDM_DEBITS(Float mDM_DEBITS) {
		MDM_DEBITS = mDM_DEBITS;
	}

	public String getMDM_TAG() {
		return MDM_TAG;
	}

	public void setMDM_TAG(String mDM_TAG) {
		MDM_TAG = mDM_TAG;
	}

	public Integer getMDM_OLD_DIST_CODE() {
		return MDM_OLD_DIST_CODE;
	}

	public void setMDM_OLD_DIST_CODE(Integer mDM_OLD_DIST_CODE) {
		MDM_OLD_DIST_CODE = mDM_OLD_DIST_CODE;
	}

	public String getMDM_VAT_TIN() {
		return MDM_VAT_TIN;
	}

	public void setMDM_VAT_TIN(String mDM_VAT_TIN) {
		MDM_VAT_TIN = mDM_VAT_TIN;
	}

	public String getMDM_LOCATION() {
		return MDM_LOCATION;
	}

	public void setMDM_LOCATION(String mDM_LOCATION) {
		MDM_LOCATION = mDM_LOCATION;
	}

	public Float getMDM_BALANCE() {
		return MDM_BALANCE;
	}

	public void setMDM_BALANCE(Float mDM_BALANCE) {
		MDM_BALANCE = mDM_BALANCE;
	}

	public String getMDM_DRUG_LICNO() {
		return MDM_DRUG_LICNO;
	}

	public void setMDM_DRUG_LICNO(String mDM_DRUG_LICNO) {
		MDM_DRUG_LICNO = mDM_DRUG_LICNO;
	}

	public Integer getMdm_compid() {
		return mdm_compid;
	}

	public void setMdm_compid(Integer mdm_compid) {
		this.mdm_compid = mdm_compid;
	}

	public Integer getMdm_diviid() {
		return mdm_diviid;
	}

	public void setMdm_diviid(Integer mdm_diviid) {
		this.mdm_diviid = mdm_diviid;
	}

	public Timestamp getTS() {
		return TS;
	}

	public void setTS(Timestamp tS) {
		TS = tS;
	}

	public Integer getTSID() {
		return TSID;
	}

	public void setTSID(Integer tSID) {
		TSID = tSID;
	}

	public Integer getMdm_purc_type() {
		return mdm_purc_type;
	}

	public void setMdm_purc_type(Integer mdm_purc_type) {
		this.mdm_purc_type = mdm_purc_type;
	}

	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	public Integer getIS_GLOBAL() {
		return IS_GLOBAL;
	}

	public void setIS_GLOBAL(Integer iS_GLOBAL) {
		IS_GLOBAL = iS_GLOBAL;
	}

	public Integer getMDM_CURRENCY() {
		return MDM_CURRENCY;
	}

	public void setMDM_CURRENCY(Integer mDM_CURRENCY) {
		MDM_CURRENCY = mDM_CURRENCY;
	}

	public Integer getMdm_hq_id() {
		return mdm_hq_id;
	}

	public void setMdm_hq_id(Integer mdm_hq_id) {
		this.mdm_hq_id = mdm_hq_id;
	}

	public Integer getRETAIL_OUTLET_ID() {
		return RETAIL_OUTLET_ID;
	}

	public void setRETAIL_OUTLET_ID(Integer rETAIL_OUTLET_ID) {
		RETAIL_OUTLET_ID = rETAIL_OUTLET_ID;
	}

	public String getMDM_CREATED_BY() {
		return MDM_CREATED_BY;
	}

	public void setMDM_CREATED_BY(String mDM_CREATED_BY) {
		MDM_CREATED_BY = mDM_CREATED_BY;
	}

	public Timestamp getMDM_CREATED_DT_TIME() {
		return MDM_CREATED_DT_TIME;
	}

	public void setMDM_CREATED_DT_TIME(Timestamp mDM_CREATED_DT_TIME) {
		MDM_CREATED_DT_TIME = mDM_CREATED_DT_TIME;
	}

	public String getMDM_UPDATED_BY() {
		return MDM_UPDATED_BY;
	}

	public void setMDM_UPDATED_BY(String mDM_UPDATED_BY) {
		MDM_UPDATED_BY = mDM_UPDATED_BY;
	}

	public Timestamp getMDM_UPDATED_DT_TIMe() {
		return MDM_UPDATED_DT_TIMe;
	}

	public void setMDM_UPDATED_DT_TIMe(Timestamp mDM_UPDATED_DT_TIMe) {
		MDM_UPDATED_DT_TIMe = mDM_UPDATED_DT_TIMe;
	}

	public Integer getMdm_formula_id() {
		return mdm_formula_id;
	}

	public void setMdm_formula_id(Integer mdm_formula_id) {
		this.mdm_formula_id = mdm_formula_id;
	}

	public Integer getMdm_with_cform() {
		return mdm_with_cform;
	}

	public void setMdm_with_cform(Integer mdm_with_cform) {
		this.mdm_with_cform = mdm_with_cform;
	}

	public Float getMdm_cst_perc() {
		return mdm_cst_perc;
	}

	public void setMdm_cst_perc(Float mdm_cst_perc) {
		this.mdm_cst_perc = mdm_cst_perc;
	}

	public Integer getMdm_nt_sec_code() {
		return mdm_nt_sec_code;
	}

	public void setMdm_nt_sec_code(Integer mdm_nt_sec_code) {
		this.mdm_nt_sec_code = mdm_nt_sec_code;
	}

	public Integer getMdm_sun() {
		return mdm_sun;
	}

	public void setMdm_sun(Integer mdm_sun) {
		this.mdm_sun = mdm_sun;
	}

	public Integer getMdm_Mon() {
		return mdm_Mon;
	}

	public void setMdm_Mon(Integer mdm_Mon) {
		this.mdm_Mon = mdm_Mon;
	}

	public Integer getMdm_tue() {
		return mdm_tue;
	}

	public void setMdm_tue(Integer mdm_tue) {
		this.mdm_tue = mdm_tue;
	}

	public Integer getMdm_Wed() {
		return mdm_Wed;
	}

	public void setMdm_Wed(Integer mdm_Wed) {
		this.mdm_Wed = mdm_Wed;
	}

	public Integer getMdm_Thu() {
		return mdm_Thu;
	}

	public void setMdm_Thu(Integer mdm_Thu) {
		this.mdm_Thu = mdm_Thu;
	}

	public Integer getMdm_Fri() {
		return mdm_Fri;
	}

	public void setMdm_Fri(Integer mdm_Fri) {
		this.mdm_Fri = mdm_Fri;
	}

	public Integer getMdm_Sat() {
		return mdm_Sat;
	}

	public void setMdm_Sat(Integer mdm_Sat) {
		this.mdm_Sat = mdm_Sat;
	}

	public Integer getMDM_SKIP_REPORT() {
		return MDM_SKIP_REPORT;
	}

	public void setMDM_SKIP_REPORT(Integer mDM_SKIP_REPORT) {
		MDM_SKIP_REPORT = mDM_SKIP_REPORT;
	}

	public String getMDM_CHEQUE_PREPARE_NAME() {
		return MDM_CHEQUE_PREPARE_NAME;
	}

	public void setMDM_CHEQUE_PREPARE_NAME(String mDM_CHEQUE_PREPARE_NAME) {
		MDM_CHEQUE_PREPARE_NAME = mDM_CHEQUE_PREPARE_NAME;
	}

	public Integer getHQ_HOMEOUTLET_ID() {
		return HQ_HOMEOUTLET_ID;
	}

	public void setHQ_HOMEOUTLET_ID(Integer hQ_HOMEOUTLET_ID) {
		HQ_HOMEOUTLET_ID = hQ_HOMEOUTLET_ID;
	}

	public Integer getMdm_PoExpDate_NDays() {
		return Mdm_PoExpDate_NDays;
	}

	public void setMdm_PoExpDate_NDays(Integer mdm_PoExpDate_NDays) {
		Mdm_PoExpDate_NDays = mdm_PoExpDate_NDays;
	}

	public Integer getMDM_SUPPLY_BASEDON() {
		return MDM_SUPPLY_BASEDON;
	}

	public void setMDM_SUPPLY_BASEDON(Integer mDM_SUPPLY_BASEDON) {
		MDM_SUPPLY_BASEDON = mDM_SUPPLY_BASEDON;
	}

	public Integer getMDM_PO_TYPE() {
		return MDM_PO_TYPE;
	}

	public void setMDM_PO_TYPE(Integer mDM_PO_TYPE) {
		MDM_PO_TYPE = mDM_PO_TYPE;
	}

	public Float getMDM_NET_CREDIT_LIMIT() {
		return MDM_NET_CREDIT_LIMIT;
	}

	public void setMDM_NET_CREDIT_LIMIT(Float mDM_NET_CREDIT_LIMIT) {
		MDM_NET_CREDIT_LIMIT = mDM_NET_CREDIT_LIMIT;
	}

	public String getMDM_gstno() {
		return MDM_gstno;
	}

	public void setMDM_gstno(String mDM_gstno) {
		MDM_gstno = mDM_gstno;
	}

	public String getMDM_pan_no() {
		return MDM_pan_no;
	}

	public void setMDM_pan_no(String mDM_pan_no) {
		MDM_pan_no = mDM_pan_no;
	}

	public String getMDM_aadhar_no() {
		return MDM_aadhar_no;
	}

	public void setMDM_aadhar_no(String mDM_aadhar_no) {
		MDM_aadhar_no = mDM_aadhar_no;
	}

	public Integer getMDM_state_code() {
		return MDM_state_code;
	}

	public void setMDM_state_code(Integer mDM_state_code) {
		MDM_state_code = mDM_state_code;
	}

	public Integer getMDM_GST_REG_TYPE() {
		return MDM_GST_REG_TYPE;
	}

	public void setMDM_GST_REG_TYPE(Integer mDM_GST_REG_TYPE) {
		MDM_GST_REG_TYPE = mDM_GST_REG_TYPE;
	}

	public String getMdm_route_driver_no() {
		return mdm_route_driver_no;
	}

	public void setMdm_route_driver_no(String mdm_route_driver_no) {
		this.mdm_route_driver_no = mdm_route_driver_no;
	}

	public Integer getMDM_COUNTRY_CODE() {
		return MDM_COUNTRY_CODE;
	}

	public void setMDM_COUNTRY_CODE(Integer mDM_COUNTRY_CODE) {
		MDM_COUNTRY_CODE = mDM_COUNTRY_CODE;
	}

	public String getMdm_invtype() {
		return mdm_invtype;
	}

	public void setMdm_invtype(String mdm_invtype) {
		this.mdm_invtype = mdm_invtype;
	}

	public Integer getMdm_route_driver_id() {
		return mdm_route_driver_id;
	}

	public void setMdm_route_driver_id(Integer mdm_route_driver_id) {
		this.mdm_route_driver_id = mdm_route_driver_id;
	}

	public Integer getMdm_rep_code() {
		return mdm_rep_code;
	}

	public void setMdm_rep_code(Integer mdm_rep_code) {
		this.mdm_rep_code = mdm_rep_code;
	}

}
