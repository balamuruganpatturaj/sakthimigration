package com.sakthi.groups.migration.gofrugal.dao;

import org.springframework.data.repository.CrudRepository;

import com.sakthi.groups.migration.gofrugal.model.MedCategoryDetail;

public interface MedCategoryDetailRepository extends CrudRepository<MedCategoryDetail, Integer> {

}
