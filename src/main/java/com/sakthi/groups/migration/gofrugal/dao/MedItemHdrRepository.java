package com.sakthi.groups.migration.gofrugal.dao;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.sakthi.groups.migration.gofrugal.model.MedItemHdr;

public interface MedItemHdrRepository extends CrudRepository<MedItemHdr, Integer> {

	@Query("SELECT u FROM MedItemHdr u WHERE u.MIH_ITEM_REPACK_CONVERTION > ?1")
	public List<MedItemHdr> findByMIH_ITEM_REPACK_CONVERTION(Double repackValue);

	@Query("SELECT u.MIH_ITEM_NAME FROM MedItemHdr u WHERE u.MIH_ITEM_CODE = ?1")
	public String findByMIH_ITEM_CODE(Integer MIH_ITEM_CODE);
	
	@Query("SELECT u.MIH_ITEM_CODE FROM MedItemHdr u WHERE u.MIH_ITEM_REPACK_CONVERTION > 0.0 AND u.MIH_PREPARED = 'R' AND u.MIH_TRADE_CONF != 'N'")
	public Set<Integer> findRepackageItemCode();
}
