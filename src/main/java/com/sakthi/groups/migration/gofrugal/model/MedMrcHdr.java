package com.sakthi.groups.migration.gofrugal.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MED_MRC_HDR")
public class MedMrcHdr {

	@Column(name = "MMH_MRC_PREFIX")
	private String MMH_MRC_PREFIX;

	@Id
	@Column(name = "MMH_MRC_NO")
	private Integer MMH_MRC_NO;

	@Column(name = "MMH_MRC_DT")
	private Timestamp mmhMrcDate;

	@Column(name = "MMH_MRC_PO_NO")
	private Integer MMH_MRC_PO_NO;

	@Column(name = "MMH_DIST_CODE")
	private Integer mmhDistCode;

	@Column(name = "MMH_NO_OF_ITEMS")
	private Double MMH_NO_OF_ITEMS;

	@Column(name = "MMH_MRC_AMT")
	private Double MMH_MRC_AMT;

	@Column(name = "MMH_PAID_AMT")
	private Double MMH_PAID_AMT;

	@Column(name = "MMH_PAID_FLAG")
	private String MMH_PAID_FLAG;

	@Column(name = "MMH_LAST_PAID_DT")
	private Timestamp MMH_LAST_PAID_DT;

	@Column(name = "MMH_PAY_DUE_DT")
	private Timestamp MMH_PAY_DUE_DT;

	@Column(name = "MMH_LAST_PAID_AMT")
	private Double MMH_LAST_PAID_AMT;

	@Column(name = "MMH_DIST_BILL_NO")
	private String MMH_DIST_BILL_NO;

	@Column(name = "MMH_DIST_BILL_DT")
	private Timestamp mmhDistBillDt;

	@Column(name = "MMH_DISC_AMT")
	private Double MMH_DISC_AMT;

	@Column(name = "MMH_CREDIT_AMT")
	private Double MMH_CREDIT_AMT;

	@Column(name = "MMH_DEBIT_AMT")
	private Double MMH_DEBIT_AMT;

	@Column(name = "MMH_DIST_BILL_AMT")
	private Double MMH_DIST_BILL_AMT;

	@Column(name = "MMH_NARRATION")
	private String MMH_NARRATION;

	@Column(name = "MMH_MRC_TYPE")
	private String MMH_MRC_TYPE;

	@Column(name = "MMH_VNO")
	private Integer MMH_VNO;

	@Column(name = "MMH_RST_TAX_AMT")
	private Double MMH_RST_TAX_AMT;

	@Column(name = "MMH_DISC_PERC")
	private Double MMH_DISC_PERC;

	@Column(name = "MMH_COMPID")
	private Integer MMH_COMPID;

	@Column(name = "MMH_DIVIID")
	private Integer MMH_DIVIID;

	@Column(name = "MMH_LOCAID")
	private Integer MMH_LOCAID;

	@Column(name = "MMH_FREIGHT")
	private Double MMH_FREIGHT;

	@Column(name = "TS")
	private Timestamp TS;

	@Column(name = "TSID")
	private Integer TSID;

	@Column(name = "mmh_purc_type")
	private Integer mmh_purc_type;

	@Column(name = "mmh_tax_amt")
	private Double mmh_tax_amt;

	@Column(name = "mmh_manual_disc")
	private Double mmh_manual_disc;

	@Column(name = "mmh_netrate_aftcashdisc")
	private Integer mmh_netrate_aftcashdisc;

	@Column(name = "mmh_erm_trans_id")
	private Integer mmh_erm_trans_id;

	@Column(name = "mmh_cess")
	private Double mmh_cess;

	@Column(name = "mmh_obser")
	private Double mmh_obser;

	@Column(name = "mmh_commision")
	private Double mmh_commision;

	@Column(name = "mmh_tax_SC_amt")
	private Double mmh_tax_SC_amt;

	@Column(name = "mmh_gin_no")
	private Integer mmh_gin_no;

	@Column(name = "mmh_log_id")
	private String mmh_log_id;

	@Column(name = "mmh_passnslipno")
	private String mmh_passnslipno;

	@Column(name = "RETAIL_OUTLET_ID")
	private Integer RETAIL_OUTLET_ID;

	@Column(name = "mmh_cess_per")
	private Double mmh_cess_per;

	@Column(name = "mmh_obser_per")
	private Double mmh_obser_per;

	@Column(name = "mmh_packchrg_amt")
	private Double mmh_packchrg_amt;

	@Column(name = "mmh_ed_cess_per")
	private Double mmh_ed_cess_per;

	@Column(name = "mmh_ed_cess_amt")
	private Double mmh_ed_cess_amt;

	@Column(name = "mmh_adnl_ed_cess_per")
	private Double mmh_adnl_ed_cess_per;

	@Column(name = "mmh_adnl_ed_cess_amt")
	private Double mmh_adnl_ed_cess_amt;

	@Column(name = "mmh_ex_duty_perc")
	private Double mmh_ex_duty_perc;

	@Column(name = "mmh_ex_duty_amt")
	private Double mmh_ex_duty_amt;

	@Column(name = "mmh_invtype")
	private Integer mmh_invtype;

	@Column(name = "mmh_mrc_refno")
	private Integer mmh_mrc_refno;

	@Column(name = "mmh_with_cform")
	private Integer mmh_with_cform;

	@Column(name = "mmh_lr_no")
	private String mmh_lr_no;

	@Column(name = "mmh_Freight_PayLedger")
	private Integer mmh_Freight_PayLedger;

	@Column(name = "mmh_PackChrg_PayLedger")
	private Integer mmh_PackChrg_PayLedger;

	@Column(name = "mmh_shapecharge_amt")
	private Double mmh_shapecharge_amt;

	@Column(name = "mmh_tcs_perc")
	private Double mmh_tcs_perc;

	@Column(name = "mmh_tcs_amt")
	private Double mmh_tcs_amt;

	@Column(name = "mmh_pur_acknow_no")
	private Integer mmh_pur_acknow_no;

	@Column(name = "mmh_chq_bank")
	private Integer mmh_chq_bank;

	@Column(name = "mmh_chq_No")
	private Integer mmh_chq_No;

	@Column(name = "mmh_chq_Date")
	private Timestamp mmh_chq_Date;

	@Column(name = "mmh_currency_id")
	private Integer mmh_currency_id;

	@Column(name = "mmh_currency_exrate")
	private Double mmh_currency_exrate;

	@Column(name = "MMH_ADDLN_DISC1_PERC")
	private Double MMH_ADDLN_DISC1_PERC;

	@Column(name = "MMH_ADDLN_DISC1_AMT")
	private Double MMH_ADDLN_DISC1_AMT;

	@Column(name = "MMH_ADDLN_DISC2_PERC")
	private Double MMH_ADDLN_DISC2_PERC;

	@Column(name = "MMH_ADDLN_DISC2_AMT")
	private Double MMH_ADDLN_DISC2_AMT;

	@Column(name = "MMH_ADDLN_DISC3_PERC")
	private Double MMH_ADDLN_DISC3_PERC;

	@Column(name = "MMH_ADDLN_DISC3_AMT")
	private Double MMH_ADDLN_DISC3_AMT;

	@Column(name = "mmh_OtherCharge")
	private Double mmh_OtherCharge;

	@Column(name = "mmh_nt_tran_no")
	private String mmh_nt_tran_no;

	@Column(name = "mmh_rn_no")
	private Integer mmh_rn_no;

	@Column(name = "mmd_loc_trans_id")
	private Integer mmd_loc_trans_id;

	@Column(name = "mmh_dist_inv_seqno")
	private Integer mmh_dist_inv_seqno;

	@Column(name = "mmh_sgst_amt")
	private Double mmh_sgst_amt;

	@Column(name = "mmh_cgst_amt")
	private Double mmh_cgst_amt;

	@Column(name = "mmh_igst_amt")
	private Double mmh_igst_amt;

	@Column(name = "mmh_gst_cess_amt")
	private Double mmh_gst_cess_amt;

	@Column(name = "mmh_tax_calc_type")
	private Integer mmh_tax_calc_type;

	@Column(name = "mmh_location_type")
	private String mmh_location_type;

	@Column(name = "mmh_Reverse_Tax_Amt")
	private Double mmh_Reverse_Tax_Amt;

	@Column(name = "mmh_extra_cess_amt")
	private Double mmh_extra_cess_amt;

	@Column(name = "mmh_Extra_Charge_TaxAmt")
	private Double mmh_Extra_Charge_TaxAmt;

	@Column(name = "mmh_Extra_Charge_TaxCode")
	private Integer mmh_Extra_Charge_TaxCode;

	@Column(name = "mmh_Permit_No")
	private String mmh_Permit_No;

	@Column(name = "mmh_travel_exp_amt")
	private Double mmh_travel_exp_amt;

	@Column(name = "mmh_load_unload_charge")
	private Double mmh_load_unload_charge;

	@Column(name = "mmh_cust_duty_amt")
	private Double mmh_cust_duty_amt;

	@Column(name = "mmh_cust_duty_no")
	private String mmh_cust_duty_no;

	@Column(name = "mmh_mrc_entry_date")
	private Timestamp mmh_mrc_entry_date;

	@Column(name = "mmh_grnapp_start_time")
	private Timestamp mmh_grnapp_start_time;

	@Column(name = "mmh_grnapp_end_time")
	private Timestamp mmh_grnapp_end_time;

	@Column(name = "MMH_TRAN_REFNO")
	private String MMH_TRAN_REFNO;

	@Column(name = "mmh_goods_tcs_amt")
	private Double mmh_goods_tcs_amt;

	@Column(name = "mmh_Gate_dtls")
	private String mmh_Gate_dtls;

	public String getMMH_MRC_PREFIX() {
		return MMH_MRC_PREFIX;
	}

	public void setMMH_MRC_PREFIX(String mMH_MRC_PREFIX) {
		MMH_MRC_PREFIX = mMH_MRC_PREFIX;
	}

	public Integer getMMH_MRC_NO() {
		return MMH_MRC_NO;
	}

	public void setMMH_MRC_NO(Integer mMH_MRC_NO) {
		MMH_MRC_NO = mMH_MRC_NO;
	}

	public Timestamp getMmhMrcDate() {
		return mmhMrcDate;
	}

	public void setMmhMrcDate(Timestamp mmhMrcDate) {
		this.mmhMrcDate = mmhMrcDate;
	}

	public Integer getMMH_MRC_PO_NO() {
		return MMH_MRC_PO_NO;
	}

	public void setMMH_MRC_PO_NO(Integer mMH_MRC_PO_NO) {
		MMH_MRC_PO_NO = mMH_MRC_PO_NO;
	}

	public Integer getMmhDistCode() {
		return mmhDistCode;
	}

	public void setMmhDistCode(Integer mmhDistCode) {
		this.mmhDistCode = mmhDistCode;
	}

	public Double getMMH_NO_OF_ITEMS() {
		return MMH_NO_OF_ITEMS;
	}

	public void setMMH_NO_OF_ITEMS(Double mMH_NO_OF_ITEMS) {
		MMH_NO_OF_ITEMS = mMH_NO_OF_ITEMS;
	}

	public Double getMMH_MRC_AMT() {
		return MMH_MRC_AMT;
	}

	public void setMMH_MRC_AMT(Double mMH_MRC_AMT) {
		MMH_MRC_AMT = mMH_MRC_AMT;
	}

	public Double getMMH_PAID_AMT() {
		return MMH_PAID_AMT;
	}

	public void setMMH_PAID_AMT(Double mMH_PAID_AMT) {
		MMH_PAID_AMT = mMH_PAID_AMT;
	}

	public String getMMH_PAID_FLAG() {
		return MMH_PAID_FLAG;
	}

	public void setMMH_PAID_FLAG(String mMH_PAID_FLAG) {
		MMH_PAID_FLAG = mMH_PAID_FLAG;
	}

	public Timestamp getMMH_LAST_PAID_DT() {
		return MMH_LAST_PAID_DT;
	}

	public void setMMH_LAST_PAID_DT(Timestamp mMH_LAST_PAID_DT) {
		MMH_LAST_PAID_DT = mMH_LAST_PAID_DT;
	}

	public Timestamp getMMH_PAY_DUE_DT() {
		return MMH_PAY_DUE_DT;
	}

	public void setMMH_PAY_DUE_DT(Timestamp mMH_PAY_DUE_DT) {
		MMH_PAY_DUE_DT = mMH_PAY_DUE_DT;
	}

	public Double getMMH_LAST_PAID_AMT() {
		return MMH_LAST_PAID_AMT;
	}

	public void setMMH_LAST_PAID_AMT(Double mMH_LAST_PAID_AMT) {
		MMH_LAST_PAID_AMT = mMH_LAST_PAID_AMT;
	}

	public String getMMH_DIST_BILL_NO() {
		return MMH_DIST_BILL_NO;
	}

	public void setMMH_DIST_BILL_NO(String mMH_DIST_BILL_NO) {
		MMH_DIST_BILL_NO = mMH_DIST_BILL_NO;
	}

	public Timestamp getMmhDistBillDt() {
		return mmhDistBillDt;
	}

	public void setMmhDistBillDt(Timestamp mmhDistBillDt) {
		this.mmhDistBillDt = mmhDistBillDt;
	}

	public Double getMMH_DISC_AMT() {
		return MMH_DISC_AMT;
	}

	public void setMMH_DISC_AMT(Double mMH_DISC_AMT) {
		MMH_DISC_AMT = mMH_DISC_AMT;
	}

	public Double getMMH_CREDIT_AMT() {
		return MMH_CREDIT_AMT;
	}

	public void setMMH_CREDIT_AMT(Double mMH_CREDIT_AMT) {
		MMH_CREDIT_AMT = mMH_CREDIT_AMT;
	}

	public Double getMMH_DEBIT_AMT() {
		return MMH_DEBIT_AMT;
	}

	public void setMMH_DEBIT_AMT(Double mMH_DEBIT_AMT) {
		MMH_DEBIT_AMT = mMH_DEBIT_AMT;
	}

	public Double getMMH_DIST_BILL_AMT() {
		return MMH_DIST_BILL_AMT;
	}

	public void setMMH_DIST_BILL_AMT(Double mMH_DIST_BILL_AMT) {
		MMH_DIST_BILL_AMT = mMH_DIST_BILL_AMT;
	}

	public String getMMH_NARRATION() {
		return MMH_NARRATION;
	}

	public void setMMH_NARRATION(String mMH_NARRATION) {
		MMH_NARRATION = mMH_NARRATION;
	}

	public String getMMH_MRC_TYPE() {
		return MMH_MRC_TYPE;
	}

	public void setMMH_MRC_TYPE(String mMH_MRC_TYPE) {
		MMH_MRC_TYPE = mMH_MRC_TYPE;
	}

	public Integer getMMH_VNO() {
		return MMH_VNO;
	}

	public void setMMH_VNO(Integer mMH_VNO) {
		MMH_VNO = mMH_VNO;
	}

	public Double getMMH_RST_TAX_AMT() {
		return MMH_RST_TAX_AMT;
	}

	public void setMMH_RST_TAX_AMT(Double mMH_RST_TAX_AMT) {
		MMH_RST_TAX_AMT = mMH_RST_TAX_AMT;
	}

	public Double getMMH_DISC_PERC() {
		return MMH_DISC_PERC;
	}

	public void setMMH_DISC_PERC(Double mMH_DISC_PERC) {
		MMH_DISC_PERC = mMH_DISC_PERC;
	}

	public Integer getMMH_COMPID() {
		return MMH_COMPID;
	}

	public void setMMH_COMPID(Integer mMH_COMPID) {
		MMH_COMPID = mMH_COMPID;
	}

	public Integer getMMH_DIVIID() {
		return MMH_DIVIID;
	}

	public void setMMH_DIVIID(Integer mMH_DIVIID) {
		MMH_DIVIID = mMH_DIVIID;
	}

	public Integer getMMH_LOCAID() {
		return MMH_LOCAID;
	}

	public void setMMH_LOCAID(Integer mMH_LOCAID) {
		MMH_LOCAID = mMH_LOCAID;
	}

	public Double getMMH_FREIGHT() {
		return MMH_FREIGHT;
	}

	public void setMMH_FREIGHT(Double mMH_FREIGHT) {
		MMH_FREIGHT = mMH_FREIGHT;
	}

	public Timestamp getTS() {
		return TS;
	}

	public void setTS(Timestamp tS) {
		TS = tS;
	}

	public Integer getTSID() {
		return TSID;
	}

	public void setTSID(Integer tSID) {
		TSID = tSID;
	}

	public Integer getMmh_purc_type() {
		return mmh_purc_type;
	}

	public void setMmh_purc_type(Integer mmh_purc_type) {
		this.mmh_purc_type = mmh_purc_type;
	}

	public Double getMmh_tax_amt() {
		return mmh_tax_amt;
	}

	public void setMmh_tax_amt(Double mmh_tax_amt) {
		this.mmh_tax_amt = mmh_tax_amt;
	}

	public Double getMmh_manual_disc() {
		return mmh_manual_disc;
	}

	public void setMmh_manual_disc(Double mmh_manual_disc) {
		this.mmh_manual_disc = mmh_manual_disc;
	}

	public Integer getMmh_netrate_aftcashdisc() {
		return mmh_netrate_aftcashdisc;
	}

	public void setMmh_netrate_aftcashdisc(Integer mmh_netrate_aftcashdisc) {
		this.mmh_netrate_aftcashdisc = mmh_netrate_aftcashdisc;
	}

	public Integer getMmh_erm_trans_id() {
		return mmh_erm_trans_id;
	}

	public void setMmh_erm_trans_id(Integer mmh_erm_trans_id) {
		this.mmh_erm_trans_id = mmh_erm_trans_id;
	}

	public Double getMmh_cess() {
		return mmh_cess;
	}

	public void setMmh_cess(Double mmh_cess) {
		this.mmh_cess = mmh_cess;
	}

	public Double getMmh_obser() {
		return mmh_obser;
	}

	public void setMmh_obser(Double mmh_obser) {
		this.mmh_obser = mmh_obser;
	}

	public Double getMmh_commision() {
		return mmh_commision;
	}

	public void setMmh_commision(Double mmh_commision) {
		this.mmh_commision = mmh_commision;
	}

	public Double getMmh_tax_SC_amt() {
		return mmh_tax_SC_amt;
	}

	public void setMmh_tax_SC_amt(Double mmh_tax_SC_amt) {
		this.mmh_tax_SC_amt = mmh_tax_SC_amt;
	}

	public Integer getMmh_gin_no() {
		return mmh_gin_no;
	}

	public void setMmh_gin_no(Integer mmh_gin_no) {
		this.mmh_gin_no = mmh_gin_no;
	}

	public String getMmh_log_id() {
		return mmh_log_id;
	}

	public void setMmh_log_id(String mmh_log_id) {
		this.mmh_log_id = mmh_log_id;
	}

	public String getMmh_passnslipno() {
		return mmh_passnslipno;
	}

	public void setMmh_passnslipno(String mmh_passnslipno) {
		this.mmh_passnslipno = mmh_passnslipno;
	}

	public Integer getRETAIL_OUTLET_ID() {
		return RETAIL_OUTLET_ID;
	}

	public void setRETAIL_OUTLET_ID(Integer rETAIL_OUTLET_ID) {
		RETAIL_OUTLET_ID = rETAIL_OUTLET_ID;
	}

	public Double getMmh_cess_per() {
		return mmh_cess_per;
	}

	public void setMmh_cess_per(Double mmh_cess_per) {
		this.mmh_cess_per = mmh_cess_per;
	}

	public Double getMmh_obser_per() {
		return mmh_obser_per;
	}

	public void setMmh_obser_per(Double mmh_obser_per) {
		this.mmh_obser_per = mmh_obser_per;
	}

	public Double getMmh_packchrg_amt() {
		return mmh_packchrg_amt;
	}

	public void setMmh_packchrg_amt(Double mmh_packchrg_amt) {
		this.mmh_packchrg_amt = mmh_packchrg_amt;
	}

	public Double getMmh_ed_cess_per() {
		return mmh_ed_cess_per;
	}

	public void setMmh_ed_cess_per(Double mmh_ed_cess_per) {
		this.mmh_ed_cess_per = mmh_ed_cess_per;
	}

	public Double getMmh_ed_cess_amt() {
		return mmh_ed_cess_amt;
	}

	public void setMmh_ed_cess_amt(Double mmh_ed_cess_amt) {
		this.mmh_ed_cess_amt = mmh_ed_cess_amt;
	}

	public Double getMmh_adnl_ed_cess_per() {
		return mmh_adnl_ed_cess_per;
	}

	public void setMmh_adnl_ed_cess_per(Double mmh_adnl_ed_cess_per) {
		this.mmh_adnl_ed_cess_per = mmh_adnl_ed_cess_per;
	}

	public Double getMmh_adnl_ed_cess_amt() {
		return mmh_adnl_ed_cess_amt;
	}

	public void setMmh_adnl_ed_cess_amt(Double mmh_adnl_ed_cess_amt) {
		this.mmh_adnl_ed_cess_amt = mmh_adnl_ed_cess_amt;
	}

	public Double getMmh_ex_duty_perc() {
		return mmh_ex_duty_perc;
	}

	public void setMmh_ex_duty_perc(Double mmh_ex_duty_perc) {
		this.mmh_ex_duty_perc = mmh_ex_duty_perc;
	}

	public Double getMmh_ex_duty_amt() {
		return mmh_ex_duty_amt;
	}

	public void setMmh_ex_duty_amt(Double mmh_ex_duty_amt) {
		this.mmh_ex_duty_amt = mmh_ex_duty_amt;
	}

	public Integer getMmh_invtype() {
		return mmh_invtype;
	}

	public void setMmh_invtype(Integer mmh_invtype) {
		this.mmh_invtype = mmh_invtype;
	}

	public Integer getMmh_mrc_refno() {
		return mmh_mrc_refno;
	}

	public void setMmh_mrc_refno(Integer mmh_mrc_refno) {
		this.mmh_mrc_refno = mmh_mrc_refno;
	}

	public Integer getMmh_with_cform() {
		return mmh_with_cform;
	}

	public void setMmh_with_cform(Integer mmh_with_cform) {
		this.mmh_with_cform = mmh_with_cform;
	}

	public String getMmh_lr_no() {
		return mmh_lr_no;
	}

	public void setMmh_lr_no(String mmh_lr_no) {
		this.mmh_lr_no = mmh_lr_no;
	}

	public Integer getMmh_Freight_PayLedger() {
		return mmh_Freight_PayLedger;
	}

	public void setMmh_Freight_PayLedger(Integer mmh_Freight_PayLedger) {
		this.mmh_Freight_PayLedger = mmh_Freight_PayLedger;
	}

	public Integer getMmh_PackChrg_PayLedger() {
		return mmh_PackChrg_PayLedger;
	}

	public void setMmh_PackChrg_PayLedger(Integer mmh_PackChrg_PayLedger) {
		this.mmh_PackChrg_PayLedger = mmh_PackChrg_PayLedger;
	}

	public Double getMmh_shapecharge_amt() {
		return mmh_shapecharge_amt;
	}

	public void setMmh_shapecharge_amt(Double mmh_shapecharge_amt) {
		this.mmh_shapecharge_amt = mmh_shapecharge_amt;
	}

	public Double getMmh_tcs_perc() {
		return mmh_tcs_perc;
	}

	public void setMmh_tcs_perc(Double mmh_tcs_perc) {
		this.mmh_tcs_perc = mmh_tcs_perc;
	}

	public Double getMmh_tcs_amt() {
		return mmh_tcs_amt;
	}

	public void setMmh_tcs_amt(Double mmh_tcs_amt) {
		this.mmh_tcs_amt = mmh_tcs_amt;
	}

	public Integer getMmh_pur_acknow_no() {
		return mmh_pur_acknow_no;
	}

	public void setMmh_pur_acknow_no(Integer mmh_pur_acknow_no) {
		this.mmh_pur_acknow_no = mmh_pur_acknow_no;
	}

	public Integer getMmh_chq_bank() {
		return mmh_chq_bank;
	}

	public void setMmh_chq_bank(Integer mmh_chq_bank) {
		this.mmh_chq_bank = mmh_chq_bank;
	}

	public Integer getMmh_chq_No() {
		return mmh_chq_No;
	}

	public void setMmh_chq_No(Integer mmh_chq_No) {
		this.mmh_chq_No = mmh_chq_No;
	}

	public Timestamp getMmh_chq_Date() {
		return mmh_chq_Date;
	}

	public void setMmh_chq_Date(Timestamp mmh_chq_Date) {
		this.mmh_chq_Date = mmh_chq_Date;
	}

	public Integer getMmh_currency_id() {
		return mmh_currency_id;
	}

	public void setMmh_currency_id(Integer mmh_currency_id) {
		this.mmh_currency_id = mmh_currency_id;
	}

	public Double getMmh_currency_exrate() {
		return mmh_currency_exrate;
	}

	public void setMmh_currency_exrate(Double mmh_currency_exrate) {
		this.mmh_currency_exrate = mmh_currency_exrate;
	}

	public Double getMMH_ADDLN_DISC1_PERC() {
		return MMH_ADDLN_DISC1_PERC;
	}

	public void setMMH_ADDLN_DISC1_PERC(Double mMH_ADDLN_DISC1_PERC) {
		MMH_ADDLN_DISC1_PERC = mMH_ADDLN_DISC1_PERC;
	}

	public Double getMMH_ADDLN_DISC1_AMT() {
		return MMH_ADDLN_DISC1_AMT;
	}

	public void setMMH_ADDLN_DISC1_AMT(Double mMH_ADDLN_DISC1_AMT) {
		MMH_ADDLN_DISC1_AMT = mMH_ADDLN_DISC1_AMT;
	}

	public Double getMMH_ADDLN_DISC2_PERC() {
		return MMH_ADDLN_DISC2_PERC;
	}

	public void setMMH_ADDLN_DISC2_PERC(Double mMH_ADDLN_DISC2_PERC) {
		MMH_ADDLN_DISC2_PERC = mMH_ADDLN_DISC2_PERC;
	}

	public Double getMMH_ADDLN_DISC2_AMT() {
		return MMH_ADDLN_DISC2_AMT;
	}

	public void setMMH_ADDLN_DISC2_AMT(Double mMH_ADDLN_DISC2_AMT) {
		MMH_ADDLN_DISC2_AMT = mMH_ADDLN_DISC2_AMT;
	}

	public Double getMMH_ADDLN_DISC3_PERC() {
		return MMH_ADDLN_DISC3_PERC;
	}

	public void setMMH_ADDLN_DISC3_PERC(Double mMH_ADDLN_DISC3_PERC) {
		MMH_ADDLN_DISC3_PERC = mMH_ADDLN_DISC3_PERC;
	}

	public Double getMMH_ADDLN_DISC3_AMT() {
		return MMH_ADDLN_DISC3_AMT;
	}

	public void setMMH_ADDLN_DISC3_AMT(Double mMH_ADDLN_DISC3_AMT) {
		MMH_ADDLN_DISC3_AMT = mMH_ADDLN_DISC3_AMT;
	}

	public Double getMmh_OtherCharge() {
		return mmh_OtherCharge;
	}

	public void setMmh_OtherCharge(Double mmh_OtherCharge) {
		this.mmh_OtherCharge = mmh_OtherCharge;
	}

	public String getMmh_nt_tran_no() {
		return mmh_nt_tran_no;
	}

	public void setMmh_nt_tran_no(String mmh_nt_tran_no) {
		this.mmh_nt_tran_no = mmh_nt_tran_no;
	}

	public Integer getMmh_rn_no() {
		return mmh_rn_no;
	}

	public void setMmh_rn_no(Integer mmh_rn_no) {
		this.mmh_rn_no = mmh_rn_no;
	}

	public Integer getMmd_loc_trans_id() {
		return mmd_loc_trans_id;
	}

	public void setMmd_loc_trans_id(Integer mmd_loc_trans_id) {
		this.mmd_loc_trans_id = mmd_loc_trans_id;
	}

	public Integer getMmh_dist_inv_seqno() {
		return mmh_dist_inv_seqno;
	}

	public void setMmh_dist_inv_seqno(Integer mmh_dist_inv_seqno) {
		this.mmh_dist_inv_seqno = mmh_dist_inv_seqno;
	}

	public Double getMmh_sgst_amt() {
		return mmh_sgst_amt;
	}

	public void setMmh_sgst_amt(Double mmh_sgst_amt) {
		this.mmh_sgst_amt = mmh_sgst_amt;
	}

	public Double getMmh_cgst_amt() {
		return mmh_cgst_amt;
	}

	public void setMmh_cgst_amt(Double mmh_cgst_amt) {
		this.mmh_cgst_amt = mmh_cgst_amt;
	}

	public Double getMmh_igst_amt() {
		return mmh_igst_amt;
	}

	public void setMmh_igst_amt(Double mmh_igst_amt) {
		this.mmh_igst_amt = mmh_igst_amt;
	}

	public Double getMmh_gst_cess_amt() {
		return mmh_gst_cess_amt;
	}

	public void setMmh_gst_cess_amt(Double mmh_gst_cess_amt) {
		this.mmh_gst_cess_amt = mmh_gst_cess_amt;
	}

	public Integer getMmh_tax_calc_type() {
		return mmh_tax_calc_type;
	}

	public void setMmh_tax_calc_type(Integer mmh_tax_calc_type) {
		this.mmh_tax_calc_type = mmh_tax_calc_type;
	}

	public String getMmh_location_type() {
		return mmh_location_type;
	}

	public void setMmh_location_type(String mmh_location_type) {
		this.mmh_location_type = mmh_location_type;
	}

	public Double getMmh_Reverse_Tax_Amt() {
		return mmh_Reverse_Tax_Amt;
	}

	public void setMmh_Reverse_Tax_Amt(Double mmh_Reverse_Tax_Amt) {
		this.mmh_Reverse_Tax_Amt = mmh_Reverse_Tax_Amt;
	}

	public Double getMmh_extra_cess_amt() {
		return mmh_extra_cess_amt;
	}

	public void setMmh_extra_cess_amt(Double mmh_extra_cess_amt) {
		this.mmh_extra_cess_amt = mmh_extra_cess_amt;
	}

	public Double getMmh_Extra_Charge_TaxAmt() {
		return mmh_Extra_Charge_TaxAmt;
	}

	public void setMmh_Extra_Charge_TaxAmt(Double mmh_Extra_Charge_TaxAmt) {
		this.mmh_Extra_Charge_TaxAmt = mmh_Extra_Charge_TaxAmt;
	}

	public Integer getMmh_Extra_Charge_TaxCode() {
		return mmh_Extra_Charge_TaxCode;
	}

	public void setMmh_Extra_Charge_TaxCode(Integer mmh_Extra_Charge_TaxCode) {
		this.mmh_Extra_Charge_TaxCode = mmh_Extra_Charge_TaxCode;
	}

	public String getMmh_Permit_No() {
		return mmh_Permit_No;
	}

	public void setMmh_Permit_No(String mmh_Permit_No) {
		this.mmh_Permit_No = mmh_Permit_No;
	}

	public Double getMmh_travel_exp_amt() {
		return mmh_travel_exp_amt;
	}

	public void setMmh_travel_exp_amt(Double mmh_travel_exp_amt) {
		this.mmh_travel_exp_amt = mmh_travel_exp_amt;
	}

	public Double getMmh_load_unload_charge() {
		return mmh_load_unload_charge;
	}

	public void setMmh_load_unload_charge(Double mmh_load_unload_charge) {
		this.mmh_load_unload_charge = mmh_load_unload_charge;
	}

	public Double getMmh_cust_duty_amt() {
		return mmh_cust_duty_amt;
	}

	public void setMmh_cust_duty_amt(Double mmh_cust_duty_amt) {
		this.mmh_cust_duty_amt = mmh_cust_duty_amt;
	}

	public String getMmh_cust_duty_no() {
		return mmh_cust_duty_no;
	}

	public void setMmh_cust_duty_no(String mmh_cust_duty_no) {
		this.mmh_cust_duty_no = mmh_cust_duty_no;
	}

	public Timestamp getMmh_mrc_entry_date() {
		return mmh_mrc_entry_date;
	}

	public void setMmh_mrc_entry_date(Timestamp mmh_mrc_entry_date) {
		this.mmh_mrc_entry_date = mmh_mrc_entry_date;
	}

	public Timestamp getMmh_grnapp_start_time() {
		return mmh_grnapp_start_time;
	}

	public void setMmh_grnapp_start_time(Timestamp mmh_grnapp_start_time) {
		this.mmh_grnapp_start_time = mmh_grnapp_start_time;
	}

	public Timestamp getMmh_grnapp_end_time() {
		return mmh_grnapp_end_time;
	}

	public void setMmh_grnapp_end_time(Timestamp mmh_grnapp_end_time) {
		this.mmh_grnapp_end_time = mmh_grnapp_end_time;
	}

	public String getMMH_TRAN_REFNO() {
		return MMH_TRAN_REFNO;
	}

	public void setMMH_TRAN_REFNO(String mMH_TRAN_REFNO) {
		MMH_TRAN_REFNO = mMH_TRAN_REFNO;
	}

	public Double getMmh_goods_tcs_amt() {
		return mmh_goods_tcs_amt;
	}

	public void setMmh_goods_tcs_amt(Double mmh_goods_tcs_amt) {
		this.mmh_goods_tcs_amt = mmh_goods_tcs_amt;
	}

	public String getMmh_Gate_dtls() {
		return mmh_Gate_dtls;
	}

	public void setMmh_Gate_dtls(String mmh_Gate_dtls) {
		this.mmh_Gate_dtls = mmh_Gate_dtls;
	}

}
