package com.sakthi.groups.migration.gofrugal.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MED_BILL_HDR")
public class MedBillHdr {

	@Id
	@Column(name = "MBH_BILL_NO")
	private Integer mbhBillNo;

	@Column(name = "MBH_BILL_DATE")
	private Timestamp MBH_BILL_DATE;

	@Column(name = "MBH_BILL_DOCT_CODE")
	private Integer MBH_BILL_DOCT_CODE;

	@Column(name = "MBH_BILL_DOCT_NAME")
	private String MBH_BILL_DOCT_NAME;

	@Column(name = "MBH_BILL_CUST_CODE")
	private Integer MBH_BILL_CUST_CODE;

	@Column(name = "MBH_BILL_CUST_NAME")
	private String MBH_BILL_CUST_NAME;

	@Column(name = "MBH_BILL_CUST_ADDR")
	private String MBH_BILL_CUST_ADDR;

	@Column(name = "MBH_BILL_DUE_DT")
	private Timestamp MBH_BILL_DUE_DT;

	@Column(name = "MBH_BILL_STATUS")
	private String MBH_BILL_STATUS;

	@Column(name = "MBH_DISC_AMOUNT")
	private Double MBH_DISC_AMOUNT;

	@Column(name = "MBH_BILL_AMOUNT")
	private Double MBH_BILL_AMOUNT;

	@Column(name = "MBH_BILL_TYPE")
	private String MBH_BILL_TYPE;

	@Column(name = "MBH_BILL_AMT_TEND")
	private Double MBH_BILL_AMT_TEND;

	@Column(name = "MBH_BILL_NARRATION")
	private String MBH_BILL_NARRATION;

	@Column(name = "MBH_TOT_ITEMS")
	private Double MBH_TOT_ITEMS;

	@Column(name = "MBH_SCH_NO")
	private Integer MBH_SCH_NO;

	@Column(name = "MBH_PROFIT")
	private Double MBH_PROFIT;

	@Column(name = "MBH_PBILL_NO")
	private Integer MBH_PBILL_NO;

	@Column(name = "MBH_VNO")
	private Integer MBH_VNO;

	@Column(name = "MBH_RSTTAXAMT")
	private Double MBH_RSTTAXAMT;

	@Column(name = "MBH_RSTSURAMT")
	private Double MBH_RSTSURAMT;

	@Column(name = "MBH_LOG_ID")
	private String MBH_LOG_ID;

	@Column(name = "MBH_SESS_ID")
	private Integer MBH_SESS_ID;

	@Column(name = "MBH_SESS_DT")
	private String MBH_SESS_DT;

	@Column(name = "MBH_BILL_DIFFAMOUNT")
	private Double MBH_BILL_DIFFAMOUNT;

	@Column(name = "MBH_BILL_CUST_ADDR1")
	private String MBH_BILL_CUST_ADDR1;

	@Column(name = "MBH_VAT_AMT")
	private Double MBH_VAT_AMT;

	@Column(name = "MBH_DISC_PERC")
	private Double MBH_DISC_PERC;

	@Column(name = "MBH_CASH_AMT")
	private Double MBH_CASH_AMT;

	@Column(name = "MBH_CHEQUE_AMT")
	private Double MBH_CHEQUE_AMT;

	@Column(name = "MBH_COUPON_AMT")
	private Double MBH_COUPON_AMT;

	@Column(name = "MBH_CARD_AMT")
	private Double MBH_CARD_AMT;

	@Column(name = "MBH_CREDIT_AMT")
	private Double MBH_CREDIT_AMT;

	@Column(name = "MBH_COUPON_SRV")
	private Double MBH_COUPON_SRV;

	@Column(name = "MBH_COUPON_SRV_AMT")
	private Double MBH_COUPON_SRV_AMT;

	@Column(name = "MBH_CARD_SRV")
	private Double MBH_CARD_SRV;

	@Column(name = "MBH_CARD_SRV_AMT")
	private Double MBH_CARD_SRV_AMT;

	@Column(name = "MBH_COUNTER_CODE")
	private String MBH_COUNTER_CODE;

	@Column(name = "MBH_schm_amt")
	private Double MBH_schm_amt;

	@Column(name = "MBH_bill_schm_amt")
	private Double MBH_bill_schm_amt;

	@Column(name = "MBH_BILL_offer_code")
	private Double MBH_BILL_offer_code;

	@Column(name = "MBH_BILL_OFFER_SLNO")
	private Double MBH_BILL_OFFER_SLNO;

	@Column(name = "mbh_grind_amt")
	private Double mbh_grind_amt;

	@Column(name = "mbh_deliv_bill")
	private Integer mbh_deliv_bill;

	@Column(name = "mbh_deliv_date")
	private Timestamp mbh_deliv_date;

	@Column(name = "mbh_deliv_time")
	private String mbh_deliv_time;

	@Column(name = "mbh_deliv_boys")
	private Integer mbh_deliv_boys;

	@Column(name = "mbh_deliv_tag")
	private Integer mbh_deliv_tag;

	@Column(name = "mbh_deliv_print_tag")
	private Integer mbh_deliv_print_tag;

	@Column(name = "mbh_price_level")
	private Integer mbh_price_level;

	@Column(name = "mbh_chq_name")
	private String mbh_chq_name;

	@Column(name = "mbh_chq_no")
	private String mbh_chq_no;

	@Column(name = "mbh_coup_name")
	private String mbh_coup_name;

	@Column(name = "mbh_card_name")
	private String mbh_card_name;

	@Column(name = "mbh_card_hold_name")
	private String mbh_card_hold_name;

	@Column(name = "mbh_card_no")
	private String mbh_card_no;

	@Column(name = "mbh_compid")
	private Integer mbh_compid;

	@Column(name = "mbh_diviid")
	private Integer mbh_diviid;

	@Column(name = "mbh_locaid")
	private Integer mbh_locaid;

	@Column(name = "mbh_bill_type_name")
	private String mbh_bill_type_name;

	@Column(name = "mbh_rep_name")
	private String mbh_rep_name;

	@Column(name = "mbh_ord_dt")
	private Timestamp mbh_ord_dt;

	@Column(name = "mbh_ord_no")
	private String mbh_ord_no;

	@Column(name = "MBH_ADDR1")
	private String MBH_ADDR1;

	@Column(name = "MBH_Manual")
	private String MBH_Manual;

	@Column(name = "mbh_loc_grpcode")
	private String mbh_loc_grpcode;

	@Column(name = "mbh_invoice")
	private String mbh_invoice;

	@Column(name = "MBH_BILL_TOTDISCAmt")
	private Double MBH_BILL_TOTDISCAmt;

	@Column(name = "TS")
	private Timestamp TS;

	@Column(name = "TSID")
	private Integer TSID;

	@Column(name = "Mbh_Sales_Type")
	private Integer Mbh_Sales_Type;

	@Column(name = "mbh_pack_charge")
	private Double mbh_pack_charge;

	@Column(name = "mbh_deliv_charge")
	private Double mbh_deliv_charge;

	@Column(name = "mbh_auth_code")
	private String mbh_auth_code;

	@Column(name = "mbh_card_expdt")
	private String mbh_card_expdt;

	@Column(name = "mbh_cheque_dt")
	private String mbh_cheque_dt;

	@Column(name = "mbh_InvType")
	private Integer mbh_InvType;

	@Column(name = "mbh_cess")
	private Double mbh_cess;

	@Column(name = "mbh_obser")
	private Double mbh_obser;

	@Column(name = "mbh_order_type")
	private Integer mbh_order_type;

	@Column(name = "mbh_Closed_Bill")
	private Integer mbh_Closed_Bill;

	@Column(name = "Mbh_payment_id")
	private Integer Mbh_payment_id;

	@Column(name = "mbh_user_id")
	private Integer mbh_user_id;

	@Column(name = "mbh_Table_no")
	private Integer mbh_Table_no;

	@Column(name = "mbh_no_of_Guests")
	private Integer mbh_no_of_Guests;

	@Column(name = "mbh_place")
	private String mbh_place;

	@Column(name = "mbh_city")
	private String mbh_city;

	@Column(name = "mbh_landmark")
	private String mbh_landmark;

	@Column(name = "mbh_feedbackId")
	private Integer mbh_feedbackId;

	@Column(name = "mbh_Delivery_remarks")
	private String mbh_Delivery_remarks;

	@Column(name = "MBH_BILL_MODIFIED")
	private Integer MBH_BILL_MODIFIED;

	@Column(name = "mbh_phone1")
	private String mbh_phone1;

	@Column(name = "mbh_phone2")
	private String mbh_phone2;

	@Column(name = "mbh_phone3")
	private String mbh_phone3;

	@Column(name = "mbh_operator")
	private String mbh_operator;

	@Column(name = "mbh_due_bill")
	private Integer mbh_due_bill;

	@Column(name = "mbh_advance_amt")
	private Double mbh_advance_amt;

	@Column(name = "mbh_ship_addr1")
	private String mbh_ship_addr1;

	@Column(name = "mbh_ship_addr2")
	private String mbh_ship_addr2;

	@Column(name = "mbh_ship_addr3")
	private String mbh_ship_addr3;

	@Column(name = "mbh_ship_Landmark")
	private String mbh_ship_Landmark;

	@Column(name = "mbh_ship_Place")
	private String mbh_ship_Place;

	@Column(name = "mbh_ship_City")
	private String mbh_ship_City;

	@Column(name = "mbh_ship_Phone")
	private String mbh_ship_Phone;

	@Column(name = "mbh_tax_sur_amt")
	private Double mbh_tax_sur_amt;

	@Column(name = "MBH_CARD_SLIP_NO")
	private String MBH_CARD_SLIP_NO;

	@Column(name = "mbh_service_charge")
	private Double mbh_service_charge;

	@Column(name = "mbh_service_charge_Amt")
	private Double mbh_service_charge_Amt;

	@Column(name = "mbh_other_charges")
	private Double mbh_other_charges;

	@Column(name = "mbh_remarks1")
	private String mbh_remarks1;

	@Column(name = "mbh_remarks2")
	private String mbh_remarks2;

	@Column(name = "mbh_shapecharge_amt")
	private Double mbh_shapecharge_amt;

	@Column(name = "mbh_DeliveryOutlet_Id")
	private Integer mbh_DeliveryOutlet_Id;

	@Column(name = "hq_websom_ordertakenby")
	private String hq_websom_ordertakenby;

	@Column(name = "mbh_session_id")
	private Integer mbh_session_id;

	@Column(name = "mbh_pay_disc_amount")
	private Double mbh_pay_disc_amount;

	@Column(name = "RETAIL_OUTLET_ID")
	private Integer RETAIL_OUTLET_ID;

	@Column(name = "mbh_manual_refno")
	private String mbh_manual_refno;

	@Column(name = "mbh_offer_code")
	private Integer mbh_offer_code;

	@Column(name = "mbh_bill_schm_per")
	private Double mbh_bill_schm_per;

	@Column(name = "hq_order_type_id")
	private Integer hq_order_type_id;

	@Column(name = "hq_channel_type_id")
	private Integer hq_channel_type_id;

	@Column(name = "mbh_calctax_booktype")
	private Integer mbh_calctax_booktype;

	@Column(name = "mbh_compliment_amt")
	private Double mbh_compliment_amt;

	@Column(name = "mbh_giftvouc_amt")
	private Double mbh_giftvouc_amt;

	@Column(name = "mbh_prepaid_amt")
	private Double mbh_prepaid_amt;

	@Column(name = "mbh_Offline_Status")
	private Integer mbh_Offline_Status;

	@Column(name = "MBH_BILL_DISC_NARRATION")
	private String MBH_BILL_DISC_NARRATION;

	@Column(name = "mbh_with_cform")
	private Integer mbh_with_cform;

	@Column(name = "mbh_online_paym_amt")
	private Double mbh_online_paym_amt;

	@Column(name = "mbh_online_paym_aut_no")
	private String mbh_online_paym_aut_no;

	@Column(name = "mbh_online_paym_ac_post")
	private Integer mbh_online_paym_ac_post;

	@Column(name = "mbh_rep_code")
	private Integer mbh_rep_code;

	@Column(name = "mbh_Dutched_Members")
	private Integer mbh_Dutched_Members;

	@Column(name = "mbh_eftpos_amt")
	private Double mbh_eftpos_amt;

	@Column(name = "mbh_eftpos_srv")
	private Double mbh_eftpos_srv;

	@Column(name = "mbh_eftpos_srv_amt")
	private Double mbh_eftpos_srv_amt;

	@Column(name = "mbh_nt_Sec_code")
	private String mbh_nt_Sec_code;

	@Column(name = "mbh_due_amt")
	private Double mbh_due_amt;

	@Column(name = "mbh_due_payment_id")
	private Integer mbh_due_payment_id;

	@Column(name = "mbh_delivery_returned")
	private Integer mbh_delivery_returned;

	@Column(name = "mbh_deliv_ret_date")
	private Timestamp mbh_deliv_ret_date;

	@Column(name = "mbh_deliv_ret_time")
	private String mbh_deliv_ret_time;

	@Column(name = "mbh_deliv_feedback")
	private String mbh_deliv_feedback;

	@Column(name = "mbh_multi_currency")
	private Double mbh_multi_currency;

	@Column(name = "mbh_coupon_disc_amt")
	private Double mbh_coupon_disc_amt;

	@Column(name = "mbh_steward_code")
	private Integer mbh_steward_code;

	@Column(name = "mbh_token_no")
	private Integer mbh_token_no;

	@Column(name = "mbh_Cash_Discounted_amt")
	private Double mbh_Cash_Discounted_amt;

	@Column(name = "mbh_Cash_NonDiscounted_amt")
	private Double mbh_Cash_NonDiscounted_amt;

	@Column(name = "mbh_split_bno_pk")
	private Integer mbh_split_bno_pk;

	@Column(name = "mbh_coupon_disc_perc")
	private Double mbh_coupon_disc_perc;

	@Column(name = "mbh_bonus_points")
	private Double mbh_bonus_points;

	@Column(name = "mbh_Websom_Billtime")
	private Timestamp mbh_Websom_Billtime;

	@Column(name = "mbh_discount_code")
	private Integer mbh_discount_code;

	@Column(name = "mbh_service_tax")
	private Double mbh_service_tax;

	@Column(name = "mbh_service_tax_amt")
	private Double mbh_service_tax_amt;

	@Column(name = "mbh_vat_on_srvchg")
	private Double mbh_vat_on_srvchg;

	@Column(name = "mbh_vat_on_srvtax")
	private Double mbh_vat_on_srvtax;

	@Column(name = "mbh_loyalty_amt")
	private Double mbh_loyalty_amt;

	@Column(name = "mbh_compliment_Remark")
	private String mbh_compliment_Remark;

	@Column(name = "mbh_tips_amt")
	private Double mbh_tips_amt;

	@Column(name = "mbh_nt_sent")
	private Integer mbh_nt_sent;

	@Column(name = "mbh_MFRDisc_amt")
	private Double mbh_MFRDisc_amt;

	@Column(name = "mbh_MFRDisc_TransNo")
	private String mbh_MFRDisc_TransNo;

	@Column(name = "mbh_purge_status")
	private Integer mbh_purge_status;

	@Column(name = "mbh_servtax_inlclusive")
	private String mbh_servtax_inlclusive;

	@Column(name = "mbh_Cess_Tag")
	private Integer mbh_Cess_Tag;

	@Column(name = "mbh_dutch_bill")
	private Integer mbh_dutch_bill;

	@Column(name = "mbh_clientUID")
	private String mbh_clientUID;

	@Column(name = "mbh_wallet_amt")
	private Double mbh_wallet_amt;

	@Column(name = "mbh_wallet_refno")
	private String mbh_wallet_refno;

	@Column(name = "mbh_zero_tax")
	private Integer mbh_zero_tax;

	@Column(name = "mbh_sgst_amt")
	private Double mbh_sgst_amt;

	@Column(name = "mbh_cgst_amt")
	private Double mbh_cgst_amt;

	@Column(name = "mbh_igst_amt")
	private Double mbh_igst_amt;

	@Column(name = "mbh_cess_amt")
	private Double mbh_cess_amt;

	@Column(name = "mbh_tax_calc_type")
	private Integer mbh_tax_calc_type;

	@Column(name = "mbh_location_type")
	private String mbh_location_type;

	@Column(name = "mbh_is_exempt")
	private Integer mbh_is_exempt;

	@Column(name = "mbh_otherCharge_tax_type")
	private Integer mbh_otherCharge_tax_type;

	@Column(name = "mbh_otherCharge_taxPerc")
	private Double mbh_otherCharge_taxPerc;

	@Column(name = "mbh_otherCharge_taxAmt")
	private Double mbh_otherCharge_taxAmt;

	@Column(name = "mbh_local_Currency_value")
	private Double mbh_local_Currency_value;

	@Column(name = "mbh_extra_cess_amt")
	private Double mbh_extra_cess_amt;

	@Column(name = "mbh_exempted_type")
	private String mbh_exempted_type;

	@Column(name = "mbh_excise_amt")
	private Double mbh_excise_amt;

	@Column(name = "mbh_offer_detail")
	private String mbh_offer_detail;

	@Column(name = "mbh_bill_Made")
	private String mbh_bill_Made;

	@Column(name = "mbh_Cust_rating")
	private Integer mbh_Cust_rating;

	@Column(name = "mbh_ship_name")
	private String mbh_ship_name;

	@Column(name = "mbh_ship_pincode")
	private String mbh_ship_pincode;

	@Column(name = "mbh_deliv_type")
	private Integer mbh_deliv_type;

	@Column(name = "MBH_TRAN_REFNO")
	private String MBH_TRAN_REFNO;

	@Column(name = "MBH_CALAMITYCESS_AMT")
	private Double MBH_CALAMITYCESS_AMT;

	@Column(name = "MBH_IS_CALAMITYCESS")
	private Integer MBH_IS_CALAMITYCESS;

	@Column(name = "mbh_vendor_cash_disc_amt")
	private Double mbh_vendor_cash_disc_amt;

	@Column(name = "mbh_bill_schm_real_per")
	private Double mbh_bill_schm_real_per;

	@Column(name = "mbh_digital_sign")
	private String mbh_digital_sign;

	@Column(name = "mbh_nif_no")
	private String mbh_nif_no;

	@Column(name = "MBH_DISC_REAL_PERC")
	private Double MBH_DISC_REAL_PERC;

	@Column(name = "mbh_albumcharge_amt")
	private Double mbh_albumcharge_amt;

	@Column(name = "mbh_tender_taxAmt")
	private Double mbh_tender_taxAmt;

	@Column(name = "mbh_tendertax_inclusive")
	private Integer mbh_tendertax_inclusive;

	@Column(name = "mbh_dont_consdr_batch")
	private Integer mbh_dont_consdr_batch;

	@Column(name = "mbh_pickup_status")
	private Integer mbh_pickup_status;

	@Column(name = "mbh_fiscal_invoice")
	private String mbh_fiscal_invoice;

	@Column(name = "mbh_RRN_amt")
	private Double mbh_RRN_amt;

	@Column(name = "mbh_tender_disc_amt")
	private Double mbh_tender_disc_amt;

	@Column(name = "mbh_ship_gstno")
	private String mbh_ship_gstno;

	@Column(name = "mbh_vehicle_no")
	private String mbh_vehicle_no;

	@Column(name = "mbh_random_no")
	private String mbh_random_no;

	@Column(name = "mbh_cc_terminal_id")
	private String mbh_cc_terminal_id;

	@Column(name = "mbh_cc_card_type")
	private String mbh_cc_card_type;

	@Column(name = "mbh_cc_tran_time")
	private String mbh_cc_tran_time;

	@Column(name = "mbh_HQ_Loyalty_points")
	private Double mbh_HQ_Loyalty_points;

	@Column(name = "mbh_aggregator_tax_paid_amt")
	private Double mbh_aggregator_tax_paid_amt;

	public Integer getMbhBillNo() {
		return mbhBillNo;
	}

	public void setMbhBillNo(Integer mbhBillNo) {
		this.mbhBillNo = mbhBillNo;
	}

	public Timestamp getMBH_BILL_DATE() {
		return MBH_BILL_DATE;
	}

	public void setMBH_BILL_DATE(Timestamp mBH_BILL_DATE) {
		MBH_BILL_DATE = mBH_BILL_DATE;
	}

	public Integer getMBH_BILL_DOCT_CODE() {
		return MBH_BILL_DOCT_CODE;
	}

	public void setMBH_BILL_DOCT_CODE(Integer mBH_BILL_DOCT_CODE) {
		MBH_BILL_DOCT_CODE = mBH_BILL_DOCT_CODE;
	}

	public String getMBH_BILL_DOCT_NAME() {
		return MBH_BILL_DOCT_NAME;
	}

	public void setMBH_BILL_DOCT_NAME(String mBH_BILL_DOCT_NAME) {
		MBH_BILL_DOCT_NAME = mBH_BILL_DOCT_NAME;
	}

	public Integer getMBH_BILL_CUST_CODE() {
		return MBH_BILL_CUST_CODE;
	}

	public void setMBH_BILL_CUST_CODE(Integer mBH_BILL_CUST_CODE) {
		MBH_BILL_CUST_CODE = mBH_BILL_CUST_CODE;
	}

	public String getMBH_BILL_CUST_NAME() {
		return MBH_BILL_CUST_NAME;
	}

	public void setMBH_BILL_CUST_NAME(String mBH_BILL_CUST_NAME) {
		MBH_BILL_CUST_NAME = mBH_BILL_CUST_NAME;
	}

	public String getMBH_BILL_CUST_ADDR() {
		return MBH_BILL_CUST_ADDR;
	}

	public void setMBH_BILL_CUST_ADDR(String mBH_BILL_CUST_ADDR) {
		MBH_BILL_CUST_ADDR = mBH_BILL_CUST_ADDR;
	}

	public Timestamp getMBH_BILL_DUE_DT() {
		return MBH_BILL_DUE_DT;
	}

	public void setMBH_BILL_DUE_DT(Timestamp mBH_BILL_DUE_DT) {
		MBH_BILL_DUE_DT = mBH_BILL_DUE_DT;
	}

	public String getMBH_BILL_STATUS() {
		return MBH_BILL_STATUS;
	}

	public void setMBH_BILL_STATUS(String mBH_BILL_STATUS) {
		MBH_BILL_STATUS = mBH_BILL_STATUS;
	}

	public Double getMBH_DISC_AMOUNT() {
		return MBH_DISC_AMOUNT;
	}

	public void setMBH_DISC_AMOUNT(Double mBH_DISC_AMOUNT) {
		MBH_DISC_AMOUNT = mBH_DISC_AMOUNT;
	}

	public Double getMBH_BILL_AMOUNT() {
		return MBH_BILL_AMOUNT;
	}

	public void setMBH_BILL_AMOUNT(Double mBH_BILL_AMOUNT) {
		MBH_BILL_AMOUNT = mBH_BILL_AMOUNT;
	}

	public String getMBH_BILL_TYPE() {
		return MBH_BILL_TYPE;
	}

	public void setMBH_BILL_TYPE(String mBH_BILL_TYPE) {
		MBH_BILL_TYPE = mBH_BILL_TYPE;
	}

	public Double getMBH_BILL_AMT_TEND() {
		return MBH_BILL_AMT_TEND;
	}

	public void setMBH_BILL_AMT_TEND(Double mBH_BILL_AMT_TEND) {
		MBH_BILL_AMT_TEND = mBH_BILL_AMT_TEND;
	}

	public String getMBH_BILL_NARRATION() {
		return MBH_BILL_NARRATION;
	}

	public void setMBH_BILL_NARRATION(String mBH_BILL_NARRATION) {
		MBH_BILL_NARRATION = mBH_BILL_NARRATION;
	}

	public Double getMBH_TOT_ITEMS() {
		return MBH_TOT_ITEMS;
	}

	public void setMBH_TOT_ITEMS(Double mBH_TOT_ITEMS) {
		MBH_TOT_ITEMS = mBH_TOT_ITEMS;
	}

	public Integer getMBH_SCH_NO() {
		return MBH_SCH_NO;
	}

	public void setMBH_SCH_NO(Integer mBH_SCH_NO) {
		MBH_SCH_NO = mBH_SCH_NO;
	}

	public Double getMBH_PROFIT() {
		return MBH_PROFIT;
	}

	public void setMBH_PROFIT(Double mBH_PROFIT) {
		MBH_PROFIT = mBH_PROFIT;
	}

	public Integer getMBH_PBILL_NO() {
		return MBH_PBILL_NO;
	}

	public void setMBH_PBILL_NO(Integer mBH_PBILL_NO) {
		MBH_PBILL_NO = mBH_PBILL_NO;
	}

	public Integer getMBH_VNO() {
		return MBH_VNO;
	}

	public void setMBH_VNO(Integer mBH_VNO) {
		MBH_VNO = mBH_VNO;
	}

	public Double getMBH_RSTTAXAMT() {
		return MBH_RSTTAXAMT;
	}

	public void setMBH_RSTTAXAMT(Double mBH_RSTTAXAMT) {
		MBH_RSTTAXAMT = mBH_RSTTAXAMT;
	}

	public Double getMBH_RSTSURAMT() {
		return MBH_RSTSURAMT;
	}

	public void setMBH_RSTSURAMT(Double mBH_RSTSURAMT) {
		MBH_RSTSURAMT = mBH_RSTSURAMT;
	}

	public String getMBH_LOG_ID() {
		return MBH_LOG_ID;
	}

	public void setMBH_LOG_ID(String mBH_LOG_ID) {
		MBH_LOG_ID = mBH_LOG_ID;
	}

	public Integer getMBH_SESS_ID() {
		return MBH_SESS_ID;
	}

	public void setMBH_SESS_ID(Integer mBH_SESS_ID) {
		MBH_SESS_ID = mBH_SESS_ID;
	}

	public String getMBH_SESS_DT() {
		return MBH_SESS_DT;
	}

	public void setMBH_SESS_DT(String mBH_SESS_DT) {
		MBH_SESS_DT = mBH_SESS_DT;
	}

	public Double getMBH_BILL_DIFFAMOUNT() {
		return MBH_BILL_DIFFAMOUNT;
	}

	public void setMBH_BILL_DIFFAMOUNT(Double mBH_BILL_DIFFAMOUNT) {
		MBH_BILL_DIFFAMOUNT = mBH_BILL_DIFFAMOUNT;
	}

	public String getMBH_BILL_CUST_ADDR1() {
		return MBH_BILL_CUST_ADDR1;
	}

	public void setMBH_BILL_CUST_ADDR1(String mBH_BILL_CUST_ADDR1) {
		MBH_BILL_CUST_ADDR1 = mBH_BILL_CUST_ADDR1;
	}

	public Double getMBH_VAT_AMT() {
		return MBH_VAT_AMT;
	}

	public void setMBH_VAT_AMT(Double mBH_VAT_AMT) {
		MBH_VAT_AMT = mBH_VAT_AMT;
	}

	public Double getMBH_DISC_PERC() {
		return MBH_DISC_PERC;
	}

	public void setMBH_DISC_PERC(Double mBH_DISC_PERC) {
		MBH_DISC_PERC = mBH_DISC_PERC;
	}

	public Double getMBH_CASH_AMT() {
		return MBH_CASH_AMT;
	}

	public void setMBH_CASH_AMT(Double mBH_CASH_AMT) {
		MBH_CASH_AMT = mBH_CASH_AMT;
	}

	public Double getMBH_CHEQUE_AMT() {
		return MBH_CHEQUE_AMT;
	}

	public void setMBH_CHEQUE_AMT(Double mBH_CHEQUE_AMT) {
		MBH_CHEQUE_AMT = mBH_CHEQUE_AMT;
	}

	public Double getMBH_COUPON_AMT() {
		return MBH_COUPON_AMT;
	}

	public void setMBH_COUPON_AMT(Double mBH_COUPON_AMT) {
		MBH_COUPON_AMT = mBH_COUPON_AMT;
	}

	public Double getMBH_CARD_AMT() {
		return MBH_CARD_AMT;
	}

	public void setMBH_CARD_AMT(Double mBH_CARD_AMT) {
		MBH_CARD_AMT = mBH_CARD_AMT;
	}

	public Double getMBH_CREDIT_AMT() {
		return MBH_CREDIT_AMT;
	}

	public void setMBH_CREDIT_AMT(Double mBH_CREDIT_AMT) {
		MBH_CREDIT_AMT = mBH_CREDIT_AMT;
	}

	public Double getMBH_COUPON_SRV() {
		return MBH_COUPON_SRV;
	}

	public void setMBH_COUPON_SRV(Double mBH_COUPON_SRV) {
		MBH_COUPON_SRV = mBH_COUPON_SRV;
	}

	public Double getMBH_COUPON_SRV_AMT() {
		return MBH_COUPON_SRV_AMT;
	}

	public void setMBH_COUPON_SRV_AMT(Double mBH_COUPON_SRV_AMT) {
		MBH_COUPON_SRV_AMT = mBH_COUPON_SRV_AMT;
	}

	public Double getMBH_CARD_SRV() {
		return MBH_CARD_SRV;
	}

	public void setMBH_CARD_SRV(Double mBH_CARD_SRV) {
		MBH_CARD_SRV = mBH_CARD_SRV;
	}

	public Double getMBH_CARD_SRV_AMT() {
		return MBH_CARD_SRV_AMT;
	}

	public void setMBH_CARD_SRV_AMT(Double mBH_CARD_SRV_AMT) {
		MBH_CARD_SRV_AMT = mBH_CARD_SRV_AMT;
	}

	public String getMBH_COUNTER_CODE() {
		return MBH_COUNTER_CODE;
	}

	public void setMBH_COUNTER_CODE(String mBH_COUNTER_CODE) {
		MBH_COUNTER_CODE = mBH_COUNTER_CODE;
	}

	public Double getMBH_schm_amt() {
		return MBH_schm_amt;
	}

	public void setMBH_schm_amt(Double mBH_schm_amt) {
		MBH_schm_amt = mBH_schm_amt;
	}

	public Double getMBH_bill_schm_amt() {
		return MBH_bill_schm_amt;
	}

	public void setMBH_bill_schm_amt(Double mBH_bill_schm_amt) {
		MBH_bill_schm_amt = mBH_bill_schm_amt;
	}

	public Double getMBH_BILL_offer_code() {
		return MBH_BILL_offer_code;
	}

	public void setMBH_BILL_offer_code(Double mBH_BILL_offer_code) {
		MBH_BILL_offer_code = mBH_BILL_offer_code;
	}

	public Double getMBH_BILL_OFFER_SLNO() {
		return MBH_BILL_OFFER_SLNO;
	}

	public void setMBH_BILL_OFFER_SLNO(Double mBH_BILL_OFFER_SLNO) {
		MBH_BILL_OFFER_SLNO = mBH_BILL_OFFER_SLNO;
	}

	public Double getMbh_grind_amt() {
		return mbh_grind_amt;
	}

	public void setMbh_grind_amt(Double mbh_grind_amt) {
		this.mbh_grind_amt = mbh_grind_amt;
	}

	public Integer getMbh_deliv_bill() {
		return mbh_deliv_bill;
	}

	public void setMbh_deliv_bill(Integer mbh_deliv_bill) {
		this.mbh_deliv_bill = mbh_deliv_bill;
	}

	public Timestamp getMbh_deliv_date() {
		return mbh_deliv_date;
	}

	public void setMbh_deliv_date(Timestamp mbh_deliv_date) {
		this.mbh_deliv_date = mbh_deliv_date;
	}

	public String getMbh_deliv_time() {
		return mbh_deliv_time;
	}

	public void setMbh_deliv_time(String mbh_deliv_time) {
		this.mbh_deliv_time = mbh_deliv_time;
	}

	public Integer getMbh_deliv_boys() {
		return mbh_deliv_boys;
	}

	public void setMbh_deliv_boys(Integer mbh_deliv_boys) {
		this.mbh_deliv_boys = mbh_deliv_boys;
	}

	public Integer getMbh_deliv_tag() {
		return mbh_deliv_tag;
	}

	public void setMbh_deliv_tag(Integer mbh_deliv_tag) {
		this.mbh_deliv_tag = mbh_deliv_tag;
	}

	public Integer getMbh_deliv_print_tag() {
		return mbh_deliv_print_tag;
	}

	public void setMbh_deliv_print_tag(Integer mbh_deliv_print_tag) {
		this.mbh_deliv_print_tag = mbh_deliv_print_tag;
	}

	public Integer getMbh_price_level() {
		return mbh_price_level;
	}

	public void setMbh_price_level(Integer mbh_price_level) {
		this.mbh_price_level = mbh_price_level;
	}

	public String getMbh_chq_name() {
		return mbh_chq_name;
	}

	public void setMbh_chq_name(String mbh_chq_name) {
		this.mbh_chq_name = mbh_chq_name;
	}

	public String getMbh_chq_no() {
		return mbh_chq_no;
	}

	public void setMbh_chq_no(String mbh_chq_no) {
		this.mbh_chq_no = mbh_chq_no;
	}

	public String getMbh_coup_name() {
		return mbh_coup_name;
	}

	public void setMbh_coup_name(String mbh_coup_name) {
		this.mbh_coup_name = mbh_coup_name;
	}

	public String getMbh_card_name() {
		return mbh_card_name;
	}

	public void setMbh_card_name(String mbh_card_name) {
		this.mbh_card_name = mbh_card_name;
	}

	public String getMbh_card_hold_name() {
		return mbh_card_hold_name;
	}

	public void setMbh_card_hold_name(String mbh_card_hold_name) {
		this.mbh_card_hold_name = mbh_card_hold_name;
	}

	public String getMbh_card_no() {
		return mbh_card_no;
	}

	public void setMbh_card_no(String mbh_card_no) {
		this.mbh_card_no = mbh_card_no;
	}

	public Integer getMbh_compid() {
		return mbh_compid;
	}

	public void setMbh_compid(Integer mbh_compid) {
		this.mbh_compid = mbh_compid;
	}

	public Integer getMbh_diviid() {
		return mbh_diviid;
	}

	public void setMbh_diviid(Integer mbh_diviid) {
		this.mbh_diviid = mbh_diviid;
	}

	public Integer getMbh_locaid() {
		return mbh_locaid;
	}

	public void setMbh_locaid(Integer mbh_locaid) {
		this.mbh_locaid = mbh_locaid;
	}

	public String getMbh_bill_type_name() {
		return mbh_bill_type_name;
	}

	public void setMbh_bill_type_name(String mbh_bill_type_name) {
		this.mbh_bill_type_name = mbh_bill_type_name;
	}

	public String getMbh_rep_name() {
		return mbh_rep_name;
	}

	public void setMbh_rep_name(String mbh_rep_name) {
		this.mbh_rep_name = mbh_rep_name;
	}

	public Timestamp getMbh_ord_dt() {
		return mbh_ord_dt;
	}

	public void setMbh_ord_dt(Timestamp mbh_ord_dt) {
		this.mbh_ord_dt = mbh_ord_dt;
	}

	public String getMbh_ord_no() {
		return mbh_ord_no;
	}

	public void setMbh_ord_no(String mbh_ord_no) {
		this.mbh_ord_no = mbh_ord_no;
	}

	public String getMBH_ADDR1() {
		return MBH_ADDR1;
	}

	public void setMBH_ADDR1(String mBH_ADDR1) {
		MBH_ADDR1 = mBH_ADDR1;
	}

	public String getMBH_Manual() {
		return MBH_Manual;
	}

	public void setMBH_Manual(String mBH_Manual) {
		MBH_Manual = mBH_Manual;
	}

	public String getMbh_loc_grpcode() {
		return mbh_loc_grpcode;
	}

	public void setMbh_loc_grpcode(String mbh_loc_grpcode) {
		this.mbh_loc_grpcode = mbh_loc_grpcode;
	}

	public String getMbh_invoice() {
		return mbh_invoice;
	}

	public void setMbh_invoice(String mbh_invoice) {
		this.mbh_invoice = mbh_invoice;
	}

	public Double getMBH_BILL_TOTDISCAmt() {
		return MBH_BILL_TOTDISCAmt;
	}

	public void setMBH_BILL_TOTDISCAmt(Double mBH_BILL_TOTDISCAmt) {
		MBH_BILL_TOTDISCAmt = mBH_BILL_TOTDISCAmt;
	}

	public Timestamp getTS() {
		return TS;
	}

	public void setTS(Timestamp tS) {
		TS = tS;
	}

	public Integer getTSID() {
		return TSID;
	}

	public void setTSID(Integer tSID) {
		TSID = tSID;
	}

	public Integer getMbh_Sales_Type() {
		return Mbh_Sales_Type;
	}

	public void setMbh_Sales_Type(Integer mbh_Sales_Type) {
		Mbh_Sales_Type = mbh_Sales_Type;
	}

	public Double getMbh_pack_charge() {
		return mbh_pack_charge;
	}

	public void setMbh_pack_charge(Double mbh_pack_charge) {
		this.mbh_pack_charge = mbh_pack_charge;
	}

	public Double getMbh_deliv_charge() {
		return mbh_deliv_charge;
	}

	public void setMbh_deliv_charge(Double mbh_deliv_charge) {
		this.mbh_deliv_charge = mbh_deliv_charge;
	}

	public String getMbh_auth_code() {
		return mbh_auth_code;
	}

	public void setMbh_auth_code(String mbh_auth_code) {
		this.mbh_auth_code = mbh_auth_code;
	}

	public String getMbh_card_expdt() {
		return mbh_card_expdt;
	}

	public void setMbh_card_expdt(String mbh_card_expdt) {
		this.mbh_card_expdt = mbh_card_expdt;
	}

	public String getMbh_cheque_dt() {
		return mbh_cheque_dt;
	}

	public void setMbh_cheque_dt(String mbh_cheque_dt) {
		this.mbh_cheque_dt = mbh_cheque_dt;
	}

	public Integer getMbh_InvType() {
		return mbh_InvType;
	}

	public void setMbh_InvType(Integer mbh_InvType) {
		this.mbh_InvType = mbh_InvType;
	}

	public Double getMbh_cess() {
		return mbh_cess;
	}

	public void setMbh_cess(Double mbh_cess) {
		this.mbh_cess = mbh_cess;
	}

	public Double getMbh_obser() {
		return mbh_obser;
	}

	public void setMbh_obser(Double mbh_obser) {
		this.mbh_obser = mbh_obser;
	}

	public Integer getMbh_order_type() {
		return mbh_order_type;
	}

	public void setMbh_order_type(Integer mbh_order_type) {
		this.mbh_order_type = mbh_order_type;
	}

	public Integer getMbh_Closed_Bill() {
		return mbh_Closed_Bill;
	}

	public void setMbh_Closed_Bill(Integer mbh_Closed_Bill) {
		this.mbh_Closed_Bill = mbh_Closed_Bill;
	}

	public Integer getMbh_payment_id() {
		return Mbh_payment_id;
	}

	public void setMbh_payment_id(Integer mbh_payment_id) {
		Mbh_payment_id = mbh_payment_id;
	}

	public Integer getMbh_user_id() {
		return mbh_user_id;
	}

	public void setMbh_user_id(Integer mbh_user_id) {
		this.mbh_user_id = mbh_user_id;
	}

	public Integer getMbh_Table_no() {
		return mbh_Table_no;
	}

	public void setMbh_Table_no(Integer mbh_Table_no) {
		this.mbh_Table_no = mbh_Table_no;
	}

	public Integer getMbh_no_of_Guests() {
		return mbh_no_of_Guests;
	}

	public void setMbh_no_of_Guests(Integer mbh_no_of_Guests) {
		this.mbh_no_of_Guests = mbh_no_of_Guests;
	}

	public String getMbh_place() {
		return mbh_place;
	}

	public void setMbh_place(String mbh_place) {
		this.mbh_place = mbh_place;
	}

	public String getMbh_city() {
		return mbh_city;
	}

	public void setMbh_city(String mbh_city) {
		this.mbh_city = mbh_city;
	}

	public String getMbh_landmark() {
		return mbh_landmark;
	}

	public void setMbh_landmark(String mbh_landmark) {
		this.mbh_landmark = mbh_landmark;
	}

	public Integer getMbh_feedbackId() {
		return mbh_feedbackId;
	}

	public void setMbh_feedbackId(Integer mbh_feedbackId) {
		this.mbh_feedbackId = mbh_feedbackId;
	}

	public String getMbh_Delivery_remarks() {
		return mbh_Delivery_remarks;
	}

	public void setMbh_Delivery_remarks(String mbh_Delivery_remarks) {
		this.mbh_Delivery_remarks = mbh_Delivery_remarks;
	}

	public Integer getMBH_BILL_MODIFIED() {
		return MBH_BILL_MODIFIED;
	}

	public void setMBH_BILL_MODIFIED(Integer mBH_BILL_MODIFIED) {
		MBH_BILL_MODIFIED = mBH_BILL_MODIFIED;
	}

	public String getMbh_phone1() {
		return mbh_phone1;
	}

	public void setMbh_phone1(String mbh_phone1) {
		this.mbh_phone1 = mbh_phone1;
	}

	public String getMbh_phone2() {
		return mbh_phone2;
	}

	public void setMbh_phone2(String mbh_phone2) {
		this.mbh_phone2 = mbh_phone2;
	}

	public String getMbh_phone3() {
		return mbh_phone3;
	}

	public void setMbh_phone3(String mbh_phone3) {
		this.mbh_phone3 = mbh_phone3;
	}

	public String getMbh_operator() {
		return mbh_operator;
	}

	public void setMbh_operator(String mbh_operator) {
		this.mbh_operator = mbh_operator;
	}

	public Integer getMbh_due_bill() {
		return mbh_due_bill;
	}

	public void setMbh_due_bill(Integer mbh_due_bill) {
		this.mbh_due_bill = mbh_due_bill;
	}

	public Double getMbh_advance_amt() {
		return mbh_advance_amt;
	}

	public void setMbh_advance_amt(Double mbh_advance_amt) {
		this.mbh_advance_amt = mbh_advance_amt;
	}

	public String getMbh_ship_addr1() {
		return mbh_ship_addr1;
	}

	public void setMbh_ship_addr1(String mbh_ship_addr1) {
		this.mbh_ship_addr1 = mbh_ship_addr1;
	}

	public String getMbh_ship_addr2() {
		return mbh_ship_addr2;
	}

	public void setMbh_ship_addr2(String mbh_ship_addr2) {
		this.mbh_ship_addr2 = mbh_ship_addr2;
	}

	public String getMbh_ship_addr3() {
		return mbh_ship_addr3;
	}

	public void setMbh_ship_addr3(String mbh_ship_addr3) {
		this.mbh_ship_addr3 = mbh_ship_addr3;
	}

	public String getMbh_ship_Landmark() {
		return mbh_ship_Landmark;
	}

	public void setMbh_ship_Landmark(String mbh_ship_Landmark) {
		this.mbh_ship_Landmark = mbh_ship_Landmark;
	}

	public String getMbh_ship_Place() {
		return mbh_ship_Place;
	}

	public void setMbh_ship_Place(String mbh_ship_Place) {
		this.mbh_ship_Place = mbh_ship_Place;
	}

	public String getMbh_ship_City() {
		return mbh_ship_City;
	}

	public void setMbh_ship_City(String mbh_ship_City) {
		this.mbh_ship_City = mbh_ship_City;
	}

	public String getMbh_ship_Phone() {
		return mbh_ship_Phone;
	}

	public void setMbh_ship_Phone(String mbh_ship_Phone) {
		this.mbh_ship_Phone = mbh_ship_Phone;
	}

	public Double getMbh_tax_sur_amt() {
		return mbh_tax_sur_amt;
	}

	public void setMbh_tax_sur_amt(Double mbh_tax_sur_amt) {
		this.mbh_tax_sur_amt = mbh_tax_sur_amt;
	}

	public String getMBH_CARD_SLIP_NO() {
		return MBH_CARD_SLIP_NO;
	}

	public void setMBH_CARD_SLIP_NO(String mBH_CARD_SLIP_NO) {
		MBH_CARD_SLIP_NO = mBH_CARD_SLIP_NO;
	}

	public Double getMbh_service_charge() {
		return mbh_service_charge;
	}

	public void setMbh_service_charge(Double mbh_service_charge) {
		this.mbh_service_charge = mbh_service_charge;
	}

	public Double getMbh_service_charge_Amt() {
		return mbh_service_charge_Amt;
	}

	public void setMbh_service_charge_Amt(Double mbh_service_charge_Amt) {
		this.mbh_service_charge_Amt = mbh_service_charge_Amt;
	}

	public Double getMbh_other_charges() {
		return mbh_other_charges;
	}

	public void setMbh_other_charges(Double mbh_other_charges) {
		this.mbh_other_charges = mbh_other_charges;
	}

	public String getMbh_remarks1() {
		return mbh_remarks1;
	}

	public void setMbh_remarks1(String mbh_remarks1) {
		this.mbh_remarks1 = mbh_remarks1;
	}

	public String getMbh_remarks2() {
		return mbh_remarks2;
	}

	public void setMbh_remarks2(String mbh_remarks2) {
		this.mbh_remarks2 = mbh_remarks2;
	}

	public Double getMbh_shapecharge_amt() {
		return mbh_shapecharge_amt;
	}

	public void setMbh_shapecharge_amt(Double mbh_shapecharge_amt) {
		this.mbh_shapecharge_amt = mbh_shapecharge_amt;
	}

	public Integer getMbh_DeliveryOutlet_Id() {
		return mbh_DeliveryOutlet_Id;
	}

	public void setMbh_DeliveryOutlet_Id(Integer mbh_DeliveryOutlet_Id) {
		this.mbh_DeliveryOutlet_Id = mbh_DeliveryOutlet_Id;
	}

	public String getHq_websom_ordertakenby() {
		return hq_websom_ordertakenby;
	}

	public void setHq_websom_ordertakenby(String hq_websom_ordertakenby) {
		this.hq_websom_ordertakenby = hq_websom_ordertakenby;
	}

	public Integer getMbh_session_id() {
		return mbh_session_id;
	}

	public void setMbh_session_id(Integer mbh_session_id) {
		this.mbh_session_id = mbh_session_id;
	}

	public Double getMbh_pay_disc_amount() {
		return mbh_pay_disc_amount;
	}

	public void setMbh_pay_disc_amount(Double mbh_pay_disc_amount) {
		this.mbh_pay_disc_amount = mbh_pay_disc_amount;
	}

	public Integer getRETAIL_OUTLET_ID() {
		return RETAIL_OUTLET_ID;
	}

	public void setRETAIL_OUTLET_ID(Integer rETAIL_OUTLET_ID) {
		RETAIL_OUTLET_ID = rETAIL_OUTLET_ID;
	}

	public String getMbh_manual_refno() {
		return mbh_manual_refno;
	}

	public void setMbh_manual_refno(String mbh_manual_refno) {
		this.mbh_manual_refno = mbh_manual_refno;
	}

	public Integer getMbh_offer_code() {
		return mbh_offer_code;
	}

	public void setMbh_offer_code(Integer mbh_offer_code) {
		this.mbh_offer_code = mbh_offer_code;
	}

	public Double getMbh_bill_schm_per() {
		return mbh_bill_schm_per;
	}

	public void setMbh_bill_schm_per(Double mbh_bill_schm_per) {
		this.mbh_bill_schm_per = mbh_bill_schm_per;
	}

	public Integer getHq_order_type_id() {
		return hq_order_type_id;
	}

	public void setHq_order_type_id(Integer hq_order_type_id) {
		this.hq_order_type_id = hq_order_type_id;
	}

	public Integer getHq_channel_type_id() {
		return hq_channel_type_id;
	}

	public void setHq_channel_type_id(Integer hq_channel_type_id) {
		this.hq_channel_type_id = hq_channel_type_id;
	}

	public Integer getMbh_calctax_booktype() {
		return mbh_calctax_booktype;
	}

	public void setMbh_calctax_booktype(Integer mbh_calctax_booktype) {
		this.mbh_calctax_booktype = mbh_calctax_booktype;
	}

	public Double getMbh_compliment_amt() {
		return mbh_compliment_amt;
	}

	public void setMbh_compliment_amt(Double mbh_compliment_amt) {
		this.mbh_compliment_amt = mbh_compliment_amt;
	}

	public Double getMbh_giftvouc_amt() {
		return mbh_giftvouc_amt;
	}

	public void setMbh_giftvouc_amt(Double mbh_giftvouc_amt) {
		this.mbh_giftvouc_amt = mbh_giftvouc_amt;
	}

	public Double getMbh_prepaid_amt() {
		return mbh_prepaid_amt;
	}

	public void setMbh_prepaid_amt(Double mbh_prepaid_amt) {
		this.mbh_prepaid_amt = mbh_prepaid_amt;
	}

	public Integer getMbh_Offline_Status() {
		return mbh_Offline_Status;
	}

	public void setMbh_Offline_Status(Integer mbh_Offline_Status) {
		this.mbh_Offline_Status = mbh_Offline_Status;
	}

	public String getMBH_BILL_DISC_NARRATION() {
		return MBH_BILL_DISC_NARRATION;
	}

	public void setMBH_BILL_DISC_NARRATION(String mBH_BILL_DISC_NARRATION) {
		MBH_BILL_DISC_NARRATION = mBH_BILL_DISC_NARRATION;
	}

	public Integer getMbh_with_cform() {
		return mbh_with_cform;
	}

	public void setMbh_with_cform(Integer mbh_with_cform) {
		this.mbh_with_cform = mbh_with_cform;
	}

	public Double getMbh_online_paym_amt() {
		return mbh_online_paym_amt;
	}

	public void setMbh_online_paym_amt(Double mbh_online_paym_amt) {
		this.mbh_online_paym_amt = mbh_online_paym_amt;
	}

	public String getMbh_online_paym_aut_no() {
		return mbh_online_paym_aut_no;
	}

	public void setMbh_online_paym_aut_no(String mbh_online_paym_aut_no) {
		this.mbh_online_paym_aut_no = mbh_online_paym_aut_no;
	}

	public Integer getMbh_online_paym_ac_post() {
		return mbh_online_paym_ac_post;
	}

	public void setMbh_online_paym_ac_post(Integer mbh_online_paym_ac_post) {
		this.mbh_online_paym_ac_post = mbh_online_paym_ac_post;
	}

	public Integer getMbh_rep_code() {
		return mbh_rep_code;
	}

	public void setMbh_rep_code(Integer mbh_rep_code) {
		this.mbh_rep_code = mbh_rep_code;
	}

	public Integer getMbh_Dutched_Members() {
		return mbh_Dutched_Members;
	}

	public void setMbh_Dutched_Members(Integer mbh_Dutched_Members) {
		this.mbh_Dutched_Members = mbh_Dutched_Members;
	}

	public Double getMbh_eftpos_amt() {
		return mbh_eftpos_amt;
	}

	public void setMbh_eftpos_amt(Double mbh_eftpos_amt) {
		this.mbh_eftpos_amt = mbh_eftpos_amt;
	}

	public Double getMbh_eftpos_srv() {
		return mbh_eftpos_srv;
	}

	public void setMbh_eftpos_srv(Double mbh_eftpos_srv) {
		this.mbh_eftpos_srv = mbh_eftpos_srv;
	}

	public Double getMbh_eftpos_srv_amt() {
		return mbh_eftpos_srv_amt;
	}

	public void setMbh_eftpos_srv_amt(Double mbh_eftpos_srv_amt) {
		this.mbh_eftpos_srv_amt = mbh_eftpos_srv_amt;
	}

	public String getMbh_nt_Sec_code() {
		return mbh_nt_Sec_code;
	}

	public void setMbh_nt_Sec_code(String mbh_nt_Sec_code) {
		this.mbh_nt_Sec_code = mbh_nt_Sec_code;
	}

	public Double getMbh_due_amt() {
		return mbh_due_amt;
	}

	public void setMbh_due_amt(Double mbh_due_amt) {
		this.mbh_due_amt = mbh_due_amt;
	}

	public Integer getMbh_due_payment_id() {
		return mbh_due_payment_id;
	}

	public void setMbh_due_payment_id(Integer mbh_due_payment_id) {
		this.mbh_due_payment_id = mbh_due_payment_id;
	}

	public Integer getMbh_delivery_returned() {
		return mbh_delivery_returned;
	}

	public void setMbh_delivery_returned(Integer mbh_delivery_returned) {
		this.mbh_delivery_returned = mbh_delivery_returned;
	}

	public Timestamp getMbh_deliv_ret_date() {
		return mbh_deliv_ret_date;
	}

	public void setMbh_deliv_ret_date(Timestamp mbh_deliv_ret_date) {
		this.mbh_deliv_ret_date = mbh_deliv_ret_date;
	}

	public String getMbh_deliv_ret_time() {
		return mbh_deliv_ret_time;
	}

	public void setMbh_deliv_ret_time(String mbh_deliv_ret_time) {
		this.mbh_deliv_ret_time = mbh_deliv_ret_time;
	}

	public String getMbh_deliv_feedback() {
		return mbh_deliv_feedback;
	}

	public void setMbh_deliv_feedback(String mbh_deliv_feedback) {
		this.mbh_deliv_feedback = mbh_deliv_feedback;
	}

	public Double getMbh_multi_currency() {
		return mbh_multi_currency;
	}

	public void setMbh_multi_currency(Double mbh_multi_currency) {
		this.mbh_multi_currency = mbh_multi_currency;
	}

	public Double getMbh_coupon_disc_amt() {
		return mbh_coupon_disc_amt;
	}

	public void setMbh_coupon_disc_amt(Double mbh_coupon_disc_amt) {
		this.mbh_coupon_disc_amt = mbh_coupon_disc_amt;
	}

	public Integer getMbh_steward_code() {
		return mbh_steward_code;
	}

	public void setMbh_steward_code(Integer mbh_steward_code) {
		this.mbh_steward_code = mbh_steward_code;
	}

	public Integer getMbh_token_no() {
		return mbh_token_no;
	}

	public void setMbh_token_no(Integer mbh_token_no) {
		this.mbh_token_no = mbh_token_no;
	}

	public Double getMbh_Cash_Discounted_amt() {
		return mbh_Cash_Discounted_amt;
	}

	public void setMbh_Cash_Discounted_amt(Double mbh_Cash_Discounted_amt) {
		this.mbh_Cash_Discounted_amt = mbh_Cash_Discounted_amt;
	}

	public Double getMbh_Cash_NonDiscounted_amt() {
		return mbh_Cash_NonDiscounted_amt;
	}

	public void setMbh_Cash_NonDiscounted_amt(Double mbh_Cash_NonDiscounted_amt) {
		this.mbh_Cash_NonDiscounted_amt = mbh_Cash_NonDiscounted_amt;
	}

	public Integer getMbh_split_bno_pk() {
		return mbh_split_bno_pk;
	}

	public void setMbh_split_bno_pk(Integer mbh_split_bno_pk) {
		this.mbh_split_bno_pk = mbh_split_bno_pk;
	}

	public Double getMbh_coupon_disc_perc() {
		return mbh_coupon_disc_perc;
	}

	public void setMbh_coupon_disc_perc(Double mbh_coupon_disc_perc) {
		this.mbh_coupon_disc_perc = mbh_coupon_disc_perc;
	}

	public Double getMbh_bonus_points() {
		return mbh_bonus_points;
	}

	public void setMbh_bonus_points(Double mbh_bonus_points) {
		this.mbh_bonus_points = mbh_bonus_points;
	}

	public Timestamp getMbh_Websom_Billtime() {
		return mbh_Websom_Billtime;
	}

	public void setMbh_Websom_Billtime(Timestamp mbh_Websom_Billtime) {
		this.mbh_Websom_Billtime = mbh_Websom_Billtime;
	}

	public Integer getMbh_discount_code() {
		return mbh_discount_code;
	}

	public void setMbh_discount_code(Integer mbh_discount_code) {
		this.mbh_discount_code = mbh_discount_code;
	}

	public Double getMbh_service_tax() {
		return mbh_service_tax;
	}

	public void setMbh_service_tax(Double mbh_service_tax) {
		this.mbh_service_tax = mbh_service_tax;
	}

	public Double getMbh_service_tax_amt() {
		return mbh_service_tax_amt;
	}

	public void setMbh_service_tax_amt(Double mbh_service_tax_amt) {
		this.mbh_service_tax_amt = mbh_service_tax_amt;
	}

	public Double getMbh_vat_on_srvchg() {
		return mbh_vat_on_srvchg;
	}

	public void setMbh_vat_on_srvchg(Double mbh_vat_on_srvchg) {
		this.mbh_vat_on_srvchg = mbh_vat_on_srvchg;
	}

	public Double getMbh_vat_on_srvtax() {
		return mbh_vat_on_srvtax;
	}

	public void setMbh_vat_on_srvtax(Double mbh_vat_on_srvtax) {
		this.mbh_vat_on_srvtax = mbh_vat_on_srvtax;
	}

	public Double getMbh_loyalty_amt() {
		return mbh_loyalty_amt;
	}

	public void setMbh_loyalty_amt(Double mbh_loyalty_amt) {
		this.mbh_loyalty_amt = mbh_loyalty_amt;
	}

	public String getMbh_compliment_Remark() {
		return mbh_compliment_Remark;
	}

	public void setMbh_compliment_Remark(String mbh_compliment_Remark) {
		this.mbh_compliment_Remark = mbh_compliment_Remark;
	}

	public Double getMbh_tips_amt() {
		return mbh_tips_amt;
	}

	public void setMbh_tips_amt(Double mbh_tips_amt) {
		this.mbh_tips_amt = mbh_tips_amt;
	}

	public Integer getMbh_nt_sent() {
		return mbh_nt_sent;
	}

	public void setMbh_nt_sent(Integer mbh_nt_sent) {
		this.mbh_nt_sent = mbh_nt_sent;
	}

	public Double getMbh_MFRDisc_amt() {
		return mbh_MFRDisc_amt;
	}

	public void setMbh_MFRDisc_amt(Double mbh_MFRDisc_amt) {
		this.mbh_MFRDisc_amt = mbh_MFRDisc_amt;
	}

	public String getMbh_MFRDisc_TransNo() {
		return mbh_MFRDisc_TransNo;
	}

	public void setMbh_MFRDisc_TransNo(String mbh_MFRDisc_TransNo) {
		this.mbh_MFRDisc_TransNo = mbh_MFRDisc_TransNo;
	}

	public Integer getMbh_purge_status() {
		return mbh_purge_status;
	}

	public void setMbh_purge_status(Integer mbh_purge_status) {
		this.mbh_purge_status = mbh_purge_status;
	}

	public String getMbh_servtax_inlclusive() {
		return mbh_servtax_inlclusive;
	}

	public void setMbh_servtax_inlclusive(String mbh_servtax_inlclusive) {
		this.mbh_servtax_inlclusive = mbh_servtax_inlclusive;
	}

	public Integer getMbh_Cess_Tag() {
		return mbh_Cess_Tag;
	}

	public void setMbh_Cess_Tag(Integer mbh_Cess_Tag) {
		this.mbh_Cess_Tag = mbh_Cess_Tag;
	}

	public Integer getMbh_dutch_bill() {
		return mbh_dutch_bill;
	}

	public void setMbh_dutch_bill(Integer mbh_dutch_bill) {
		this.mbh_dutch_bill = mbh_dutch_bill;
	}

	public String getMbh_clientUID() {
		return mbh_clientUID;
	}

	public void setMbh_clientUID(String mbh_clientUID) {
		this.mbh_clientUID = mbh_clientUID;
	}

	public Double getMbh_wallet_amt() {
		return mbh_wallet_amt;
	}

	public void setMbh_wallet_amt(Double mbh_wallet_amt) {
		this.mbh_wallet_amt = mbh_wallet_amt;
	}

	public String getMbh_wallet_refno() {
		return mbh_wallet_refno;
	}

	public void setMbh_wallet_refno(String mbh_wallet_refno) {
		this.mbh_wallet_refno = mbh_wallet_refno;
	}

	public Integer getMbh_zero_tax() {
		return mbh_zero_tax;
	}

	public void setMbh_zero_tax(Integer mbh_zero_tax) {
		this.mbh_zero_tax = mbh_zero_tax;
	}

	public Double getMbh_sgst_amt() {
		return mbh_sgst_amt;
	}

	public void setMbh_sgst_amt(Double mbh_sgst_amt) {
		this.mbh_sgst_amt = mbh_sgst_amt;
	}

	public Double getMbh_cgst_amt() {
		return mbh_cgst_amt;
	}

	public void setMbh_cgst_amt(Double mbh_cgst_amt) {
		this.mbh_cgst_amt = mbh_cgst_amt;
	}

	public Double getMbh_igst_amt() {
		return mbh_igst_amt;
	}

	public void setMbh_igst_amt(Double mbh_igst_amt) {
		this.mbh_igst_amt = mbh_igst_amt;
	}

	public Double getMbh_cess_amt() {
		return mbh_cess_amt;
	}

	public void setMbh_cess_amt(Double mbh_cess_amt) {
		this.mbh_cess_amt = mbh_cess_amt;
	}

	public Integer getMbh_tax_calc_type() {
		return mbh_tax_calc_type;
	}

	public void setMbh_tax_calc_type(Integer mbh_tax_calc_type) {
		this.mbh_tax_calc_type = mbh_tax_calc_type;
	}

	public String getMbh_location_type() {
		return mbh_location_type;
	}

	public void setMbh_location_type(String mbh_location_type) {
		this.mbh_location_type = mbh_location_type;
	}

	public Integer getMbh_is_exempt() {
		return mbh_is_exempt;
	}

	public void setMbh_is_exempt(Integer mbh_is_exempt) {
		this.mbh_is_exempt = mbh_is_exempt;
	}

	public Integer getMbh_otherCharge_tax_type() {
		return mbh_otherCharge_tax_type;
	}

	public void setMbh_otherCharge_tax_type(Integer mbh_otherCharge_tax_type) {
		this.mbh_otherCharge_tax_type = mbh_otherCharge_tax_type;
	}

	public Double getMbh_otherCharge_taxPerc() {
		return mbh_otherCharge_taxPerc;
	}

	public void setMbh_otherCharge_taxPerc(Double mbh_otherCharge_taxPerc) {
		this.mbh_otherCharge_taxPerc = mbh_otherCharge_taxPerc;
	}

	public Double getMbh_otherCharge_taxAmt() {
		return mbh_otherCharge_taxAmt;
	}

	public void setMbh_otherCharge_taxAmt(Double mbh_otherCharge_taxAmt) {
		this.mbh_otherCharge_taxAmt = mbh_otherCharge_taxAmt;
	}

	public Double getMbh_local_Currency_value() {
		return mbh_local_Currency_value;
	}

	public void setMbh_local_Currency_value(Double mbh_local_Currency_value) {
		this.mbh_local_Currency_value = mbh_local_Currency_value;
	}

	public Double getMbh_extra_cess_amt() {
		return mbh_extra_cess_amt;
	}

	public void setMbh_extra_cess_amt(Double mbh_extra_cess_amt) {
		this.mbh_extra_cess_amt = mbh_extra_cess_amt;
	}

	public String getMbh_exempted_type() {
		return mbh_exempted_type;
	}

	public void setMbh_exempted_type(String mbh_exempted_type) {
		this.mbh_exempted_type = mbh_exempted_type;
	}

	public Double getMbh_excise_amt() {
		return mbh_excise_amt;
	}

	public void setMbh_excise_amt(Double mbh_excise_amt) {
		this.mbh_excise_amt = mbh_excise_amt;
	}

	public String getMbh_offer_detail() {
		return mbh_offer_detail;
	}

	public void setMbh_offer_detail(String mbh_offer_detail) {
		this.mbh_offer_detail = mbh_offer_detail;
	}

	public String getMbh_bill_Made() {
		return mbh_bill_Made;
	}

	public void setMbh_bill_Made(String mbh_bill_Made) {
		this.mbh_bill_Made = mbh_bill_Made;
	}

	public Integer getMbh_Cust_rating() {
		return mbh_Cust_rating;
	}

	public void setMbh_Cust_rating(Integer mbh_Cust_rating) {
		this.mbh_Cust_rating = mbh_Cust_rating;
	}

	public String getMbh_ship_name() {
		return mbh_ship_name;
	}

	public void setMbh_ship_name(String mbh_ship_name) {
		this.mbh_ship_name = mbh_ship_name;
	}

	public String getMbh_ship_pincode() {
		return mbh_ship_pincode;
	}

	public void setMbh_ship_pincode(String mbh_ship_pincode) {
		this.mbh_ship_pincode = mbh_ship_pincode;
	}

	public Integer getMbh_deliv_type() {
		return mbh_deliv_type;
	}

	public void setMbh_deliv_type(Integer mbh_deliv_type) {
		this.mbh_deliv_type = mbh_deliv_type;
	}

	public String getMBH_TRAN_REFNO() {
		return MBH_TRAN_REFNO;
	}

	public void setMBH_TRAN_REFNO(String mBH_TRAN_REFNO) {
		MBH_TRAN_REFNO = mBH_TRAN_REFNO;
	}

	public Double getMBH_CALAMITYCESS_AMT() {
		return MBH_CALAMITYCESS_AMT;
	}

	public void setMBH_CALAMITYCESS_AMT(Double mBH_CALAMITYCESS_AMT) {
		MBH_CALAMITYCESS_AMT = mBH_CALAMITYCESS_AMT;
	}

	public Integer getMBH_IS_CALAMITYCESS() {
		return MBH_IS_CALAMITYCESS;
	}

	public void setMBH_IS_CALAMITYCESS(Integer mBH_IS_CALAMITYCESS) {
		MBH_IS_CALAMITYCESS = mBH_IS_CALAMITYCESS;
	}

	public Double getMbh_vendor_cash_disc_amt() {
		return mbh_vendor_cash_disc_amt;
	}

	public void setMbh_vendor_cash_disc_amt(Double mbh_vendor_cash_disc_amt) {
		this.mbh_vendor_cash_disc_amt = mbh_vendor_cash_disc_amt;
	}

	public Double getMbh_bill_schm_real_per() {
		return mbh_bill_schm_real_per;
	}

	public void setMbh_bill_schm_real_per(Double mbh_bill_schm_real_per) {
		this.mbh_bill_schm_real_per = mbh_bill_schm_real_per;
	}

	public String getMbh_digital_sign() {
		return mbh_digital_sign;
	}

	public void setMbh_digital_sign(String mbh_digital_sign) {
		this.mbh_digital_sign = mbh_digital_sign;
	}

	public String getMbh_nif_no() {
		return mbh_nif_no;
	}

	public void setMbh_nif_no(String mbh_nif_no) {
		this.mbh_nif_no = mbh_nif_no;
	}

	public Double getMBH_DISC_REAL_PERC() {
		return MBH_DISC_REAL_PERC;
	}

	public void setMBH_DISC_REAL_PERC(Double mBH_DISC_REAL_PERC) {
		MBH_DISC_REAL_PERC = mBH_DISC_REAL_PERC;
	}

	public Double getMbh_albumcharge_amt() {
		return mbh_albumcharge_amt;
	}

	public void setMbh_albumcharge_amt(Double mbh_albumcharge_amt) {
		this.mbh_albumcharge_amt = mbh_albumcharge_amt;
	}

	public Double getMbh_tender_taxAmt() {
		return mbh_tender_taxAmt;
	}

	public void setMbh_tender_taxAmt(Double mbh_tender_taxAmt) {
		this.mbh_tender_taxAmt = mbh_tender_taxAmt;
	}

	public Integer getMbh_tendertax_inclusive() {
		return mbh_tendertax_inclusive;
	}

	public void setMbh_tendertax_inclusive(Integer mbh_tendertax_inclusive) {
		this.mbh_tendertax_inclusive = mbh_tendertax_inclusive;
	}

	public Integer getMbh_dont_consdr_batch() {
		return mbh_dont_consdr_batch;
	}

	public void setMbh_dont_consdr_batch(Integer mbh_dont_consdr_batch) {
		this.mbh_dont_consdr_batch = mbh_dont_consdr_batch;
	}

	public Integer getMbh_pickup_status() {
		return mbh_pickup_status;
	}

	public void setMbh_pickup_status(Integer mbh_pickup_status) {
		this.mbh_pickup_status = mbh_pickup_status;
	}

	public String getMbh_fiscal_invoice() {
		return mbh_fiscal_invoice;
	}

	public void setMbh_fiscal_invoice(String mbh_fiscal_invoice) {
		this.mbh_fiscal_invoice = mbh_fiscal_invoice;
	}

	public Double getMbh_RRN_amt() {
		return mbh_RRN_amt;
	}

	public void setMbh_RRN_amt(Double mbh_RRN_amt) {
		this.mbh_RRN_amt = mbh_RRN_amt;
	}

	public Double getMbh_tender_disc_amt() {
		return mbh_tender_disc_amt;
	}

	public void setMbh_tender_disc_amt(Double mbh_tender_disc_amt) {
		this.mbh_tender_disc_amt = mbh_tender_disc_amt;
	}

	public String getMbh_ship_gstno() {
		return mbh_ship_gstno;
	}

	public void setMbh_ship_gstno(String mbh_ship_gstno) {
		this.mbh_ship_gstno = mbh_ship_gstno;
	}

	public String getMbh_vehicle_no() {
		return mbh_vehicle_no;
	}

	public void setMbh_vehicle_no(String mbh_vehicle_no) {
		this.mbh_vehicle_no = mbh_vehicle_no;
	}

	public String getMbh_random_no() {
		return mbh_random_no;
	}

	public void setMbh_random_no(String mbh_random_no) {
		this.mbh_random_no = mbh_random_no;
	}

	public String getMbh_cc_terminal_id() {
		return mbh_cc_terminal_id;
	}

	public void setMbh_cc_terminal_id(String mbh_cc_terminal_id) {
		this.mbh_cc_terminal_id = mbh_cc_terminal_id;
	}

	public String getMbh_cc_card_type() {
		return mbh_cc_card_type;
	}

	public void setMbh_cc_card_type(String mbh_cc_card_type) {
		this.mbh_cc_card_type = mbh_cc_card_type;
	}

	public String getMbh_cc_tran_time() {
		return mbh_cc_tran_time;
	}

	public void setMbh_cc_tran_time(String mbh_cc_tran_time) {
		this.mbh_cc_tran_time = mbh_cc_tran_time;
	}

	public Double getMbh_HQ_Loyalty_points() {
		return mbh_HQ_Loyalty_points;
	}

	public void setMbh_HQ_Loyalty_points(Double mbh_HQ_Loyalty_points) {
		this.mbh_HQ_Loyalty_points = mbh_HQ_Loyalty_points;
	}

	public Double getMbh_aggregator_tax_paid_amt() {
		return mbh_aggregator_tax_paid_amt;
	}

	public void setMbh_aggregator_tax_paid_amt(Double mbh_aggregator_tax_paid_amt) {
		this.mbh_aggregator_tax_paid_amt = mbh_aggregator_tax_paid_amt;
	}

}
