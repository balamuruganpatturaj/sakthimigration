package com.sakthi.groups.migration.gofrugal.dao;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.sakthi.groups.migration.gofrugal.model.MedBillDetails;

public interface MedBillDetailsRepository extends CrudRepository<MedBillDetails, Integer> {

	@Query("SELECT u FROM MedBillDetails u WHERE u.mbdSplitBnoPk = ?1")
	List<MedBillDetails> findByMbdSplitBnoPk(Integer mBDBillNo);

	@Query("SELECT u FROM MedBillDetails u WHERE u.mBDBillNo = ?1")
	List<MedBillDetails> findByMBDBillNo(Integer mBDBillNo);

	@Query("SELECT u.MBD_ITEM_CODE FROM MedBillDetails u WHERE u.mBDBillNo IN (?1)")
	public Set<Integer> findByMBDBillNoIn(List<Integer> mBDBillNo);

}
