package com.sakthi.groups.migration.gofrugal.model;

import java.io.Serializable;

public class MedBillDetailsId implements Serializable {

	public MedBillDetailsId() {
		super();
	}

	public MedBillDetailsId(Integer mBDBillNo, Integer mBD_BILL_SL_NO) {
		super();
		this.mBDBillNo = mBDBillNo;
		this.MBD_BILL_SL_NO = mBD_BILL_SL_NO;
	}

	private static final long serialVersionUID = 1L;

	private Integer mBDBillNo;
	private Integer MBD_BILL_SL_NO;

	public Integer getmBDBillNo() {
		return mBDBillNo;
	}

	public void setmBDBillNo(Integer mBDBillNo) {
		this.mBDBillNo = mBDBillNo;
	}

	public Integer getMBD_BILL_SL_NO() {
		return MBD_BILL_SL_NO;
	}

	public void setMBD_BILL_SL_NO(Integer mBD_BILL_SL_NO) {
		MBD_BILL_SL_NO = mBD_BILL_SL_NO;
	}

}
