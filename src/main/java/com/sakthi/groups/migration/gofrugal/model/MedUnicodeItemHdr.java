package com.sakthi.groups.migration.gofrugal.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "med_unicode_item_hdr")
public class MedUnicodeItemHdr {

	@Id
	@Column(name = "MUIH_UNICODE_ID")
	private Integer MUIH_UNICODE_ID;

	@Column(name = "MUIH_ITEM_CODE")
	private Integer MUIH_ITEM_CODE;

	@Column(name = "MUIH_MED_ITEM_NAME")
	private String MUIH_MED_ITEM_NAME;

	@Column(name = "MUIH_UNICODE_ITEM_NAME")
	private String MUIH_UNICODE_ITEM_NAME;

	@Column(name = "MUIH_ITEM_HEXA_CODE")
	private String MUIH_ITEM_HEXA_CODE;

	@Column(name = "MUIH_UNICODE_lANGUAGE")
	private String MUIH_UNICODE_lANGUAGE;

	public Integer getMUIH_UNICODE_ID() {
		return MUIH_UNICODE_ID;
	}

	public void setMUIH_UNICODE_ID(Integer mUIH_UNICODE_ID) {
		MUIH_UNICODE_ID = mUIH_UNICODE_ID;
	}

	public Integer getMUIH_ITEM_CODE() {
		return MUIH_ITEM_CODE;
	}

	public void setMUIH_ITEM_CODE(Integer mUIH_ITEM_CODE) {
		MUIH_ITEM_CODE = mUIH_ITEM_CODE;
	}

	public String getMUIH_MED_ITEM_NAME() {
		return MUIH_MED_ITEM_NAME;
	}

	public void setMUIH_MED_ITEM_NAME(String mUIH_MED_ITEM_NAME) {
		MUIH_MED_ITEM_NAME = mUIH_MED_ITEM_NAME;
	}

	public String getMUIH_UNICODE_ITEM_NAME() {
		return MUIH_UNICODE_ITEM_NAME;
	}

	public void setMUIH_UNICODE_ITEM_NAME(String mUIH_UNICODE_ITEM_NAME) {
		MUIH_UNICODE_ITEM_NAME = mUIH_UNICODE_ITEM_NAME;
	}

	public String getMUIH_ITEM_HEXA_CODE() {
		return MUIH_ITEM_HEXA_CODE;
	}

	public void setMUIH_ITEM_HEXA_CODE(String mUIH_ITEM_HEXA_CODE) {
		MUIH_ITEM_HEXA_CODE = mUIH_ITEM_HEXA_CODE;
	}

	public String getMUIH_UNICODE_lANGUAGE() {
		return MUIH_UNICODE_lANGUAGE;
	}

	public void setMUIH_UNICODE_lANGUAGE(String mUIH_UNICODE_lANGUAGE) {
		MUIH_UNICODE_lANGUAGE = mUIH_UNICODE_lANGUAGE;
	}

}
