package com.sakthi.groups.migration.gofrugal.dao;

import org.springframework.data.repository.CrudRepository;

import com.sakthi.groups.migration.gofrugal.model.MedBillQuotHdr;

public interface MedBillQuotHdrRepository extends CrudRepository<MedBillQuotHdr, Integer> {

}
