package com.sakthi.groups.migration.gofrugal.dao;

import org.springframework.data.repository.CrudRepository;

import com.sakthi.groups.migration.gofrugal.model.MedTaxMaster;

public interface MedTaxMasterRepository extends CrudRepository<MedTaxMaster, Integer> {

}
