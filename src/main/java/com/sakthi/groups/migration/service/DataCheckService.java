package com.sakthi.groups.migration.service;

import java.io.ByteArrayOutputStream;

public interface DataCheckService {

	ByteArrayOutputStream checkStaleProduct(Integer noOfDaysBefore, String entryType);
	
}
