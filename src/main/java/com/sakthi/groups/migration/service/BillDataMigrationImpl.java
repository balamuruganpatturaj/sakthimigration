package com.sakthi.groups.migration.service;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.sakthi.groups.migration.portal.model.BillAnalyticsData;
import com.sakthi.groups.migration.portal.model.BillSearchCriteria;
import com.sakthi.groups.migration.portal.model.BillTrendData;
import com.sakthi.groups.migration.portal.model.ProductSupplierWiseData;
import com.sakthi.groups.migration.portal.model.SalesPurchaseTrendData;
import com.sakthi.groups.migration.portal.model.TopGrossTrendData;
import com.sakthi.groups.migration.portal.model.TopSellingTrendData;

@Service
public class BillDataMigrationImpl implements BillDataMigration {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public BillTrendData getSalesTrendData(BillSearchCriteria searchCriteria) {
		List<BillTrendData> billTrendData = new ArrayList<>();
		StringBuffer stringBuff = new StringBuffer();
		String findQuery = "SELECT SUM(MBH_BILL_AMOUNT) SALES_AMOUNT, SUM(MBH_PROFIT) PROFIT, SUM(MBH_CASH_AMT) CASH_AMOUNT, SUM(MBH_CARD_AMT) CARD_AMOUNT,"
				+ " SUM(MBH_DUE_AMT) DUE_AMOUNT, SUM(MBH_WALLET_AMT) UPI_AMOUNT, SUM(MBH_CREDIT_AMT) CREDIT_AMOUNT,"
				+ " SUM(MBH_LOYALTY_AMT) LOYALTY_AOUNT FROM MED_BILL_HDR WHERE MBH_BILL_NO > 0 ";
		stringBuff.append(findQuery);
		logger.info(searchCriteria.getBillStartDate());
		if (searchCriteria.getBillStartDate() != null) {
			logger.info(searchCriteria.getBillStartDate());
			Date startDate = stringToDateFormatted(searchCriteria.getBillStartDate().toString());
			Date endDate = stringToDateFormatted(searchCriteria.getBillEndDate().toString());
			String dateWhereClause = " AND MBH_BILL_DATE BETWEEN '" + new Timestamp(startDate.getTime()) + "' AND '"
					+ new Timestamp(endDate.getTime()) + "'";
			stringBuff.append(dateWhereClause);
		}

		if (searchCriteria.getCounterName() != null && !"".equals(searchCriteria.getCounterName())) {
			String counterWhereClause = " AND MBH_COUNTER_CODE = '" + searchCriteria.getCounterName() + "'";
			stringBuff.append(counterWhereClause);
		}

		logger.info("Query to get bill Amt: {}", stringBuff.toString());
		billTrendData = jdbcTemplate.query(stringBuff.toString(),
				(resultSet, rowNum) -> new BillTrendData(resultSet.getDouble("SALES_AMOUNT"),
						resultSet.getDouble("PROFIT"), resultSet.getDouble("CASH_AMOUNT"),
						resultSet.getDouble("CARD_AMOUNT"), resultSet.getDouble("UPI_AMOUNT"),
						resultSet.getDouble("DUE_AMOUNT"), resultSet.getDouble("CREDIT_AMOUNT"),
						resultSet.getDouble("LOYALTY_AOUNT")));
		return billTrendData.get(0);
	}

	@SuppressWarnings("deprecation")
	private Date stringToDateFormatted(String dateString) {
		Date formattedDate = null;
		String dateFormat = "MM/dd/yyyy";
		String stringToDateFormat = "yyyy-MM-dd'T'HH:mm:ssZ";

		DateFormat dateFomatter = new SimpleDateFormat(dateFormat);
		DateFormat stringToDateFormatter = new SimpleDateFormat(stringToDateFormat, Locale.US);
		try {
			String[] lastSectionArray = dateString.split("[.]");
			String lastSection = lastSectionArray[lastSectionArray.length - 1];
			Date stringToDate = stringToDateFormatter.parse(dateString.replaceAll("." + lastSection, "+0000"));
			logger.debug("stringToDate: {}", stringToDate);
			formattedDate = new Date(dateFomatter.format(stringToDate));
		} catch (ParseException e) {
			e.printStackTrace();
			logger.error("Failed to convert String to date formatted {}", e.getMessage());
		}
		return formattedDate;
	}

	@Override
	public List<TopSellingTrendData> getTopSellingProduct(BillSearchCriteria searchCriteria) {
		List<TopSellingTrendData> topSellingDataList = new ArrayList<>();
		String customerWhereClause = "";
		StringBuffer stringBuff = new StringBuffer();
		String findQuery = "SELECT TOP (10) MIH_ITEM_NAME ITEM_NAME, MBD_ITEM_CODE ITEM_CODE, SUM(MBD_ITEM_QTY) SALE_COUNT "
				+ "FROM MED_BILL_DTL, MED_ITEM_HDR WHERE MBD_BILL_NO IN (SELECT MBH_BILL_NO FROM MED_BILL_HDR WHERE MBD_ITEM_CODE = MIH_ITEM_CODE "
				+ "AND MBH_BILL_NO > 0 ";
		stringBuff.append(findQuery);
		if (searchCriteria.getBillStartDate() != null) {
			Date startDate = stringToDateFormatted(searchCriteria.getBillStartDate().toString());
			Date endDate = stringToDateFormatted(searchCriteria.getBillEndDate().toString());
			String dateWhereClause = " AND MBH_BILL_DATE BETWEEN '" + new Timestamp(startDate.getTime()) + "' AND '"
					+ new Timestamp(endDate.getTime()) + "' ";
			stringBuff.append(dateWhereClause);
		}
		if (searchCriteria.getCustomerName() != null && !"".equals(searchCriteria.getCustomerName())) {
			if (searchCriteria.getCustomerName().replaceAll("[0-9]", "").equalsIgnoreCase("")) {
				BigInteger customerData = new BigInteger(searchCriteria.getCustomerName());
				customerWhereClause = " AND (MBH_BILL_CUST_CODE = " + customerData + " OR MBH_PHONE1 = '" + customerData
						+ "' OR MBH_PHONE2 = '" + customerData + "') ";
			} else {
				customerWhereClause = " AND LOWER(MBH_BILL_CUST_NAME) LIKE LOWER('%"
						+ searchCriteria.getCustomerName().replace(" ", "%") + "%') ";
			}
			stringBuff.append(customerWhereClause);
		}
		stringBuff.append(") GROUP BY MBD_ITEM_CODE, MIH_ITEM_NAME ORDER BY SALE_COUNT DESC");
		logger.info("Query to find top selling: {}", stringBuff.toString());
		topSellingDataList = jdbcTemplate.query(stringBuff.toString(),
				(resultSet, rowNum) -> new TopSellingTrendData(resultSet.getString("ITEM_NAME"),
						resultSet.getInt("ITEM_CODE"), resultSet.getInt("SALE_COUNT")));
		return topSellingDataList;
	}

	@Override
	public List<TopGrossTrendData> getTopGrossProduct(BillSearchCriteria searchCriteria) {
		List<TopGrossTrendData> topGrossDataList = new ArrayList<>();
		String customerWhereClause = "";
		StringBuffer stringBuff = new StringBuffer();
		String findQuery = "SELECT TOP (10) MIH_ITEM_NAME ITEM_NAME, MBD_ITEM_CODE ITEM_CODE, SUM(MBD_ITEM_AMOUNT) GROSS_SALE "
				+ "FROM MED_BILL_DTL, MED_ITEM_HDR WHERE MBD_BILL_NO IN (SELECT MBH_BILL_NO FROM MED_BILL_HDR WHERE MBD_ITEM_CODE = MIH_ITEM_CODE "
				+ "AND MBH_BILL_NO > 0 ";
		stringBuff.append(findQuery);
		if (searchCriteria.getBillStartDate() != null) {
			Date startDate = stringToDateFormatted(searchCriteria.getBillStartDate().toString());
			Date endDate = stringToDateFormatted(searchCriteria.getBillEndDate().toString());
			String dateWhereClause = " AND MBH_BILL_DATE BETWEEN '" + new Timestamp(startDate.getTime()) + "' AND '"
					+ new Timestamp(endDate.getTime()) + "' ";
			stringBuff.append(dateWhereClause);
		}
		if (searchCriteria.getCustomerName() != null && !"".equals(searchCriteria.getCustomerName())) {
			if (searchCriteria.getCustomerName().replaceAll("[0-9]", "").equalsIgnoreCase("")) {
				BigInteger customerData = new BigInteger(searchCriteria.getCustomerName());
				customerWhereClause = " AND (MBH_BILL_CUST_CODE = " + customerData + " OR MBH_PHONE1 = '" + customerData
						+ "' OR MBH_PHONE2 = '" + customerData + "') ";
			} else {
				customerWhereClause = " AND LOWER(MBH_BILL_CUST_NAME) LIKE LOWER('%"
						+ searchCriteria.getCustomerName().replace(" ", "%") + "%') ";
			}
			stringBuff.append(customerWhereClause);
		}
		stringBuff.append(") GROUP BY MBD_ITEM_CODE, MIH_ITEM_NAME ORDER BY GROSS_SALE DESC");
		logger.info("Query to find top selling: {}", stringBuff.toString());
		topGrossDataList = jdbcTemplate.query(stringBuff.toString(),
				(resultSet, rowNum) -> new TopGrossTrendData(resultSet.getString("ITEM_NAME"),
						resultSet.getInt("ITEM_CODE"), resultSet.getDouble("GROSS_SALE")));
		return topGrossDataList;
	}

	@Override
	public List<BillAnalyticsData> getBillAnalyticsData(BillSearchCriteria searchCriteria) {
		List<BillAnalyticsData> billAnalyticsList = new ArrayList<>();
		String customerWhereClause = "";
		String loyaltyCustomerClause = "";
		StringBuffer stringBuff = new StringBuffer();
		if (searchCriteria.getCustomerName() != null && !"".equals(searchCriteria.getCustomerName())) {
			if (searchCriteria.getCustomerName().replaceAll("[0-9]", "").equalsIgnoreCase("")) {
				BigInteger customerData = new BigInteger(searchCriteria.getCustomerName());
				customerWhereClause = " AND (MBH_BILL_CUST_CODE = " + customerData + " OR MBH_PHONE1 = '" + customerData
						+ "' OR MBH_PHONE2 = '" + customerData + "') ";
			} else {
				customerWhereClause = " AND LOWER(MBH_BILL_CUST_NAME) LIKE LOWER('%"
						+ searchCriteria.getCustomerName().replace(" ", "%") + "%') ";
			}
		}
		if (searchCriteria.isLoyaltyAllowed()) {
			loyaltyCustomerClause = "AND mcm_loyalty_allowed = 1";
		}
		String findQuery = "SELECT MBH_BILL_CUST_CODE, MCM_CUST_NAME, MCM_CUST_ADDR1, MCM_CUST_ADDR2, MCM_CUST_PIN, MCM_CUST_TEL, MCM_PLACE, "
				+ "MCM_PHONE1, MCM_PHONE2, BILL_COUNT, SALES_AMOUNT, PROFIT FROM ( SELECT COUNT(MBH_BILL_CUST_CODE) BILL_COUNT, MBH_BILL_CUST_CODE,  "
				+ "SUM(SALES_AMT) SALES_AMOUNT, SUM(PROFIT_AMT) PROFIT  FROM( SELECT MBH_SPLIT_BNO_PK MBH_BILL_NO, MBH_BILL_CUST_CODE, "
				+ "SUM(MBH_BILL_AMOUNT) SALES_AMT, SUM(MBH_PROFIT) PROFIT_AMT FROM MED_BILL_HDR, MED_CUSTOMER_MAST WHERE MBH_BILL_CUST_CODE = MCM_CUST_CODE "
				+ "AND MBH_BILL_NO > 0 " + customerWhereClause + loyaltyCustomerClause + " AND MBH_BILL_TYPE != 'CR' "
				+ "AND MBH_BILL_DATE BETWEEN '%s' AND '%s' AND MBH_BILL_CUST_CODE != 0 AND MBH_INVTYPE IN (1, 2) AND MBH_SPLIT_BNO_PK != 0 "
				+ "GROUP BY MBH_BILL_CUST_CODE, MBH_SPLIT_BNO_PK UNION SELECT MBH_BILL_NO, MBH_BILL_CUST_CODE, MBH_BILL_AMOUNT SALES_AMT, "
				+ "MBH_PROFIT PROFIT_AMT FROM MED_BILL_HDR, MED_CUSTOMER_MAST WHERE MBH_BILL_CUST_CODE = MCM_CUST_CODE AND MBH_BILL_NO > 0 "
				+ customerWhereClause + loyaltyCustomerClause
				+ " AND MBH_BILL_TYPE != 'CR' AND MBH_BILL_DATE BETWEEN '%s' AND '%s' "
				+ "AND MBH_BILL_CUST_CODE != 0 AND MBH_INVTYPE IN (1, 2) AND MBH_SPLIT_BNO_PK = 0) INNER_QUERY GROUP BY MBH_BILL_CUST_CODE) BILL_DETAIL, "
				+ "MED_CUSTOMER_MAST WHERE MBH_BILL_CUST_CODE = MCM_CUST_CODE ";

		if (searchCriteria.getBillStartDate() != null) {
			Date startDate = stringToDateFormatted(searchCriteria.getBillStartDate().toString());
			Date endDate = stringToDateFormatted(searchCriteria.getBillEndDate().toString());
			stringBuff.append(
					String.format(findQuery, new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()),
							new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime())));
		}
		if (searchCriteria.getBillFilterMinCount() != null) {
			String countFilterClause = "";
			if (searchCriteria.getBillFilterMaxCount() != null) {
				countFilterClause = " AND BILL_COUNT >= " + searchCriteria.getBillFilterMinCount()
						+ " AND BILL_COUNT <= " + searchCriteria.getBillFilterMaxCount();
			} else {
				countFilterClause = " AND BILL_COUNT >= " + searchCriteria.getBillFilterMinCount();
			}
			stringBuff.append(countFilterClause);
		}
		if (searchCriteria.getBillFilterMinAmt() != null) {
			String billFilterAmtClause = "";
			if (searchCriteria.getBillFilterMaxAmt() != null) {
				billFilterAmtClause = " AND SALES_AMOUNT >= " + searchCriteria.getBillFilterMinAmt()
						+ " AND SALES_AMOUNT <= " + searchCriteria.getBillFilterMaxAmt();
			} else {
				billFilterAmtClause = " AND SALES_AMOUNT >= " + searchCriteria.getBillFilterMinAmt();
			}
			stringBuff.append(billFilterAmtClause);
		}
		if (searchCriteria.getBillFilterProfitMinAmt() != null) {
			String billFilterProfitAmtClause = "";
			if (searchCriteria.getBillFilterProfitMaxAmt() != null) {
				billFilterProfitAmtClause = " AND PROFIT >= " + searchCriteria.getBillFilterProfitMinAmt()
						+ " AND PROFIT <= " + searchCriteria.getBillFilterProfitMaxAmt();
			} else {
				billFilterProfitAmtClause = " AND PROFIT >= " + searchCriteria.getBillFilterProfitMinAmt();
			}
			stringBuff.append(billFilterProfitAmtClause);
		}

		stringBuff.append(" ORDER BY SALES_AMOUNT DESC");
		logger.info("Query used to find bill analytics data {}", stringBuff.toString());
		billAnalyticsList = jdbcTemplate.query(stringBuff.toString(),
				(resultSet, rowNum) -> new BillAnalyticsData(resultSet.getInt("MBH_BILL_CUST_CODE"),
						resultSet.getString("MCM_CUST_NAME"),
						checkStringValue(resultSet.getString("MCM_CUST_ADDR1")) + " "
								+ checkStringValue(resultSet.getString("MCM_CUST_ADDR2")) + " "
								+ checkStringValue(resultSet.getString("MCM_PLACE")) + " "
								+ checkStringValue(resultSet.getString("MCM_CUST_PIN")),
						generatePhoneNoSet(resultSet.getString("MCM_CUST_TEL"), resultSet.getString("MCM_PHONE1"),
								resultSet.getString("MCM_PHONE2")),
						resultSet.getInt("BILL_COUNT"), resultSet.getDouble("SALES_AMOUNT"),
						resultSet.getDouble("PROFIT"), false, false));
		logger.info("Result size of bill analytics data {}", billAnalyticsList.size());
		return billAnalyticsList;
	}

	@Override
	public Integer getProductCount(BillSearchCriteria searchCriteria) {
		Integer productCount = 0;
		String customerWhereClause = "";
		StringBuffer stringBuff = new StringBuffer();
		String findQuery = "SELECT COUNT(DISTINCT MBD_ITEM_CODE) PROD_COUNT FROM MED_BILL_DTL "
				+ "WHERE MBD_BILL_NO IN (SELECT MBH_BILL_NO FROM MED_BILL_HDR WHERE  MBH_BILL_NO > 0 ";
		stringBuff.append(findQuery);
		if (searchCriteria.getBillStartDate() != null) {
			Date startDate = stringToDateFormatted(searchCriteria.getBillStartDate().toString());
			Date endDate = stringToDateFormatted(searchCriteria.getBillEndDate().toString());
			String dateWhereClause = " AND MBH_BILL_DATE BETWEEN '" + new Timestamp(startDate.getTime()) + "' AND '"
					+ new Timestamp(endDate.getTime()) + "' ";
			stringBuff.append(dateWhereClause);
		}
		if (searchCriteria.getCustomerName() != null && !"".equals(searchCriteria.getCustomerName())) {
			if (searchCriteria.getCustomerName().replaceAll("[0-9]", "").equalsIgnoreCase("")) {
				BigInteger customerData = new BigInteger(searchCriteria.getCustomerName());
				customerWhereClause = " AND (MBH_BILL_CUST_CODE = " + customerData + " OR MBH_PHONE1 = '" + customerData
						+ "' OR MBH_PHONE2 = '" + customerData + "') ";
			} else {
				customerWhereClause = " AND LOWER(MBH_BILL_CUST_NAME) LIKE LOWER('%"
						+ searchCriteria.getCustomerName().replace(" ", "%") + "%') ";
			}
			stringBuff.append(customerWhereClause);
		}
		stringBuff.append(" )");
		logger.info("Query to find product count {}", stringBuff.toString());
		productCount = jdbcTemplate.queryForObject(stringBuff.toString(), Integer.class);
		return productCount;
	}

	private Set<String> generatePhoneNoSet(String phoneNum1, String phoneNum2, String phoneNum3) {
		Set<String> phoneNumberSet = new HashSet<>();
		if (!"".equals(phoneNum1.replace(".", ""))) {
			phoneNumberSet.add(phoneNum1);
		}
		if (!"".equals(phoneNum2.replace(".", ""))) {
			phoneNumberSet.add(phoneNum2);
		}
		if (!"".equals(phoneNum3.replace(".", ""))) {
			phoneNumberSet.add(phoneNum3);
		}
		return phoneNumberSet;
	}

	private String checkStringValue(String addressValue) {
		return addressValue.replace(".", "");
	}

	@Override
	public List<SalesPurchaseTrendData> getPurchaseTrend(BillSearchCriteria searchCriteria) {
		List<SalesPurchaseTrendData> purchaseTrendList = new ArrayList<>();
		String customerWhereClause = "";
		StringBuffer stringBuff = new StringBuffer();
		String findQuery = "SELECT MIH_ITEM_CODE, MIH_ITEM_NAME, MMH_DIST_BILL_DT, SUM(MMD_RECD_QTY) AS QUANTITY FROM MED_MRC_DTL, MED_ITEM_HDR, "
				+ "MED_MRC_HDR WHERE MMD_ITEM_CODE = MIH_ITEM_CODE AND MMD_MRC_NO = MMH_MRC_NO AND MMH_MRC_NO > 0 AND MIH_PREPARED NOT IN ('H', 'R') ";
		stringBuff.append(findQuery);
		if (searchCriteria.getBillStartDate() != null) {
			Date startDate = stringToDateFormatted(searchCriteria.getBillStartDate().toString());
			Date endDate = stringToDateFormatted(searchCriteria.getBillEndDate().toString());
			String dateWhereClause = " AND MMH_DIST_BILL_DT BETWEEN '" + new Timestamp(startDate.getTime()) + "' AND '"
					+ new Timestamp(endDate.getTime()) + "' ";
			stringBuff.append(dateWhereClause);
		}
		if (searchCriteria.getItemCode() != null) {
			String itemCodeWhereClause = " AND MIH_ITEM_CODE = " + searchCriteria.getItemCode();
			stringBuff.append(itemCodeWhereClause);
		}
		if (searchCriteria.getCustomerName() != null && !"".equals(searchCriteria.getCustomerName())) {
			if (searchCriteria.getCustomerName().replaceAll("[0-9]", "").equalsIgnoreCase("")) {
				BigInteger customerData = new BigInteger(searchCriteria.getCustomerName());
				customerWhereClause = " AND (MBH_BILL_CUST_CODE = " + customerData + " OR MBH_PHONE1 = '" + customerData
						+ "' OR MBH_PHONE2 = '" + customerData + "') ";
			} else {
				customerWhereClause = " AND LOWER(MBH_BILL_CUST_NAME) LIKE LOWER('%"
						+ searchCriteria.getCustomerName().replace(" ", "%") + "%') ";
			}
			stringBuff.append(customerWhereClause);
		}
		stringBuff.append(" GROUP BY MIH_ITEM_CODE, MIH_ITEM_NAME, MMH_DIST_BILL_DT ORDER BY MMH_DIST_BILL_DT");
		logger.info("Query to find purchase trend: {}", stringBuff.toString());
		purchaseTrendList = jdbcTemplate.query(stringBuff.toString(),
				(resultSet, rowNum) -> new SalesPurchaseTrendData(resultSet.getString("MIH_ITEM_NAME"),
						resultSet.getInt("MIH_ITEM_CODE"), resultSet.getDate("MMH_DIST_BILL_DT"),
						resultSet.getDouble("QUANTITY")));
		return purchaseTrendList;
	}

	@Override
	public List<SalesPurchaseTrendData> getSalesTrend(BillSearchCriteria searchCriteria) {
		List<SalesPurchaseTrendData> purchaseTrendList = new ArrayList<>();
		String customerWhereClause = "";
		StringBuffer stringBuff = new StringBuffer();
		String findQuery = "SELECT MIH_ITEM_CODE, MIH_ITEM_NAME, MBH_BILL_DATE, SUM(MBD_ITEM_QTY) AS QUANTITY FROM MED_BILL_DTL, MED_ITEM_HDR, "
				+ "MED_BILL_HDR WHERE MBD_ITEM_CODE = MIH_ITEM_CODE AND MBH_BILL_NO = MBD_BILL_NO AND MBH_BILL_NO > 0  ";
		stringBuff.append(findQuery);
		if (searchCriteria.getBillStartDate() != null) {
			Date startDate = stringToDateFormatted(searchCriteria.getBillStartDate().toString());
			Date endDate = stringToDateFormatted(searchCriteria.getBillEndDate().toString());
			String dateWhereClause = " AND MBH_BILL_DATE BETWEEN '" + new Timestamp(startDate.getTime()) + "' AND '"
					+ new Timestamp(endDate.getTime()) + "' ";
			stringBuff.append(dateWhereClause);
		}
		if (searchCriteria.getItemCode() != null) {
			String itemCodeWhereClause = " AND MIH_ITEM_CODE = " + searchCriteria.getItemCode();
			stringBuff.append(itemCodeWhereClause);
		}
		if (searchCriteria.getCustomerName() != null && !"".equals(searchCriteria.getCustomerName())) {
			if (searchCriteria.getCustomerName().replaceAll("[0-9]", "").equalsIgnoreCase("")) {
				BigInteger customerData = new BigInteger(searchCriteria.getCustomerName());
				customerWhereClause = " AND (MBH_BILL_CUST_CODE = " + customerData + " OR MBH_PHONE1 = '" + customerData
						+ "' OR MBH_PHONE2 = '" + customerData + "') ";
			} else {
				customerWhereClause = " AND LOWER(MBH_BILL_CUST_NAME) LIKE LOWER('%"
						+ searchCriteria.getCustomerName().replace(" ", "%") + "%') ";
			}
			stringBuff.append(customerWhereClause);
		}
		stringBuff.append(" GROUP BY MIH_ITEM_CODE, MIH_ITEM_NAME, MBH_BILL_DATE ORDER BY MBH_BILL_DATE");
		logger.info("Query to find sales trend: {}", stringBuff.toString());
		purchaseTrendList = jdbcTemplate.query(stringBuff.toString(),
				(resultSet, rowNum) -> new SalesPurchaseTrendData(resultSet.getString("MIH_ITEM_NAME"),
						resultSet.getInt("MIH_ITEM_CODE"), resultSet.getDate("MBH_BILL_DATE"),
						resultSet.getDouble("QUANTITY")));
		return purchaseTrendList;
	}

	@Override
	public List<ProductSupplierWiseData> getSupplierWiseData(BillSearchCriteria searchCriteria) {
		List<ProductSupplierWiseData> supplierWiseData = new ArrayList<>();
		String supplierDataQuery = "SELECT ITEM_DATA.ITEM_CODE, ITEM_DATA.PRODUCT_NAME, PURCHASE_QUANTITY, LAST_PURCHASE_DATE, SALES_QUANTITY, "
				+ "LAST_SALE_DATE FROM (SELECT DISTINCT MMD_ITEM_CODE ITEM_CODE, MIH_ITEM_NAME PRODUCT_NAME FROM MED_ITEM_HDR, MED_MRC_DTL, MED_MRC_HDR "
				+ "WHERE  MMD_MRC_NO = MMH_MRC_NO AND MMD_ITEM_CODE = MIH_ITEM_CODE AND MMH_DIST_CODE = %s) ITEM_DATA LEFT JOIN "
				+ "(SELECT CASE WHEN PUR_ITEM_CODE IS NULL THEN SAL_ITEM_CODE ELSE PUR_ITEM_CODE END AS ITEM_CODE, CASE WHEN PUR_PRODUCT_NAME IS NULL "
				+ "THEN SAL_PRODUCT_NAME ELSE PUR_PRODUCT_NAME END AS PRODUCT_NAME, PURCHASE_QUANTITY, LAST_PURCHASE_DATE, SALES_QUANTITY, LAST_SALE_DATE "
				+ "FROM (SELECT MIH_ITEM_CODE PUR_ITEM_CODE, MIH_ITEM_NAME PUR_PRODUCT_NAME, SUM(MMD_RECD_QTY) AS PURCHASE_QUANTITY, MAX(MMH_DIST_BILL_DT) "
				+ "LAST_PURCHASE_DATE FROM MED_MRC_DTL, MED_ITEM_HDR, MED_MRC_HDR WHERE MMD_ITEM_CODE = MIH_ITEM_CODE AND MMD_MRC_NO = MMH_MRC_NO AND "
				+ "MMH_MRC_NO > 0 AND MIH_PREPARED NOT IN ( 'H', 'R' ) AND MMH_DIST_BILL_DT BETWEEN '%s' AND '%s' AND MIH_ITEM_CODE IN "
				+ "(SELECT DISTINCT MMD_ITEM_CODE ITEM_CODE FROM MED_ITEM_HDR, MED_MRC_DTL, MED_MRC_HDR WHERE MMD_MRC_NO = MMH_MRC_NO AND "
				+ "MMD_ITEM_CODE = MIH_ITEM_CODE AND MMH_DIST_CODE = %s) GROUP  BY MIH_ITEM_CODE, MIH_ITEM_NAME) PURCHASE_DATA "
				+ "FULL OUTER JOIN (SELECT MIH_ITEM_CODE SAL_ITEM_CODE, MIH_ITEM_NAME SAL_PRODUCT_NAME, SUM(MBD_ITEM_QTY)  AS SALES_QUANTITY, "
				+ "MAX(MBH_BILL_DATE) LAST_SALE_DATE FROM   MED_BILL_DTL, MED_ITEM_HDR, MED_BILL_HDR WHERE  MBD_ITEM_CODE = MIH_ITEM_CODE AND "
				+ "MBH_BILL_NO = MBD_BILL_NO AND MBH_BILL_NO > 0 AND MBH_BILL_DATE BETWEEN '%s' AND '%s' AND MIH_ITEM_CODE IN "
				+ "(SELECT DISTINCT MMD_ITEM_CODE ITEM_CODE FROM MED_ITEM_HDR, MED_MRC_DTL, MED_MRC_HDR WHERE MMD_MRC_NO = MMH_MRC_NO AND "
				+ "MMD_ITEM_CODE = MIH_ITEM_CODE AND MMH_DIST_CODE = %s) GROUP  BY MIH_ITEM_CODE, MIH_ITEM_NAME) SALES_DATA "
				+ "ON PUR_ITEM_CODE = SAL_ITEM_CODE AND PUR_PRODUCT_NAME = SAL_PRODUCT_NAME) TREND_DATA ON ITEM_DATA.ITEM_CODE = TREND_DATA.ITEM_CODE "
				+ "AND ITEM_DATA.PRODUCT_NAME = TREND_DATA.PRODUCT_NAME ORDER BY ITEM_DATA.PRODUCT_NAME";
		Date startDate = stringToDateFormatted(searchCriteria.getBillStartDate().toString());
		Date endDate = stringToDateFormatted(searchCriteria.getBillEndDate().toString());
		String supplierFormattedQuery = String.format(supplierDataQuery, searchCriteria.getSupplierCode(),
				new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()), searchCriteria.getSupplierCode(),
				new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()), searchCriteria.getSupplierCode());
		logger.info("Query used is {}", supplierFormattedQuery);
		supplierWiseData = jdbcTemplate.query(supplierFormattedQuery,
				(resultSet, rowNum) -> new ProductSupplierWiseData(resultSet.getInt("ITEM_CODE"),
						resultSet.getString("PRODUCT_NAME"), resultSet.getDouble("PURCHASE_QUANTITY"),
						resultSet.getDouble("SALES_QUANTITY"), resultSet.getDate("LAST_PURCHASE_DATE"),
						resultSet.getDate("LAST_SALE_DATE")));
		/*
		 * supplierWiseData = jdbcTemplate.query(findItemCodeQuery, (resultSet, rowNum),
		 * -> new ProductSupplierWiseData(resultSet.getInt("ITEM_CODE"),
		 * resultSet.getString("PRODUCT_NAME"), null, null, null, null));
		 * supplierWiseData.forEach(supplierData -> { StringBuffer purchaseStringBuff =
		 * new StringBuffer(); String purchaseQuery =
		 * "SELECT SUM(MMD_RECD_QTY) AS QUANTITY, MAX(MMH_DIST_BILL_DT) LAST_PURCHASE_DATE FROM "
		 * +
		 * "MED_MRC_DTL, MED_ITEM_HDR, MED_MRC_HDR WHERE MMD_ITEM_CODE = MIH_ITEM_CODE AND MMD_MRC_NO = MMH_MRC_NO AND MMH_MRC_NO > 0 "
		 * + "AND MIH_PREPARED NOT IN ('H', 'R')";
		 * purchaseStringBuff.append(purchaseQuery); if
		 * (searchCriteria.getBillStartDate() != null) { Date startDate =
		 * stringToDateFormatted(searchCriteria.getBillStartDate().toString()); Date
		 * endDate = stringToDateFormatted(searchCriteria.getBillEndDate().toString());
		 * String dateWhereClause = " AND MMH_DIST_BILL_DT BETWEEN '" + new
		 * Timestamp(startDate.getTime()) + "' AND '" + new Timestamp(endDate.getTime())
		 * + "' "; purchaseStringBuff.append(dateWhereClause); } if
		 * (supplierData.getItemCode() != null) { String itemCodeWhereClause =
		 * " AND MIH_ITEM_CODE = " + supplierData.getItemCode();
		 * purchaseStringBuff.append(itemCodeWhereClause); }
		 * purchaseStringBuff.append(" GROUP BY MIH_ITEM_CODE, MIH_ITEM_NAME");
		 * logger.info("Query to find purchase trend: {}",
		 * purchaseStringBuff.toString()); List<Map<String, Object>> queryPurchaseList =
		 * jdbcTemplate.queryForList(purchaseStringBuff.toString());
		 * if(queryPurchaseList.isEmpty()) { supplierData.setPurchaseCount(0.0);
		 * supplierData.setLastPurchaseDate(null); } else { BigDecimal purchaseCount =
		 * new BigDecimal(queryPurchaseList.get(0).get("QUANTITY").toString());
		 * supplierData.setPurchaseCount(purchaseCount.doubleValue());
		 * supplierData.setLastPurchaseDate((Date)
		 * queryPurchaseList.get(0).get("LAST_PURCHASE_DATE")); }
		 * 
		 * StringBuffer salesStringBuff = new StringBuffer(); String salesQuery =
		 * "SELECT SUM(MBD_ITEM_QTY) AS QUANTITY, MAX(MBH_BILL_DATE) LAST_SALES_DATE  FROM MED_BILL_DTL, MED_ITEM_HDR, "
		 * +
		 * "MED_BILL_HDR WHERE MBD_ITEM_CODE = MIH_ITEM_CODE AND MBH_BILL_NO = MBD_BILL_NO AND MBH_BILL_NO > 0  "
		 * ; salesStringBuff.append(salesQuery); if (searchCriteria.getBillStartDate()
		 * != null) { Date startDate =
		 * stringToDateFormatted(searchCriteria.getBillStartDate().toString()); Date
		 * endDate = stringToDateFormatted(searchCriteria.getBillEndDate().toString());
		 * String dateWhereClause = " AND MBH_BILL_DATE BETWEEN '" + new
		 * Timestamp(startDate.getTime()) + "' AND '" + new Timestamp(endDate.getTime())
		 * + "' "; salesStringBuff.append(dateWhereClause); } if
		 * (supplierData.getItemCode() != null) { String itemCodeWhereClause =
		 * " AND MIH_ITEM_CODE = " + supplierData.getItemCode();
		 * salesStringBuff.append(itemCodeWhereClause); }
		 * salesStringBuff.append(" GROUP BY MIH_ITEM_CODE, MIH_ITEM_NAME");
		 * logger.info("Query to find purchase trend: {}", salesStringBuff.toString());
		 * List<Map<String, Object>> querySalesList =
		 * jdbcTemplate.queryForList(salesStringBuff.toString());
		 * if(querySalesList.isEmpty()) { supplierData.setSalesCount(0.0);
		 * supplierData.setLastSaleDate(null); } else { BigDecimal salesCount = new
		 * BigDecimal(querySalesList.get(0).get("QUANTITY").toString());
		 * supplierData.setSalesCount(salesCount.doubleValue());
		 * supplierData.setLastSaleDate((Date)
		 * querySalesList.get(0).get("LAST_SALES_DATE")); }
		 * 
		 * });
		 */
		return supplierWiseData;
	}
}
