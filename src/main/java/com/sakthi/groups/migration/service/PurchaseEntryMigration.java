package com.sakthi.groups.migration.service;

import java.util.List;

import com.sakthi.groups.migration.portal.model.PurchaseBillEntry;
import com.sakthi.groups.migration.portal.model.PurchaseBillItemDetails;

public interface PurchaseEntryMigration {

	boolean migratePurchaseEntry(boolean incremental);

	List<PurchaseBillEntry> getPurchaseBillEntry(Integer supplierCode);

	List<PurchaseBillItemDetails> getPurchaseBillItemEntry(Integer itemCode);
	
	List<PurchaseBillItemDetails> getGrnBillItemEntry(Integer grnRefNo);

}
