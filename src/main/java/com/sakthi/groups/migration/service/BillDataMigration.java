package com.sakthi.groups.migration.service;

import java.util.List;

import com.sakthi.groups.migration.portal.model.BillAnalyticsData;
import com.sakthi.groups.migration.portal.model.BillSearchCriteria;
import com.sakthi.groups.migration.portal.model.BillTrendData;
import com.sakthi.groups.migration.portal.model.ProductSupplierWiseData;
import com.sakthi.groups.migration.portal.model.SalesPurchaseTrendData;
import com.sakthi.groups.migration.portal.model.TopGrossTrendData;
import com.sakthi.groups.migration.portal.model.TopSellingTrendData;

public interface BillDataMigration {

	BillTrendData getSalesTrendData(BillSearchCriteria searchCriteria);

	List<TopSellingTrendData> getTopSellingProduct(BillSearchCriteria searchCriteria);

	List<BillAnalyticsData> getBillAnalyticsData(BillSearchCriteria searchCriteria);

	Integer getProductCount(BillSearchCriteria searchCriteria);

	List<TopGrossTrendData> getTopGrossProduct(BillSearchCriteria searchCriteria);
	
	List<SalesPurchaseTrendData> getPurchaseTrend(BillSearchCriteria searchCriteria);
	
	List<SalesPurchaseTrendData> getSalesTrend(BillSearchCriteria searchCriteria);

	List<ProductSupplierWiseData> getSupplierWiseData(BillSearchCriteria searchCriteria);

}
