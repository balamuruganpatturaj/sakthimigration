package com.sakthi.groups.migration.service;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.sakthi.groups.migration.portal.dao.MigProductRepository;
import com.sakthi.groups.migration.portal.model.ProductMaster;
import com.sakthi.groups.migration.portal.model.ProductPrice;
import com.sakthi.groups.migration.util.CommonFunction;

@Service
public class DataCheckServiceImpl implements DataCheckService {

	@Autowired
	private CommonFunction commonFunction;
	
	@Autowired
	private MigProductRepository productRepo;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	
	private List<String> headerDetails = Arrays.asList("CODE", "ALIAS", "NAME", "CATEGORY 1", "CATEGORY 2",
			"CATEGORY 3", "STATUS", "GST", "SELLING", "MRP", "NETCOST", "EANCODE");

	@Override
	public ByteArrayOutputStream checkStaleProduct(Integer noOfDaysBefore,
			String entryType) {
		ByteArrayOutputStream byteArrayStream = new ByteArrayOutputStream();
		logger.info("No of days before {}", noOfDaysBefore);
		Set<Integer> staleItemCode = new HashSet<>();
		switch (entryType) {
		case "PURCHASE":
			staleItemCode = getStalePurchaseItemCode(noOfDaysBefore);
			break;

		case "SALES":
			staleItemCode = getStaleSalesItemCode(noOfDaysBefore);
			break;

		case "BOTH":
			staleItemCode = getBothStaleItemCode(noOfDaysBefore);
			break;
		}
		List<ProductMaster> productMasterList = getProductDetails(staleItemCode);
		logger.info("productMasterList size {}", productMasterList.size());
		byteArrayStream = exportProductDetails(productMasterList);
		return byteArrayStream;
	}

	private Set<Integer> getStalePurchaseItemCode(Integer noOfDaysBefore) {
		Set<Integer> stalePurchaseItemCodeSet = new HashSet<Integer>();
		String stalePurchaseQuery = "SELECT MIH_ITEM_CODE FROM MED_ITEM_HDR WHERE MIH_TRADE_CONF != 'N' AND MIH_PREPARED NOT IN ('H', 'R') AND "
				+ "MIH_ITEM_CODE NOT IN (SELECT DISTINCT MMD_ITEM_CODE FROM MED_MRC_DTL WHERE MMD_MRC_NO IN "
				+ "(SELECT MMH_MRC_NO FROM MED_MRC_HDR WHERE MMH_DIST_BILL_DT > '%s')) ORDER BY MIH_ITEM_NAME";
		String dateString = dateFormat.format(this.commonFunction.getDateFromSlot(noOfDaysBefore));
		stalePurchaseQuery = String.format(stalePurchaseQuery, dateString);
		List<Integer> itemCodeList = new ArrayList<>();
		logger.info("Query to get stale purchase {}", stalePurchaseQuery);
		itemCodeList = jdbcTemplate.query(stalePurchaseQuery, (resultSet, rowNum) -> resultSet.getInt("MIH_ITEM_CODE"));
		stalePurchaseItemCodeSet.addAll(itemCodeList);
		return stalePurchaseItemCodeSet;
	}

	private Set<Integer> getStaleSalesItemCode(Integer noOfDaysBefore) {
		Set<Integer> stalePurchaseItemCodeSet = new HashSet<Integer>();
		String stalePurchaseQuery = "SELECT MIH_ITEM_CODE FROM MED_ITEM_HDR WHERE MIH_TRADE_CONF != 'N' AND "
				+ "MIH_ITEM_CODE NOT IN (SELECT DISTINCT MBD_ITEM_CODE FROM MED_BILL_DTL WHERE MBD_BILL_NO IN "
				+ "(SELECT MBH_BILL_NO FROM MED_BILL_HDR WHERE MBH_BILL_DATE > '%s')) ORDER BY MIH_ITEM_NAME";
		String dateString = dateFormat.format(this.commonFunction.getDateFromSlot(noOfDaysBefore));
		stalePurchaseQuery = String.format(stalePurchaseQuery, dateString);
		List<Integer> itemCodeList = new ArrayList<>();
		logger.info("Query to get stale sales {}", stalePurchaseQuery);
		itemCodeList = jdbcTemplate.query(stalePurchaseQuery, (resultSet, rowNum) -> resultSet.getInt("MIH_ITEM_CODE"));
		stalePurchaseItemCodeSet.addAll(itemCodeList);
		return stalePurchaseItemCodeSet;
	}
	
	private Set<Integer> getBothStaleItemCode(Integer noOfDaysBefore) {
		Set<Integer> bothStaleItemCodeSet = new HashSet<Integer>();
		String stalePurchaseQuery = "SELECT MIH_ITEM_CODE FROM MED_ITEM_HDR WHERE MIH_TRADE_CONF != 'N' AND MIH_PREPARED NOT IN ('H', 'R') AND "
				+ "MIH_ITEM_CODE NOT IN ( SELECT DISTINCT ITEM_CODE FROM ( SELECT DISTINCT MMD_ITEM_CODE ITEM_CODE FROM MED_MRC_DTL WHERE "
				+ "MMD_MRC_NO IN (SELECT MMH_MRC_NO FROM MED_MRC_HDR WHERE MMH_DIST_BILL_DT > '%s') UNION SELECT DISTINCT MBD_ITEM_CODE ITEM_CODE "
				+ "FROM MED_BILL_DTL WHERE MBD_BILL_NO IN (SELECT MBH_BILL_NO FROM MED_BILL_HDR WHERE MBH_BILL_DATE > '%s') ) SALES_PURCHASE) "
				+ "ORDER BY MIH_ITEM_NAME;";
		String dateString = dateFormat.format(this.commonFunction.getDateFromSlot(noOfDaysBefore));
		stalePurchaseQuery = String.format(stalePurchaseQuery, dateString, dateString);
		List<Integer> itemCodeList = new ArrayList<>();
		logger.info("Query to get stale sales {}", stalePurchaseQuery);
		itemCodeList = jdbcTemplate.query(stalePurchaseQuery, (resultSet, rowNum) -> resultSet.getInt("MIH_ITEM_CODE"));
		bothStaleItemCodeSet.addAll(itemCodeList);
		return bothStaleItemCodeSet;
	}
	
	private List<ProductMaster> getProductDetails(Set<Integer> itemCodeSet) {
		List<ProductMaster> prodMasterList = new ArrayList<>();
		prodMasterList = productRepo.findByProductCodeIn(itemCodeSet);
		return prodMasterList;
	}
	
	public ByteArrayOutputStream exportProductDetails(List<ProductMaster> prodMasterList) {
		ByteArrayOutputStream byteArrayStream = new ByteArrayOutputStream();
		ProductMaster[] prodDetailArray = prodMasterList.toArray(new ProductMaster[prodMasterList.size()]);
		XSSFWorkbook workBook = new XSSFWorkbook();
		try {
			Integer rowNum = 0;
			XSSFCellStyle headerStyle = createCellStyle(workBook, true);
			Sheet sheet = workBook.createSheet("Product Details");
			rowNum = populateRowValue(headerStyle, null, sheet, rowNum);
			for (int i = 0; i < prodMasterList.size(); i++) {
				XSSFCellStyle rowCellStyle = createCellStyle(workBook, false);
				rowNum = populateRowValue(rowCellStyle, prodDetailArray[i], sheet, rowNum);
			}
			sheet.createFreezePane(0, 1);
			for (int i = 0; i < headerDetails.size(); i++) {
				sheet.autoSizeColumn(i);
			}
			sheet.setAutoFilter(
					new CellRangeAddress(0, sheet.getLastRowNum(), 0, sheet.getRow(0).getLastCellNum() - 1));
			workBook.write(byteArrayStream);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				workBook.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return byteArrayStream;
	}
	
	private XSSFCellStyle createCellStyle(XSSFWorkbook workBook, boolean isHeader) {
		XSSFColor color = new XSSFColor(Color.WHITE, null);
		XSSFCellStyle cellStyle = workBook.createCellStyle();
		if (isHeader) {
			color = new XSSFColor(Color.LIGHT_GRAY, null);
			cellStyle.setAlignment(HorizontalAlignment.CENTER);
		} else {
			cellStyle.setAlignment(HorizontalAlignment.LEFT);
		}
		cellStyle.setFillForegroundColor(color);
		cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		cellStyle.setBorderBottom(BorderStyle.THIN);
		cellStyle.setBorderLeft(BorderStyle.THIN);
		cellStyle.setBorderRight(BorderStyle.THIN);
		cellStyle.setBorderTop(BorderStyle.THIN);
		return cellStyle;
	}
	
	private Integer populateRowValue(XSSFCellStyle rowStyle, ProductMaster prodMaster, Sheet sheet, Integer rowNum) {
		int columNum = 0;
		Row row = sheet.createRow(rowNum++);
		ProductPrice prodPrice = new ProductPrice();
		if (prodMaster != null) {
			prodPrice = prodMaster.getProductPrice().get(0);
		}
		for (int i = 0; i < headerDetails.size(); i++) {
			Cell cell = row.createCell(columNum++);
			switch (headerDetails.get(i)) {
			case "CODE":
				cell.setCellValue(prodMaster == null ? "Product Code" : prodMaster.getProductCode().toString());
				cell.setCellStyle(rowStyle);
				break;

			case "ALIAS":
				cell.setCellValue(prodMaster == null ? "Product Alias" : prodMaster.getProductAlias());
				cell.setCellStyle(rowStyle);
				break;

			case "NAME":
				cell.setCellValue(prodMaster == null ? "Product Name" : prodMaster.getProductName());
				cell.setCellStyle(rowStyle);
				break;

			case "CATEGORY 1":
				cell.setCellValue(prodMaster == null ? "CATEGORY 1" : prodMaster.getProductCategory1());
				cell.setCellStyle(rowStyle);
				break;

			case "CATEGORY 2":
				cell.setCellValue(prodMaster == null ? "CATEGORY 2" : prodMaster.getProductCategory2());
				cell.setCellStyle(rowStyle);
				break;

			case "CATEGORY 3":
				cell.setCellValue(prodMaster == null ? "CATEGORY 3" : prodMaster.getProductCompany());
				cell.setCellStyle(rowStyle);
				break;

			case "STATUS":
				cell.setCellValue(prodMaster == null ? "STATUS" : String.valueOf(prodMaster.isProductStatus()));
				cell.setCellStyle(rowStyle);
				break;

			case "GST":
				cell.setCellValue(prodMaster == null ? "GST" : String.valueOf(prodMaster.getGstTaxSlab()));
				cell.setCellStyle(rowStyle);
				break;

			case "SELLING":
				cell.setCellValue(prodMaster == null ? "Selling Price" : String.valueOf(prodPrice.getSellingPrice()));
				cell.setCellStyle(rowStyle);
				break;

			case "MRP":
				cell.setCellValue(prodMaster == null ? "MRP" : String.valueOf(prodPrice.getMaxRetailPrice()));
				cell.setCellStyle(rowStyle);
				break;

			case "NETCOST":
				cell.setCellValue(prodMaster == null ? "Net Cost" : String.valueOf(prodPrice.getPurchasePrice()));
				cell.setCellStyle(rowStyle);
				break;

			case "EANCODE":
				cell.setCellValue(prodMaster == null ? "Ean Code" : String.valueOf(prodMaster.getProductEANCode()));
				cell.setCellStyle(rowStyle);
				break;
			}
		}
		return rowNum;
	}
}
