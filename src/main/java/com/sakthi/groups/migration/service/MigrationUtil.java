package com.sakthi.groups.migration.service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.util.StringUtils;

public class MigrationUtil {
	
	public static final String MAP_FILE = "bamini.exe";
	private static Map<String, String> map = new LinkedHashMap<String, String>();

	static {
		try {
			InputStream in = getContextClassLoader().getResourceAsStream(MAP_FILE);
			BufferedReader reader = new BufferedReader(new InputStreamReader(in));
			String line = "";
			while ((line = reader.readLine()) != null) {
				if (line.contains("==")) {
					String[] tokens = StringUtils.split(line, "==");
					map.put("=", tokens[1]);
				} else if (line.contains("=")) {
					String[] tokens = StringUtils.split(line, "=");
					if (tokens.length == 2) {
						map.put(tokens[0], tokens[1]);
					}
				}
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	private static ClassLoader getContextClassLoader() {
	    return Thread.currentThread().getContextClassLoader();
	}

	public static Map<String, String> getMap() {
		return map;
	}
}
