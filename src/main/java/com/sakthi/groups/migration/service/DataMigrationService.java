package com.sakthi.groups.migration.service;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.sakthi.groups.migration.portal.model.BillInfo;
import com.sakthi.groups.migration.portal.model.BillItemDetails;
import com.sakthi.groups.migration.portal.model.BillSearchCriteria;
import com.sakthi.groups.migration.portal.model.ProductDetails;

public interface DataMigrationService {

	boolean migrateSupplierDetails(boolean incrementalSync);

	boolean migrateCustomerDetails(boolean incrementalSync);

	boolean migrateProductDetails(boolean incrementalSync);

	boolean migratePurchaseHistoryDetails(boolean incrementalSync);

	boolean migrateCustomerLoyalty(boolean incrementalSync);

	Set<ProductDetails> getProductDetails(InputStream excelFileStream);

	ByteArrayOutputStream exportProductDetailsAsStream(Set<ProductDetails> prodDetailsSet);

	Map<String, ProductDetails> exportProductDetail();

	List<BillInfo> getBillInfo(BillSearchCriteria searchCriteria);

	List<BillItemDetails> getBillItemDetails(Integer billNo, boolean isQuotationBill);

	Double getBillAmount(Integer mBillNo, String phoneNumber);

	Integer getPBillNumber(Integer mBillNo, String phoneNumber);

	List<String> getCounterName();

}
