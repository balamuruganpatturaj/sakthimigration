package com.sakthi.groups.migration.service;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections4.ListUtils;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.sakthi.groups.migration.gofrugal.dao.MedBillDetailsRepository;
import com.sakthi.groups.migration.gofrugal.dao.MedItemHdrRepository;
import com.sakthi.groups.migration.gofrugal.dao.MedMrcDetailRepository;
import com.sakthi.groups.migration.gofrugal.dao.MedMrcHdrRepository;
import com.sakthi.groups.migration.gofrugal.model.MedMrcHdr;
import com.sakthi.groups.migration.portal.dao.MigProductRepository;
import com.sakthi.groups.migration.portal.model.ProductMaster;
import com.sakthi.groups.migration.portal.model.ProductPrice;
import com.sakthi.groups.migration.portal.model.RodeoExportData;
import com.sakthi.groups.migration.util.CommonFunction;

@Service
public class RodeoDataCheckServiceImpl implements RodeoDataCheckService {

	@Autowired
	private MedMrcHdrRepository mrcHdrRepo;

	@Autowired
	private MedMrcDetailRepository mrdDetailRepo;
	
	@Autowired
	private CommonFunction commonFunction;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private MedBillDetailsRepository medDetailsRepo;

	@Autowired
	private MigProductRepository productRepo;

	@Autowired
	private MedItemHdrRepository itemHdrRepo;

	ObjectMapper mapper = new ObjectMapper();

	private List<String> headerDetails = Arrays.asList("CODE", "ALIAS", "NAME", "CATEGORY 1", "CATEGORY 2",
			"CATEGORY 3", "STATUS", "GST", "SELLING", "MRP", "NETCOST", "EANCODE");

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

	private void readCsvFile(InputStream excelStream, Set<Integer> activeItemCodeSet,
			Set<Integer> inActiveItemCodeSet) {
		List<RodeoExportData> rodeoExportDataList = new ArrayList<>();
		try {
			CSVReader csvReader = new CSVReaderBuilder(new InputStreamReader(excelStream)).withSkipLines(1).build();
			String[] rodeoEntry = null;
			while ((rodeoEntry = csvReader.readNext()) != null) {
				Date offerStartDate = rodeoEntry[10] != null && !rodeoEntry[10].equalsIgnoreCase("")
						? new SimpleDateFormat("MM/dd/yyyy").parse(rodeoEntry[10])
						: null;
				Date offerEndDate = rodeoEntry[11] != null && !rodeoEntry[11].equalsIgnoreCase("")
						? new SimpleDateFormat("MM/dd/yyyy").parse(rodeoEntry[11])
						: null;
				Double offerPrice = rodeoEntry[9] != null && !rodeoEntry[9].equalsIgnoreCase("")
						? Double.valueOf(rodeoEntry[9])
						: null;
				Integer itemCode = Integer.valueOf(rodeoEntry[4].replaceAll("[\", =]", ""));

				RodeoExportData rodeoExportData = new RodeoExportData(Integer.valueOf(rodeoEntry[0]),
						Integer.valueOf(rodeoEntry[1]), rodeoEntry[2], rodeoEntry[3].replaceAll("[\", =]", ""),
						Integer.valueOf(rodeoEntry[4].replaceAll("[\", =]", "")), Integer.valueOf(rodeoEntry[5]),
						Double.valueOf(rodeoEntry[6]), Double.valueOf(rodeoEntry[7]), Double.valueOf(rodeoEntry[8]),
						offerPrice, offerStartDate, offerEndDate, Boolean.valueOf(rodeoEntry[12]));
				if (itemCode == 103) {
					System.out.println("RodeoExportData: " + mapper.writeValueAsString(rodeoExportData));
				}
				if (rodeoExportData.isProductStatus()) {
					activeItemCodeSet.add(rodeoExportData.getItemCode());
				} else {
					inActiveItemCodeSet.add(rodeoExportData.getItemCode());
				}
				rodeoExportDataList.add(rodeoExportData);
			}
			logger.info("activeItemCodeSet size: {}", activeItemCodeSet.size());
			logger.info("inActiveItemCodeSet size: {}", inActiveItemCodeSet.size());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private static <T> List<Set<T>> split(Set<T> original, int subsetSize) {

		if (subsetSize <= 0) {
			throw new IllegalArgumentException("Incorrect max size");
		}

		if (original == null || original.isEmpty()) {
			return Collections.emptyList();
		}

		int subsetCount = (int) (Math.ceil((double) original.size() / subsetSize));
		ArrayList<Set<T>> subsets = new ArrayList<Set<T>>(subsetCount);
		Iterator<T> iterator = original.iterator();

		for (int i = 0; i < subsetCount; i++) {
			Set<T> subset = new LinkedHashSet<T>(subsetSize);
			for (int j = 0; j < subsetSize && iterator.hasNext(); j++) {
				subset.add(iterator.next());
			}
			subsets.add(subset);
		}
		return subsets;
	}

	private Set<Integer> getPurchaseEntryItemCode(Integer noOfDaysBefore) {
		Set<Integer> purchaseItemCodeSet = new HashSet<Integer>();
		Set<Integer> mrcNoSet = new HashSet<Integer>();
		List<MedMrcHdr> medMdrHdrList = mrcHdrRepo.findByMmhMrcDateGreaterThan(this.commonFunction.getDateFromSlot(noOfDaysBefore));
		medMdrHdrList.forEach(medMdrHdr -> mrcNoSet.add(medMdrHdr.getMMH_MRC_NO()));
		List<Set<Integer>> mrcNoSetList = split(mrcNoSet, 200);
		mrcNoSetList.forEach(mrcNoSetLoop -> {
			Set<Integer> itemCodeSet = mrdDetailRepo.findByMIH_ITEM_CODE(mrcNoSetLoop);
			purchaseItemCodeSet.addAll(itemCodeSet);
		});
		logger.info("purchaseItemCodeSet size: {}", purchaseItemCodeSet.size());
		return purchaseItemCodeSet;
	}

	private Set<Integer> getSalesItemCode(Integer noOfDaysBefore) {
		Set<Integer> salesItemCodeSet = new HashSet<Integer>();
		String query = "SELECT MBH_BILL_NO FROM MED_BILL_HDR WHERE MBH_BILL_DATE > '%s' ORDER BY MBH_BILL_NO ASC;";
		String dateString = dateFormat.format(this.commonFunction.getDateFromSlot(noOfDaysBefore));
		query = String.format(query, dateString);
		logger.debug("query: {}", query);
		List<Integer> billNoList = new ArrayList<>();
		billNoList = jdbcTemplate.query(query, (resultSet, rowNum) -> resultSet.getInt("MBH_BILL_NO"));
		logger.debug("billNoList size: {}", billNoList.size());
		List<List<Integer>> billNumberSplitList = ListUtils.partition(billNoList, 200);
		logger.debug("billNumberSplitList size: {}", billNumberSplitList.size());
		billNumberSplitList.forEach(billNumList -> {
			logger.debug("Started processing the list {}", billNumList.get(0));
			Set<Integer> itemCodeSet = medDetailsRepo.findByMBDBillNoIn(billNumList);
			salesItemCodeSet.addAll(itemCodeSet);
		});
		logger.info("salesItemCodeSet size: {}", salesItemCodeSet.size());
		return salesItemCodeSet;
	}

	private Set<Integer> getRepackageItemCode() {
		Set<Integer> repackageItemCodeSet = new HashSet<Integer>();
		repackageItemCodeSet = itemHdrRepo.findRepackageItemCode();
		return repackageItemCodeSet;
	}

	@SuppressWarnings({ "deprecation", "unused" })
	private void readInputExcelFile(InputStream excelStream) {
		Set<Integer> activeItemCodeSet = new HashSet<>();
		Set<Integer> inActiveItemCodeSet = new HashSet<>();
		try {
			Workbook workBook = WorkbookFactory.create(excelStream);
			Sheet sheet = workBook.getSheetAt(0);
			Iterator<Row> rowIterator = sheet.iterator();
			while (rowIterator.hasNext()) {
				RodeoExportData rodeoExportData = new RodeoExportData();
				Row row = rowIterator.next();
				if (row.getRowNum() == 0) {
					continue;
				}
				Iterator<Cell> cellIterator = row.cellIterator();
				while (cellIterator.hasNext()) {
					Cell productIdCell = cellIterator.next();
					productIdCell.setCellType(CellType.NUMERIC);
					rodeoExportData.setProductId((int) productIdCell.getNumericCellValue());
					Cell skuIdCell = cellIterator.next();
					skuIdCell.setCellType(CellType.NUMERIC);
					rodeoExportData.setSkuId((int) skuIdCell.getNumericCellValue());
					Cell nameCell = cellIterator.next();
					nameCell.setCellType(CellType.STRING);
					rodeoExportData.setProductName(nameCell.getStringCellValue());
					Cell eanCodeCell = cellIterator.next();
					eanCodeCell.setCellType(CellType.STRING);
					rodeoExportData.setBarCode(eanCodeCell.getStringCellValue());
					Cell itemCodeCell = cellIterator.next();
					itemCodeCell.setCellType(CellType.NUMERIC);
					rodeoExportData.setItemCode((int) itemCodeCell.getNumericCellValue());
					Cell gstCell = cellIterator.next();
					gstCell.setCellType(CellType.NUMERIC);
					rodeoExportData.setGstPercent((int) gstCell.getNumericCellValue());
					Cell mrpCell = cellIterator.next();
					mrpCell.setCellType(CellType.NUMERIC);
					rodeoExportData.setMaxRetailPrice((Double) mrpCell.getNumericCellValue());
					Cell purchasePriceCell = cellIterator.next();
					purchasePriceCell.setCellType(CellType.NUMERIC);
					rodeoExportData.setPurchasePrice((Double) purchasePriceCell.getNumericCellValue());
					Cell sellingPriceCell = cellIterator.next();
					sellingPriceCell.setCellType(CellType.NUMERIC);
					rodeoExportData.setSellingPrice((Double) sellingPriceCell.getNumericCellValue());
					Cell offerPriceCell = cellIterator.next();
					offerPriceCell.setCellType(CellType.NUMERIC);
					rodeoExportData.setOfferPrice((Double) offerPriceCell.getNumericCellValue());
					Cell offerStartDateCell = cellIterator.next();
					offerStartDateCell.setCellType(CellType.NUMERIC);
					rodeoExportData.setOfferStartDate(offerStartDateCell.getDateCellValue());
					Cell offerEndDateCell = cellIterator.next();
					offerEndDateCell.setCellType(CellType.NUMERIC);
					rodeoExportData.setOfferEndDate(offerEndDateCell.getDateCellValue());
					Cell statusCell = cellIterator.next();
					statusCell.setCellType(CellType.STRING);
					rodeoExportData.setProductStatus(Boolean.valueOf(statusCell.getStringCellValue().toLowerCase()));
				}
				if (rodeoExportData.isProductStatus()) {
					activeItemCodeSet.add(rodeoExportData.getItemCode());
				} else {
					inActiveItemCodeSet.add(rodeoExportData.getItemCode());
				}
			}
			logger.info("activeItemCodeSet size: {}", activeItemCodeSet.size());
			logger.info("inActiveItemCodeSet size: {}", inActiveItemCodeSet.size());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private Set<Integer> validateItemNotExists(Set<Integer> sourceItemCodeSet, Set<Integer> activeItemCodeSet) {
		Set<Integer> itemCodeSet = new HashSet<>();
		sourceItemCodeSet.forEach(itemCode -> {
			if (!activeItemCodeSet.contains(itemCode)) {
				if(itemCode == 103) {
					System.out.println("Item 103 available in active item code");
				}
				itemCodeSet.add(itemCode);
			}
		});
		return itemCodeSet;
	}

	private Set<Integer> validateItemExists(Set<Integer> sourceItemCodeSet, Set<Integer> inActiveItemCodeSet) {
		Set<Integer> itemCodeSet = new HashSet<>();
		sourceItemCodeSet.forEach(itemCode -> {
			if (inActiveItemCodeSet.contains(itemCode)) {
				itemCodeSet.add(itemCode);
			}
		});
		return itemCodeSet;
	}

	private List<ProductMaster> getProductDetails(Set<Integer> itemCodeSet) {
		List<ProductMaster> prodMasterList = new ArrayList<>();
		prodMasterList = productRepo.findByProductCodeIn(itemCodeSet);
		return prodMasterList;
	}

	private XSSFCellStyle createCellStyle(XSSFWorkbook workBook, boolean isHeader) {
		XSSFColor color = new XSSFColor(Color.WHITE, null);
		XSSFCellStyle cellStyle = workBook.createCellStyle();
		if (isHeader) {
			color = new XSSFColor(Color.LIGHT_GRAY, null);
			cellStyle.setAlignment(HorizontalAlignment.CENTER);
		} else {
			cellStyle.setAlignment(HorizontalAlignment.LEFT);
		}
		cellStyle.setFillForegroundColor(color);
		cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		cellStyle.setBorderBottom(BorderStyle.THIN);
		cellStyle.setBorderLeft(BorderStyle.THIN);
		cellStyle.setBorderRight(BorderStyle.THIN);
		cellStyle.setBorderTop(BorderStyle.THIN);
		return cellStyle;
	}

	private Integer populateRowValue(XSSFCellStyle rowStyle, ProductMaster prodMaster, Sheet sheet, Integer rowNum) {
		int columNum = 0;
		Row row = sheet.createRow(rowNum++);
		ProductPrice prodPrice = new ProductPrice();
		if (prodMaster != null) {
			prodPrice = prodMaster.getProductPrice().get(0);
		}
		for (int i = 0; i < headerDetails.size(); i++) {
			Cell cell = row.createCell(columNum++);
			switch (headerDetails.get(i)) {
			case "CODE":
				cell.setCellValue(prodMaster == null ? "Product Code" : prodMaster.getProductCode().toString());
				cell.setCellStyle(rowStyle);
				break;

			case "ALIAS":
				cell.setCellValue(prodMaster == null ? "Product Alias" : prodMaster.getProductAlias());
				cell.setCellStyle(rowStyle);
				break;

			case "NAME":
				cell.setCellValue(prodMaster == null ? "Product Name" : prodMaster.getProductName());
				cell.setCellStyle(rowStyle);
				break;

			case "CATEGORY 1":
				cell.setCellValue(prodMaster == null ? "CATEGORY 1" : prodMaster.getProductCategory1());
				cell.setCellStyle(rowStyle);
				break;

			case "CATEGORY 2":
				cell.setCellValue(prodMaster == null ? "CATEGORY 2" : prodMaster.getProductCategory2());
				cell.setCellStyle(rowStyle);
				break;

			case "CATEGORY 3":
				cell.setCellValue(prodMaster == null ? "CATEGORY 3" : prodMaster.getProductCompany());
				cell.setCellStyle(rowStyle);
				break;

			case "STATUS":
				cell.setCellValue(prodMaster == null ? "STATUS" : String.valueOf(prodMaster.isProductStatus()));
				cell.setCellStyle(rowStyle);
				break;

			case "GST":
				cell.setCellValue(prodMaster == null ? "GST" : String.valueOf(prodMaster.getGstTaxSlab()));
				cell.setCellStyle(rowStyle);
				break;

			case "SELLING":
				cell.setCellValue(prodMaster == null ? "Selling Price" : String.valueOf(prodPrice.getSellingPrice()));
				cell.setCellStyle(rowStyle);
				break;

			case "MRP":
				cell.setCellValue(prodMaster == null ? "MRP" : String.valueOf(prodPrice.getMaxRetailPrice()));
				cell.setCellStyle(rowStyle);
				break;

			case "NETCOST":
				cell.setCellValue(prodMaster == null ? "Net Cost" : String.valueOf(prodPrice.getPurchasePrice()));
				cell.setCellStyle(rowStyle);
				break;

			case "EANCODE":
				cell.setCellValue(prodMaster == null ? "Ean Code" : String.valueOf(prodMaster.getProductEANCode()));
				cell.setCellStyle(rowStyle);
				break;
			}
		}
		return rowNum;
	}

	public ByteArrayOutputStream exportProductDetails(List<ProductMaster> prodMasterList) {
		ByteArrayOutputStream byteArrayStream = new ByteArrayOutputStream();
		ProductMaster[] prodDetailArray = prodMasterList.toArray(new ProductMaster[prodMasterList.size()]);
		XSSFWorkbook workBook = new XSSFWorkbook();
		try {
			Integer rowNum = 0;
			XSSFCellStyle headerStyle = createCellStyle(workBook, true);
			Sheet sheet = workBook.createSheet("Product Details");
			rowNum = populateRowValue(headerStyle, null, sheet, rowNum);
			for (int i = 0; i < prodMasterList.size(); i++) {
				XSSFCellStyle rowCellStyle = createCellStyle(workBook, false);
				rowNum = populateRowValue(rowCellStyle, prodDetailArray[i], sheet, rowNum);
			}
			sheet.createFreezePane(0, 1);
			for (int i = 0; i < headerDetails.size(); i++) {
				sheet.autoSizeColumn(i);
			}
			sheet.setAutoFilter(
					new CellRangeAddress(0, sheet.getLastRowNum(), 0, sheet.getRow(0).getLastCellNum() - 1));
			workBook.write(byteArrayStream);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				workBook.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return byteArrayStream;
	}

	private boolean writeExcelProductData(String fileName, ByteArrayOutputStream byteArrayStream) {
		boolean returnStatus = false;
		try (OutputStream outputStream = new FileOutputStream(fileName)) {
			byteArrayStream.writeTo(outputStream);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return returnStatus;
	}

	@Override
	public ByteArrayOutputStream checkActivePurchaseItems(FileInputStream fileInputStream, Integer noOfDaysBefore) {
		ByteArrayOutputStream byteArrayStream = new ByteArrayOutputStream();
		Set<Integer> activeItemCodeSet = new HashSet<>();
		Set<Integer> inActiveItemCodeSet = new HashSet<>();
		readCsvFile(fileInputStream, activeItemCodeSet, inActiveItemCodeSet);
		Set<Integer> purchaseItemCodeSet = getPurchaseEntryItemCode(noOfDaysBefore);
		Set<Integer> purchaseActiveItemCodeSet = validateItemNotExists(purchaseItemCodeSet, activeItemCodeSet);
		logger.info("purchaseActiveItemCodeSet size {}", purchaseActiveItemCodeSet.size());
		List<ProductMaster> productMasterList = getProductDetails(purchaseActiveItemCodeSet);
		logger.info("productMasterList size {}", productMasterList.size());
		byteArrayStream = exportProductDetails(productMasterList);
		return byteArrayStream;
	}

	@Override
	public ByteArrayOutputStream checkActiveSalesItems(FileInputStream fileInputStream, Integer noOfDaysBefore) {
		ByteArrayOutputStream byteArrayStream = new ByteArrayOutputStream();
		Set<Integer> activeItemCodeSet = new HashSet<>();
		Set<Integer> inActiveItemCodeSet = new HashSet<>();
		readCsvFile(fileInputStream, activeItemCodeSet, inActiveItemCodeSet);
		Set<Integer> salesItemCodeSet = getSalesItemCode(noOfDaysBefore);
		Set<Integer> salesActiveItemCodeSet = validateItemNotExists(salesItemCodeSet, activeItemCodeSet);
		List<ProductMaster> productMasterList = getProductDetails(salesActiveItemCodeSet);
		byteArrayStream = exportProductDetails(productMasterList);
		return byteArrayStream;
	}

	@Override
	public ByteArrayOutputStream checkInActivePurchaseItems(FileInputStream fileInputStream, Integer noOfDaysBefore) {
		ByteArrayOutputStream byteArrayStream = new ByteArrayOutputStream();
		Set<Integer> activeItemCodeSet = new HashSet<>();
		Set<Integer> inActiveItemCodeSet = new HashSet<>();
		readCsvFile(fileInputStream, activeItemCodeSet, inActiveItemCodeSet);
		Set<Integer> purchaseItemCodeSet = getPurchaseEntryItemCode(noOfDaysBefore);
		Set<Integer> purchaseInActiveItemCodeSet = validateItemExists(purchaseItemCodeSet, inActiveItemCodeSet);
		List<ProductMaster> productMasterList = getProductDetails(purchaseInActiveItemCodeSet);
		byteArrayStream = exportProductDetails(productMasterList);
		return byteArrayStream;
	}

	@Override
	public ByteArrayOutputStream checkInActiveSalesItems(FileInputStream fileInputStream, Integer noOfDaysBefore) {
		ByteArrayOutputStream byteArrayStream = new ByteArrayOutputStream();
		Set<Integer> activeItemCodeSet = new HashSet<>();
		Set<Integer> inActiveItemCodeSet = new HashSet<>();
		readCsvFile(fileInputStream, activeItemCodeSet, inActiveItemCodeSet);
		Set<Integer> salesItemCodeSet = getSalesItemCode(noOfDaysBefore);
		Set<Integer> salesInActiveItemCodeSet = validateItemExists(salesItemCodeSet, inActiveItemCodeSet);
		List<ProductMaster> productMasterList = getProductDetails(salesInActiveItemCodeSet);
		byteArrayStream = exportProductDetails(productMasterList);
		return byteArrayStream;
	}

	@Override
	public ByteArrayOutputStream checkRepackageItems(FileInputStream fileInputStream, Integer noOfDaysBefore) {
		ByteArrayOutputStream byteArrayStream = new ByteArrayOutputStream();
		Set<Integer> activeItemCodeSet = new HashSet<>();
		Set<Integer> inActiveItemCodeSet = new HashSet<>();
		readCsvFile(fileInputStream, activeItemCodeSet, inActiveItemCodeSet);
		Set<Integer> repackItemCodeSet = getRepackageItemCode();
		Set<Integer> repackageItemCodeSet = validateItemNotExists(repackItemCodeSet, activeItemCodeSet);
		List<ProductMaster> productMasterList = getProductDetails(repackageItemCodeSet);
		byteArrayStream = exportProductDetails(productMasterList);
		return byteArrayStream;
	}

	@Override
	public void checkRodeoDataWithGofrugal() {
		try {
			FileInputStream excelFile = new FileInputStream(new File("D:\\Sakthi\\Products 5_21_2023_5_08_04 PM.csv"));
			ByteArrayOutputStream byteArrayRepackStream = checkActivePurchaseItems(excelFile, 35);
			writeExcelProductData("D:\\Sakthi\\Purchase_Missing_Items.xlsx", byteArrayRepackStream);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

}
