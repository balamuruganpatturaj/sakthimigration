package com.sakthi.groups.migration.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bson.types.Binary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sakthi.groups.migration.portal.dao.DeliveryDetailRepository;
import com.sakthi.groups.migration.portal.model.BillInfo;
import com.sakthi.groups.migration.portal.model.BillItemDetails;
import com.sakthi.groups.migration.portal.model.BillSearchCriteria;
import com.sakthi.groups.migration.portal.model.DeliveryOrderDetail;

@Service
public class DeliveryBillSyncServiceImpl implements DeliveryBillSyncService {

	@Autowired
	private DeliveryDetailRepository deliveryDetailRepo;

	@Autowired
	private DataMigrationService dataMigrationService;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public final String OUT_FOR_DELIVERY = "Out for Delivery";
	public final String ORDER_PLACED = "Order Placed";
	public final String ORDER_DELIVERED = "Delivered";
	public final String PAYMENT_PENDING = "Payment Pending";
	public final String ORDER_CANCELLED = "Cancelled";
	public final String NOT_AVAILABLE = "Not Available";
	public final String PARTIALLY_PAID = "Partially Paid";
	public final String CASH_PAYMENT = "Cash";
	public final String CARD_PAYMENT = "Card";
	public final String DIGITAL_PAYMENT = "UPI/Net Banking";

	private Map<String, Binary> generateBillPdfFile(BillInfo billInfo, List<BillItemDetails> billItemDetails) {
		Map<String, Binary> billFileDetailMap = new HashMap<>();
		return billFileDetailMap;
	}

	private Map<String, Binary> getBillFileDetails(BillInfo billInfo) {
		Map<String, Binary> billFileDetailMap = new HashMap<>();
		List<BillItemDetails> billItemDetails = dataMigrationService.getBillItemDetails(billInfo.getBillNo(), false);
		billFileDetailMap = this.generateBillPdfFile(billInfo, billItemDetails);
		return billFileDetailMap;
	}

	@SuppressWarnings("deprecation")
	private void syncFromGoFrugal(BillInfo billInfo, DeliveryOrderDetail deliveryDetail, boolean incremental) {
		if (incremental) {
			deliveryDetail.setBillAmount(billInfo.getBillAmt());
			Map<String, Binary> billFileDetailMap = this.getBillFileDetails(billInfo);
			billFileDetailMap.entrySet().stream().forEach(map -> {
				deliveryDetail.setBillFileName(map.getKey());
				deliveryDetail.setBillFile(map.getValue());
			});
			if (!billInfo.getBillType().equalsIgnoreCase("DU")) {
				if (billInfo.getBillType().equalsIgnoreCase("CA")) {
					deliveryDetail.setPayment(billInfo.getCashAmt());
					deliveryDetail.setPaymentMode("Cash");
					deliveryDetail.setBalance(0.0);
				} else if (billInfo.getBillType().equalsIgnoreCase("CC")) {
					deliveryDetail.setPayment(billInfo.getCashAmt());
					deliveryDetail.setPaymentMode("Digital Payment");
					deliveryDetail.setBalance(0.0);
				}
				deliveryDetail.setDeliveryStatus("Delivered");
			}
			deliveryDetail.setBillModifiedDate(billInfo.getModificationTime());
			deliveryDetailRepo.save(deliveryDetail);
		} else {
			Double payment = 0.0;
			String paymentMode = "";
			Double balance = 0.0;
			switch (billInfo.getBillType()) {
			case "DU":
				payment = 0.0;
				paymentMode = this.PAYMENT_PENDING;
				balance = billInfo.getBillAmt();
				break;
			case "CA":
				payment = billInfo.getCashAmt();
				paymentMode = this.CASH_PAYMENT;
				balance = billInfo.getBillAmt() - billInfo.getCashAmt();
				break;
			case "SP":
				if (billInfo.getCashAmt() > 0 && billInfo.getCardAmt() > 0) {
					if (billInfo.getRedeemedPoints() > 0) {
						payment = Double.sum(billInfo.getCardAmt(),
								Double.sum(billInfo.getCashAmt(), billInfo.getRedeemedPoints()));
					} else {
						payment = Double.sum(billInfo.getCardAmt(), billInfo.getCashAmt());
					}
					paymentMode = this.CARD_PAYMENT;
				} else if (billInfo.getCashAmt() > 0) {
					if (billInfo.getRedeemedPoints() > 0) {
						payment = Double.sum(billInfo.getCashAmt(), billInfo.getRedeemedPoints());
					} else {
						payment = billInfo.getCashAmt();
					}
					paymentMode = this.CASH_PAYMENT;
				} else {
					if (billInfo.getRedeemedPoints() > 0) {
						payment = Double.sum(billInfo.getCardAmt(), billInfo.getRedeemedPoints());
					} else {
						payment = billInfo.getCardAmt();
					}
					paymentMode = this.CARD_PAYMENT;
				}

				balance = billInfo.getBillAmt() - payment;
				break;
			case "CC":
				payment = billInfo.getCardAmt();
				paymentMode = this.CARD_PAYMENT;
				balance = billInfo.getBillAmt() - billInfo.getCardAmt();
				break;
			default:
				break;
			}
			String dateFormat = "MM/dd/yyyy";
			DateFormat dateFomatter = new SimpleDateFormat(dateFormat);
			Date formattedDate = new Date(dateFomatter.format(billInfo.getBillDate()));
			logger.debug("Formatted date {}", dateFomatter.format(billInfo.getBillDate()));
			Set<String> phoneNumberSet = new HashSet<>();
			if (!".".equals(billInfo.getPhoneNo1()) && !"".equals(billInfo.getPhoneNo1())) {
				phoneNumberSet.add(billInfo.getPhoneNo1());
			}
			if (!".".equals(billInfo.getPhoneNo2()) && !"".equals(billInfo.getPhoneNo2())) {
				phoneNumberSet.add(billInfo.getPhoneNo2());
			}
			if (!".".equals(billInfo.getPhoneNo3()) && !"".equals(billInfo.getPhoneNo3())) {
				phoneNumberSet.add(billInfo.getPhoneNo3());
			}
			String customerAddr = billInfo.getCustomerAddr() + "-" + billInfo.getCustomerArea();
			customerAddr = customerAddr.replaceAll("-\\.", "");
			DeliveryOrderDetail deliveryOrder = new DeliveryOrderDetail(billInfo.getCustomerCode(),
					billInfo.getCustomerName(), customerAddr,
					phoneNumberSet.toString().replaceAll("\\[", "").replaceAll("\\]", ""), formattedDate,
					billInfo.getBillType(), billInfo.getBillNo(), billInfo.getInvoiceNo(), billInfo.getBillAmt(),
					payment, paymentMode, balance, this.ORDER_PLACED, null, null, null, null, null, false, false, false,
					billInfo.getModificationTime());
			deliveryDetailRepo.save(deliveryOrder);
		}
	}

	@Override
	public void syncDeliveryBills() {
		BillSearchCriteria searchCriteria = new BillSearchCriteria();
		searchCriteria.setDeliveryBill(true);
		List<BillInfo> billInfoList = dataMigrationService.getBillInfo(searchCriteria);
		billInfoList.forEach(billInfo -> {
			DeliveryOrderDetail deliveryDetail = deliveryDetailRepo.findByBillNo(billInfo.getBillNo());
			if (deliveryDetail != null) {
				if (!deliveryDetail.getBillType().equalsIgnoreCase(billInfo.getBillType())) {
					this.syncFromGoFrugal(billInfo, deliveryDetail, true);
				} else if (deliveryDetail.getBillModifiedDate().compareTo(billInfo.getModificationTime()) != 0) {
					this.syncFromGoFrugal(billInfo, deliveryDetail, true);
				}
			} else {
				System.out.println("Invoking Full Sync");
				this.syncFromGoFrugal(billInfo, null, false);
			}
		});
	}

}
