package com.sakthi.groups.migration.service;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;

public interface RodeoDataCheckService {
	
	public void checkRodeoDataWithGofrugal();

	ByteArrayOutputStream checkActivePurchaseItems(FileInputStream fileInputStream, Integer noOfDaysBefore);

	ByteArrayOutputStream checkInActivePurchaseItems(FileInputStream fileInputStream, Integer noOfDaysBefore);

	ByteArrayOutputStream checkActiveSalesItems(FileInputStream fileInputStream, Integer noOfDaysBefore);

	ByteArrayOutputStream checkInActiveSalesItems(FileInputStream fileInputStream, Integer noOfDaysBefore);

	ByteArrayOutputStream checkRepackageItems(FileInputStream fileInputStream, Integer noOfDaysBefore);

}
