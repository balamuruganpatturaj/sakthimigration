package com.sakthi.groups.migration.service;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoNamespace;
import com.mongodb.client.model.RenameCollectionOptions;
import com.sakthi.groups.migration.gofrugal.dao.CounterNameDtlRepository;
import com.sakthi.groups.migration.gofrugal.dao.MedBillDetailsRepository;
import com.sakthi.groups.migration.gofrugal.dao.MedBillHdrRepository;
import com.sakthi.groups.migration.gofrugal.dao.MedBillQuotDtlRepository;
import com.sakthi.groups.migration.gofrugal.dao.MedCategoryDetailRepository;
import com.sakthi.groups.migration.gofrugal.dao.MedCustomerRepository;
import com.sakthi.groups.migration.gofrugal.dao.MedDistrubutorRepository;
import com.sakthi.groups.migration.gofrugal.dao.MedItemEanCodeRepository;
import com.sakthi.groups.migration.gofrugal.dao.MedItemHdrRepository;
import com.sakthi.groups.migration.gofrugal.dao.MedPriceLevelRepository;
import com.sakthi.groups.migration.gofrugal.dao.MedTaxMasterRepository;
import com.sakthi.groups.migration.gofrugal.dao.MedUnicodeItemHdrRepository;
import com.sakthi.groups.migration.gofrugal.model.CounterNameDetails;
import com.sakthi.groups.migration.gofrugal.model.MedBillDetails;
import com.sakthi.groups.migration.gofrugal.model.MedBillHdr;
import com.sakthi.groups.migration.gofrugal.model.MedBillQuotDetail;
import com.sakthi.groups.migration.gofrugal.model.MedCategoryDetail;
import com.sakthi.groups.migration.gofrugal.model.MedCustomerMaster;
import com.sakthi.groups.migration.gofrugal.model.MedDistrubutorMaster;
import com.sakthi.groups.migration.gofrugal.model.MedItemDtl;
import com.sakthi.groups.migration.gofrugal.model.MedItemEanCode;
import com.sakthi.groups.migration.gofrugal.model.MedItemHdr;
import com.sakthi.groups.migration.gofrugal.model.MedPriceLevel;
import com.sakthi.groups.migration.gofrugal.model.MedPriceLevelItem;
import com.sakthi.groups.migration.gofrugal.model.MedTaxMaster;
import com.sakthi.groups.migration.gofrugal.model.MedUnicodeItemHdr;
import com.sakthi.groups.migration.portal.dao.MigCustomerRepository;
import com.sakthi.groups.migration.portal.dao.MigProductRepository;
import com.sakthi.groups.migration.portal.dao.MigProductTempRepository;
import com.sakthi.groups.migration.portal.dao.MigSupplierRepository;
import com.sakthi.groups.migration.portal.dao.ProductEanCodeRepository;
import com.sakthi.groups.migration.portal.model.BillInfo;
import com.sakthi.groups.migration.portal.model.BillItemDetails;
import com.sakthi.groups.migration.portal.model.BillSearchCriteria;
import com.sakthi.groups.migration.portal.model.CustomerMaster;
import com.sakthi.groups.migration.portal.model.EancodeInputModel;
import com.sakthi.groups.migration.portal.model.ProductDetails;
import com.sakthi.groups.migration.portal.model.ProductEanCode;
import com.sakthi.groups.migration.portal.model.ProductMasterTemp;
import com.sakthi.groups.migration.portal.model.ProductPrice;
import com.sakthi.groups.migration.portal.model.ProductPriceLevel;
import com.sakthi.groups.migration.portal.model.SupplierMaster;
import com.sakthi.groups.migration.portal.model.UnicodeModel;

@Service
public class DataMigrationServiceImpl implements DataMigrationService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Value("${sakthi.export.output.file}")
	private String outputFileName;

	@Value("${sakthi.delivery.sync.frequency}")
	private Integer syncFrequency;

	private List<String> headerDetails = Arrays.asList("CODE", "ALIAS", "NAME", "EANCODE", "SELLING", "MRP", "NETCOST");

	@Autowired
	private MedDistrubutorRepository medDistrubutorRepo;

	@Autowired
	private MigSupplierRepository supplierMasterRepo;

	@Autowired
	private MedCustomerRepository medCustomerRepo;

	@Autowired
	private MigCustomerRepository customerRepo;

	@Autowired
	private MigProductRepository productRepo;

	@Autowired
	private MigProductTempRepository productTempRepo;

	@Autowired
	private MedItemHdrRepository medItemHdrRepo;

	@Autowired
	private MedItemEanCodeRepository medItemEanCodeRepo;

	@Autowired
	private MedTaxMasterRepository medTaxRepo;

	@Autowired
	private MedUnicodeItemHdrRepository unicodeRepo;

	@Autowired
	private MedPriceLevelRepository priceLevelRepo;

	@Autowired
	private MedCategoryDetailRepository categoryRepo;

	@Autowired
	private CounterNameDtlRepository counterRepo;

	@Autowired
	private MedBillHdrRepository medBillHdrRepo;

	@Autowired
	private ProductEanCodeRepository eanCodeRepo;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private MongoTemplate mongoTemplate;

	@Value("${spring.data.mongodb.database}")
	private String dbName;

	@Autowired
	private MedBillDetailsRepository medDetailsRepo;

	@Autowired
	private MedBillQuotDtlRepository quotDetailsRepo;

	ObjectMapper mapper = new ObjectMapper();

	@Override
	public boolean migrateSupplierDetails(boolean incrementalSync) {
		boolean syncStatus = false;
		if (incrementalSync) {
			logger.info("Incremental sync yet to be supported");
		} else {
			logger.info("Starting migration of Supplier details from GoFrugal");
			supplierMasterRepo.deleteAll();
			Iterable<MedDistrubutorMaster> distributorMaster = medDistrubutorRepo.findAll();
			distributorMaster.forEach(action -> {
				logger.info("Processing {}", action.getMdmDistName());
				SupplierMaster supplierMaster = new SupplierMaster();
				supplierMaster.setSupplierCode(checkNullValue(action.getMdmDistCode()));
				supplierMaster.setSupplierName(action.getMdmDistName());
				StringBuffer sb = new StringBuffer();
				sb.append(checkNullValue(action.getMdmDistAddr1()) + "\n");
				sb.append(checkNullValue(action.getMdmDistAddr3()) + "\n");
				sb.append(checkNullValue(action.getMdmDistAddr3()));
				supplierMaster.setSupplierAddress(sb.toString());
				supplierMaster.setPincode(action.getMdmDistPin());
				Set<String> phoneNo = new HashSet<>();
				if (action.getMdmDistTelOff() != null && action.getMdmDistTelOff() != "") {
					phoneNo.add(checkNullValue(action.getMdmDistTelOff()));
				}
				if (action.getMdmDistTelRes() != null && action.getMdmDistTelRes() != "") {
					phoneNo.add(checkNullValue(action.getMdmDistTelRes()));
				}
				if (action.getMdmDistCellPhone() != null && action.getMdmDistCellPhone() != "") {
					phoneNo.add(checkNullValue(action.getMdmDistCellPhone()));
				}
				supplierMaster.setPhoneNo(phoneNo);
				supplierMaster.setEmailId(checkNullValue(action.getMdmDistEmailId()));
				supplierMaster.setStatus(true);
				supplierMaster.setLocality("Tamil Nadu");
				supplierMaster.setSupplierGstNo(checkNullValue(action.getMDM_gstno()));
				supplierMaster.setSupplierPanNo(checkNullValue(action.getMDM_pan_no()));
				Set<String> contactPerson = new HashSet<>();
				if (action.getMdmContactPerson() != null && action.getMdmContactPerson() != "") {
					contactPerson.add(checkNullValue(action.getMdmContactPerson()));
				}
				if (action.getMdmSalesMan1() != null && action.getMdmSalesMan1() != "") {
					contactPerson.add(checkNullValue(action.getMdmSalesMan1()));
				}
				if (action.getMdmSalesMan2() != null && action.getMdmSalesMan2() != "") {
					contactPerson.add(checkNullValue(action.getMdmSalesMan2()));
				}
				supplierMaster.setContactPerson(contactPerson);
				supplierMaster.setTradeDiscount(checkNullValue(action.getMDM_TRADE_DISC_PER()));
				supplierMaster.setCreatedBy(checkNullValue(action.getMDM_CREATED_BY()));
				supplierMaster.setCreatedTime(action.getMDM_CREATED_DT_TIME());
				supplierMaster.setUpdatedBy(checkNullValue(action.getMDM_UPDATED_BY()));
				supplierMaster.setUpdatedTime(action.getMDM_UPDATED_DT_TIMe());
				supplierMasterRepo.save(supplierMaster);
			});
			syncStatus = true;
			logger.info("Migration of Supplier details from GoFrugal completed");
		}
		return syncStatus;
	}

	@Override
	public boolean migrateCustomerDetails(boolean incrementalSync) {
		boolean syncStatus = false;
		if (incrementalSync) {
			logger.info("Incremental sync yet to be supported");
		} else {
			logger.info("Starting migration of Customer details from GoFrugal");
			customerRepo.deleteAll();
			Iterable<MedCustomerMaster> customerMaster = medCustomerRepo.findAll();
			customerMaster.forEach(customer -> {
				logger.debug("Processing {}", customer.getMCM_CUST_NAME());
				CustomerMaster custMaster = new CustomerMaster();
				custMaster.setCustomerId(customer.getMcm_cust_id());
				custMaster.setCustomerCode(customer.getMCM_CUST_CODE());
				custMaster.setCustomerName(customer.getMCM_CUST_NAME());
				StringBuffer sb = new StringBuffer();
				sb.append(checkNullValue(customer.getMCM_CUST_ADDR1()) + "\n");
				sb.append(checkNullValue(customer.getMCM_CUST_ADDR2()) + "\n");
				sb.append(checkNullValue(customer.getMcm_addr3()) + "\n");
				custMaster.setCustomerAddress(sb.toString());
				custMaster.setPincode(customer.getMCM_CUST_PIN());
				custMaster.setCustomerArea(customer.getMcm_place());
				Set<String> phoneNo = new HashSet<>();
				if (customer.getMCM_CUST_TEL() != null && customer.getMCM_CUST_TEL() != "") {
					phoneNo.add(checkNullValue(customer.getMCM_CUST_TEL()));
				}
				if (customer.getMcm_phone1() != null && customer.getMcm_phone1() != "") {
					phoneNo.add(checkNullValue(customer.getMcm_phone1()));
				}
				if (customer.getMcm_phone2() != null && customer.getMcm_phone2() != "") {
					phoneNo.add(checkNullValue(customer.getMcm_phone2()));
				}
				custMaster.setCustomerPhoneNumber(phoneNo);
				custMaster.setCustomerEmailId(customer.getMcm_email());
				custMaster.setCreditAllowed(customer.getMcm_credit_allowed() == 0 ? false : true);
				custMaster.setCreditBalance(customer.getMCM_CUST_CREDIT_BAL());
				custMaster.setCreditLimit(customer.getMCM_CUST_CREDIT_LIMIT());
				custMaster.setCustomerStatus(customer.getMCM_CUST_STATUS() == "A" ? true : false);
				custMaster.setCustomerDiscount(customer.getMcm_disc_perc());
				custMaster.setCustomerPriceLevel(customer.getMcm_price_level() != 0 ? "Whole Sale" : "None");
				custMaster.setLoyaltyAllowed(customer.getMcm_loyalty_allowed() == 0 ? false : true);
				custMaster.setCreatedBy(customer.getMCM_CREATED_BY());
				custMaster.setCreatedDate(customer.getMCM_CREATED_DT_TIME());
				custMaster.setUpdatedBy(customer.getMCM_UPDATED_BY());
				custMaster.setUpdatedDate(customer.getMCM_UPDATED_DT_TIMe());
				custMaster.setGstCompanyName(customer.getMcm_cust_company_name());
				custMaster.setGstNumber(customer.getMCM_gstno());
				custMaster.setPanNumber(customer.getMCM_pan_no());
				String aadharNumber = checkNullValue(customer.getMCM_aadhar_no()) == "" ? "0"
						: customer.getMCM_aadhar_no();
				custMaster.setAadharNumber(Integer.valueOf(aadharNumber));
				Set<String> contactPerson = new HashSet<>();
				if (customer.getMcm_Contact_name() != null && customer.getMcm_Contact_name() != "") {
					contactPerson.add(checkNullValue(customer.getMcm_Contact_name()));
				}
				custMaster.setContactPerson(contactPerson);
				customerRepo.save(custMaster);
			});
			syncStatus = true;
			logger.info("Migration of Customer details from GoFrugal completed");
		}
		return syncStatus;
	}

	private String checkNullValue(String checkValue) {

		if (checkValue == null || "".equals(checkValue) || ".".equals(checkValue)) {
			checkValue = "";
		}
		return checkValue;
	}

	private Integer checkNullValue(Integer checkValue) {

		if (checkValue == null) {
			checkValue = 0;
		}
		return checkValue;
	}

	private Float checkNullValue(Float checkValue) {

		if (checkValue == null) {
			checkValue = 0F;
		}
		return checkValue;
	}

	@Override
	public boolean migrateProductDetails(boolean incrementalSync) {
		boolean syncStatus = false;
		if (incrementalSync) {
			logger.info("Incremental sync yet to be supported");
		} else {
			logger.info("Starting migration of Product details from GoFrugal");
			productTempRepo.deleteAll();
			Map<Integer, String> taxSlabMap = new HashMap<>();
			Map<Integer, UnicodeModel> unicodeMap = new HashMap<>();
			Map<Integer, Set<String>> eanCodeMap = new HashMap<>();
			Map<Integer, String> categoryMap = new HashMap<>();
			Map<Integer, Map<String, String>> compDistMap = new HashMap<>();
			Map<Integer, List<ProductPrice>> priceDetailMap = new HashMap<>();
			Map<Integer, Map<String, String>> prodRelationMap = new HashMap<>();
			populateTaxSlabMap(taxSlabMap);
			populateUnicodeMap(unicodeMap);
			populateEanCodeMap(eanCodeMap);
			populateCategoryMap(categoryMap);
			populateProductRelation(prodRelationMap);
			populatePriceDetailMap(priceDetailMap, compDistMap);
			Iterable<MedItemHdr> productItemHdr = medItemHdrRepo.findAll();
			Map<Integer, String> duplicateAliasEntryMap = new HashMap<>();
			populateDuplicateAliasEntryMap(duplicateAliasEntryMap);
			productItemHdr.forEach(itemEntry -> {
				ProductMasterTemp prodMaster = new ProductMasterTemp();
				Integer MIH_ITEM_CODE = itemEntry.getMIH_ITEM_CODE();
				prodMaster.setProductCode(MIH_ITEM_CODE);
				prodMaster.setProductName(itemEntry.getMIH_ITEM_NAME());
				prodMaster.setLooseSaleAllowed(itemEntry.getMIH_LOOSE_SALE_ALLOWED().equals("Y") ? true : false);
				prodMaster.setProductCategory1(categoryMap.get(itemEntry.getMIH_CATEGORY_1()));
				prodMaster.setProductCategory2(categoryMap.get(itemEntry.getMIH_CATEGORY_3()));
				prodMaster.setProductCompany(categoryMap.get(itemEntry.getMIH_CATEGORY_2()));
				if (duplicateAliasEntryMap.get(MIH_ITEM_CODE) != null) {
					prodMaster.setProductAlias(duplicateAliasEntryMap.get(MIH_ITEM_CODE).toUpperCase());
				} else {
					prodMaster.setProductAlias(itemEntry.getMIH_ITEM_ALIAS().toUpperCase());
				}
				prodMaster.setDecimalAllowed((itemEntry.getMih_decimal_point() > 0) ? true : false);
				prodMaster.setProductShortName(itemEntry.getMih_item_name_short());
				prodMaster.setLoyaltyAllowed((itemEntry.getMIH_LOYALTY_ALLOWED() == 1) ? true : false);
				prodMaster.setProductStatus("N".equalsIgnoreCase(itemEntry.getMIH_TRADE_CONF()) ? false : true);
				String allowRateEdit = itemEntry.getMIH_ALLOW_RATE_EDIT().trim();
				if (allowRateEdit.equals("D") || allowRateEdit.equals("Y")) {
					prodMaster.setRateEditAllowed(true);
				} else {
					prodMaster.setRateEditAllowed(false);
				}
				String negativeStock = itemEntry.getMIH_ALLOW_NEGATIVE_STOCK().trim();
				if (negativeStock.equals("D") || negativeStock.equals("Y")) {
					prodMaster.setNegStockAllowed(true);
				} else {
					prodMaster.setNegStockAllowed(false);
				}
				Integer gstCode = itemEntry.getMIH_GST_CODE();
				prodMaster.setHsnCode(itemEntry.getMIH_HSN_CODE());
				Double bulkConversion = itemEntry.getMIH_ITEM_REPACK_CONVERTION();
				if (bulkConversion != 0.00) {
					prodMaster.setProductRelation(prodRelationMap.get(MIH_ITEM_CODE).get("RELATION"));
					prodMaster.setProductRelatedWith(prodRelationMap.get(MIH_ITEM_CODE).get("RELATED"));
					prodMaster.setRelationConversion(
							Double.valueOf(prodRelationMap.get(MIH_ITEM_CODE).get("CONVERSION")));
				}
				if (compDistMap.get(MIH_ITEM_CODE) != null) {
					prodMaster.setCompanyCode(compDistMap.get(MIH_ITEM_CODE).get("COMPANY"));
					prodMaster.setSupplierName(compDistMap.get(MIH_ITEM_CODE).get("SUPPLIER"));
				}
				if (MIH_ITEM_CODE == 5189) {
					logger.info(eanCodeMap.get(MIH_ITEM_CODE).toString());
				}
				prodMaster.setProductEANCode(eanCodeMap.get(MIH_ITEM_CODE));
				prodMaster.setGstTaxSlab(taxSlabMap.get(gstCode));
				UnicodeModel unicodeModel = unicodeMap.get(MIH_ITEM_CODE);
				if (unicodeModel != null) {
					prodMaster.setLocalLanguageName(unicodeModel.getLocalLanguageName());
					prodMaster.setBaminiFontName(unicodeModel.getBaminiFontName());
				}
				prodMaster.setProductPrice(priceDetailMap.get(MIH_ITEM_CODE));
				if (MIH_ITEM_CODE == 20502 || MIH_ITEM_CODE == 20512) {
					try {
						logger.info("entry: {}", mapper.writeValueAsString(prodMaster));
					} catch (JsonProcessingException e) {
						e.printStackTrace();
					}
				}
				productTempRepo.save(prodMaster);
			});
			MongoNamespace mongoNamespace = new MongoNamespace(dbName, "product_master");
			RenameCollectionOptions renameCollectionOption = new RenameCollectionOptions().dropTarget(true);
			mongoTemplate.getCollection("product_master_tmp").renameCollection(mongoNamespace, renameCollectionOption);
			logger.info("Migration of product details from GoFrugal completed");
			syncStatus = true;
		}
		return syncStatus;
	}

	private void populateDuplicateAliasEntryMap(Map<Integer, String> duplicateAliasEntryMap) {
		duplicateAliasEntryMap.put(6240, "ABST40");
		duplicateAliasEntryMap.put(17635, "ANFSFW80G");
		duplicateAliasEntryMap.put(5652, "BMR");
		duplicateAliasEntryMap.put(19430, "BTWC25");
		duplicateAliasEntryMap.put(2281, "CFH450G");
		duplicateAliasEntryMap.put(19633, "CNP1K");
		duplicateAliasEntryMap.put(5218, "FIGF250G");
		duplicateAliasEntryMap.put(5557, "GNBD");
		duplicateAliasEntryMap.put(19575, "HBD25G");
		duplicateAliasEntryMap.put(6170, "HFWL50ML");
		duplicateAliasEntryMap.put(19577, "IDP25G");
		duplicateAliasEntryMap.put(19169, "KRA40");
		duplicateAliasEntryMap.put(18087, "MMYG30");
		duplicateAliasEntryMap.put(19993, "MN50G");
		duplicateAliasEntryMap.put(19553, "NPMGF45");
		duplicateAliasEntryMap.put(5818, "R2MDS");
		duplicateAliasEntryMap.put(8270, "RPR1K85");
		duplicateAliasEntryMap.put(5527, "SBG");
		duplicateAliasEntryMap.put(19155, "BCDDPC75");
		duplicateAliasEntryMap.put(4676, "SMM50R");
		duplicateAliasEntryMap.put(19523, "TMR1K");
		duplicateAliasEntryMap.put(1226, "TM200ML");
		duplicateAliasEntryMap.put(13300, "TWTGSB");
		duplicateAliasEntryMap.put(1557, "VP100GM");
		duplicateAliasEntryMap.put(25791, "SSR99");
		duplicateAliasEntryMap.put(25858, "RCHNR");
	}

	private void populateProductRelation(Map<Integer, Map<String, String>> prodRelationMap) {
		List<MedItemHdr> itemHdrList = medItemHdrRepo.findByMIH_ITEM_REPACK_CONVERTION(0.00);
		itemHdrList.forEach(product -> {
			Map<String, String> prodRelDtlMap = new HashMap<>();
			String relation = "";
			switch (product.getMIH_PREPARED()) {
			case "B":
				relation = "Bulk";
				break;

			case "H":
				relation = "Child";
				break;

			case "P":
				relation = "Parent";
				break;

			case "R":
				relation = "Repackage";
				break;
			}

			if (product.getMih_item_bulk_code() == 0.00) {
				prodRelDtlMap.put("RELATION", relation);
				prodRelDtlMap.put("RELATED", null);
				prodRelDtlMap.put("CONVERSION", product.getMIH_ITEM_REPACK_CONVERTION().toString());
			} else {
				prodRelDtlMap.put("RELATION", relation);
				Optional<MedItemHdr> itemHdr = medItemHdrRepo.findById(product.getMih_item_bulk_code().intValue());
				prodRelDtlMap.put("RELATED", itemHdr.get().getMIH_ITEM_NAME());
				prodRelDtlMap.put("CONVERSION", product.getMIH_ITEM_REPACK_CONVERTION().toString());
			}
			prodRelationMap.put(product.getMIH_ITEM_CODE(), prodRelDtlMap);
		});
	}

	private void populateDistributorMap(Map<Integer, String> supplierMap) {
		Iterable<MedDistrubutorMaster> distributorMaster = medDistrubutorRepo.findAll();
		distributorMaster.forEach(supplier -> {
			supplierMap.put(supplier.getMdmDistCode(), supplier.getMdmDistName());
		});
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void populatePriceLevelMap(Map<Integer, List<ProductPriceLevel>> priceLevelMap,
			Map<Integer, String> priceLvlNameMap) {
		String priceLevelQuery = "SELECT MPLI_ITEM_CODE, MPLI_PRICE_LEVEL_CODE, MPLI_RATE, MPLI_SELLING FROM MED_ITEM_DTL A, MED_PRICE_LEVEL_ITEM PRICE "
				+ "WHERE MID_MRP = MPLI_SELLING AND MID_MRC_DT = (SELECT MAX(MID_MRC_DT) FROM MED_ITEM_DTL B WHERE A.MID_ITEM_CODE = B.MID_ITEM_CODE) "
				+ "AND MPLI_ITEM_CODE = MID_ITEM_CODE;";
		List<MedPriceLevelItem> medPriceLevelList = (List<MedPriceLevelItem>) jdbcTemplate.query(priceLevelQuery,
				new BeanPropertyRowMapper(MedPriceLevelItem.class));
		medPriceLevelList.forEach(priceLvlDtl -> {
			List<ProductPriceLevel> priceLevelList = new ArrayList<>();
			ProductPriceLevel priceLevel = new ProductPriceLevel();
			priceLevel.setPriceCategory(priceLvlNameMap.get(priceLvlDtl.getMpli_price_level_code()));
			priceLevel.setSellingPrice(priceLvlDtl.getMpli_rate());
			priceLevelList.add(priceLevel);
			priceLevelMap.put(priceLvlDtl.getMpli_item_code(), priceLevelList);
		});
	}

	private void populatePriceLvlNameMap(Map<Integer, String> priceLvlNameMap) {
		Iterable<MedPriceLevel> priceLevelMaster = priceLevelRepo.findAll();
		priceLevelMaster.forEach(priceLvlName -> {
			priceLvlNameMap.put(priceLvlName.getMpl_code(), priceLvlName.getMpl_name());
		});
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void populatePriceDetailMap(Map<Integer, List<ProductPrice>> priceDetailMap,
			Map<Integer, Map<String, String>> compDistMap) {
		Map<Integer, String> supplierMap = new HashMap<>();
		Map<Integer, List<ProductPriceLevel>> priceLevelMap = new HashMap<>();
		Map<Integer, String> priceLvlNameMap = new HashMap<>();
		populateDistributorMap(supplierMap);
		populatePriceLvlNameMap(priceLvlNameMap);
		populatePriceLevelMap(priceLevelMap, priceLvlNameMap);
		String itemDetailQuery = "SELECT MID_ITEM_CODE, MID_BAL_STOCK, MID_MRP, MID_PUR_PRICE, MID_SALE_TAX, MID_MAX_RATE, "
				+ "MID_DIST_CODE, MID_COMPID FROM MED_ITEM_DTL A WHERE MID_ROW_ID = (SELECT MAX(MID_ROW_ID) FROM "
				+ "MED_ITEM_DTL B WHERE A.MID_ITEM_CODE = B.MID_ITEM_CODE)";
		List<MedItemDtl> MedItemDtlList = (List<MedItemDtl>) jdbcTemplate.query(itemDetailQuery,
				new BeanPropertyRowMapper(MedItemDtl.class));
		MedItemDtlList.forEach(itemDtl -> {
			List<ProductPrice> prodPriceList = new ArrayList<>();
			Map<String, String> compDistValueMap = new HashMap<>();
			ProductPrice prodPrice = new ProductPrice();
			prodPrice.setAvailableStock(itemDtl.getMID_BAL_STOCK());
			prodPrice.setSellingPrice(itemDtl.getMID_MRP());
			prodPrice.setPurchasePrice(itemDtl.getMID_PUR_PRICE());
			prodPrice.setMaxRetailPrice(itemDtl.getMid_max_rate());
			if (priceLevelMap.get(itemDtl.getMID_ITEM_CODE()) != null) {
				prodPrice.setPriceLevel(priceLevelMap.get(itemDtl.getMID_ITEM_CODE()));
			}
			prodPriceList.add(prodPrice);
			priceDetailMap.put(itemDtl.getMID_ITEM_CODE(), prodPriceList);
			compDistValueMap.put("COMPANY", itemDtl.getMid_compid() == 1 ? "ALL" : "SAKTHI STORES");
			compDistValueMap.put("SUPPLIER", supplierMap.get(itemDtl.getMID_DIST_CODE()));
			compDistMap.put(itemDtl.getMID_ITEM_CODE(), compDistValueMap);
		});
	}

	private void populateCategoryMap(Map<Integer, String> categoryMap) {
		Iterable<MedCategoryDetail> categoryDetailList = categoryRepo.findAll();
		categoryDetailList.forEach(category -> categoryMap.put(category.getMCD_CAT_CODE(), category.getMCD_CAT_NAME()));
	}

	private void populateEanCodeMap(Map<Integer, Set<String>> eanCodeMap) {
		Iterable<MedItemEanCode> eanCodeList = medItemEanCodeRepo.findAll();
		eanCodeRepo.deleteAll();
		eanCodeList.forEach(eanCodeEntry -> {
			Integer itemCode = eanCodeEntry.getMie_Item_code();
			ProductEanCode eanCode = new ProductEanCode(eanCodeEntry.getMie_Eancode(), itemCode);
			eanCodeRepo.save(eanCode);
			Set<String> eanEntrySet = eanCodeMap.get(itemCode);

			if (eanEntrySet != null && !eanEntrySet.isEmpty()) {
				eanEntrySet.add(eanCodeEntry.getMie_Eancode());
			} else {
				eanEntrySet = new HashSet<>();
				eanEntrySet.add(eanCodeEntry.getMie_Eancode());
			}
			eanCodeMap.put(itemCode, eanEntrySet);
		});
	}

	private void populateUnicodeMap(Map<Integer, UnicodeModel> unicodeMap) {
		Map<String, String> map = MigrationUtil.getMap();
		Iterable<MedUnicodeItemHdr> unicodeHdrList = unicodeRepo.findAll();
		unicodeHdrList.forEach(unicode -> {
			String baminiFontName = unicode.getMUIH_UNICODE_ITEM_NAME();
			unicodeMap.put(unicode.getMUIH_ITEM_CODE(),
					new UnicodeModel(getUnicodeText(baminiFontName, map), baminiFontName));
		});
	}

	private String getUnicodeText(String text, Map<String, String> map) {
		String unicodeText = text;
		for (String key : map.keySet()) {
			if (unicodeText != null && unicodeText.contains(key)) {
				unicodeText = unicodeText.replace(key, map.get(key));
			}
		}
		logger.debug("unicodeText {}", unicodeText);
		return unicodeText;
	}

	private void populateTaxSlabMap(Map<Integer, String> taxSlabMap) {
		Iterable<MedTaxMaster> taxMasterList = medTaxRepo.findAll();
		taxMasterList.forEach(taxEntry -> taxSlabMap.put(taxEntry.getMtm_tax_code(),
				taxEntry.getMtm_tax_desc().replace("GST ", "").replace(" Tax", "")));
	}

	@Override
	public boolean migratePurchaseHistoryDetails(boolean incrementalSync) {
		logger.info("Yet to be implemented");
		return false;
	}

	@Override
	public boolean migrateCustomerLoyalty(boolean incrementalSync) {
		logger.info("Yet to be implemented");
		return false;
	}

	public Map<String, ProductDetails> exportProductDetail() {
		Map<String, Integer> eanCodeItemMap = new HashMap<>();
		Map<Integer, Set<String>> itemEanCodeMap = new HashMap<>();
		Map<Integer, ProductPrice> priceDetailMap = new HashMap<>();
		Map<String, ProductDetails> prodMasterMap = new HashMap<>();
		populateEanCodeMap(eanCodeItemMap, itemEanCodeMap);
		populatePriceDetailMap(priceDetailMap);
		Iterable<MedItemHdr> productItemHdr = medItemHdrRepo.findAll();
		productItemHdr.forEach(itemEntry -> {
			ProductDetails prodMaster = new ProductDetails();
			Integer MIH_ITEM_CODE = itemEntry.getMIH_ITEM_CODE();
			logger.info("Started processing {}", MIH_ITEM_CODE);
			ProductPrice prodPrice = priceDetailMap.get(MIH_ITEM_CODE);
			logger.info("prodPrice processing {}", prodPrice);
			if (prodPrice != null) {
				prodMaster.setProductCode(MIH_ITEM_CODE);
				prodMaster.setProductName(itemEntry.getMIH_ITEM_NAME());
				prodMaster.setProductAlias(itemEntry.getMIH_ITEM_ALIAS().toUpperCase());
				prodMaster.setProductStatus(itemEntry.getMIH_TRADE_CONF() == "N" ? false : true);
				prodMaster.setSellingPrice(prodPrice.getSellingPrice());
				prodMaster.setMaxRetailPrice(prodPrice.getMaxRetailPrice());
				prodMaster.setPurchasePrice(prodPrice.getPurchasePrice());
				logger.info("MIH_ITEM_CODE {}", MIH_ITEM_CODE);
				if (MIH_ITEM_CODE == 353) {
					logger.info("itemEanCodeMap.get(MIH_ITEM_CODE) {}", itemEanCodeMap.get(MIH_ITEM_CODE));
				}
				logger.info("MIH_ITEM_CODE {}", MIH_ITEM_CODE);
				if (itemEanCodeMap.get(MIH_ITEM_CODE) != null) {
					itemEanCodeMap.get(MIH_ITEM_CODE).forEach(eanCode -> {
						prodMasterMap.put(eanCode, prodMaster);
					});
				}
			}
		});
		return prodMasterMap;
	}

	private void populateEanCodeMap(Map<String, Integer> eanCodeItemMap, Map<Integer, Set<String>> itemEanCodeMap) {
		Iterable<MedItemEanCode> eanCodeList = medItemEanCodeRepo.findAll();
		eanCodeList.forEach(eanCodeEntry -> {
			Integer itemCode = eanCodeEntry.getMie_Item_code();
			Set<String> eanEntrySet = itemEanCodeMap.get(itemCode);
			eanCodeItemMap.put(eanCodeEntry.getMie_Eancode(), itemCode);
			if (eanEntrySet != null && !eanEntrySet.isEmpty()) {
				eanEntrySet.add(eanCodeEntry.getMie_Eancode());
			} else {
				eanEntrySet = new HashSet<>();
				eanEntrySet.add(eanCodeEntry.getMie_Eancode());
			}
			itemEanCodeMap.put(itemCode, eanEntrySet);
		});
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void populatePriceDetailMap(Map<Integer, ProductPrice> priceDetailMap) {
		String itemDetailQuery = "SELECT MID_ITEM_CODE, MID_BAL_STOCK, MID_MRP, MID_PUR_PRICE, MID_SALE_TAX, MID_MAX_RATE, "
				+ "MID_DIST_CODE, MID_COMPID FROM MED_ITEM_DTL A WHERE MID_ROW_ID = (SELECT MAX(MID_ROW_ID) FROM "
				+ "MED_ITEM_DTL B WHERE A.MID_ITEM_CODE = B.MID_ITEM_CODE)";
		List<MedItemDtl> MedItemDtlList = (List<MedItemDtl>) jdbcTemplate.query(itemDetailQuery,
				new BeanPropertyRowMapper(MedItemDtl.class));
		MedItemDtlList.forEach(itemDtl -> {
			ProductPrice prodPrice = new ProductPrice();
			prodPrice.setSellingPrice(itemDtl.getMID_MRP());
			prodPrice.setPurchasePrice(itemDtl.getMID_PUR_PRICE());
			prodPrice.setMaxRetailPrice(itemDtl.getMid_max_rate());
			priceDetailMap.put(itemDtl.getMID_ITEM_CODE(), prodPrice);
		});
	}

	@SuppressWarnings("deprecation")
	public Set<EancodeInputModel> readInputExcelFile(InputStream excelStream) {
		Set<EancodeInputModel> eanCodeInputSet = new HashSet<>();
		try {
			Workbook workBook = WorkbookFactory.create(excelStream);
			Sheet sheet = workBook.getSheetAt(0);
			Iterator<Row> rowIterator = sheet.iterator();
			while (rowIterator.hasNext()) {
				EancodeInputModel eanCodeInput = new EancodeInputModel();
				Row row = rowIterator.next();
				if (row.getRowNum() == 0) {
					continue;
				}
				Iterator<Cell> cellIterator = row.cellIterator();
				while (cellIterator.hasNext()) {
					Cell eancodeCell = cellIterator.next();
					eancodeCell.setCellType(CellType.STRING);
					eanCodeInput.setEanCode(eancodeCell.getStringCellValue());
					Cell mrpCell = cellIterator.next();
					eanCodeInput.setMaxRetailPrice(new BigDecimal(mrpCell.getNumericCellValue()));
				}
				eanCodeInputSet.add(eanCodeInput);
			}
			logger.info("eanCodeInputList size: {}", eanCodeInputSet.size());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return eanCodeInputSet;
	}

	public Set<ProductDetails> parseInputEancode(Set<EancodeInputModel> eanCodeInputSet,
			Map<String, ProductDetails> prodDetailMap) {
		Set<ProductDetails> prodDetailSet = new HashSet<>();
		eanCodeInputSet.forEach(eanCodeModel -> {
			String eancode = eanCodeModel.getEanCode();
			logger.info("eancode: {}", eancode);
			ProductDetails prodDetail = prodDetailMap.get(eancode);
			if (prodDetail != null) {
				try {
					prodDetail.setProductEANCode(eancode != null ? eancode : null);
					prodDetailSet.add(prodDetail);
					logger.info(mapper.writeValueAsString(prodDetail));
				} catch (JsonProcessingException e) {
					e.printStackTrace();
				}
			}
		});
		return prodDetailSet;
	}

	private XSSFCellStyle createCellStyle(XSSFWorkbook workBook, boolean isHeader) {
		XSSFColor color = new XSSFColor(Color.WHITE, null);
		XSSFCellStyle cellStyle = workBook.createCellStyle();
		if (isHeader) {
			color = new XSSFColor(Color.LIGHT_GRAY, null);
			cellStyle.setAlignment(HorizontalAlignment.CENTER);
		} else {
			cellStyle.setAlignment(HorizontalAlignment.LEFT);
		}
		cellStyle.setFillForegroundColor(color);
		cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		cellStyle.setBorderBottom(BorderStyle.THIN);
		cellStyle.setBorderLeft(BorderStyle.THIN);
		cellStyle.setBorderRight(BorderStyle.THIN);
		cellStyle.setBorderTop(BorderStyle.THIN);
		return cellStyle;
	}

	private Integer populateRowValue(XSSFCellStyle rowStyle, ProductDetails prodDetails, Sheet sheet, Integer rowNum) {
		int columNum = 0;
		Row row = sheet.createRow(rowNum++);
		for (int i = 0; i < headerDetails.size(); i++) {
			Cell cell = row.createCell(columNum++);
			switch (headerDetails.get(i)) {
			case "CODE":
				cell.setCellValue(prodDetails == null ? "Product Code" : prodDetails.getProductCode().toString());
				cell.setCellStyle(rowStyle);
				break;

			case "ALIAS":
				cell.setCellValue(prodDetails == null ? "Product Alias" : prodDetails.getProductAlias());
				cell.setCellStyle(rowStyle);
				break;

			case "NAME":
				cell.setCellValue(prodDetails == null ? "Product Name" : prodDetails.getProductName());
				cell.setCellStyle(rowStyle);
				break;

			case "EANCODE":
				cell.setCellValue(prodDetails == null ? "EAN Code" : prodDetails.getProductEANCode());
				cell.setCellStyle(rowStyle);
				break;

			case "SELLING":
				cell.setCellValue(prodDetails == null ? "Selling Price" : prodDetails.getSellingPrice().toString());
				cell.setCellStyle(rowStyle);
				break;

			case "MRP":
				cell.setCellValue(prodDetails == null ? "MRP" : prodDetails.getMaxRetailPrice().toString());
				cell.setCellStyle(rowStyle);
				break;

			case "NETCOST":
				cell.setCellValue(prodDetails == null ? "Net Cost" : prodDetails.getPurchasePrice().toString());
				cell.setCellStyle(rowStyle);
				break;
			}
		}
		return rowNum;
	}

	public ByteArrayOutputStream exportProductDetails(Set<ProductDetails> prodDetailsSet) {
		ByteArrayOutputStream byteArrayStream = new ByteArrayOutputStream();
		ProductDetails[] prodDetailArray = prodDetailsSet.toArray(new ProductDetails[prodDetailsSet.size()]);
		XSSFWorkbook workBook = new XSSFWorkbook();
		try {
			Integer rowNum = 0;
			XSSFCellStyle headerStyle = createCellStyle(workBook, true);
			Sheet sheet = workBook.createSheet("Product Details");
			rowNum = populateRowValue(headerStyle, null, sheet, rowNum);
			for (int i = 0; i < prodDetailsSet.size(); i++) {
				XSSFCellStyle rowCellStyle = createCellStyle(workBook, false);
				rowNum = populateRowValue(rowCellStyle, prodDetailArray[i], sheet, rowNum);
			}
			sheet.createFreezePane(0, 1);
			for (int i = 0; i < headerDetails.size(); i++) {
				sheet.autoSizeColumn(i);
			}
			sheet.setAutoFilter(
					new CellRangeAddress(0, sheet.getLastRowNum(), 0, sheet.getRow(0).getLastCellNum() - 1));
			workBook.write(byteArrayStream);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				workBook.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return byteArrayStream;
	}

	@Override
	public Set<ProductDetails> getProductDetails(InputStream excelFileStream) {
		Map<String, ProductDetails> prodDetailsMap = this.exportProductDetail();
		Set<EancodeInputModel> activeEanCodeSet = this.readInputExcelFile(excelFileStream);
		Set<ProductDetails> prodDetailsList = this.parseInputEancode(activeEanCodeSet, prodDetailsMap);
		return prodDetailsList;
	}

	@Override
	public ByteArrayOutputStream exportProductDetailsAsStream(Set<ProductDetails> prodDetailsSet) {
		return this.exportProductDetails(prodDetailsSet);
	}

	@Override
	public List<String> getCounterName() {
		List<String> counterList = new ArrayList<>();
		Iterable<CounterNameDetails> counterIterate = counterRepo.findAll();
		counterIterate.forEach(counter -> counterList.add(counter.getCn_ctr_code()));
		return counterList;
	}

	private boolean chooseWhereField(Integer billNo) {
		boolean type2Inv = false;
		MedBillHdr medBillHdr = medBillHdrRepo.findByMbhBillNo(billNo);
		if (medBillHdr.getMbh_InvType() == 2) {
			type2Inv = true;
		} else if (medBillHdr.getMbh_InvType() == 1 && medBillHdr.getMbh_split_bno_pk() == 0) {
			type2Inv = true;
		} else if (!medBillHdr.getMBH_BILL_TYPE().equalsIgnoreCase("CA")) {
			type2Inv = true;
		}
		return type2Inv;
	}

	@Override
	public List<BillInfo> getBillInfo(BillSearchCriteria searchCriteria) {
		List<BillInfo> billInfoList = new ArrayList<>();
		List<BillInfo> bill2InfoList = new ArrayList<>();
		Map<Integer, BillInfo> billInfoMap = new HashMap<>();
		String sqlQuery = "";
		String selectQuery = "";
		String groupByQuery = "";
		String notSplitQuery = "";
		if (searchCriteria.isQuotationBill()) {
			sqlQuery = "SELECT MBH_BILL_NO, MED_BILLQUOT_HDR.TS MBH_BILL_TIME, MBH_BILL_CUST_CODE, MBH_BILL_CUST_NAME, MBH_BILL_CUST_ADDR, MBH_DISC_AMOUNT, "
					+ "MBH_BILL_AMOUNT, MBH_BILL_TYPE, MBH_BILL_AMT_TEND, MBH_TOT_ITEMS, MBH_PROFIT, MBH_PBILL_NO, MBH_LOG_ID, MBH_BILL_CUST_ADDR1, "
					+ "MBH_VAT_AMT, MBH_CASH_AMT, MBH_COUPON_AMT, MBH_CARD_AMT, 0 AS MBH_WALLET_AMT, MBH_COUNTER_CODE, MBH_DELIV_BILL, MBH_PRICE_LEVEL, MBH_MANUAL, "
					+ "'' MBH_PLACE, '' MBH_CITY, '' MBH_PHONE1, '' MBH_PHONE2, ''  MBH_PHONE3, MBH_SGST_AMT, MBH_CGST_AMT, MBH_IGST_AMT, "
					+ "loyaltyPoints LOYALTY_POINTS, mld_points REDEEMED_POINTS, MCM_GSTNO, NULL MBH_SPLIT_BNO_PK, NULL MBH_INVTYPE, MED_BILLQUOT_HDR.TS MODIFIED_TIME FROM MED_BILLQUOT_HDR "
					+ "LEFT JOIN VU_LOYALTY_POINTS ON MBH_BILL_CUST_CODE = customerCode "
					+ "LEFT JOIN MED_CUSTOMER_MAST ON MBH_BILL_CUST_CODE = MCM_CUST_CODE "
					+ "LEFT JOIN med_loyalty_dtl ON mld_cust_code = MBH_BILL_CUST_CODE AND mld_pbill_no = MBH_PBILL_NO AND mld_tran_type = 'REDEEM' "
					+ "WHERE MBH_BILL_NO > 0 ";
		} else {
			selectQuery = "SELECT BILL_DETAIL.MBH_BILL_NO, HDR.MBH_BILL_TIME, MBH_BILL_CUST_CODE, MBH_BILL_CUST_NAME, MBH_BILL_CUST_ADDR, "
					+ "BILL_DETAIL.MBH_DISC_AMOUNT, BILL_DETAIL.MBH_BILL_AMOUNT, MBH_BILL_TYPE, BILL_DETAIL.MBH_BILL_AMT_TEND, "
					+ "BILL_DETAIL.MBH_TOT_ITEMS, BILL_DETAIL.MBH_PROFIT, MBH_PBILL_NO, MBH_LOG_ID, MBH_BILL_CUST_ADDR1, BILL_DETAIL.MBH_VAT_AMT, "
					+ "BILL_DETAIL.MBH_CASH_AMT, BILL_DETAIL.MBH_COUPON_AMT, BILL_DETAIL.MBH_CARD_AMT, BILL_DETAIL.MBH_WALLET_AMT, MBH_COUNTER_CODE, MBH_DELIV_BILL, "
					+ "MBH_PRICE_LEVEL, MBH_MANUAL, MBH_PLACE, MBH_CITY, MBH_PHONE1, MBH_PHONE2, MBH_PHONE3, BILL_DETAIL.MBH_SGST_AMT, "
					+ "BILL_DETAIL.MBH_CGST_AMT, BILL_DETAIL.MBH_IGST_AMT, loyaltyPoints LOYALTY_POINTS, mld_points REDEEMED_POINTS, "
					+ "MCM_GSTNO, MBH_SPLIT_BNO_PK, MBH_INVTYPE, HDR.TS MODIFIED_TIME FROM ((%1$s) UNION (%2$s)) BILL_DETAIL "
					+ "LEFT JOIN MED_BILL_HDR HDR ON HDR.MBH_BILL_NO = BILL_DETAIL.MBH_BILL_NO "
					+ "LEFT JOIN VU_LOYALTY_POINTS ON MBH_BILL_CUST_CODE = customerCode "
					+ "LEFT JOIN MED_CUSTOMER_MAST ON MBH_BILL_CUST_CODE = MCM_CUST_CODE "
					+ "LEFT JOIN med_loyalty_dtl ON mld_cust_code = MBH_BILL_CUST_CODE AND mld_pbill_no = MBH_PBILL_NO AND mld_tran_type = 'REDEEM'";

			groupByQuery = "SELECT MBH_SPLIT_BNO_PK MBH_BILL_NO, SUM(MBH_DISC_AMOUNT) MBH_DISC_AMOUNT, SUM(MBH_BILL_AMOUNT) MBH_BILL_AMOUNT, "
					+ "SUM(MBH_BILL_AMT_TEND) MBH_BILL_AMT_TEND, SUM(MBH_TOT_ITEMS) MBH_TOT_ITEMS, SUM(MBH_PROFIT) MBH_PROFIT, "
					+ "SUM(MBH_VAT_AMT) MBH_VAT_AMT, SUM(MBH_CASH_AMT) MBH_CASH_AMT, SUM(MBH_COUPON_AMT) MBH_COUPON_AMT, "
					+ "SUM(MBH_CARD_AMT) MBH_CARD_AMT, SUM(MBH_WALLET_AMT) MBH_WALLET_AMT, SUM(MBH_SGST_AMT) MBH_SGST_AMT, SUM(MBH_CGST_AMT) MBH_CGST_AMT, "
					+ "SUM(MBH_IGST_AMT) MBH_IGST_AMT FROM MED_BILL_HDR WHERE MBH_INVTYPE IN (1,2) AND MBH_SPLIT_BNO_PK != 0 ";

			notSplitQuery = "SELECT MBH_BILL_NO, MBH_DISC_AMOUNT, MBH_BILL_AMOUNT, MBH_BILL_AMT_TEND, MBH_TOT_ITEMS, MBH_PROFIT, MBH_VAT_AMT, "
					+ "MBH_CASH_AMT, MBH_COUPON_AMT, MBH_CARD_AMT, MBH_WALLET_AMT, MBH_SGST_AMT, MBH_CGST_AMT, MBH_IGST_AMT FROM MED_BILL_HDR WHERE MBH_BILL_NO > 0 "
					+ "AND MBH_INVTYPE IN (1,2) AND MBH_SPLIT_BNO_PK = 0  ";

		}

		StringBuffer stringBuff = new StringBuffer();
		StringBuffer groupByQueryBuff = new StringBuffer();
		StringBuffer notSplitQueryBuff = new StringBuffer();

		if (searchCriteria.isQuotationBill()) {
			stringBuff.append(sqlQuery);
		} else {
			stringBuff.append(selectQuery);
			groupByQueryBuff.append(groupByQuery);
			notSplitQueryBuff.append(notSplitQuery);
		}

		if (searchCriteria.getBillStartDate() != null) {
			Date startDate = stringToDateFormatted(searchCriteria.getBillStartDate().toString());
			Date endDate = stringToDateFormatted(searchCriteria.getBillEndDate().toString());
			String dateWhereClause = " AND MBH_BILL_DATE BETWEEN '" + new Timestamp(startDate.getTime()) + "' AND '"
					+ new Timestamp(endDate.getTime()) + "'";
			if (searchCriteria.isQuotationBill()) {
				stringBuff.append(dateWhereClause);
			} else {
				groupByQueryBuff.append(dateWhereClause);
				notSplitQueryBuff.append(dateWhereClause);
			}
		}
		if (searchCriteria.getBillStartAmt() != null) {
			String billAmtWhereClause = "";
			if (searchCriteria.getBillEndAmt() != null) {
				billAmtWhereClause = " AND MBH_BILL_AMOUNT > " + searchCriteria.getBillStartAmt()
						+ " AND MBH_BILL_AMOUNT < " + searchCriteria.getBillEndAmt();
			} else {
				billAmtWhereClause = " AND MBH_BILL_AMOUNT >= " + searchCriteria.getBillStartAmt();
			}
			if (searchCriteria.isQuotationBill()) {
				stringBuff.append(billAmtWhereClause);
			}
		}
		if (searchCriteria.getBillNumber() != null) {
			String billNoWhereClause = "";
			if (searchCriteria.isPBillSearch()) {
				billNoWhereClause = " AND MBH_PBILL_NO = " + searchCriteria.getBillNumber();
			} else {
				if (chooseWhereField(searchCriteria.getBillNumber())) {
					billNoWhereClause = " AND MBH_BILL_NO = " + searchCriteria.getBillNumber();
				} else {
					billNoWhereClause = " AND MBH_SPLIT_BNO_PK = " + searchCriteria.getBillNumber();
				}
			}
			if (searchCriteria.isQuotationBill()) {
				stringBuff.append(billNoWhereClause);
			}
		}
		if (searchCriteria.getCounterName() != null && !"".equals(searchCriteria.getCounterName())) {
			String counterWhereClause = " AND MBH_COUNTER_CODE = '" + searchCriteria.getCounterName() + "'";
			if (searchCriteria.isQuotationBill()) {
				stringBuff.append(counterWhereClause);
			} else {
				groupByQueryBuff.append(counterWhereClause);
				notSplitQueryBuff.append(counterWhereClause);
			}
		}
		if (searchCriteria.getCustomerName() != null && !"".equals(searchCriteria.getCustomerName())) {
			String customerWhereClause = "";
			if (searchCriteria.getCustomerName().replaceAll("[0-9]", "").equalsIgnoreCase("")) {
				BigInteger customerData = new BigInteger(searchCriteria.getCustomerName());
				customerWhereClause = " AND (MBH_BILL_CUST_CODE = " + customerData + " OR MBH_PHONE1 = '" + customerData
						+ "' OR MBH_PHONE2 = '" + customerData + "') ";
			} else {
				customerWhereClause = " AND LOWER(MBH_BILL_CUST_NAME) LIKE LOWER('%"
						+ searchCriteria.getCustomerName().replace(" ", "%") + "%') ";
			}
			if (searchCriteria.isQuotationBill()) {
				stringBuff.append(customerWhereClause);
			} else {
				groupByQueryBuff.append(customerWhereClause);
				notSplitQueryBuff.append(customerWhereClause);
			}
		}
		if (searchCriteria.isDeliveryBill()) {
			String deliveryWhereClause = " AND MBH_DELIV_BILL = 1 AND MBH_BILL_DATE >= DATEADD(DAY,-" + syncFrequency
					+ ", GETDATE())";
			if (searchCriteria.isQuotationBill()) {
				stringBuff.append(deliveryWhereClause);
			} else {
				groupByQueryBuff.append(deliveryWhereClause);
				notSplitQueryBuff.append(deliveryWhereClause);
			}
		}
		if (searchCriteria.isQuotationBill()) {
			stringBuff.append(" ORDER BY MBH_PBILL_NO, MBH_COUNTER_CODE DESC");
		} else {
			groupByQueryBuff.append(" GROUP BY MBH_SPLIT_BNO_PK ");
			String finalQuery = String.format(stringBuff.toString(), groupByQueryBuff.toString(),
					notSplitQueryBuff.toString());
			stringBuff = new StringBuffer(finalQuery);
			String billAmtWhereClause = "";
			if (searchCriteria.getBillStartAmt() != null) {
				if (searchCriteria.getBillEndAmt() != null) {
					billAmtWhereClause = " BILL_DETAIL.MBH_BILL_AMOUNT > " + searchCriteria.getBillStartAmt()
							+ " AND BILL_DETAIL.MBH_BILL_AMOUNT < " + searchCriteria.getBillEndAmt();
				} else {
					billAmtWhereClause = " BILL_DETAIL.MBH_BILL_AMOUNT >= " + searchCriteria.getBillStartAmt();
				}
			}
			if (searchCriteria.getBillNumber() != null) {
				if (searchCriteria.isPBillSearch()) {
					stringBuff.append(" WHERE MBH_PBILL_NO = " + searchCriteria.getBillNumber());
				} else {
					stringBuff.append(" WHERE BILL_DETAIL.MBH_BILL_NO = " + searchCriteria.getBillNumber());
				}
				if (searchCriteria.getBillStartAmt() != null) {
					stringBuff.append(" AND " + billAmtWhereClause);
				}
			} else {
				if (searchCriteria.getBillStartAmt() != null) {
					stringBuff.append(" WHERE " + billAmtWhereClause);
				}

			}
		}
		logger.info("Query to get bill details: {}", stringBuff.toString());
		billInfoList = jdbcTemplate.query(stringBuff.toString(),
				(resultSet, rowNum) -> new BillInfo(resultSet.getInt("MBH_BILL_NO"),
						resultSet.getTimestamp("MBH_BILL_TIME"), resultSet.getInt("MBH_BILL_CUST_CODE"),
						resultSet.getString("MBH_BILL_CUST_NAME"), resultSet.getString("MBH_BILL_CUST_ADDR"),
						resultSet.getDouble("MBH_DISC_AMOUNT"), resultSet.getDouble("MBH_BILL_AMOUNT"),
						resultSet.getString("MBH_BILL_TYPE"), resultSet.getDouble("MBH_BILL_AMT_TEND"),
						resultSet.getDouble("MBH_TOT_ITEMS"), resultSet.getDouble("MBH_PROFIT"),
						resultSet.getInt("MBH_PBILL_NO"), resultSet.getString("MBH_LOG_ID"),
						resultSet.getString("MBH_BILL_CUST_ADDR1"), resultSet.getDouble("MBH_VAT_AMT"),
						resultSet.getDouble("MBH_CASH_AMT"), resultSet.getDouble("MBH_COUPON_AMT"),
						resultSet.getDouble("MBH_CARD_AMT"), resultSet.getDouble("MBH_WALLET_AMT"),
						resultSet.getString("MBH_COUNTER_CODE"), resultSet.getInt("MBH_DELIV_BILL") > 0 ? true : false,
						resultSet.getInt("MBH_PRICE_LEVEL"), resultSet.getString("MBH_MANUAL"),
						resultSet.getString("MBH_PLACE"), resultSet.getString("MBH_CITY"),
						resultSet.getString("MBH_PHONE1"), resultSet.getString("MBH_PHONE2"),
						resultSet.getString("MBH_PHONE3"), resultSet.getDouble("MBH_SGST_AMT"),
						resultSet.getDouble("MBH_CGST_AMT"), resultSet.getDouble("MBH_IGST_AMT"),
						resultSet.getDouble("LOYALTY_POINTS"), resultSet.getDouble("REDEEMED_POINTS"),
						resultSet.getString("MCM_GSTNO"), resultSet.getInt("MBH_SPLIT_BNO_PK"),
						resultSet.getInt("MBH_INVTYPE"), resultSet.getTimestamp("MODIFIED_TIME")));
		if (searchCriteria.getBillNumber() != null) {
			if (!searchCriteria.isPBillSearch() && billInfoList.size() > 1) {
				Collections.sort(billInfoList, new Comparator<BillInfo>() {
					@Override
					public int compare(BillInfo o1, BillInfo o2) {
						return o1.getInvType() - o2.getInvType();
					}
				});
				BillInfo bill2Info = new BillInfo();
				billInfoList.forEach(billInfo -> {
					if (billInfo.getInvType() == 1) {
						bill2Info.setBillNo(billInfo.getBillNo());
						bill2Info.setBillDate(billInfo.getBillDate());
						bill2Info.setCustomerCode(billInfo.getCustomerCode());
						bill2Info.setCustomerName(billInfo.getCustomerName());
						bill2Info.setCustomerAddr(billInfo.getCustomerAddr());
						bill2Info.setDiscountAmt(billInfo.getDiscountAmt());
						bill2Info.setBillAmt(billInfo.getBillAmt());
						bill2Info.setBillType(billInfo.getBillType());
						bill2Info.setTenderAmt(billInfo.getTenderAmt());
						bill2Info.setTotalItems(billInfo.getTotalItems());
						bill2Info.setProfit(billInfo.getProfit());
						bill2Info.setInvoiceNo(billInfo.getInvoiceNo());
						bill2Info.setLogInUser(billInfo.getLogInUser());
						bill2Info.setCustomerArea(billInfo.getCustomerArea());
						bill2Info.setVatAmt(billInfo.getVatAmt());
						bill2Info.setCashAmt(billInfo.getCashAmt());
						bill2Info.setCouponAmt(billInfo.getCouponAmt());
						bill2Info.setCardAmt(billInfo.getCardAmt());
						bill2Info.setCounterName(billInfo.getCounterName());
						bill2Info.setPriceLevelCode(billInfo.getPriceLevelCode());
						bill2Info.setCustomerStreet(billInfo.getCustomerStreet());
						bill2Info.setCustomerPlace(billInfo.getCustomerPlace());
						bill2Info.setCustomerCity(billInfo.getCustomerCity());
						bill2Info.setPhoneNo1(billInfo.getPhoneNo1());
						bill2Info.setPhoneNo2(billInfo.getPhoneNo2());
						bill2Info.setPhoneNo3(billInfo.getPhoneNo3());
						bill2Info.setsGstAmt(billInfo.getsGstAmt());
						bill2Info.setcGstAmt(billInfo.getcGstAmt());
						bill2Info.setiGstAmt(billInfo.getiGstAmt());
						bill2Info.setLoyaltyPoints(billInfo.getLoyaltyPoints());
						bill2Info.setRedeemedPoints(billInfo.getRedeemedPoints());
						bill2Info.setGstNo(billInfo.getGstNo());
						bill2Info.setSplitBillNo(billInfo.getSplitBillNo());
						bill2Info.setInvType(billInfo.getInvType());
					} else {
						bill2Info.setDiscountAmt(bill2Info.getDiscountAmt() + billInfo.getDiscountAmt());
						bill2Info.setBillAmt(bill2Info.getBillAmt() + billInfo.getBillAmt());
						bill2Info.setTenderAmt(bill2Info.getTenderAmt() + billInfo.getTenderAmt());
						bill2Info.setTotalItems(bill2Info.getTotalItems() + billInfo.getTotalItems());
						bill2Info.setProfit(bill2Info.getProfit() + billInfo.getProfit());
						bill2Info.setVatAmt(bill2Info.getVatAmt() + billInfo.getVatAmt());
						bill2Info.setCashAmt(bill2Info.getCashAmt() + billInfo.getCashAmt());
						bill2Info.setCouponAmt(bill2Info.getCouponAmt() + billInfo.getCouponAmt());
						bill2Info.setCardAmt(bill2Info.getCardAmt() + billInfo.getCardAmt());
						bill2Info.setsGstAmt(bill2Info.getsGstAmt() + billInfo.getsGstAmt());
						bill2Info.setcGstAmt(bill2Info.getcGstAmt() + billInfo.getcGstAmt());
						bill2Info.setiGstAmt(bill2Info.getiGstAmt() + billInfo.getiGstAmt());
					}
					billInfoMap.put(bill2Info.getBillNo(), bill2Info);
				});
				billInfoMap.entrySet().stream().forEach(billEntry -> bill2InfoList.add(billEntry.getValue()));
				return bill2InfoList;
			}
		}
		return billInfoList;
	}

	@SuppressWarnings("deprecation")
	private Date stringToDateFormatted(String dateString) {
		Date formattedDate = null;
		String dateFormat = "MM/dd/yyyy";
		String stringToDateFormat = "yyyy-MM-dd'T'HH:mm:ssZ";

		DateFormat dateFomatter = new SimpleDateFormat(dateFormat);
		DateFormat stringToDateFormatter = new SimpleDateFormat(stringToDateFormat, Locale.US);
		try {
			String[] lastSectionArray = dateString.split("[.]");
			String lastSection = lastSectionArray[lastSectionArray.length - 1];
			Date stringToDate = stringToDateFormatter.parse(dateString.replaceAll("." + lastSection, "+0000"));
			logger.debug("stringToDate: {}", stringToDate);
			formattedDate = new Date(dateFomatter.format(stringToDate));
		} catch (ParseException e) {
			e.printStackTrace();
			logger.error("Failed to convert String to date formatted {}", e.getMessage());
		}
		return formattedDate;
	}

	@Cacheable("nativenames")
	private String getNativeName(Integer productCode) {
		String nativeName = "";
		nativeName = productRepo.findByProductCode(productCode).getLocalLanguageName();
		return nativeName;
	}

	private Double getGSTAmount(Double existingValue, Double newValue) {
		Double newGstAmount = 0.0;
		logger.debug("existingValue {}, newValue {}", existingValue, newValue);
		newGstAmount = existingValue + newValue;
		logger.debug("newGstAmount {}", newGstAmount);
		return newGstAmount;
	}

	private Map<Integer, BillItemDetails> getBillItemDetailsMap(List<MedBillDetails> billDetailsList,
			Map<Integer, BillItemDetails> billItemMap) {
		billDetailsList.forEach(billDetail -> {
			Integer itemCode = billDetail.getMBD_ITEM_CODE();
			BillItemDetails existingBillDetail = billItemMap.get(itemCode);
			if (existingBillDetail == null) {
				BillItemDetails billItemDetail = new BillItemDetails();
				String itemName = medItemHdrRepo.findByMIH_ITEM_CODE(itemCode);
				String itemNativeName = "";
				if (productRepo.findByProductCode(itemCode) != null) {
					itemNativeName = productRepo.findByProductCode(itemCode).getBaminiFontName();
				}

				billItemDetail.setBillNo(billDetail.getmBDBillNo());
				billItemDetail.setBillSlNo(billDetail.getMbd_org_slno());
				billItemDetail.setItemName(itemName);
				billItemDetail.setItemNativeName(itemNativeName);
				billItemDetail.setItemUnit(billDetail.getMBD_ITEM_UNIT());
				billItemDetail.setItemQuantity(billDetail.getMBD_ITEM_QTY());
				billItemDetail.setItemRate(billDetail.getMBD_ITEM_RATE());
				billItemDetail.setItemTaxAmt(billDetail.getMBD_ITEM_TAX_AMT());
				billItemDetail.setItemTotalAmt(billDetail.getMBD_ITEM_AMOUNT());
				billItemDetail.setItemPurchaseRate(billDetail.getMBD_PUR_RATE());
				billItemDetail.setItemTaxPercent(billDetail.getMBD_ITEM_TAX_PERC());
				billItemDetail.setTotalTaxAmt(billDetail.getMBD_TOT_TAX_AMT());
				billItemDetail.setItemMaxRate(billDetail.getMbd_max_rate());
				billItemDetail.setItemPriceLevel(billDetail.getMbd_price_level());
				billItemDetail.setQuantityPriceLvl(billDetail.getMbd_qty_pricelvl());
				billItemDetail.setQuantityPriceLvlSelling(billDetail.getMbd_qty_pricelvl_selling());
				billItemDetail.setItemEanCode(billDetail.getMbd_eancode());
				billItemDetail.setPurchasePriceWithoutTax(billDetail.getMbd_pur_price_wot());
				billItemDetail.setItemProfit(billDetail.getMbd_profit_amt());
				billItemDetail.setNetAmt(billDetail.getMbd_net_amt());
				billItemDetail.setsGstPercent(billDetail.getMbd_sgst_perc());
				billItemDetail.setsGstAmt(billDetail.getMbd_sgst_amt());
				billItemDetail.setcGstPercent(billDetail.getMbd_cgst_perc());
				billItemDetail.setcGstAmt(billDetail.getMbd_cgst_amt());
				billItemDetail.setiGstPercent(billDetail.getMbd_igst_perc());
				billItemDetail.setiGstAmt(billDetail.getMbd_igst_amt());
				billItemDetail.setItemGstCode(billDetail.getMbd_gst_code());
				Double savedAmt = (billDetail.getMbd_max_rate() - billDetail.getMBD_ITEM_RATE())
						* billDetail.getMBD_ITEM_QTY();
				billItemDetail.setSavedAmount(savedAmt);
				billItemMap.put(itemCode, billItemDetail);
			} else {
				Double existingQty = existingBillDetail.getItemQuantity();
				Double newQty = existingQty + billDetail.getMBD_ITEM_QTY();
				Double existingSavedAmt = existingBillDetail.getSavedAmount();
				Double currentSavedAmt = (billDetail.getMbd_max_rate() - billDetail.getMBD_ITEM_RATE())
						* billDetail.getMBD_ITEM_QTY();
				Double newSavedAmt = existingSavedAmt + currentSavedAmt;
				existingBillDetail.setItemQuantity(newQty);
				existingBillDetail.setSavedAmount(newSavedAmt);
				Double existingTotalAmt = existingBillDetail.getItemTotalAmt();
				Double newTotalAmt = existingTotalAmt + billDetail.getMBD_ITEM_AMOUNT();
				existingBillDetail.setItemTotalAmt(newTotalAmt);
				existingBillDetail.setItemTaxAmt(
						getGSTAmount(existingBillDetail.getItemTaxAmt(), billDetail.getMBD_ITEM_TAX_AMT()));
				existingBillDetail.setTotalTaxAmt(
						getGSTAmount(existingBillDetail.getTotalTaxAmt(), billDetail.getMBD_TOT_TAX_AMT()));
				existingBillDetail
						.setsGstAmt(getGSTAmount(existingBillDetail.getsGstAmt(), billDetail.getMbd_sgst_amt()));
				existingBillDetail
						.setcGstAmt(getGSTAmount(existingBillDetail.getcGstAmt(), billDetail.getMbd_cgst_amt()));
				existingBillDetail
						.setiGstAmt(getGSTAmount(existingBillDetail.getiGstAmt(), billDetail.getMbd_igst_amt()));
				billItemMap.put(itemCode, existingBillDetail);
			}
		});
		return billItemMap;
	}

	@Override
	public List<BillItemDetails> getBillItemDetails(Integer billNo, boolean isQuotationBill) {
		Map<Integer, BillItemDetails> billItemMap = new HashMap<>();
		List<BillItemDetails> billItemList = new ArrayList<>();
		List<MedBillDetails> billDetailsList = new ArrayList<>();
		if (isQuotationBill) {
			List<MedBillQuotDetail> quotBillDetailsList = quotDetailsRepo.findByMBDBillNo(billNo);

			quotBillDetailsList.forEach(billDetail -> {
				Integer itemCode = billDetail.getMBD_ITEM_CODE();
				BillItemDetails existingBillDetail = billItemMap.get(itemCode);
				if (existingBillDetail == null) {
					BillItemDetails billItemDetail = new BillItemDetails();
					String itemName = medItemHdrRepo.findByMIH_ITEM_CODE(itemCode);
					String itemNativeName = productRepo.findByProductCode(itemCode).getBaminiFontName();
					billItemDetail.setBillNo(billDetail.getmBDBillNo());
					billItemDetail.setBillSlNo(billDetail.getMBD_BILL_SL_NO());
					billItemDetail.setItemName(itemName);
					billItemDetail.setItemNativeName(itemNativeName);
					billItemDetail.setItemUnit(billDetail.getMBD_ITEM_UNIT());
					billItemDetail.setItemQuantity(billDetail.getMBD_ITEM_QTY());
					billItemDetail.setItemRate(billDetail.getMBD_ITEM_RATE());
					billItemDetail.setItemTaxAmt(billDetail.getMBD_ITEM_TAX_AMT());
					billItemDetail.setItemTotalAmt(billDetail.getMBD_ITEM_AMOUNT());
					billItemDetail.setItemPurchaseRate(billDetail.getMBD_PUR_RATE());
					billItemDetail.setItemTaxPercent(billDetail.getMBD_ITEM_TAX_PERC());
					billItemDetail.setTotalTaxAmt(billDetail.getMBD_TOT_TAX_AMT());
					billItemDetail.setItemMaxRate(billDetail.getMbd_max_rate());
					billItemDetail.setItemPriceLevel(billDetail.getMbd_price_level());
					billItemDetail.setQuantityPriceLvl(billDetail.getMbd_qty_pricelvl());
					billItemDetail.setPurchasePriceWithoutTax(billDetail.getMbd_pur_price_wot());
					billItemDetail.setsGstPercent(billDetail.getMbd_sgst_perc());
					billItemDetail.setsGstAmt(billDetail.getMbd_sgst_amt());
					billItemDetail.setcGstPercent(billDetail.getMbd_cgst_perc());
					billItemDetail.setcGstAmt(billDetail.getMbd_cgst_amt());
					billItemDetail.setiGstPercent(billDetail.getMbd_igst_perc());
					billItemDetail.setiGstAmt(billDetail.getMbd_igst_amt());
					billItemDetail.setItemGstCode(billDetail.getMbd_gst_code().toString());
					Double savedAmt = (billDetail.getMbd_max_rate() - billDetail.getMBD_ITEM_RATE())
							* billDetail.getMBD_ITEM_QTY();
					billItemDetail.setSavedAmount(savedAmt);
					billItemMap.put(itemCode, billItemDetail);
				} else {
					Double existingQty = existingBillDetail.getItemQuantity();
					Double newQty = existingQty + billDetail.getMBD_ITEM_QTY();
					Double existingSavedAmt = existingBillDetail.getSavedAmount();
					Double currentSavedAmt = (billDetail.getMbd_max_rate() - billDetail.getMBD_ITEM_RATE())
							* billDetail.getMBD_ITEM_QTY();
					Double newSavedAmt = existingSavedAmt + currentSavedAmt;
					existingBillDetail.setItemQuantity(newQty);
					existingBillDetail.setSavedAmount(newSavedAmt);
					existingBillDetail.setItemTaxAmt(
							getGSTAmount(existingBillDetail.getItemTaxAmt(), billDetail.getMBD_ITEM_TAX_AMT()));
					existingBillDetail
							.setsGstAmt(getGSTAmount(existingBillDetail.getsGstAmt(), billDetail.getMbd_sgst_amt()));
					existingBillDetail
							.setcGstAmt(getGSTAmount(existingBillDetail.getcGstAmt(), billDetail.getMbd_cgst_amt()));
					existingBillDetail
							.setiGstAmt(getGSTAmount(existingBillDetail.getiGstAmt(), billDetail.getMbd_igst_amt()));
					billItemMap.put(itemCode, existingBillDetail);
				}
			});
		} else {
			if (chooseWhereField(billNo)) {
				billDetailsList = medDetailsRepo.findByMBDBillNo(billNo);
			} else {
				billDetailsList = medDetailsRepo.findByMbdSplitBnoPk(billNo);
			}
			getBillItemDetailsMap(billDetailsList, billItemMap);
		}

		billItemMap.entrySet().stream().forEach(billEntry -> billItemList.add(billEntry.getValue()));
		Collections.sort(billItemList, new Comparator<BillItemDetails>() {
			public int compare(BillItemDetails billItem1, BillItemDetails billItem2) {
				return ((Integer) billItem1.getBillSlNo()).compareTo(billItem2.getBillSlNo());
			}
		});
		try {
			logger.debug("entry: {}", mapper.writeValueAsString(billItemList));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return billItemList;
	}

	@Override
	public Double getBillAmount(Integer mBillNo, String phoneNumber) {
		Double billAmt = 0.0;
		String findQuery = "SELECT MBH_BILL_AMOUNT FROM MED_BILL_HDR BILL, MED_CUSTOMER_MAST CUST WHERE MBH_PBILL_NO = %s AND "
				+ "MBH_BILL_CUST_CODE = MCM_CUST_CODE AND (mcm_phone1 = '%s' OR mcm_phone2 = '%s' OR MCM_CUST_TEL = '%s')";
		findQuery = String.format(findQuery, mBillNo, phoneNumber, phoneNumber, phoneNumber);
		logger.debug("Query to get bill Amt: {}", findQuery);
		billAmt = jdbcTemplate.queryForObject(findQuery, Double.class);
		return billAmt;
	}

	@Override
	public Integer getPBillNumber(Integer mBillNo, String phoneNumber) {
		Integer pBillNumber = 0;
		String findQuery = "SELECT MBH_BILL_NO FROM MED_BILL_HDR BILL, MED_CUSTOMER_MAST CUST WHERE MBH_PBILL_NO = %s AND "
				+ "MBH_BILL_CUST_CODE = MCM_CUST_CODE AND (mcm_phone1 = '%s' OR mcm_phone2 = '%s' OR MCM_CUST_TEL = '%s')";
		findQuery = String.format(findQuery, mBillNo, phoneNumber, phoneNumber, phoneNumber);
		logger.debug("Query to get bill number: {}", findQuery);
		pBillNumber = jdbcTemplate.queryForObject(findQuery, Integer.class);
		return pBillNumber;
	}

}
