package com.sakthi.groups.migration.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.sakthi.groups.migration.gofrugal.dao.MedMrcDetailRepository;
import com.sakthi.groups.migration.gofrugal.dao.MedMrcHdrRepository;
import com.sakthi.groups.migration.gofrugal.model.MedMrcDetail;
import com.sakthi.groups.migration.gofrugal.model.MedMrcHdr;
import com.sakthi.groups.migration.portal.dao.MigProductRepository;
import com.sakthi.groups.migration.portal.dao.MigSupplierRepository;
import com.sakthi.groups.migration.portal.dao.PurchaseBillEntryRepository;
import com.sakthi.groups.migration.portal.dao.PurchaseBillItemRepository;
import com.sakthi.groups.migration.portal.model.ProductMaster;
import com.sakthi.groups.migration.portal.model.PurchaseBillEntry;
import com.sakthi.groups.migration.portal.model.PurchaseBillItemDetails;
import com.sakthi.groups.migration.portal.model.SupplierMaster;

@Service
public class PurchaseEntryMigrationImpl implements PurchaseEntryMigration {

	@Autowired
	private MedMrcHdrRepository mrcHdrRepo;

	@Autowired
	private MedMrcDetailRepository mrdDetailRepo;

	@Autowired
	private PurchaseBillEntryRepository billEntryRepo;

	@Autowired
	private PurchaseBillItemRepository billItemRepo;

	@Autowired
	private MigSupplierRepository supplierRepo;

	@Autowired
	private MigProductRepository productRepo;

	@Value("${sakthi.purchase.entry.slot}")
	private Integer dateBeforePurchaseEntry;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public boolean migratePurchaseEntry(boolean incremental) {
		boolean migrationStatus = false;
		if (incremental) {
			logger.info("Yet to be implemented");
		} else {
			logger.info("Purchase entry migration begin");
			Set<Integer> mrcNoSet = migrateBillEntry();
			migrateBillItemEntry(mrcNoSet);
			logger.info("Purchase entry migration ends");
		}
		return migrationStatus;
	}

	private PurchaseBillEntry getBillEntryBean(MedMrcHdr medHdr) {
		PurchaseBillEntry purchaseBillEntry = new PurchaseBillEntry(medHdr.getMMH_MRC_PREFIX(), medHdr.getMMH_MRC_NO(),
				medHdr.getMmhMrcDate(), medHdr.getMmhDistCode(), medHdr.getMMH_NO_OF_ITEMS(), medHdr.getMMH_MRC_AMT(),
				medHdr.getMMH_PAID_AMT(), medHdr.getMMH_PAID_FLAG(), medHdr.getMMH_PAY_DUE_DT(),
				medHdr.getMMH_LAST_PAID_AMT(), medHdr.getMMH_DIST_BILL_NO(), medHdr.getMmhDistBillDt(),
				medHdr.getMMH_DISC_AMT(), medHdr.getMMH_CREDIT_AMT(), medHdr.getMMH_DEBIT_AMT(),
				medHdr.getMMH_DIST_BILL_AMT(), medHdr.getMMH_MRC_TYPE(), medHdr.getMMH_DISC_PERC(),
				medHdr.getMMH_COMPID(), medHdr.getMMH_DIVIID(), medHdr.getMMH_LOCAID(), medHdr.getMMH_FREIGHT(),
				medHdr.getMmh_purc_type(), medHdr.getMmh_tax_amt(), medHdr.getMmh_manual_disc(),
				medHdr.getMmh_netrate_aftcashdisc(), medHdr.getMmh_packchrg_amt(), medHdr.getMmh_invtype(),
				medHdr.getMmh_mrc_refno(), medHdr.getMmh_tcs_perc(), medHdr.getMmh_tcs_amt(),
				medHdr.getMmh_pur_acknow_no(), medHdr.getMmh_sgst_amt(), medHdr.getMmh_cgst_amt(),
				medHdr.getMmh_igst_amt(), medHdr.getMmh_gst_cess_amt(), medHdr.getMmh_tax_calc_type(),
				medHdr.getMmh_mrc_entry_date());
		return purchaseBillEntry;
	}

	private Set<Integer> migrateBillEntry() {
		Set<Integer> mrcNoSet = new HashSet<Integer>();
		logger.debug("Migration bill entry begins");
		Long purchaseEntryList = billEntryRepo.count();
		if (purchaseEntryList > 0) {
			logger.debug("Incremental Migration bill entry");
			LocalDate currentDate = LocalDate.now();
			LocalDate startingDate = currentDate.plusDays(-Long.valueOf(dateBeforePurchaseEntry));
			Date purchaseSlot = Date.from(startingDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
			List<MedMrcHdr> medMdrHdrList = mrcHdrRepo.findByMmhMrcDateGreaterThan(purchaseSlot);
			medMdrHdrList.forEach(medMdrHdr -> {
				PurchaseBillEntry billEntry = billEntryRepo.findByMrcNo(medMdrHdr.getMMH_MRC_NO());
				PurchaseBillEntry purchaseBillEntry = getBillEntryBean(medMdrHdr);
				if (billEntry == null) {
					logger.debug("New purchase bill entry");
					billEntryRepo.save(purchaseBillEntry);
					mrcNoSet.add(purchaseBillEntry.getMrcNo());
				} else {
					logger.debug("GoFrugal Date: {}", new Date(medMdrHdr.getMmhMrcDate().getTime()));
					logger.debug("Portal Date: {}", billEntry.getPurchaseEnterDate());
					if (new Date(medMdrHdr.getMmhMrcDate().getTime())
							.compareTo(billEntry.getPurchaseEnterDate()) != 0) {
						logger.debug("Updating purchase bill entry");
						purchaseBillEntry.setId(billEntry.getId());
						billEntryRepo.save(purchaseBillEntry);
						mrcNoSet.add(purchaseBillEntry.getMrcNo());
					}
				}
			});
		} else {
			logger.debug("Full Migration bill entry");
			Iterable<MedMrcHdr> medHdrIterate = mrcHdrRepo.findAll();
			medHdrIterate.forEach(medHdr -> {
				PurchaseBillEntry purchaseBillEntry = getBillEntryBean(medHdr);
				billEntryRepo.save(purchaseBillEntry);
			});
			mrcNoSet.add(null);
		}
		return mrcNoSet;
	}

	private PurchaseBillItemDetails getBillItemBean(MedMrcDetail medMrcDetail) {
		MedMrcHdr billEntry = mrcHdrRepo.findByMmhMrcNo(medMrcDetail.getMmdMrcNo());
		PurchaseBillItemDetails purchaseBillItem = new PurchaseBillItemDetails(medMrcDetail.getMmdMrcNo(),
				medMrcDetail.getMMD_MRC_SL_NO(), billEntry.getMmhDistCode(), billEntry.getMMH_DIST_BILL_NO(),
				billEntry.getMmhDistBillDt(), medMrcDetail.getMMD_ITEM_CODE(), medMrcDetail.getMMD_RECD_QTY(),
				medMrcDetail.getMMD_FREE_QTY_TAG(), medMrcDetail.getMMD_FREE_QTY(), medMrcDetail.getMMD_PUR_RATE(),
				medMrcDetail.getMMD_PUR_TAX_PER(), medMrcDetail.getMMD_PUR_TAX_AMT(), medMrcDetail.getMMD_PUR_PRICE(),
				medMrcDetail.getMMD_PUR_AMOUNT(), medMrcDetail.getMMD_MRP(), medMrcDetail.getMMD_SALE_TAX_PER(),
				medMrcDetail.getMMD_SALE_TAX_AMT(), medMrcDetail.getMMD_SALE_RATE(), medMrcDetail.getMMD_BATCH_NO(),
				medMrcDetail.getMMD_BATCH_DT(), medMrcDetail.getMMD_MFG_DT(), medMrcDetail.getMMD_EXPIRY_DT(),
				medMrcDetail.getMMD_MRC_PREFIX(), medMrcDetail.getMMD_OFFER_PER_QTY(),
				medMrcDetail.getMMD_CASH_DISC_AMT(), medMrcDetail.getMmd_compid(), medMrcDetail.getMmd_diviid(),
				medMrcDetail.getMmd_locaid(), medMrcDetail.getMMD_FREIGHT(), medMrcDetail.getMmd_max_rate(),
				medMrcDetail.getMmd_tcs_perc(), medMrcDetail.getMmd_tcs_amt(), medMrcDetail.getMmd_margin_perc(),
				medMrcDetail.getMmd_profit_perc(), medMrcDetail.getMmd_prev_margin_perc(),
				medMrcDetail.getMmd_prev_profit_perc(), medMrcDetail.getMmd_prev_pur_price(),
				medMrcDetail.getMmd_eancode(), medMrcDetail.getMmd_sgst_perc(), medMrcDetail.getMmd_sgst_amt(),
				medMrcDetail.getMmd_cgst_perc(), medMrcDetail.getMmd_cgst_amt(), medMrcDetail.getMmd_igst_perc(),
				medMrcDetail.getMmd_igst_amt(), medMrcDetail.getMmd_gst_cess_perc(), medMrcDetail.getMmd_gst_cess_amt(),
				medMrcDetail.getMmd_gst_code());
		return purchaseBillItem;
	}

	private void migrateBillItemEntry(Set<Integer> mrcNoSet) {
		if (mrcNoSet.size() == 1 && mrcNoSet.contains(null)) {
			logger.debug("Full migration of bill items");
			Iterable<MedMrcDetail> medItemDetailIterate = mrdDetailRepo.findAll();
			medItemDetailIterate.forEach(medItem -> {
				PurchaseBillItemDetails purchaseBillItem = getBillItemBean(medItem);
				billItemRepo.save(purchaseBillItem);
			});
		} else {
			List<MedMrcDetail> medItemDetailList = mrdDetailRepo.findByMmdMrcNoIn(mrcNoSet);
			medItemDetailList.forEach(medItem -> {
				PurchaseBillItemDetails purchaseBillItem = getBillItemBean(medItem);
				PurchaseBillItemDetails existingBean = billItemRepo.findByMrcNoAndMrcSerialNo(medItem.getMmdMrcNo(),
						medItem.getMMD_MRC_SL_NO());
				if (existingBean == null) {
					logger.debug("New purchase bill entry");
					billItemRepo.save(purchaseBillItem);
				} else {
					logger.debug("Updating existing of bill items");
					purchaseBillItem.setId(existingBean.getId());
					billItemRepo.save(purchaseBillItem);
				}
			});
		}
	}

	@Override
	public List<PurchaseBillEntry> getPurchaseBillEntry(Integer supplierCode) {
		List<MedMrcHdr> medMdrHdrList = mrcHdrRepo.findByMmhDistCodeOrderByMmhDistBillDtDesc(supplierCode);
		List<PurchaseBillEntry> billEntryList = new ArrayList<>();
		SupplierMaster supplierMaster = supplierRepo.findBySupplierCode(supplierCode);
		medMdrHdrList.forEach(medMdr -> {
			PurchaseBillEntry purchaseBillEntry = getBillEntryBean(medMdr);
			if (supplierMaster != null) {
				purchaseBillEntry.setDistributorName(supplierMaster.getSupplierName());
			}
			billEntryList.add(purchaseBillEntry);
		});
		return billEntryList;
	}

	@Override
	public List<PurchaseBillItemDetails> getPurchaseBillItemEntry(Integer itemCode) {
		List<MedMrcDetail> medItemDetailList = mrdDetailRepo.findByMmdItemCode(itemCode);
		List<PurchaseBillItemDetails> purchaseBillItemList = new ArrayList<>();
		ProductMaster productMaster = productRepo.findByProductCode(itemCode);
		medItemDetailList.forEach(medItem -> {
			PurchaseBillItemDetails purchaseBillItem = getBillItemBean(medItem);
			if (productMaster != null) {
				purchaseBillItem.setProductName(productMaster.getProductName());
			}
			SupplierMaster supplierMaster = supplierRepo.findBySupplierCode(purchaseBillItem.getDistributorCode());
			if (supplierMaster != null) {
				purchaseBillItem.setDistributorName(supplierMaster.getSupplierName());
			}
			purchaseBillItemList.add(purchaseBillItem);
		});
		return purchaseBillItemList;
	}

	@Override
	public List<PurchaseBillItemDetails> getGrnBillItemEntry(Integer grnRefNo) {
		List<MedMrcDetail> medItemDetailList = mrdDetailRepo.findByMrcNo(grnRefNo);
		List<PurchaseBillItemDetails> purchaseBillItemList = new ArrayList<>();
		medItemDetailList.forEach(medItem -> {
			PurchaseBillItemDetails purchaseBillItem = getBillItemBean(medItem);
			ProductMaster productMaster = productRepo.findByProductCode(purchaseBillItem.getItemCode());
			if (productMaster != null) {
				purchaseBillItem.setProductName(productMaster.getProductName());
			}
			SupplierMaster supplierMaster = supplierRepo.findBySupplierCode(purchaseBillItem.getDistributorCode());
			if (supplierMaster != null) {
				purchaseBillItem.setDistributorName(supplierMaster.getSupplierName());
			}
			purchaseBillItemList.add(purchaseBillItem);
		});
		return purchaseBillItemList;
	}
}
