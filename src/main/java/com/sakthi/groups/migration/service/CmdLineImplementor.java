package com.sakthi.groups.migration.service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoNamespace;
import com.sakthi.groups.migration.gofrugal.dao.MedDistrubutorRepository;
import com.sakthi.groups.migration.gofrugal.model.MedDistrubutorMaster;
import com.sakthi.groups.migration.portal.model.BillInfo;
import com.sakthi.groups.migration.portal.model.BillItemDetails;
import com.sakthi.groups.migration.portal.model.BillSearchCriteria;
import com.sakthi.groups.migration.portal.model.EancodeInputModel;
import com.sakthi.groups.migration.portal.model.ProductDetails;
import com.sakthi.groups.migration.portal.model.ProductPrice;
import com.sakthi.groups.migration.portal.model.PurchaseBillItemDetails;

@SuppressWarnings("unused")
@Component
@Profile("dev")
public class CmdLineImplementor implements CommandLineRunner {

	@Autowired
	private DataMigrationService dataMigrationService;
	
	@Autowired
	private PurchaseEntryMigration purchaseEnterMig;

	@Autowired
	private MedDistrubutorRepository medDistrubutorRepo;
	
	@Autowired
	private RodeoDataCheckService rodeoCheckService;
	
	@Autowired
	private BillDataMigration billMigration;

	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Autowired
	private DataCheckService dataCheckService;
	
	@Autowired
	private DeliveryBillSyncService deliveryBill;

	@Value("${spring.data.mongodb.database}")
	private String dbName;

	private ObjectMapper mapper = new ObjectMapper();

	@Override
	public void run(String... args) throws Exception {
		System.out.println("Started Processing the data");
		// Iterable<MedDistrubutorMaster> distributorMaster =
		// medDistrubutorRepo.findAll();
		/*
		 * dataMigrationService.migrateSupplierDetails();
		 * dataMigrationService.migrateCustomerDetails();
		 */

		//dataMigrationService.migrateProductDetails(false);
		// invokeExportFile();
		//invokeGetBillInfo();
		// getBillAmount(16225, "9940733524");
		// migrateCustomerDetails();
		// Map<String, String> map = MigrationUtil.getMap();
		// getUnicodeText("Ntjh Nkup 10&", map);
		//purchaseEnterMig.migratePurchaseEntry(false);
		//checkPurchaseEntry();
		//invokeRodeoDataCompare();
		//invokeDeliveryBillSync();
		//invokeBillMigration();
		//checkStaleData();
		//checkSalesPurchaseTrend();
		checkSupplierWiseData();
	}
	
	private void checkSupplierWiseData() throws JsonProcessingException {
		BillSearchCriteria searchCriteria = new BillSearchCriteria();
		String billStartDate = "2023-12-01T00:00:16.754Z";
		String billEndDate = "2024-06-12T13:07:16.754Z";
		searchCriteria.setBillStartDate(billStartDate);
		searchCriteria.setBillEndDate(billEndDate);
		searchCriteria.setSupplierCode(8253);
		System.out.println(mapper.writeValueAsString(billMigration.getSupplierWiseData(searchCriteria)));
	}
	
	private void checkSalesPurchaseTrend() throws JsonProcessingException {
		BillSearchCriteria searchCriteria = new BillSearchCriteria();
		String billStartDate = "2023-12-01T00:00:16.754Z";
		String billEndDate = "2024-01-28T13:07:16.754Z";
		searchCriteria.setBillStartDate(billStartDate);
		searchCriteria.setBillEndDate(billEndDate);
		searchCriteria.setItemCode(15326);
		System.out.println(mapper.writeValueAsString(billMigration.getPurchaseTrend(searchCriteria)));
		System.out.println(mapper.writeValueAsString(billMigration.getSalesTrend(searchCriteria)));
	}
	
	private void checkStaleData() throws IOException {
		//dataCheckService.checkStaleProduct(365, "PURCHASE");
		//dataCheckService.checkStaleProduct(365, "SALES");
		
		ByteArrayOutputStream byteArrayStream = dataCheckService.checkStaleProduct(365, "BOTH");
		try (OutputStream outputStream = new FileOutputStream(
				"C:\\Users\\balam\\Desktop\\UNUSED\\StaleProducts.xlsx")) {
			byteArrayStream.writeTo(outputStream);
		}
	}

	private void invokeDeliveryBillSync() {
		deliveryBill.syncDeliveryBills();
	}

	private void migrateCustomerDetails() {
		dataMigrationService.migrateCustomerDetails(false);
	}

	private void getBillAmount(Integer billNo, String phoneNo) {
		System.out.println(dataMigrationService.getBillAmount(billNo, phoneNo));
	}
	
	private void invokeBillMigration() {
		BillSearchCriteria searchCriteria = new BillSearchCriteria();
		String billStartDate = "2022-11-01T13:07:16.754Z";
		String billEndDate = "2022-11-14T13:07:16.754Z";
		searchCriteria.setBillStartDate(billStartDate);
		searchCriteria.setBillEndDate(billEndDate);
		searchCriteria.setBillFilterMinCount(5);
		searchCriteria.setBillFilterMinAmt(1200.0);
		searchCriteria.setBillFilterMaxAmt(1500.0);
		try {
			//System.out.println(mapper.writeValueAsString(billMigration.getSalesTrendData(searchCriteria)));
			//System.out.println(mapper.writeValueAsString(billMigration.getTopSellingProduct(searchCriteria)));
			System.out.println(mapper.writeValueAsString(billMigration.getBillAnalyticsData(searchCriteria).get(0)));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
	}
	
	private void checkPurchaseEntry() {
		try {
			System.out.println(mapper.writeValueAsString(purchaseEnterMig.getPurchaseBillEntry(286)));
			List<PurchaseBillItemDetails> purchaseBillDetail = purchaseEnterMig.getPurchaseBillItemEntry(16736);
			System.out.println(purchaseBillDetail.size());
			System.out.println(mapper.writeValueAsString(purchaseBillDetail));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void invokeExportFile() throws IOException {
		FileInputStream excelFile = new FileInputStream(
				new File("C:\\Users\\ADMIN\\Desktop\\ProductEanCode_3Yrs.xlsx"));
		Set<ProductDetails> prodDetailsSet = dataMigrationService.getProductDetails(excelFile);
		ByteArrayOutputStream byteArrayStream = dataMigrationService.exportProductDetailsAsStream(prodDetailsSet);
		try (OutputStream outputStream = new FileOutputStream(
				"C:\\Users\\ADMIN\\Desktop\\ProductExportDetails_3Yrs.xlsx")) {
			byteArrayStream.writeTo(outputStream);
		}
	}
	
	private void invokeRodeoDataCompare() throws IOException {
		rodeoCheckService.checkRodeoDataWithGofrugal();
	}

	private void invokeGetBillInfo() throws ParseException {
		BillSearchCriteria searchCriteria = new BillSearchCriteria();
		String billStartDate = "2022-08-21T18:30:00.000Z";
		String billEndDate = "2022-11-31T13:07:16.754Z";
		searchCriteria.setBillStartDate(billStartDate);
		searchCriteria.setBillEndDate(billEndDate);
		searchCriteria.setPBillSearch(true);
		searchCriteria.setBillStartAmt(200.0);
		//searchCriteria.setBillNumber(17558);
		searchCriteria.setCustomerName("5365");
		//searchCriteria.setQuotationBill(false);
		//searchCriteria.setDeliveryBill(true);
		List<BillInfo> billInfoList = dataMigrationService.getBillInfo(searchCriteria);
		try {
			System.out.println(mapper.writeValueAsString(billInfoList.size()));
			//System.out.println("billInfoList: " + mapper.writeValueAsString(billInfoList));
			List<BillItemDetails> billDetailList = dataMigrationService
					.getBillItemDetails(883115, false);
			System.out.println(mapper.writeValueAsString(billDetailList.size()));
			System.out.println(mapper.writeValueAsString(billDetailList));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static String getUnicodeText(String text, Map<String, String> map) {
		System.out.println("unicodeText " + text);
		System.out.println("map " + map);
		String unicodeText = text;
		for (String key : map.keySet()) {
			if (unicodeText != null && unicodeText.contains(key)) {
				unicodeText = unicodeText.replace(key, map.get(key));
			}
		}
		System.out.println("unicodeText " + unicodeText);
		return unicodeText;
	}
}
