package com.sakthi.groups.migration;

import java.util.HashMap;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
@EnableJpaRepositories(basePackages = "com.sakthi.groups.migration.gofrugal.dao", entityManagerFactoryRef = "entEntityManagerFactory", transactionManagerRef = "entTransactionManager")
public class GoFrugalDBConfig {

	@Value("${spring.datasource.url}")
	private String jdbcUrl;

	@Value("${spring.datasource.username}")
	private String jdbcUser;

	@Value("${spring.datasource.password}")
	private String jdbcPass;

	@Value("${spring.datasource.driver-class-name}")
	private String jdbcClass;

	@Value("${spring.jpa.hibernate.dialect}")
	private String hibDialect;

	@Value("${spring.jpa.hibernate.ddl-auto}")
	private String hibDdlAuto;

	@Bean
	@Primary
	public LocalContainerEntityManagerFactoryBean entEntityManagerFactory() {
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(entDataSource());
		em.setPackagesToScan(new String[] { "com.sakthi.groups.migration.gofrugal.model" });

		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		em.setJpaVendorAdapter(vendorAdapter);
		HashMap<String, Object> properties = new HashMap<>();
		properties.put("hibernate.hbm2ddl.auto", hibDdlAuto);
		properties.put("hibernate.dialect", hibDialect);
		em.setJpaPropertyMap(properties);

		return em;
	}

	@Bean
	@Primary
	public DataSource entDataSource() {

		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(jdbcClass);
		dataSource.setUrl(jdbcUrl);
		dataSource.setUsername(jdbcUser);
		dataSource.setPassword(jdbcPass);

		return dataSource;
	}

	@Bean
	@Primary
	public PlatformTransactionManager entTransactionManager() {

		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(entEntityManagerFactory().getObject());
		return transactionManager;
	}
}
